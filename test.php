
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
	<title>Delta works</title>
	<link rel="shortcut icon" type="image/png" href="https://datatables.net/media/images/favicon.png">
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
	<link rel="stylesheet" type="text/css" href="https://datatables.net/media/css/site-examples.css?_=19472395a2969da78c8a4c707e72123a">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css">
	<style type="text/css" class="init">
	
	/* Ensure that the demo table scrolls */
	th, td {
		white-space: nowrap;
		padding-left: 40px !important;
		padding-right: 40px !important;
	}
	div.dataTables_wrapper {
		width: 1200px;
		margin: 0 auto;
	}

	</style>
	<script type="text/javascript" src="/media/js/site.js?_=5e8f232afab336abc1a1b65046a73460"></script>
	<script type="text/javascript" src="/media/js/dynamic.php?comments-page=extensions%2Ffixedcolumns%2Fexamples%2Fstyling%2Frowspan.html" async></script>
	<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
	<script type="text/javascript" language="javascript" src="./demo.js"></script>
	<script type="text/javascript" class="init">
	

$(document).ready(function() {
	var table = $('#example').DataTable( {
		scrollY:        "600px",
		scrollX:        true,
		scrollCollapse: true,
		paging:         false,
		fixedColumns:   true
	} );
} );


	</script>
</head>
<body class="wide comments example">



				<table id="example" class="stripe row-border order-column" style="width:100%">
 					<thead>
       <tr align="center" bgcolor="#e0e7ef" style="text-align:center;">
          <th style="color:#ffffff;background-color: #777777;" rowspan="2">NO</th>
          <th style="color:#ffffff;background-color: #777777;" colspan="6">차량정보</th>
          <th style="color:#ffffff;background-color: #777777;" colspan="4">자동차보험</th>
          <th style="color:#ffffff;background-color: #777777;" colspan="4">적재물보험</th>
          <th style="color:#ffffff;background-color: #777777;" colspan="5">구매정보</th>
          <th style="color:#ffffff;background-color: #777777;" colspan="2">매각정보</th>
          <th style="color:#ffffff;background-color: #777777;" colspan="4">자동차정밀(정기)검사</th>
          <th style="color:#ffffff;background-color: #777777;" colspan="4">최종차주정보</th>
          <th style="color:#ffffff;background-color: #777777;" rowspan="2">수정</th>
		</tr>
		<tr align="center">
          <th style="color:#ffffff;background-color: #777777;">차량번호</th>
          <th style="color:#ffffff;background-color: #777777;">차대번호</th>
          <th style="color:#ffffff;background-color: #777777;">년식</th>
          <th style="color:#ffffff;background-color: #777777;">용도</th>
          <th style="color:#ffffff;background-color: #777777;">차종</th>
          <th style="color:#ffffff;background-color: #777777;">톤수</th>

          <th style="color:#ffffff;background-color: #777777;">보험사</th>
          <th style="color:#ffffff;background-color: #777777;">보험가입일</th>
          <th style="color:#ffffff;background-color: #777777;">보험만기일</th>
          <th style="color:#ffffff;background-color: #777777;">분납횟수</th>

          <th style="color:#ffffff;background-color: #777777;">보험사</th>
          <th style="color:#ffffff;background-color: #777777;">보험가입일</th>
          <th style="color:#ffffff;background-color: #777777;">보험만기일</th>
          <th style="color:#ffffff;background-color: #777777;">분납여부</th>

          <th style="color:#ffffff;background-color: #777777;">구매(회사)자</th>
          <th style="color:#ffffff;background-color: #777777;">구매일</th>
          <th style="color:#ffffff;background-color: #777777;">매입원가</th>
          <th style="color:#ffffff;background-color: #777777;">취등록세</th>
          <th style="color:#ffffff;background-color: #777777;">매입가합계</th>

          <th style="color:#ffffff;background-color: #777777;">최종판매일</th>
          <th style="color:#ffffff;background-color: #777777;">판매금액</th>

          <th style="color:#ffffff;background-color: #777777;">최종검사일</th>
          <th style="color:#ffffff;background-color: #777777;">다음검사일</th>
          <th style="color:#ffffff;background-color: #777777;">개시일</th>
          <th style="color:#ffffff;background-color: #777777;">만료일</th>

          <th style="color:#ffffff;background-color: #777777;">배차지</th>
          <th style="color:#ffffff;background-color: #777777;">위ㆍ수탁관리회사</th>
          <th style="color:#ffffff;background-color: #777777;">이전차주명</th>
          <th style="color:#ffffff;background-color: #777777;">현차주명</th>
		</tr>
					</thead>
					<tbody>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1431 </td>
		  			<td align="center" onClick="viewHistory('info1','1693')" style="cursor:hand;">경기82바6084</td>
			<td align="center" onClick="viewHistory('info1','1693')" style="cursor:hand;">KMFZSZ7KBJU568647</td>
			<td align="center">2018-06-07</td>
			<td align="center">영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1693')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1693')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1693')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1693')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1693')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1693')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1693')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1693')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1693')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1693')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1693')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1693')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">파주우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">손웅기</td>
		            <td align="center" style="text-align:left;padding-left:5px;">손웅기</td>
          <td align="center" style="text-align:left;padding-left:5px;">손웅기</td>
          <td align="center"><a href="javascript:goEdit('1693');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1693');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1430 </td>
		  			<td align="center" onClick="viewHistory('info1','1692')" style="cursor:hand;">서울82바5950</td>
			<td align="center" onClick="viewHistory('info1','1692')" style="cursor:hand;">KNCSJZ76AKK279940</td>
			<td align="center">2018-05-25</td>
			<td align="center">일반영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1692')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1692')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1692')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1692')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1692')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1692')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1692')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1692')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1692')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1692')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1692')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1692')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">덕양우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)세종기업물류</td>
		            <td align="center" style="text-align:left;padding-left:5px;">세종기업(남기창)</td>
          <td align="center" style="text-align:left;padding-left:5px;">남기창</td>
          <td align="center"><a href="javascript:goEdit('1692');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1692');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1429 </td>
		  			<td align="center" onClick="viewHistory('info1','1691')" style="cursor:hand;">광주89아2003</td>
			<td align="center" onClick="viewHistory('info1','1691')" style="cursor:hand;">KMFZSZ7KAGU227055</td>
			<td align="center">2015-06-23</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1xhs</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1691')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1691')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1691')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1691')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1691')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1691')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1691')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1691')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1691')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1691')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1691')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1691')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">파주우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)삼양통운</td>
		            <td align="center" style="text-align:left;padding-left:5px;">삼양통운(김지용)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김지용</td>
          <td align="center"><a href="javascript:goEdit('1691');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1691');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1428 </td>
		  			<td align="center" onClick="viewHistory('info1','1690')" style="cursor:hand;">광주89아1970</td>
			<td align="center" onClick="viewHistory('info1','1690')" style="cursor:hand;">KMFZCX7JBBU648014</td>
			<td align="center">2010-06-28</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1690')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1690')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1690')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1690')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1690')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1690')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1690')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1690')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1690')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1690')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1690')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1690')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">덕양우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)삼양통운</td>
		            <td align="center" style="text-align:left;padding-left:5px;">삼양통운(정종성)</td>
          <td align="center" style="text-align:left;padding-left:5px;">정종성</td>
          <td align="center"><a href="javascript:goEdit('1690');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1690');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1427 </td>
		  			<td align="center" onClick="viewHistory('info1','1689')" style="cursor:hand;">86조5990</td>
			<td align="center" onClick="viewHistory('info1','1689')" style="cursor:hand;">KMFZSZ7KBJU541796</td>
			<td align="center">2018-03-23</td>
			<td align="center">자가용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1689')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1689')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1689')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1689')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1689')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1689')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1689')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1689')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1689')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1689')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1689')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1689')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">파주우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">이대근</td>
		            <td align="center" style="text-align:left;padding-left:5px;">이대근</td>
          <td align="center" style="text-align:left;padding-left:5px;">이대근</td>
          <td align="center"><a href="javascript:goEdit('1689');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1689');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1426 </td>
		  			<td align="center" onClick="viewHistory('info1','1688')" style="cursor:hand;">서울86자7163</td>
			<td align="center" onClick="viewHistory('info1','1688')" style="cursor:hand;">KNCSJZ76BKK271460</td>
			<td align="center">2018-04-24</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1688')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1688')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1688')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1688')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1688')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1688')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1688')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1688')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1688')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1688')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1688')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1688')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">노원우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)세종기업물류</td>
		            <td align="center" style="text-align:left;padding-left:5px;">세종기업(한백)</td>
          <td align="center" style="text-align:left;padding-left:5px;">한백</td>
          <td align="center"><a href="javascript:goEdit('1688');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1688');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1425 </td>
		  			<td align="center" onClick="viewHistory('info1','1687')" style="cursor:hand;">서울89자4282</td>
			<td align="center" onClick="viewHistory('info1','1687')" style="cursor:hand;">KNCSJZ76BKK271443</td>
			<td align="center">2018-04-24</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1687')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1687')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1687')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1687')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1687')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1687')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1687')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1687')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1687')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1687')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1687')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1687')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">동수원우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)서현운수</td>
		            <td align="center" style="text-align:left;padding-left:5px;">박원규</td>
          <td align="center" style="text-align:left;padding-left:5px;">박원규</td>
          <td align="center"><a href="javascript:goEdit('1687');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1687');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1424 </td>
		  			<td align="center" onClick="viewHistory('info1','1686')" style="cursor:hand;">울산90자3685</td>
			<td align="center" onClick="viewHistory('info1','1686')" style="cursor:hand;">KNFSTX76BJK236572</td>
			<td align="center">2017-12-18</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1686')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1686')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1686')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1686')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1686')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1686')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1686')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1686')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1686')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1686')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1686')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1686')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">울산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">스마트(정근근)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">스마트(정근근)</td>
          <td align="center" style="text-align:left;padding-left:5px;">한정근</td>
          <td align="center"><a href="javascript:goEdit('1686');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1686');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1423 </td>
		  			<td align="center" onClick="viewHistory('info1','1685')" style="cursor:hand;">서울90바1300</td>
			<td align="center" onClick="viewHistory('info1','1685')" style="cursor:hand;">KMFZ7KAFU147584</td>
			<td align="center">2014-10-27</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1685')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1685')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1685')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1685')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1685')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1685')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1685')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1685')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1685')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1685')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1685')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1685')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">동수원우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)명화로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">고만성</td>
          <td align="center" style="text-align:left;padding-left:5px;">고만성</td>
          <td align="center"><a href="javascript:goEdit('1685');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1685');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1422 </td>
		  			<td align="center" onClick="viewHistory('info1','1684')" style="cursor:hand;">광주89아2029</td>
			<td align="center" onClick="viewHistory('info1','1684')" style="cursor:hand;">KNFSTZ76AJK190721</td>
			<td align="center">2017-08-21</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1684')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1684')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1684')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1684')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1684')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1684')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1684')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1684')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1684')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1684')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1684')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1684')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">일산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)삼양통운</td>
		            <td align="center" style="text-align:left;padding-left:5px;">삼양통운(김재범)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김재범</td>
          <td align="center"><a href="javascript:goEdit('1684');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1684');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1421 </td>
		  			<td align="center" onClick="viewHistory('info1','1683')" style="cursor:hand;">경기86아4971</td>
			<td align="center" onClick="viewHistory('info1','1683')" style="cursor:hand;">KMFZCS7HP8U376098</td>
			<td align="center">2008-01-21</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1683')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1683')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1683')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1683')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1683')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1683')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1683')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1683')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1683')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1683')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1683')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1683')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">평택우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">장영범</td>
		            <td align="center" style="text-align:left;padding-left:5px;">장영범</td>
          <td align="center" style="text-align:left;padding-left:5px;">장영범</td>
          <td align="center"><a href="javascript:goEdit('1683');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1683');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1420 </td>
		  			<td align="center" onClick="viewHistory('info1','1682')" style="cursor:hand;">경기87바6792</td>
			<td align="center" onClick="viewHistory('info1','1682')" style="cursor:hand;">KMFZCZ7JABU638565</td>
			<td align="center">2010-05-27</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1682')" style="cursor:hand;">동부화재</td>
          <td align="center" onClick="viewHistory('insure1','1682')" style="cursor:hand;">2018-04-11</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1682')" style="cursor:hand">2019-04-11</td>
			  			  <td align="center" onClick="viewHistory('insure1','1682')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1682')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1682')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1682')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1682')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1682')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1682')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1682')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1682')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)휴먼엘지</td>
		            <td align="center" style="text-align:left;padding-left:5px;">김선향(수도용)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김선향(문옥섭)</td>
          <td align="center"><a href="javascript:goEdit('1682');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1682');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1419 </td>
		  			<td align="center" onClick="viewHistory('info1','1681')" style="cursor:hand;">경기91배1163</td>
			<td align="center" onClick="viewHistory('info1','1681')" style="cursor:hand;">KMFZSZ7KADU965129</td>
			<td align="center">2013-03-15</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1681')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1681')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1681')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1681')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1681')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1681')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1681')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1681')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1681')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1681')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1681')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1681')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">오산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">롯데택배(정봉훈)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">롯데택배(정봉훈)</td>
          <td align="center" style="text-align:left;padding-left:5px;">정봉훈</td>
          <td align="center"><a href="javascript:goEdit('1681');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1681');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1418 </td>
		  			<td align="center" onClick="viewHistory('info1','1680')" style="cursor:hand;">83두8331</td>
			<td align="center" onClick="viewHistory('info1','1680')" style="cursor:hand;">KMFZSZ7KBHU362353</td>
			<td align="center">2016-08-19</td>
			<td align="center">자가용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1680')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1680')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1680')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1680')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1680')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1680')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1680')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1680')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1680')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1680')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1680')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1680')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">덕양우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">모근섭</td>
		            <td align="center" style="text-align:left;padding-left:5px;">모근섭</td>
          <td align="center" style="text-align:left;padding-left:5px;">모근섭</td>
          <td align="center"><a href="javascript:goEdit('1680');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1680');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1417 </td>
		  			<td align="center" onClick="viewHistory('info1','1679')" style="cursor:hand;">서울86자7204</td>
			<td align="center" onClick="viewHistory('info1','1679')" style="cursor:hand;">KNCSJZ76BKK274495</td>
			<td align="center">2018-05-15</td>
			<td align="center">용달영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1679')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1679')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1679')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1679')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1679')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1679')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1679')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1679')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1679')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1679')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1679')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1679')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">덕양우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)세종기업물류</td>
		            <td align="center" style="text-align:left;padding-left:5px;">세종기업(인기만)</td>
          <td align="center" style="text-align:left;padding-left:5px;">인기만</td>
          <td align="center"><a href="javascript:goEdit('1679');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1679');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1416 </td>
		  			<td align="center" onClick="viewHistory('info1','1678')" style="cursor:hand;">광주89아2034</td>
			<td align="center" onClick="viewHistory('info1','1678')" style="cursor:hand;">KNCSJZ76AEK866992</td>
			<td align="center">2014-03-31</td>
			<td align="center">용달영업용</td>
			<td align="center">파워게이트(삼양통)</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1678')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure1','1678')" style="cursor:hand;">2018-06-19</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1678')" style="cursor:hand">2019-06-19</td>
			  			  <td align="center" onClick="viewHistory('insure1','1678')" style="cursor:hand;">1</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1678')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1678')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1678')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1678')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1678')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1678')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1678')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1678')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">케이티지엘에스(주)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">삼양통운(이민혁)</td>
          <td align="center" style="text-align:left;padding-left:5px;">이민혁</td>
          <td align="center"><a href="javascript:goEdit('1678');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1678');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1415 </td>
		  			<td align="center" onClick="viewHistory('info1','1677')" style="cursor:hand;">충남80자7004</td>
			<td align="center" onClick="viewHistory('info1','1677')" style="cursor:hand;">KNCUA28131S093632</td>
			<td align="center">2001-05-24</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">0.6톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1677')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1677')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1677')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1677')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1677')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1677')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1677')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1677')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1677')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1677')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1677')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1677')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">대전충남혈액원</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">용달(유근우)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">용달(유근우)</td>
          <td align="center" style="text-align:left;padding-left:5px;">유근우</td>
          <td align="center"><a href="javascript:goEdit('1677');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1677');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1414 </td>
		  			<td align="center" onClick="viewHistory('info1','1676')" style="cursor:hand;">충남80자7543</td>
			<td align="center" onClick="viewHistory('info1','1676')" style="cursor:hand;">KNCUA88191S078006</td>
			<td align="center">2001-03-28</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">0.6톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1676')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1676')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1676')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1676')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1676')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1676')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1676')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1676')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1676')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1676')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1676')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1676')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">대전충남혈액원</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">가가콜밴(이순옥)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">가가콜밴(이순옥)</td>
          <td align="center" style="text-align:left;padding-left:5px;">이순옥</td>
          <td align="center"><a href="javascript:goEdit('1676');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1676');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1413 </td>
		  			<td align="center" onClick="viewHistory('info1','1675')" style="cursor:hand;">서울85자1380</td>
			<td align="center" onClick="viewHistory('info1','1675')" style="cursor:hand;">KMFZCX7KBFU102445</td>
			<td align="center">2014-05-29</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1675')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1675')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1675')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1675')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1675')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1675')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1675')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1675')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1675')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1675')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1675')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1675')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">김병룡</td>
		            <td align="center" style="text-align:left;padding-left:5px;">김병룡</td>
          <td align="center" style="text-align:left;padding-left:5px;">김병룡</td>
          <td align="center"><a href="javascript:goEdit('1675');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1675');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1412 </td>
		  			<td align="center" onClick="viewHistory('info1','1674')" style="cursor:hand;">제주97사9055</td>
			<td align="center" onClick="viewHistory('info1','1674')" style="cursor:hand;">KNCWJZ76ADK770358</td>
			<td align="center">2013-04-09</td>
			<td align="center">일반영업용</td>
			<td align="center">파워게이트</td>
			<td align="center">1.2톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1674')" style="cursor:hand;">동부화재</td>
          <td align="center" onClick="viewHistory('insure1','1674')" style="cursor:hand;">2018-2-19</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1674')" style="cursor:hand">2019-02-19</td>
			  			  <td align="center" onClick="viewHistory('insure1','1674')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1674')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1674')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1674')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1674')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1674')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1674')" style="cursor:hand;">2018-09-01</td>
          <td align="center" onClick="viewHistory('dchk','1674')" style="cursor:hand;background-color:#ff0000;">2018-08-02</td>
          <td align="center" onClick="viewHistory('dchk','1674')" style="cursor:hand;">2018-10-31</td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)델타온</td>
		            <td align="center" style="text-align:left;padding-left:5px;">엄유섭</td>
          <td align="center" style="text-align:left;padding-left:5px;">엄유섭</td>
          <td align="center"><a href="javascript:goEdit('1674');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1674');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1411 </td>
		  			<td align="center" onClick="viewHistory('info1','1673')" style="cursor:hand;">서울91자1645</td>
			<td align="center" onClick="viewHistory('info1','1673')" style="cursor:hand;">KMFZCX7KAEU041078</td>
			<td align="center">2013-11-28</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1673')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1673')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1673')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1673')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1673')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1673')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1673')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1673')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1673')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1673')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1673')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1673')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">덕양우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">용달(최윤남)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">용달(최윤남)</td>
          <td align="center" style="text-align:left;padding-left:5px;">최윤남</td>
          <td align="center"><a href="javascript:goEdit('1673');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1673');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1410 </td>
		  			<td align="center" onClick="viewHistory('info1','1672')" style="cursor:hand;">서울80배2379</td>
			<td align="center" onClick="viewHistory('info1','1672')" style="cursor:hand;">KRGNAAC312B012187</td>
			<td align="center">2002-09-27</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1672')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1672')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1672')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1672')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1672')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1672')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1672')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1672')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1672')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1672')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1672')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1672')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">은평우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">장윤</td>
		            <td align="center" style="text-align:left;padding-left:5px;">장윤</td>
          <td align="center" style="text-align:left;padding-left:5px;">장윤</td>
          <td align="center"><a href="javascript:goEdit('1672');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1672');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1409 </td>
		  			<td align="center" onClick="viewHistory('info1','1671')" style="cursor:hand;">88보6875</td>
			<td align="center" onClick="viewHistory('info1','1671')" style="cursor:hand;">KMFZCY7KBJU466670</td>
			<td align="center">2017-07-03</td>
			<td align="center">자가용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1671')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1671')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1671')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1671')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1671')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1671')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1671')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1671')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1671')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1671')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1671')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1671')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">은평우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">황영숙</td>
		            <td align="center" style="text-align:left;padding-left:5px;">황영숙</td>
          <td align="center" style="text-align:left;padding-left:5px;">황영숙</td>
          <td align="center"><a href="javascript:goEdit('1671');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1671');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1408 </td>
		  			<td align="center" onClick="viewHistory('info1','1670')" style="cursor:hand;">울산90자2623</td>
			<td align="center" onClick="viewHistory('info1','1670')" style="cursor:hand;">KNFSTZ76BJK242895</td>
			<td align="center">2018-01-05</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1670')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1670')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1670')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1670')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1670')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1670')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1670')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1670')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1670')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1670')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1670')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1670')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">동울산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">건영물류(김의근)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">건영물류(김의근)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김의근</td>
          <td align="center"><a href="javascript:goEdit('1670');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1670');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1407 </td>
		  			<td align="center" onClick="viewHistory('info1','1669')" style="cursor:hand;">서울89자4268</td>
			<td align="center" onClick="viewHistory('info1','1669')" style="cursor:hand;">KNFSTZ6AGK101053</td>
			<td align="center">2016-07-08</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1669')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1669')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1669')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1669')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1669')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1669')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1669')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1669')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1669')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1669')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1669')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1669')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">경기혈액원</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)서현운수</td>
		            <td align="center" style="text-align:left;padding-left:5px;">서현운수(이성학)</td>
          <td align="center" style="text-align:left;padding-left:5px;">이성학</td>
          <td align="center"><a href="javascript:goEdit('1669');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1669');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1406 </td>
		  			<td align="center" onClick="viewHistory('info1','1668')" style="cursor:hand;">제주96자6038</td>
			<td align="center" onClick="viewHistory('info1','1668')" style="cursor:hand;">KNFWXZ76ADK780846</td>
			<td align="center">2013-05-22</td>
			<td align="center">용달영업용</td>
			<td align="center">파워게이트</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1668')" style="cursor:hand;">동부화재</td>
          <td align="center" onClick="viewHistory('insure1','1668')" style="cursor:hand;">2018-05-13</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1668')" style="cursor:hand">2019-05-13</td>
			  			  <td align="center" onClick="viewHistory('insure1','1668')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1668')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1668')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1668')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1668')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1668')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1668')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1668')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1668')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)아이디일일구닷컴</td>
		            <td align="center" style="text-align:left;padding-left:5px;">김수희(이영진)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김수희(이영진)</td>
          <td align="center"><a href="javascript:goEdit('1668');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1668');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1405 </td>
		  			<td align="center" onClick="viewHistory('info1','1667')" style="cursor:hand;">경기91배5051</td>
			<td align="center" onClick="viewHistory('info1','1667')" style="cursor:hand;">KMFZCZ7JABU665352</td>
			<td align="center">2010-08-31</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1667')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1667')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1667')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1667')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1667')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1667')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1667')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1667')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1667')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1667')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1667')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1667')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">파주우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">백성준</td>
		            <td align="center" style="text-align:left;padding-left:5px;">백성준</td>
          <td align="center" style="text-align:left;padding-left:5px;">백성준</td>
          <td align="center"><a href="javascript:goEdit('1667');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1667');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1404 </td>
		  			<td align="center" onClick="viewHistory('info1','1666')" style="cursor:hand;">경기96자6855</td>
			<td align="center" onClick="viewHistory('info1','1666')" style="cursor:hand;">KNCSJZ76ADK814384</td>
			<td align="center">2013-10-02</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1666')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1666')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1666')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1666')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1666')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1666')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1666')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1666')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1666')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1666')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1666')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1666')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">도봉우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">조기용</td>
		            <td align="center" style="text-align:left;padding-left:5px;">조기용</td>
          <td align="center" style="text-align:left;padding-left:5px;">조기용</td>
          <td align="center"><a href="javascript:goEdit('1666');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1666');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1403 </td>
		  			<td align="center" onClick="viewHistory('info1','1664')" style="cursor:hand;">93구7783</td>
			<td align="center" onClick="viewHistory('info1','1664')" style="cursor:hand;">KNFSTZ76AGK113764</td>
			<td align="center">2016-09-02</td>
			<td align="center">자가용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1664')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1664')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1664')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1664')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1664')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1664')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1664')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1664')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1664')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1664')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1664')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1664')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">제주우편집중국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">박현희</td>
		            <td align="center" style="text-align:left;padding-left:5px;">박현희</td>
          <td align="center" style="text-align:left;padding-left:5px;">박현희</td>
          <td align="center"><a href="javascript:goEdit('1664');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1664');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1402 </td>
		  			<td align="center" onClick="viewHistory('info1','1663')" style="cursor:hand;">경기89아4223</td>
			<td align="center" onClick="viewHistory('info1','1663')" style="cursor:hand;">KMFZCZ7KADU935557</td>
			<td align="center">2012-12-13</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1663')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1663')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1663')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1663')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1663')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1663')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1663')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1663')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1663')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1663')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1663')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1663')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">동수원우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">한형택</td>
		            <td align="center" style="text-align:left;padding-left:5px;">한형택</td>
          <td align="center" style="text-align:left;padding-left:5px;">한형택</td>
          <td align="center"><a href="javascript:goEdit('1663');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1663');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1401 </td>
		  			<td align="center" onClick="viewHistory('info1','1662')" style="cursor:hand;">울산90자3658</td>
			<td align="center" onClick="viewHistory('info1','1662')" style="cursor:hand;">KMFZSZ7KACU863576</td>
			<td align="center">2012-04-18</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1662')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1662')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1662')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1662')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1662')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1662')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1662')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1662')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1662')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1662')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1662')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1662')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">울산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">박종명</td>
		            <td align="center" style="text-align:left;padding-left:5px;">박종명</td>
          <td align="center" style="text-align:left;padding-left:5px;">박종명</td>
          <td align="center"><a href="javascript:goEdit('1662');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1662');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1400 </td>
		  			<td align="center" onClick="viewHistory('info1','1661')" style="cursor:hand;">경기91자6458</td>
			<td align="center" onClick="viewHistory('info1','1661')" style="cursor:hand;">KMFZCZ7KBCU882468</td>
			<td align="center">2012-06-22</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1661')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1661')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1661')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1661')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1661')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1661')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1661')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1661')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1661')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1661')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1661')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1661')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">남울산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)일석로지스틱</td>
		            <td align="center" style="text-align:left;padding-left:5px;">이인호</td>
          <td align="center" style="text-align:left;padding-left:5px;">이인호</td>
          <td align="center"><a href="javascript:goEdit('1661');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1661');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1399 </td>
		  			<td align="center" onClick="viewHistory('info1','1660')" style="cursor:hand;">경기80바9989</td>
			<td align="center" onClick="viewHistory('info1','1660')" style="cursor:hand;">KMFWBX7KAHU903796</td>
			<td align="center">2017-03-06</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1660')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1660')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1660')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1660')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1660')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1660')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1660')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1660')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1660')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1660')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1660')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1660')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">경기혈액원</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)대신통운</td>
		            <td align="center" style="text-align:left;padding-left:5px;">대신통운(이갑규)</td>
          <td align="center" style="text-align:left;padding-left:5px;">이갑규</td>
          <td align="center"><a href="javascript:goEdit('1660');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1660');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1398 </td>
		  			<td align="center" onClick="viewHistory('info1','1659')" style="cursor:hand;">경기80아1928</td>
			<td align="center" onClick="viewHistory('info1','1659')" style="cursor:hand;">KNFSTZ76BHK169168</td>
			<td align="center">2017-03-30</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center"></td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1659')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1659')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1659')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1659')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1659')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1659')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1659')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1659')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1659')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1659')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1659')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1659')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">경기혈액원</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">곽기철</td>
		            <td align="center" style="text-align:left;padding-left:5px;">곽기철</td>
          <td align="center" style="text-align:left;padding-left:5px;">곽기철</td>
          <td align="center"><a href="javascript:goEdit('1659');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1659');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1397 </td>
		  			<td align="center" onClick="viewHistory('info1','1658')" style="cursor:hand;">경기80바9933</td>
			<td align="center" onClick="viewHistory('info1','1658')" style="cursor:hand;">KMFWBX7JBBU331178</td>
			<td align="center">2011-01-14</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center"></td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1658')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1658')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1658')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1658')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1658')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1658')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1658')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1658')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1658')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1658')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1658')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1658')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">경기혈액원</td>
		  				<td align="center" style="text-align:left;padding-left:5px;"></td>
		            <td align="center" style="text-align:left;padding-left:5px;">대신통운(김수철)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김수철</td>
          <td align="center"><a href="javascript:goEdit('1658');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1658');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1396 </td>
		  			<td align="center" onClick="viewHistory('info1','1657')" style="cursor:hand;">경기91사1090</td>
			<td align="center" onClick="viewHistory('info1','1657')" style="cursor:hand;">KNCSJZ76AGK109622</td>
			<td align="center">2016-08-29</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1657')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1657')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1657')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1657')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1657')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1657')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1657')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1657')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1657')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1657')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1657')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1657')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">서수원우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">정치영</td>
		            <td align="center" style="text-align:left;padding-left:5px;">정치영</td>
          <td align="center" style="text-align:left;padding-left:5px;">정치영</td>
          <td align="center"><a href="javascript:goEdit('1657');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1657');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1395 </td>
		  			<td align="center" onClick="viewHistory('info1','1656')" style="cursor:hand;">서울85자5562</td>
			<td align="center" onClick="viewHistory('info1','1656')" style="cursor:hand;">KNFWXZ76ADK812969</td>
			<td align="center">2013-10-04</td>
			<td align="center">용달영업용</td>
			<td align="center">파워게이트</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1656')" style="cursor:hand;">삼성화재</td>
          <td align="center" onClick="viewHistory('insure1','1656')" style="cursor:hand;">2018-07-24</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1656')" style="cursor:hand">2019-07-24</td>
			  			  <td align="center" onClick="viewHistory('insure1','1656')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1656')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1656')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1656')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1656')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1656')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1656')" style="cursor:hand;">2018-10-03</td>
          <td align="center" onClick="viewHistory('dchk','1656')" style="cursor:hand;">2018-09-04</td>
          <td align="center" onClick="viewHistory('dchk','1656')" style="cursor:hand;">2018-11-02</td>
		  <td align="center" style="text-align:left;padding-left:5px;">한승공제주지사</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)아이디일일구닷컴</td>
		            <td align="center" style="text-align:left;padding-left:5px;">이든기업(강이든)</td>
          <td align="center" style="text-align:left;padding-left:5px;">강이든</td>
          <td align="center"><a href="javascript:goEdit('1656');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1656');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1394 </td>
		  			<td align="center" onClick="viewHistory('info1','1655')" style="cursor:hand;">서울89자7107</td>
			<td align="center" onClick="viewHistory('info1','1655')" style="cursor:hand;">KMFZSZ7KAJU471221</td>
			<td align="center">2017-07-26</td>
			<td align="center">용달영업용</td>
			<td align="center">파워게이트</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1655')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure1','1655')" style="cursor:hand;">2017-08-11</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1655')" style="cursor:hand;background-color:#ff0000">2018-08-11</td>
						  <td align="center" onClick="viewHistory('insure1','1655')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1655')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1655')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1655')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1655')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1655')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1655')" style="cursor:hand;background-color:#ff0000;">2018-07-25</td>
          <td align="center" onClick="viewHistory('dchk','1655')" style="cursor:hand;color:red;"><strike>2018-06-26</strike></td>
          <td align="center" onClick="viewHistory('dchk','1655')" style="cursor:hand;">2018-08-24</td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)아이디일일구닷컴</td>
		            <td align="center" style="text-align:left;padding-left:5px;">세부운수(이영갑)</td>
          <td align="center" style="text-align:left;padding-left:5px;">이영갑(이동진)</td>
          <td align="center"><a href="javascript:goEdit('1655');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1655');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1393 </td>
		  			<td align="center" onClick="viewHistory('info1','1654')" style="cursor:hand;">서울89자7051  </td>
			<td align="center" onClick="viewHistory('info1','1654')" style="cursor:hand;">KMFZSZ7KAAJU470943</td>
			<td align="center">2017-07-26 </td>
			<td align="center">용달영업용</td>
			<td align="center">파워게이트</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1654')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure1','1654')" style="cursor:hand;">2017-08-11</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1654')" style="cursor:hand;background-color:#ff0000">2018-08-11</td>
						  <td align="center" onClick="viewHistory('insure1','1654')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1654')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1654')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1654')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1654')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1654')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1654')" style="cursor:hand;background-color:#ff0000;">2018-07-26</td>
          <td align="center" onClick="viewHistory('dchk','1654')" style="cursor:hand;color:red;"><strike>2018-06-27</strike></td>
          <td align="center" onClick="viewHistory('dchk','1654')" style="cursor:hand;">2018-08-25</td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)아이디일일구닷컴</td>
		            <td align="center" style="text-align:left;padding-left:5px;">세부운수(이영갑)</td>
          <td align="center" style="text-align:left;padding-left:5px;">권혁노(이영갑)</td>
          <td align="center"><a href="javascript:goEdit('1654');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1654');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1392 </td>
		  			<td align="center" onClick="viewHistory('info1','1651')" style="cursor:hand;">92로6931</td>
			<td align="center" onClick="viewHistory('info1','1651')" style="cursor:hand;">KMFZSZ7KACU862795</td>
			<td align="center">2012-04-23</td>
			<td align="center">자가용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1651')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1651')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1651')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1651')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1651')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1651')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1651')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1651')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1651')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1651')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1651')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1651')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">평택우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">우리택배(유일용)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">우리택배(유일용)</td>
          <td align="center" style="text-align:left;padding-left:5px;">유일용</td>
          <td align="center"><a href="javascript:goEdit('1651');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1651');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1391 </td>
		  			<td align="center" onClick="viewHistory('info1','1650')" style="cursor:hand;">서울85자6128</td>
			<td align="center" onClick="viewHistory('info1','1650')" style="cursor:hand;">KNFSTZ76AFK000366</td>
			<td align="center">2015-07-06</td>
			<td align="center">용달영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1650')" style="cursor:hand;">삼성화재</td>
          <td align="center" onClick="viewHistory('insure1','1650')" style="cursor:hand;">2018-07-31</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1650')" style="cursor:hand">2019-07-30</td>
			  			  <td align="center" onClick="viewHistory('insure1','1650')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1650')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure2','1650')" style="cursor:hand;">2017-08-04</td>
		          <td align="center" onClick="viewHistory('insure2','1650')" style="cursor:hand;background-color:#ff0000;">2018-08-04</td>
		          <td align="center" onClick="viewHistory('insure2','1650')" style="cursor:hand;">1</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1650')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1650')" style="cursor:hand;background-color:#ff0000;">2018-07-25</td>
          <td align="center" onClick="viewHistory('dchk','1650')" style="cursor:hand;color:red;"><strike>2018-06-26</strike></td>
          <td align="center" onClick="viewHistory('dchk','1650')" style="cursor:hand;">2018-08-24</td>
		  <td align="center" style="text-align:left;padding-left:5px;">울산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)아이디일일구닷컴</td>
		            <td align="center" style="text-align:left;padding-left:5px;">아이디(김현삼)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김현삼</td>
          <td align="center"><a href="javascript:goEdit('1650');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1650');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1390 </td>
		  			<td align="center" onClick="viewHistory('info1','1649')" style="cursor:hand;">서울86바1959</td>
			<td align="center" onClick="viewHistory('info1','1649')" style="cursor:hand;">KMFZSZ7KBJU472947</td>
			<td align="center">2017-07-28</td>
			<td align="center">일반영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1649')" style="cursor:hand;">동부화재</td>
          <td align="center" onClick="viewHistory('insure1','1649')" style="cursor:hand;">2017-10-23</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1649')" style="cursor:hand">2018-10-23</td>
			  			  <td align="center" onClick="viewHistory('insure1','1649')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1649')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1649')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1649')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1649')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1649')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1649')" style="cursor:hand;background-color:#ff0000;">2018-07-27</td>
          <td align="center" onClick="viewHistory('dchk','1649')" style="cursor:hand;color:red;"><strike>2018-06-28</strike></td>
          <td align="center" onClick="viewHistory('dchk','1649')" style="cursor:hand;">2018-08-26</td>
		  <td align="center" style="text-align:left;padding-left:5px;">일산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">케이티지엘에스(주)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">호로직스(신호섭)</td>
          <td align="center" style="text-align:left;padding-left:5px;">신호섭</td>
          <td align="center"><a href="javascript:goEdit('1649');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1649');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1389 </td>
		  			<td align="center" onClick="viewHistory('info1','1648')" style="cursor:hand;">서울89자1244</td>
			<td align="center" onClick="viewHistory('info1','1648')" style="cursor:hand;">KMFZ927KBJU462612</td>
			<td align="center">2017-07-03</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1648')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1648')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1648')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1648')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1648')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure2','1648')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1648')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1648')" style="cursor:hand;">1</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1648')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1648')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1648')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1648')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">평택우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)창조지엘에스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">김홍희</td>
          <td align="center" style="text-align:left;padding-left:5px;">김홍희</td>
          <td align="center"><a href="javascript:goEdit('1648');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1648');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1388 </td>
		  			<td align="center" onClick="viewHistory('info1','1647')" style="cursor:hand;">광주89아2031</td>
			<td align="center" onClick="viewHistory('info1','1647')" style="cursor:hand;">KMFZSZ7KBHU428605</td>
			<td align="center">2017-03-20</td>
			<td align="center">용달영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1647')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure1','1647')" style="cursor:hand;">2017-07-10</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1647')" style="cursor:hand;color:red"><strike>2018-07-10</strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1647')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1647')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure2','1647')" style="cursor:hand;">2017-07-10</td>
				  <td align="center" onClick="viewHistory('insure2','1647')" style="cursor:hand;color:red;"><strike>2018-07-10</strike></td>
		          <td align="center" onClick="viewHistory('insure2','1647')" style="cursor:hand;">1</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1647')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1647')" style="cursor:hand;color:red;"><strike>2018-03-29</strike></td>
          <td align="center" onClick="viewHistory('dchk','1647')" style="cursor:hand;color:red;"><strike>2018-02-27</strike></td>
          <td align="center" onClick="viewHistory('dchk','1647')" style="cursor:hand;color:red;"><strike>2018-04-28</strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">화성우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)델타온</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해(곽혁)</td>
          <td align="center" style="text-align:left;padding-left:5px;">곽혁</td>
          <td align="center"><a href="javascript:goEdit('1647');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1647');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1387 </td>
		  			<td align="center" onClick="viewHistory('info1','1646')" style="cursor:hand;">서울89자7100</td>
			<td align="center" onClick="viewHistory('info1','1646')" style="cursor:hand;">KNFSTZ76BJK191130</td>
			<td align="center">2017-06-30</td>
			<td align="center">용달영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1646')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure1','1646')" style="cursor:hand;">2018-07-10</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1646')" style="cursor:hand">2019-07-10</td>
			  			  <td align="center" onClick="viewHistory('insure1','1646')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1646')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure2','1646')" style="cursor:hand;">2017-08-04</td>
		          <td align="center" onClick="viewHistory('insure2','1646')" style="cursor:hand;background-color:#ff0000;">2018-08-04</td>
		          <td align="center" onClick="viewHistory('insure2','1646')" style="cursor:hand;">1</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1646')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1646')" style="cursor:hand;color:red;"><strike>2018-06-29</strike></td>
          <td align="center" onClick="viewHistory('dchk','1646')" style="cursor:hand;color:red;"><strike>2018-05-30</strike></td>
          <td align="center" onClick="viewHistory('dchk','1646')" style="cursor:hand;background-color:#ff0000;">2018-07-28</td>
		  <td align="center" style="text-align:left;padding-left:5px;">파주우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)아이디일일구닷컴</td>
		            <td align="center" style="text-align:left;padding-left:5px;">아이디(이영훈)</td>
          <td align="center" style="text-align:left;padding-left:5px;">이영훈</td>
          <td align="center"><a href="javascript:goEdit('1646');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1646');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1386 </td>
		  			<td align="center" onClick="viewHistory('info1','1645')" style="cursor:hand;">서울87자9217</td>
			<td align="center" onClick="viewHistory('info1','1645')" style="cursor:hand;">KNCSJZ76BJK190639</td>
			<td align="center">2017-07-05</td>
			<td align="center">용달영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1645')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure1','1645')" style="cursor:hand;">2018-07-10</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1645')" style="cursor:hand">2019-07-10</td>
			  			  <td align="center" onClick="viewHistory('insure1','1645')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1645')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure2','1645')" style="cursor:hand;">2017-08-04</td>
		          <td align="center" onClick="viewHistory('insure2','1645')" style="cursor:hand;background-color:#ff0000;">2018-08-04</td>
		          <td align="center" onClick="viewHistory('insure2','1645')" style="cursor:hand;">1</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1645')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1645')" style="cursor:hand;color:red;"><strike>2018-07-04</strike></td>
          <td align="center" onClick="viewHistory('dchk','1645')" style="cursor:hand;color:red;"><strike>2018-06-05</strike></td>
          <td align="center" onClick="viewHistory('dchk','1645')" style="cursor:hand;background-color:#ff0000;">2018-08-03</td>
		  <td align="center" style="text-align:left;padding-left:5px;">화성우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)아이디일일구닷컴</td>
		            <td align="center" style="text-align:left;padding-left:5px;">아이디(박상욱)</td>
          <td align="center" style="text-align:left;padding-left:5px;">박상욱</td>
          <td align="center"><a href="javascript:goEdit('1645');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1645');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1385 </td>
		  			<td align="center" onClick="viewHistory('info1','1644')" style="cursor:hand;">광주89아1965</td>
			<td align="center" onClick="viewHistory('info1','1644')" style="cursor:hand;">KNFSTZ76BHK164918</td>
			<td align="center">2017-03-12</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1644')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1644')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1644')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1644')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1644')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure2','1644')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1644')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1644')" style="cursor:hand;">2</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1644')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1644')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1644')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1644')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">남울산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해로지스(김상근)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김상근</td>
          <td align="center"><a href="javascript:goEdit('1644');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1644');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1384 </td>
		  			<td align="center" onClick="viewHistory('info1','1643')" style="cursor:hand;">부산91아9649</td>
			<td align="center" onClick="viewHistory('info1','1643')" style="cursor:hand;">KNFSTZ76AEK859627</td>
			<td align="center">2014-03-18</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1643')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1643')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1643')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1643')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1643')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1643')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1643')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1643')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1643')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1643')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1643')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1643')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">남울산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">KET정종민</td>
		            <td align="center" style="text-align:left;padding-left:5px;">KET정종민</td>
          <td align="center" style="text-align:left;padding-left:5px;">정원호</td>
          <td align="center"><a href="javascript:goEdit('1643');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1643');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1383 </td>
		  			<td align="center" onClick="viewHistory('info1','1642')" style="cursor:hand;">경기90아4985</td>
			<td align="center" onClick="viewHistory('info1','1642')" style="cursor:hand;">KNCSJZ76AGK102683</td>
			<td align="center">2016-08-01</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1642')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1642')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1642')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1642')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1642')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1642')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1642')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1642')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1642')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1642')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1642')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1642')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">도봉우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">서현운수(박성철)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">서현운수(박성철)</td>
          <td align="center" style="text-align:left;padding-left:5px;">박성철</td>
          <td align="center"><a href="javascript:goEdit('1642');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1642');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1382 </td>
		  			<td align="center" onClick="viewHistory('info1','1641')" style="cursor:hand;">경기88사9518</td>
			<td align="center" onClick="viewHistory('info1','1641')" style="cursor:hand;">KMFWBH7JP9U107602</td>
			<td align="center">2008-10-31</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1641')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1641')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1641')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1641')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1641')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1641')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1641')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1641')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1641')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1641')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1641')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1641')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">도봉우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">이재용</td>
		            <td align="center" style="text-align:left;padding-left:5px;">이재용</td>
          <td align="center" style="text-align:left;padding-left:5px;">이재용</td>
          <td align="center"><a href="javascript:goEdit('1641');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1641');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1381 </td>
		  			<td align="center" onClick="viewHistory('info1','1640')" style="cursor:hand;">광주89아2035</td>
			<td align="center" onClick="viewHistory('info1','1640')" style="cursor:hand;">KMFWBX7KBHU918126</td>
			<td align="center">2017-06-07</td>
			<td align="center">용달영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1640')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure1','1640')" style="cursor:hand;">2018-06-12</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1640')" style="cursor:hand">2019-06-12</td>
			  			  <td align="center" onClick="viewHistory('insure1','1640')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1640')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure2','1640')" style="cursor:hand;">2017-06-12</td>
				  <td align="center" onClick="viewHistory('insure2','1640')" style="cursor:hand;color:red;"><strike>2018-06-12</strike></td>
		          <td align="center" onClick="viewHistory('insure2','1640')" style="cursor:hand;">1</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1640')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1640')" style="cursor:hand;color:red;"><strike>2018-06-06</strike></td>
          <td align="center" onClick="viewHistory('dchk','1640')" style="cursor:hand;color:red;"><strike>2018-05-07</strike></td>
          <td align="center" onClick="viewHistory('dchk','1640')" style="cursor:hand;color:red;"><strike>2018-04-05</strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">양산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)델타온</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해로지스(한지훈)</td>
          <td align="center" style="text-align:left;padding-left:5px;">한지훈</td>
          <td align="center"><a href="javascript:goEdit('1640');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1640');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1380 </td>
		  			<td align="center" onClick="viewHistory('info1','1639')" style="cursor:hand;">광주89아2032</td>
			<td align="center" onClick="viewHistory('info1','1639')" style="cursor:hand;">KNFSTZ76AFK976721</td>
			<td align="center">2015-05-11</td>
			<td align="center">용달영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1639')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure1','1639')" style="cursor:hand;">2018-06-26</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1639')" style="cursor:hand">2019-06-26</td>
			  			  <td align="center" onClick="viewHistory('insure1','1639')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1639')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure2','1639')" style="cursor:hand;">2017-06-26</td>
				  <td align="center" onClick="viewHistory('insure2','1639')" style="cursor:hand;color:red;"><strike>2018-06-26</strike></td>
		          <td align="center" onClick="viewHistory('insure2','1639')" style="cursor:hand;">1</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1639')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1639')" style="cursor:hand;color:red;"><strike>2018-05-10</strike></td>
          <td align="center" onClick="viewHistory('dchk','1639')" style="cursor:hand;color:red;"><strike>2018-04-11</strike></td>
          <td align="center" onClick="viewHistory('dchk','1639')" style="cursor:hand;color:red;"><strike>2018-06-09</strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">남울산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)델타온</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해로지스(박준태)</td>
          <td align="center" style="text-align:left;padding-left:5px;">박준태</td>
          <td align="center"><a href="javascript:goEdit('1639');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1639');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1379 </td>
		  			<td align="center" onClick="viewHistory('info1','1638')" style="cursor:hand;">광주89아2036</td>
			<td align="center" onClick="viewHistory('info1','1638')" style="cursor:hand;">KN4HNW6NL8K278649</td>
			<td align="center">2007-10-10</td>
			<td align="center">용달영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1638')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure1','1638')" style="cursor:hand;">2018-06-12</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1638')" style="cursor:hand">2019-06-12</td>
			  			  <td align="center" onClick="viewHistory('insure1','1638')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1638')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure2','1638')" style="cursor:hand;">2017-06-12</td>
				  <td align="center" onClick="viewHistory('insure2','1638')" style="cursor:hand;color:red;"><strike>2018-06-12</strike></td>
		          <td align="center" onClick="viewHistory('insure2','1638')" style="cursor:hand;">1</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1638')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1638')" style="cursor:hand;">2018-12-10</td>
          <td align="center" onClick="viewHistory('dchk','1638')" style="cursor:hand;">2018-11-11</td>
          <td align="center" onClick="viewHistory('dchk','1638')" style="cursor:hand;">2019-01-09</td>
		  <td align="center" style="text-align:left;padding-left:5px;">양산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)델타온</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해로지스(강희식)</td>
          <td align="center" style="text-align:left;padding-left:5px;">강희식</td>
          <td align="center"><a href="javascript:goEdit('1638');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1638');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1378 </td>
		  			<td align="center" onClick="viewHistory('info1','1637')" style="cursor:hand;">인천85아1371</td>
			<td align="center" onClick="viewHistory('info1','1637')" style="cursor:hand;">KMFWBX7KBCU480910</td>
			<td align="center">2012-05-10</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1637')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1637')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1637')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1637')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1637')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1637')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1637')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1637')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1637')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1637')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1637')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1637')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">경기혈액원</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">김건종</td>
		            <td align="center" style="text-align:left;padding-left:5px;">김건종</td>
          <td align="center" style="text-align:left;padding-left:5px;">김건종</td>
          <td align="center"><a href="javascript:goEdit('1637');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1637');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1377 </td>
		  			<td align="center" onClick="viewHistory('info1','1636')" style="cursor:hand;">광주89아1988</td>
			<td align="center" onClick="viewHistory('info1','1636')" style="cursor:hand;">KMFZCZ7KBJU455539</td>
			<td align="center">201706-12</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1636')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1636')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1636')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1636')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1636')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1636')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1636')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1636')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1636')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1636')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1636')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1636')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">일산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해로지스(김정진)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김정진</td>
          <td align="center"><a href="javascript:goEdit('1636');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1636');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1376 </td>
		  			<td align="center" onClick="viewHistory('info1','1635')" style="cursor:hand;">경북88자1651</td>
			<td align="center" onClick="viewHistory('info1','1635')" style="cursor:hand;">KMFZSZ7KBHU419866</td>
			<td align="center">2017-03-13</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1.2톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1635')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1635')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1635')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1635')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1635')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1635')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1635')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1635')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1635')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1635')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1635')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1635')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">현진테크(전운배)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">현진테크(전운배)</td>
          <td align="center" style="text-align:left;padding-left:5px;">전운배</td>
          <td align="center"><a href="javascript:goEdit('1635');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1635');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1375 </td>
		  			<td align="center" onClick="viewHistory('info1','1634')" style="cursor:hand;">경기93자7350</td>
			<td align="center" onClick="viewHistory('info1','1634')" style="cursor:hand;">KMFZCZ7KAEU973723</td>
			<td align="center">2013-04-09</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1634')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1634')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1634')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1634')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1634')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1634')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1634')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1634')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1634')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1634')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1634')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1634')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)세진트랜스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">세진트랜스(신의식)</td>
          <td align="center" style="text-align:left;padding-left:5px;">신의식</td>
          <td align="center"><a href="javascript:goEdit('1634');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1634');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1374 </td>
		  			<td align="center" onClick="viewHistory('info1','1633')" style="cursor:hand;">강원80자5643</td>
			<td align="center" onClick="viewHistory('info1','1633')" style="cursor:hand;">KMFZSZ7KACU885441</td>
			<td align="center">2012-07-13</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1633')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1633')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1633')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1633')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1633')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1633')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1633')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1633')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1633')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1633')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1633')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1633')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국전력(강원)</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">이우현</td>
		            <td align="center" style="text-align:left;padding-left:5px;">이우현</td>
          <td align="center" style="text-align:left;padding-left:5px;">이우현</td>
          <td align="center"><a href="javascript:goEdit('1633');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1633');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1373 </td>
		  			<td align="center" onClick="viewHistory('info1','1632')" style="cursor:hand;">서울89자3834</td>
			<td align="center" onClick="viewHistory('info1','1632')" style="cursor:hand;">KMFZSZ7JABU744498</td>
			<td align="center">2011-04-21</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1632')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1632')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1632')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1632')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1632')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1632')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1632')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1632')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1632')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1632')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1632')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1632')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국전력(강원)</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)동광지엘에스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">동광(최승순)</td>
          <td align="center" style="text-align:left;padding-left:5px;">최승순(4)</td>
          <td align="center"><a href="javascript:goEdit('1632');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1632');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1372 </td>
		  			<td align="center" onClick="viewHistory('info1','1631')" style="cursor:hand;">서울86자4458</td>
			<td align="center" onClick="viewHistory('info1','1631')" style="cursor:hand;">KMFZSZ7JAAU678635</td>
			<td align="center">2010-10-10</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1631')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1631')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1631')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1631')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1631')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1631')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1631')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1631')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1631')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1631')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1631')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1631')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국전력(강원)</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)동광지엘에스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">동광(남궁정운)</td>
          <td align="center" style="text-align:left;padding-left:5px;">남궁정운(1)</td>
          <td align="center"><a href="javascript:goEdit('1631');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1631');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1371 </td>
		  			<td align="center" onClick="viewHistory('info1','1630')" style="cursor:hand;">서울89자3835</td>
			<td align="center" onClick="viewHistory('info1','1630')" style="cursor:hand;">KMFZSZ7JAAU613506</td>
			<td align="center">2010-03-03</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1630')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1630')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1630')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1630')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1630')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1630')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1630')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1630')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1630')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1630')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1630')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1630')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국전력(강원)</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)동광지엘에스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">동광지엘(백지성)</td>
          <td align="center" style="text-align:left;padding-left:5px;">백지성</td>
          <td align="center"><a href="javascript:goEdit('1630');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1630');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1370 </td>
		  			<td align="center" onClick="viewHistory('info1','1629')" style="cursor:hand;">광주89아2037</td>
			<td align="center" onClick="viewHistory('info1','1629')" style="cursor:hand;">KNCSJZ76AEK902848</td>
			<td align="center">2014-07-18</td>
			<td align="center">용달영업용</td>
			<td align="center">파워게이트</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1629')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure1','1629')" style="cursor:hand;">2018-06-05</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1629')" style="cursor:hand">2019-06-05</td>
			  			  <td align="center" onClick="viewHistory('insure1','1629')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1629')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure2','1629')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1629')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1629')" style="cursor:hand;">3</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1629')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1629')" style="cursor:hand;color:red;"><strike>2018-07-04</strike></td>
          <td align="center" onClick="viewHistory('dchk','1629')" style="cursor:hand;color:red;"><strike>2018-06-05</strike></td>
          <td align="center" onClick="viewHistory('dchk','1629')" style="cursor:hand;background-color:#ff0000;">2018-08-03</td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">케이티지엘에스(주)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">(남해)김병원</td>
          <td align="center" style="text-align:left;padding-left:5px;">김병원</td>
          <td align="center"><a href="javascript:goEdit('1629');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1629');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1369 </td>
		  			<td align="center" onClick="viewHistory('info1','1628')" style="cursor:hand;">서울85자8940</td>
			<td align="center" onClick="viewHistory('info1','1628')" style="cursor:hand;">KNFSTZ76BHK177100</td>
			<td align="center">2017-05-02</td>
			<td align="center">용달영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1628')" style="cursor:hand;">동부화재</td>
          <td align="center" onClick="viewHistory('insure1','1628')" style="cursor:hand;">2018-06-20</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1628')" style="cursor:hand">2019-06-20</td>
			  			  <td align="center" onClick="viewHistory('insure1','1628')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1628')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1628')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1628')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1628')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1628')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1628')" style="cursor:hand;color:red;"><strike>2018-05-01</strike></td>
          <td align="center" onClick="viewHistory('dchk','1628')" style="cursor:hand;color:red;"><strike>2018-04-02</strike></td>
          <td align="center" onClick="viewHistory('dchk','1628')" style="cursor:hand;color:red;"><strike>2018-05-31</strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">제주우편집중국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)델타온</td>
		            <td align="center" style="text-align:left;padding-left:5px;">허태현</td>
          <td align="center" style="text-align:left;padding-left:5px;">허태현</td>
          <td align="center"><a href="javascript:goEdit('1628');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1628');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1368 </td>
		  			<td align="center" onClick="viewHistory('info1','1627')" style="cursor:hand;">광주89아1995</td>
			<td align="center" onClick="viewHistory('info1','1627')" style="cursor:hand;">KNFSE0D439K379421</td>
			<td align="center">2009-02-25</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1627')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1627')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1627')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1627')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1627')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure2','1627')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1627')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1627')" style="cursor:hand;">3</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1627')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1627')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1627')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1627')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">울산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해로지스(허재웅)</td>
          <td align="center" style="text-align:left;padding-left:5px;">허재웅</td>
          <td align="center"><a href="javascript:goEdit('1627');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1627');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1367 </td>
		  			<td align="center" onClick="viewHistory('info1','1626')" style="cursor:hand;">서울80자3525</td>
			<td align="center" onClick="viewHistory('info1','1626')" style="cursor:hand;">KMFZSZ7KAFU107315</td>
			<td align="center">2014-06-17</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1626')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1626')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1626')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1626')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1626')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1626')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1626')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1626')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1626')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1626')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1626')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1626')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">김태호</td>
		            <td align="center" style="text-align:left;padding-left:5px;">김태호</td>
          <td align="center" style="text-align:left;padding-left:5px;">김태호</td>
          <td align="center"><a href="javascript:goEdit('1626');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1626');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1366 </td>
		  			<td align="center" onClick="viewHistory('info1','1625')" style="cursor:hand;">서울85자8957</td>
			<td align="center" onClick="viewHistory('info1','1625')" style="cursor:hand;">KNFWXZ76ADK711791</td>
			<td align="center">2012-09-05</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1.2톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1625')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure1','1625')" style="cursor:hand;">2018-05-17</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1625')" style="cursor:hand">2019-05-17</td>
			  			  <td align="center" onClick="viewHistory('insure1','1625')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1625')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1625')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1625')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1625')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1625')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1625')" style="cursor:hand;color:red;"><strike>2018-03-30</strike></td>
          <td align="center" onClick="viewHistory('dchk','1625')" style="cursor:hand;color:red;"><strike>2018-02-27</strike></td>
          <td align="center" onClick="viewHistory('dchk','1625')" style="cursor:hand;color:red;"><strike>2018-04-29</strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한승공제주지사</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)델타온</td>
		            <td align="center" style="text-align:left;padding-left:5px;">창건(박창건)</td>
          <td align="center" style="text-align:left;padding-left:5px;">박창건</td>
          <td align="center"><a href="javascript:goEdit('1625');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1625');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1365 </td>
		  			<td align="center" onClick="viewHistory('info1','1624')" style="cursor:hand;">광주89아1996</td>
			<td align="center" onClick="viewHistory('info1','1624')" style="cursor:hand;">KNCSE03438K334974</td>
			<td align="center">2008-08-16</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1624')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1624')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1624')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1624')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1624')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1624')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1624')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1624')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1624')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1624')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1624')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1624')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">경산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">케이티김준영</td>
          <td align="center" style="text-align:left;padding-left:5px;">김준영</td>
          <td align="center"><a href="javascript:goEdit('1624');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1624');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1364 </td>
		  			<td align="center" onClick="viewHistory('info1','1623')" style="cursor:hand;">경기81자8427</td>
			<td align="center" onClick="viewHistory('info1','1623')" style="cursor:hand;">KLY2B11ZDCC111066</td>
			<td align="center">2011-08-18</td>
			<td align="center">영업용</td>
			<td align="center">경형화물</td>
			<td align="center">1톤미만</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1623')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1623')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1623')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1623')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1623')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1623')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1623')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1623')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1623')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1623')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1623')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1623')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">덕양우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">택배(안복순)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">택배(안복순)</td>
          <td align="center" style="text-align:left;padding-left:5px;">안복순</td>
          <td align="center"><a href="javascript:goEdit('1623');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1623');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1363 </td>
		  			<td align="center" onClick="viewHistory('info1','1622')" style="cursor:hand;">서울89자4303</td>
			<td align="center" onClick="viewHistory('info1','1622')" style="cursor:hand;">KNCSJZ76AHK156305</td>
			<td align="center">2017-02-13</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1622')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1622')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1622')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1622')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1622')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1622')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1622')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1622')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1622')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1622')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1622')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1622')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">서수원우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)서현운수</td>
		            <td align="center" style="text-align:left;padding-left:5px;">김동욱</td>
          <td align="center" style="text-align:left;padding-left:5px;">김동욱</td>
          <td align="center"><a href="javascript:goEdit('1622');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1622');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1362 </td>
		  			<td align="center" onClick="viewHistory('info1','1621')" style="cursor:hand;">경기91사1078</td>
			<td align="center" onClick="viewHistory('info1','1621')" style="cursor:hand;">KMFZSZ7KBFU178778</td>
			<td align="center">2015-02-09</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1621')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1621')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1621')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1621')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1621')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1621')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1621')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1621')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1621')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1621')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1621')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1621')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">화성우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">용달(강준모)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">용달(강준모)</td>
          <td align="center" style="text-align:left;padding-left:5px;">강준모</td>
          <td align="center"><a href="javascript:goEdit('1621');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1621');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1361 </td>
		  			<td align="center" onClick="viewHistory('info1','1620')" style="cursor:hand;">서울86바3522</td>
			<td align="center" onClick="viewHistory('info1','1620')" style="cursor:hand;">KMFZCZ7KAFU107684</td>
			<td align="center">2014-06-09</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1620')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1620')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1620')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1620')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1620')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1620')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1620')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1620')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1620')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1620')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1620')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1620')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">중앙혈액검사센터</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)한솔로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">명진(이덕희)</td>
          <td align="center" style="text-align:left;padding-left:5px;">이덕희</td>
          <td align="center"><a href="javascript:goEdit('1620');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1620');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1360 </td>
		  			<td align="center" onClick="viewHistory('info1','1619')" style="cursor:hand;">서울89자4040</td>
			<td align="center" onClick="viewHistory('info1','1619')" style="cursor:hand;">KLY2B11ZDAC549244</td>
			<td align="center">2010-03-26</td>
			<td align="center">영업용</td>
			<td align="center">경영화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1619')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1619')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1619')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1619')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1619')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1619')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1619')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1619')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1619')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1619')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1619')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1619')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">중앙혈액검사센터</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">한상수</td>
		            <td align="center" style="text-align:left;padding-left:5px;">한상수</td>
          <td align="center" style="text-align:left;padding-left:5px;">한상수</td>
          <td align="center"><a href="javascript:goEdit('1619');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1619');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1359 </td>
		  			<td align="center" onClick="viewHistory('info1','1618')" style="cursor:hand;">경기80아1882</td>
			<td align="center" onClick="viewHistory('info1','1618')" style="cursor:hand;">KMFZSN7JP9U447922</td>
			<td align="center">2008-08-28</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1618')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1618')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1618')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1618')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1618')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1618')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1618')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1618')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1618')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1618')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1618')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1618')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">동수원우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">조영재</td>
		            <td align="center" style="text-align:left;padding-left:5px;">조영재</td>
          <td align="center" style="text-align:left;padding-left:5px;">조영재</td>
          <td align="center"><a href="javascript:goEdit('1618');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1618');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1358 </td>
		  			<td align="center" onClick="viewHistory('info1','1617')" style="cursor:hand;">경기80바4254</td>
			<td align="center" onClick="viewHistory('info1','1617')" style="cursor:hand;">KMFZCZ7KADU923659</td>
			<td align="center">2013-02-06</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center"></td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1617')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1617')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1617')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1617')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1617')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1617')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1617')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1617')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1617')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1617')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1617')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1617')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">화성우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">서기찬</td>
		            <td align="center" style="text-align:left;padding-left:5px;">서기찬</td>
          <td align="center" style="text-align:left;padding-left:5px;">서기찬</td>
          <td align="center"><a href="javascript:goEdit('1617');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1617');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1357 </td>
		  			<td align="center" onClick="viewHistory('info1','1616')" style="cursor:hand;">울산90자3365</td>
			<td align="center" onClick="viewHistory('info1','1616')" style="cursor:hand;">KMFZCZ7KAGU294174</td>
			<td align="center">2016-01-15</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1616')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1616')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1616')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1616')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1616')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1616')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1616')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1616')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1616')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1616')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1616')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1616')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">바로물류(신상섭)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">바로물류(신상섭)</td>
          <td align="center" style="text-align:left;padding-left:5px;">신상섭</td>
          <td align="center"><a href="javascript:goEdit('1616');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1616');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1356 </td>
		  			<td align="center" onClick="viewHistory('info1','1615')" style="cursor:hand;">광주89아1986</td>
			<td align="center" onClick="viewHistory('info1','1615')" style="cursor:hand;">KNCSJZ76AGK074063</td>
			<td align="center">2016-03-29</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1615')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1615')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1615')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1615')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1615')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1615')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1615')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1615')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1615')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1615')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1615')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1615')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">평택우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해로지스(김진호)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김진호</td>
          <td align="center"><a href="javascript:goEdit('1615');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1615');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1355 </td>
		  			<td align="center" onClick="viewHistory('info1','1614')" style="cursor:hand;">광주89아1989</td>
			<td align="center" onClick="viewHistory('info1','1614')" style="cursor:hand;">KNFSTZ76BGK091475</td>
			<td align="center">2016-05-20`</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1614')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1614')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1614')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1614')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1614')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure2','1614')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1614')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1614')" style="cursor:hand;">2</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1614')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1614')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1614')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1614')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">평택우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해로지스(허진)</td>
          <td align="center" style="text-align:left;padding-left:5px;">허진</td>
          <td align="center"><a href="javascript:goEdit('1614');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1614');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1354 </td>
		  			<td align="center" onClick="viewHistory('info1','1613')" style="cursor:hand;">광주89아1987</td>
			<td align="center" onClick="viewHistory('info1','1613')" style="cursor:hand;">KMFZSZ7KAHU430019</td>
			<td align="center">2017-04-21</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1613')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1613')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1613')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1613')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1613')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure2','1613')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1613')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1613')" style="cursor:hand;">2</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1613')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1613')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1613')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1613')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">파주우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해로지스(윤재창)</td>
          <td align="center" style="text-align:left;padding-left:5px;">윤재창</td>
          <td align="center"><a href="javascript:goEdit('1613');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1613');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1353 </td>
		  			<td align="center" onClick="viewHistory('info1','1611')" style="cursor:hand;">광주89아1979</td>
			<td align="center" onClick="viewHistory('info1','1611')" style="cursor:hand;">KMFZSZ7KBHU354513</td>
			<td align="center">2016-07-18</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1611')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1611')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1611')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1611')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1611')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure2','1611')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1611')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1611')" style="cursor:hand;">2</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1611')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1611')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1611')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1611')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">평택우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해로지스(김성민)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김성민</td>
          <td align="center"><a href="javascript:goEdit('1611');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1611');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1352 </td>
		  			<td align="center" onClick="viewHistory('info1','1610')" style="cursor:hand;">광주89아1982</td>
			<td align="center" onClick="viewHistory('info1','1610')" style="cursor:hand;">KNFSTZ76AHK130893</td>
			<td align="center">2016-11-28</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1610')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1610')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1610')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1610')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1610')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure2','1610')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1610')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1610')" style="cursor:hand;">2</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1610')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1610')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1610')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1610')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">일산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">정우물류(조덕형)</td>
          <td align="center" style="text-align:left;padding-left:5px;">조덕형</td>
          <td align="center"><a href="javascript:goEdit('1610');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1610');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1351 </td>
		  			<td align="center" onClick="viewHistory('info1','1609')" style="cursor:hand;">광주89아1973</td>
			<td align="center" onClick="viewHistory('info1','1609')" style="cursor:hand;">KMFZSZ7JABU746294</td>
			<td align="center">2011-05-08</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1609')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1609')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1609')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1609')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1609')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure2','1609')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1609')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1609')" style="cursor:hand;">3</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1609')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1609')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1609')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1609')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">일산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해로지스(박익수)</td>
          <td align="center" style="text-align:left;padding-left:5px;">박익수</td>
          <td align="center"><a href="javascript:goEdit('1609');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1609');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1350 </td>
		  			<td align="center" onClick="viewHistory('info1','1608')" style="cursor:hand;">서울84자6251</td>
			<td align="center" onClick="viewHistory('info1','1608')" style="cursor:hand;">KMFZCZ7KBGU235469</td>
			<td align="center">2015-07-15</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1608')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1608')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1608')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1608')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1608')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1608')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1608')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1608')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1608')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1608')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1608')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1608')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">서울용달(전주환)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">서울용달(전주환)</td>
          <td align="center" style="text-align:left;padding-left:5px;">전주환</td>
          <td align="center"><a href="javascript:goEdit('1608');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1608');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1349 </td>
		  			<td align="center" onClick="viewHistory('info1','1607')" style="cursor:hand;">서울85자1561</td>
			<td align="center" onClick="viewHistory('info1','1607')" style="cursor:hand;">KMFZCZ7KBEU030587</td>
			<td align="center">2013-10-18</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1607')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1607')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1607')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1607')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1607')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1607')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1607')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1607')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1607')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1607')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1607')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1607')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">개별용달(손승희)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">개별용달(손승희)</td>
          <td align="center" style="text-align:left;padding-left:5px;">손승희</td>
          <td align="center"><a href="javascript:goEdit('1607');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1607');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1348 </td>
		  			<td align="center" onClick="viewHistory('info1','1606')" style="cursor:hand;">경북83아8918</td>
			<td align="center" onClick="viewHistory('info1','1606')" style="cursor:hand;">KLY2B11ZDAC536554</td>
			<td align="center">2009-12-03</td>
			<td align="center">영업용</td>
			<td align="center">경형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1606')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1606')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1606')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1606')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1606')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1606')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1606')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1606')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1606')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1606')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1606')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1606')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">중앙혈액검사센터</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)전차연통운</td>
		            <td align="center" style="text-align:left;padding-left:5px;">가나콜링(이영재)</td>
          <td align="center" style="text-align:left;padding-left:5px;">이영재</td>
          <td align="center"><a href="javascript:goEdit('1606');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1606');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1347 </td>
		  			<td align="center" onClick="viewHistory('info1','1605')" style="cursor:hand;">서울86자2472</td>
			<td align="center" onClick="viewHistory('info1','1605')" style="cursor:hand;">KLY2B11ZDBC108345</td>
			<td align="center">2010-10-14</td>
			<td align="center">영업용</td>
			<td align="center">경형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1605')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1605')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1605')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1605')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1605')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1605')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1605')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1605')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1605')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1605')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1605')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1605')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">중앙혈액검사센터</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">방방특송(방민제)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">방방특송(방민제)</td>
          <td align="center" style="text-align:left;padding-left:5px;">방민제</td>
          <td align="center"><a href="javascript:goEdit('1605');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1605');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1346 </td>
		  			<td align="center" onClick="viewHistory('info1','1604')" style="cursor:hand;">서울84자7555</td>
			<td align="center" onClick="viewHistory('info1','1604')" style="cursor:hand;">KLY2B11ZDDC032108</td>
			<td align="center">2013-01-18</td>
			<td align="center">영업용</td>
			<td align="center">경형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1604')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1604')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1604')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1604')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1604')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1604')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1604')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1604')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1604')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1604')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1604')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1604')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">중앙혈액검사센터</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">용달(김준회)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">용달(김준회)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김준회</td>
          <td align="center"><a href="javascript:goEdit('1604');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1604');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1345 </td>
		  			<td align="center" onClick="viewHistory('info1','1603')" style="cursor:hand;">서울85자5736</td>
			<td align="center" onClick="viewHistory('info1','1603')" style="cursor:hand;">KLY2B51EDEC024762</td>
			<td align="center">2014-02-26</td>
			<td align="center">영업용</td>
			<td align="center">경형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1603')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1603')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1603')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1603')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1603')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1603')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1603')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1603')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1603')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1603')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1603')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1603')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">중앙혈액검사센터</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">도익배</td>
		            <td align="center" style="text-align:left;padding-left:5px;">도익배</td>
          <td align="center" style="text-align:left;padding-left:5px;">도익배</td>
          <td align="center"><a href="javascript:goEdit('1603');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1603');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1344 </td>
		  			<td align="center" onClick="viewHistory('info1','1602')" style="cursor:hand;">서울86자7752</td>
			<td align="center" onClick="viewHistory('info1','1602')" style="cursor:hand;">KMFZSZ7KAFU113862</td>
			<td align="center">2014-07-08</td>
			<td align="center">용달영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1602')" style="cursor:hand;">동부화재</td>
          <td align="center" onClick="viewHistory('insure1','1602')" style="cursor:hand;">2018-04-13</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1602')" style="cursor:hand">2019-04-13</td>
			  			  <td align="center" onClick="viewHistory('insure1','1602')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1602')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure2','1602')" style="cursor:hand;">2017-11-04</td>
				  <td align="center" onClick="viewHistory('insure2','1602')" style="cursor:hand;">2018-11-04</td>
		          <td align="center" onClick="viewHistory('insure2','1602')" style="cursor:hand;">1</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1602')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1602')" style="cursor:hand;">2018-11-19</td>
          <td align="center" onClick="viewHistory('dchk','1602')" style="cursor:hand;">2018-10-20</td>
          <td align="center" onClick="viewHistory('dchk','1602')" style="cursor:hand;">2018-12-18</td>
		  <td align="center" style="text-align:left;padding-left:5px;">화성우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)델타온</td>
		            <td align="center" style="text-align:left;padding-left:5px;">조헌주</td>
          <td align="center" style="text-align:left;padding-left:5px;">조헌주</td>
          <td align="center"><a href="javascript:goEdit('1602');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1602');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1343 </td>
		  			<td align="center" onClick="viewHistory('info1','1601')" style="cursor:hand;">광주89아1983</td>
			<td align="center" onClick="viewHistory('info1','1601')" style="cursor:hand;">KMFZSZ7JACU796734</td>
			<td align="center">2011-10-05</td>
			<td align="center">자가용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1601')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1601')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1601')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1601')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1601')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure2','1601')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1601')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1601')" style="cursor:hand;">2</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1601')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1601')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1601')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1601')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">파주우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">박동규</td>
          <td align="center" style="text-align:left;padding-left:5px;">박동규</td>
          <td align="center"><a href="javascript:goEdit('1601');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1601');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1342 </td>
		  			<td align="center" onClick="viewHistory('info1','1600')" style="cursor:hand;">서울82바1318</td>
			<td align="center" onClick="viewHistory('info1','1600')" style="cursor:hand;">KNCWJZ76ADK781590</td>
			<td align="center">2013-05-23</td>
			<td align="center">일반영업용</td>
			<td align="center">파워게이트</td>
			<td align="center">1.2톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1600')" style="cursor:hand;">동부화재</td>
          <td align="center" onClick="viewHistory('insure1','1600')" style="cursor:hand;">2018-02-23</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1600')" style="cursor:hand">2019-02-23</td>
			  			  <td align="center" onClick="viewHistory('insure1','1600')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1600')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1600')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1600')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1600')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1600')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1600')" style="cursor:hand;color:red;"><strike>2018-07-01</strike></td>
          <td align="center" onClick="viewHistory('dchk','1600')" style="cursor:hand;color:red;"><strike>2018-06-02</strike></td>
          <td align="center" onClick="viewHistory('dchk','1600')" style="cursor:hand;">2018-08-31</td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)델타온</td>
		            <td align="center" style="text-align:left;padding-left:5px;">경기86아2687(김중호)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김중호(경동)</td>
          <td align="center"><a href="javascript:goEdit('1600');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1600');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1341 </td>
		  			<td align="center" onClick="viewHistory('info1','1599')" style="cursor:hand;">서울85바5806</td>
			<td align="center" onClick="viewHistory('info1','1599')" style="cursor:hand;">KNCSJZ76AHK163926</td>
			<td align="center">2017-03-09</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1599')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1599')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1599')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1599')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1599')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1599')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1599')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1599')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1599')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1599')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1599')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1599')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">은평우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)서현운수</td>
		            <td align="center" style="text-align:left;padding-left:5px;">서현운수(김영환)</td>
          <td align="center" style="text-align:left;padding-left:5px;">김영환</td>
          <td align="center"><a href="javascript:goEdit('1599');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1599');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1340 </td>
		  			<td align="center" onClick="viewHistory('info1','1598')" style="cursor:hand;">광주89아1974</td>
			<td align="center" onClick="viewHistory('info1','1598')" style="cursor:hand;">KMFZSX7KBFU109572</td>
			<td align="center">2014-06-20</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1598')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1598')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1598')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1598')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1598')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure2','1598')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1598')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1598')" style="cursor:hand;">2</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1598')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1598')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1598')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1598')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">일산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">남해로지스(이주원)</td>
          <td align="center" style="text-align:left;padding-left:5px;">이주원</td>
          <td align="center"><a href="javascript:goEdit('1598');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1598');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1339 </td>
		  			<td align="center" onClick="viewHistory('info1','1597')" style="cursor:hand;">광주89아1978</td>
			<td align="center" onClick="viewHistory('info1','1597')" style="cursor:hand;">KMFZSS7HP7U258225</td>
			<td align="center">2007-01-02</td>
			<td align="center">영업용</td>
			<td align="center">중형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1597')" style="cursor:hand;">AXI손해보험</td>
          <td align="center" onClick="viewHistory('insure1','1597')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1597')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1597')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1597')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1597')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1597')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1597')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1597')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1597')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1597')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1597')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">평택우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(유)남해로지스</td>
		            <td align="center" style="text-align:left;padding-left:5px;">김용석</td>
          <td align="center" style="text-align:left;padding-left:5px;">김용석</td>
          <td align="center"><a href="javascript:goEdit('1597');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1597');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1338 </td>
		  			<td align="center" onClick="viewHistory('info1','1596')" style="cursor:hand;">서울85바5897</td>
			<td align="center" onClick="viewHistory('info1','1596')" style="cursor:hand;">KNCSJZ76AHK160754</td>
			<td align="center">2017-02-28</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1596')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1596')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1596')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1596')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1596')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1596')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1596')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1596')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1596')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1596')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1596')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1596')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">덕양우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">(주)청현운수</td>
		            <td align="center" style="text-align:left;padding-left:5px;">청현운수(송재호)</td>
          <td align="center" style="text-align:left;padding-left:5px;">송재호</td>
          <td align="center"><a href="javascript:goEdit('1596');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1596');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1337 </td>
		  			<td align="center" onClick="viewHistory('info1','1595')" style="cursor:hand;">서울89자5935</td>
			<td align="center" onClick="viewHistory('info1','1595')" style="cursor:hand;">KMFZSZ7KAHU420878</td>
			<td align="center">2017-03-15</td>
			<td align="center">용달영업용</td>
			<td align="center">내장탑</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1595')" style="cursor:hand;">동부화재</td>
          <td align="center" onClick="viewHistory('insure1','1595')" style="cursor:hand;">2018-04-17</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1595')" style="cursor:hand">2019-04-17</td>
			  			  <td align="center" onClick="viewHistory('insure1','1595')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1595')" style="cursor:hand;">화물공제조합</td>
          <td align="center" onClick="viewHistory('insure2','1595')" style="cursor:hand;">2017-06-30</td>
				  <td align="center" onClick="viewHistory('insure2','1595')" style="cursor:hand;color:red;"><strike>2018-06-30</strike></td>
		          <td align="center" onClick="viewHistory('insure2','1595')" style="cursor:hand;">1</td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1595')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1595')" style="cursor:hand;color:red;"><strike>2018-03-14</strike></td>
          <td align="center" onClick="viewHistory('dchk','1595')" style="cursor:hand;color:red;"><strike>2018-02-15</strike></td>
          <td align="center" onClick="viewHistory('dchk','1595')" style="cursor:hand;color:red;"><strike>2018-04-13</strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">서수원우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">케이티지엘에스(주)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">이의진</td>
          <td align="center" style="text-align:left;padding-left:5px;">이의진</td>
          <td align="center"><a href="javascript:goEdit('1595');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1595');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1336 </td>
		  			<td align="center" onClick="viewHistory('info1','1594')" style="cursor:hand;">서울82바9263</td>
			<td align="center" onClick="viewHistory('info1','1594')" style="cursor:hand;">KNCWJZ74ABK613253</td>
			<td align="center">2011-09-01</td>
			<td align="center">일반영업용</td>
			<td align="center">파워게이트</td>
			<td align="center">1.2톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1594')" style="cursor:hand;">동부화재</td>
          <td align="center" onClick="viewHistory('insure1','1594')" style="cursor:hand;">2018-07-24</td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td align="center" onClick="viewHistory('insure1','1594')" style="cursor:hand">2019-07-24</td>
			  			  <td align="center" onClick="viewHistory('insure1','1594')" style="cursor:hand;">6</td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1594')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1594')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1594')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1594')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1594')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1594')" style="cursor:hand;color:red;"><strike>2018-02-28</strike></td>
          <td align="center" onClick="viewHistory('dchk','1594')" style="cursor:hand;color:red;"><strike>2018-01-29</strike></td>
          <td align="center" onClick="viewHistory('dchk','1594')" style="cursor:hand;color:red;"><strike>2018-03-27</strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국승강기안전공단</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">케이티지엘에스(주)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">장수영</td>
          <td align="center" style="text-align:left;padding-left:5px;">장수영</td>
          <td align="center"><a href="javascript:goEdit('1594');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1594');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1335 </td>
		  			<td align="center" onClick="viewHistory('info1','1593')" style="cursor:hand;">서울86자4542</td>
			<td align="center" onClick="viewHistory('info1','1593')" style="cursor:hand;">KMFWVH7HP1U309943</td>
			<td align="center">2001-02-05</td>
			<td align="center">영업용</td>
			<td align="center">중형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1593')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1593')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1593')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1593')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1593')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1593')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1593')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1593')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1593')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1593')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1593')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1593')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">서울남부혈액원</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">TOP콜밴(강만석)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">TOP콜밴(강만석)</td>
          <td align="center" style="text-align:left;padding-left:5px;">강만석</td>
          <td align="center"><a href="javascript:goEdit('1593');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1593');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1334 </td>
		  			<td align="center" onClick="viewHistory('info1','1592')" style="cursor:hand;">서울80자8870</td>
			<td align="center" onClick="viewHistory('info1','1592')" style="cursor:hand;">KNCUA28131S106509</td>
			<td align="center">2001-07-10</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1592')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1592')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1592')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1592')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1592')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1592')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1592')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1592')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1592')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1592')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1592')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1592')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">서울남부혈액원</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">콜벤(홍양표)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">콜벤(홍양표)</td>
          <td align="center" style="text-align:left;padding-left:5px;">홍양표</td>
          <td align="center"><a href="javascript:goEdit('1592');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1592');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1333 </td>
		  			<td align="center" onClick="viewHistory('info1','1591')" style="cursor:hand;">부산90아7136</td>
			<td align="center" onClick="viewHistory('info1','1591')" style="cursor:hand;">KMFZSS7JP8U339487</td>
			<td align="center">2007-09-04</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1591')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1591')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1591')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1591')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1591')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1591')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1591')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1591')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1591')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1591')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1591')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1591')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">남울산우체국</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">대동운수(주)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">대동운수(문세철)</td>
          <td align="center" style="text-align:left;padding-left:5px;">문세철</td>
          <td align="center"><a href="javascript:goEdit('1591');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1591');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <td height="25" align="center">1332 </td>
		  			<td align="center" onClick="viewHistory('info1','1590')" style="cursor:hand;">대구80자7573</td>
			<td align="center" onClick="viewHistory('info1','1590')" style="cursor:hand;">KMFWBX7KBGU739324</td>
			<td align="center">2015-04-29</td>
			<td align="center">영업용</td>
			<td align="center">소형화물</td>
			<td align="center">1톤</td>
		  <!-- 자동차보험 //-->
          <td align="center" onClick="viewHistory('insure1','1590')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure1','1590')" style="cursor:hand;"></td>
<script>
//속도가 느려져서 안쓰기로 했습니다.
//엄청 좋은 css 
function blinker(){
	$('.blink_me').fadeOut(2000);
	$('.blink_me').fadeIn(2000);
}

setInterval(blinker, 3000);
</script>
<!-- 만기일이 31일 이내면 빨간색 표시 31일 이전 이면 빨간색 짝대기 표시 그외 미동없음. -->
			  <td  align="center" onClick="viewHistory('insure1','1590')" style="cursor:hand;color:red"><strike></strike></td>
			  			  <td align="center" onClick="viewHistory('insure1','1590')" style="cursor:hand;"></td>

 <!-- 적재물보험 //-->
         <td align="center" onClick="viewHistory('insure2','1590')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('insure2','1590')" style="cursor:hand;"></td>
				  <td align="center" onClick="viewHistory('insure2','1590')" style="cursor:hand;color:red;"><strike></strike></td>
		          <td align="center" onClick="viewHistory('insure2','1590')" style="cursor:hand;"></td>

          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>
          <td align="center" style="cursor:hand;"></td>

          <td align="center"></td>
          <td align="center"></td>

		<!--자동차정밀(정기)검사-->
          <td align="center" onClick="viewHistory('tchk','1590')" style="cursor:hand;"></td>
          <td align="center" onClick="viewHistory('tchk','1590')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1590')" style="cursor:hand;color:red;"><strike></strike></td>
          <td align="center" onClick="viewHistory('dchk','1590')" style="cursor:hand;color:red;"><strike></strike></td>
		  <td align="center" style="text-align:left;padding-left:5px;">한국전력(대구)</td>
		  				<td align="center" style="text-align:left;padding-left:5px;">대구용달(최재진)</td>
		            <td align="center" style="text-align:left;padding-left:5px;">대구용달(최재진)</td>
          <td align="center" style="text-align:left;padding-left:5px;">최재진</td>
          <td align="center"><a href="javascript:goEdit('1590');" class="button red" title="수정"> 수정 </a><a href="javascript:goDelete('1590');" class="button red" title="삭제"> 삭제 </a></td>
		 </tr>
					</tbody>
					<tfoot>
        <tr bgcolor="#ffffff" onmouseover="showProp2(this, this.cells,true,'')" onmouseout="showProp2(this, this.cells,false,'#ffffff')" style="">
          <th height="25" align="center">합계 </th>
		  			<th align="center" onClick="viewHistory('info1','1590')" style="cursor:hand;"></th>
			<th align="center" onClick="viewHistory('info1','1590')" style="cursor:hand;"></th>
			<th align="center"></th>
			<th align="center"></th>
			<th align="center"></th>
			<th align="center"></th>
		  <!-- 자동차보험 //-->
          <th align="center" onClick="viewHistory('insure1','1590')" style="cursor:hand;"></th>
          <th align="center" onClick="viewHistory('insure1','1590')" style="cursor:hand;"></th>
			  <th  align="center" onClick="viewHistory('insure1','1590')" style="cursor:hand;color:red"><strike></strike></th>
			  			  <th align="center" onClick="viewHistory('insure1','1590')" style="cursor:hand;"></th>

 <!-- 적재물보험 //-->
         <th align="center" onClick="viewHistory('insure2','1590')" style="cursor:hand;"></th>
          <th align="center" onClick="viewHistory('insure2','1590')" style="cursor:hand;"></th>
				  <th align="center" onClick="viewHistory('insure2','1590')" style="cursor:hand;color:red;"><strike></strike></th>
		          <th align="center" onClick="viewHistory('insure2','1590')" style="cursor:hand;"></th>

          <th align="center" style="cursor:hand;"></th>
          <th align="center" style="cursor:hand;"></th>
          <th align="center" style="cursor:hand;"></th>
          <th align="center" style="cursor:hand;"></th>
          <th align="center" style="cursor:hand;"></th>

          <th align="center"></th>
          <th align="center"></th>

		<!--자동차정밀(정기)검사-->
          <th align="center" onClick="viewHistory('tchk','1590')" style="cursor:hand;"></th>
          <th align="center" onClick="viewHistory('tchk','1590')" style="cursor:hand;color:red;"><strike></strike></th>
          <th align="center" onClick="viewHistory('dchk','1590')" style="cursor:hand;color:red;"><strike></strike></th>
          <th align="center" onClick="viewHistory('dchk','1590')" style="cursor:hand;color:red;"><strike></strike></th>
		  <th align="center" style="text-align:left;padding-left:5px;"></th>
		  				<th align="center" style="text-align:left;padding-left:5px;"></th>
		            <th align="center" style="text-align:left;padding-left:5px;"></th>
          <th align="center" style="text-align:left;padding-left:5px;"></th>
          <th align="center"></th>
		 </tr>
					</tfoot>
</table>













</body>
</html>