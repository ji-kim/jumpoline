<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('agent_model');
    }

    public function agent_list()
    {
        $data['title'] = "에이전트 인사관리";
        $data['active'] = "1";
        $data['subview'] = $this->load->view('admin/agent/agent_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load

    }


}
