<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cowork extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('cowork_model');
        $this->load->model('utilities_model');
    }

    public function buildChild($parent, $menu)
    {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

	// 관리비 생성
    public function generate_mfee($df_month=NULL)
    {
        $data['df_month']	= $df_month;
        $data['title']		= "관리비 & 수수료 생성";

		$flag = $this->input->post('flag', TRUE);
        if (!empty($this->input->post('df_month', TRUE))) $data['df_month'] = $this->input->post('df_month', TRUE);

		//if(empty($data['df_month'])) exit;
        
		if (!empty($flag)) { // check employee id is empty or not
            $data['flag'] = 1;
            $data['calculate_progress'] = $this->input->post('calculate_progress', TRUE);
            $data['ws_co_id'] = $this->input->post('ws_co_id', TRUE);
            $data['co_code'] = $this->input->post('co_code', TRUE);
            $data['co_name'] = $this->input->post('co_name', TRUE);
            $data['ct_id'] = $this->input->post('ct_id', TRUE);
			if ($flag == '1') { // 초기화 & 생성
			
				//계약단위  
				if($data['calculate_progress'] == "through_contracts" && !empty($data['ct_id'])) {
					//기초 입력값 체크
					//if(empty($data['ct_id'])) exit;

					//기존 데이터 삭제
					$this->cowork_model->_table_name = 'tbl_delivery_fee'; //table name
					$this->cowork_model->db->where('df_month',$data['df_month']);
					$this->cowork_model->db->where('ct_id',$data['ct_id']);
					$all_mfee_list = $this->cowork_model->get();
					if (!empty($all_mfee_list)) {
						foreach ($all_mfee_list as $mfee_info) {
							//tbl_delivery_fee_add
							$sql = "DELETE FROM tbl_delivery_fee_add WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_fixmfee
							$sql = "DELETE FROM tbl_delivery_fee_fixmfee WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_gongje
							$sql = "DELETE FROM tbl_delivery_fee_gongje WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_insur
							$sql = "DELETE FROM tbl_delivery_fee_insur WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_refund_gongje
							$sql = "DELETE FROM tbl_delivery_fee_refund_gongje WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_levy
							$sql = "DELETE FROM tbl_delivery_fee_levy WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);

							$sql = "DELETE FROM tbl_delivery_fee_levy_log WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
						}
					}

					$sql = "DELETE FROM tbl_delivery_fee_added_items WHERE df_month='".$data['df_month']."' and ws_co_id = '".$data['ws_co_id']."'";
					$this->db->query($sql);
					
					$sql = "DELETE FROM tbl_delivery_fee WHERE ct_id='".$data['ct_id']."' and df_month='".$data['df_month']."'";
					$this->db->query($sql);

					//해당 파트너 리스팅
					$this->cowork_model->_table_name = 'tbl_scontract_co a, tbl_members dp, tbl_asset_truck tr, tbl_members co, tbl_contract ct'; //table name
					$this->cowork_model->db->select('a.*, dp.*, tr.car_1, tr.ws_co_id as ws_co_id, co.co_name as rco_name, co.dp_id as rco_id');
					$this->cowork_model->db->where("a.dp_id = dp.dp_id and dp.tr_id = tr.idx and ( dp.code = co.code and co.mb_type = 'customer') and ct.idx = '".$data['ct_id']."'")->where('a.ct_id',$data['ct_id']);
					$data['all_dp_list'] = $this->cowork_model->get();
					$data['total_count'] = count($data['all_dp_list']);

				} else if($data['calculate_progress'] == "through_coops" && $data['ws_co_id']) { //거래처단위

					//기초 입력값 체크
					//if(empty($data['ct_id'])) exit;

					//기존 데이터 삭제

					$this->cowork_model->_table_name = 'tbl_delivery_fee'; //table name
					$this->cowork_model->db->where('df_month',$data['df_month']);
					$this->cowork_model->db->where('gongje_req_co',$data['ws_co_id']);
					$all_mfee_list = $this->cowork_model->get();
					if (!empty($all_mfee_list)) {
						foreach ($all_mfee_list as $mfee_info) {
							//tbl_delivery_fee_add
							$sql = "DELETE FROM tbl_delivery_fee_add WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_fixmfee
							$sql = "DELETE FROM tbl_delivery_fee_fixmfee WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_gongje
							$sql = "DELETE FROM tbl_delivery_fee_gongje WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_insur
							$sql = "DELETE FROM tbl_delivery_fee_insur WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_refund_gongje
							$sql = "DELETE FROM tbl_delivery_fee_refund_gongje WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_levy
							$sql = "DELETE FROM tbl_delivery_fee_levy WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);

							$sql = "DELETE FROM tbl_delivery_fee_levy_log WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
						}
					}
					$sql = "DELETE FROM tbl_delivery_fee WHERE gongje_req_co='".$data['ws_co_id']."' and df_month='".$data['df_month']."'";
					$this->db->query($sql);

					//해당 파트너 리스팅
					$this->cowork_model->_table_name = ' tbl_members dp, tbl_members co'; //table name
					$this->cowork_model->db->select('distinct(dp.dp_id) , dp.*, tr.car_1, tr.ws_co_id as ws_co_id, co.co_name as rco_name, co.dp_id as rco_id');
					$this->cowork_model->db->join('tbl_asset_truck tr', 'tr.idx = dp.tr_id', 'left');
					$this->cowork_model->db->where(" ( dp.code = co.code and co.mb_type = 'customer') and tr.ws_co_id = '".$data['ws_co_id']."'");
					$data['all_dp_list'] = $this->cowork_model->get();
					$data['total_count'] = count($data['all_dp_list']);

					//수당
					$this->cowork_model->_table_name = 'tbl_dp_assign_item'; //table name
					$this->cowork_model->db->where('tbl_dp_assign_item.type', 'sudang');
					$this->cowork_model->db->where('tbl_dp_assign_item.ws_co_id', $data['ws_co_id']);
					$this->cowork_model->_order_by = 'item';
					$data['all_sudang_group'] = $this->cowork_model->get();
					//공제
					$this->cowork_model->_table_name = 'tbl_dp_assign_item'; //table name
					$this->cowork_model->db->where('tbl_dp_assign_item.type', 'gongje');
					$this->cowork_model->db->where('tbl_dp_assign_item.ws_co_id', $data['ws_co_id']);
					$this->cowork_model->_order_by = 'item';
					$data['all_gongje_group'] = $this->cowork_model->get();

				}

			} else if ($flag == '2') { // 초기화
				//계약단위 ? 거래처단위
				if($data['calculate_progress'] == "through_contracts" && !empty($data['ct_id'])) {
					//기존 데이터 삭제
					$this->cowork_model->_table_name = 'tbl_delivery_fee'; //table name
					$this->cowork_model->db->where('df_month',$data['df_month']);
					if(!empty($data['ct_id'])) $this->cowork_model->db->where('ct_id',$data['ct_id']);
					$all_mfee_list = $this->cowork_model->get();
					if (!empty($all_mfee_list)) {
						foreach ($all_mfee_list as $mfee_info) {
							//tbl_delivery_fee_add
							$sql = "DELETE FROM tbl_delivery_fee_add WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_fixmfee
							$sql = "DELETE FROM tbl_delivery_fee_fixmfee WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_gongje
							$sql = "DELETE FROM tbl_delivery_fee_gongje WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_insur
							$sql = "DELETE FROM tbl_delivery_fee_insur WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_refund_gongje
							$sql = "DELETE FROM tbl_delivery_fee_refund_gongje WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
							//tbl_delivery_fee_levy
							$sql = "DELETE FROM tbl_delivery_fee_levy WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);

							$sql = "DELETE FROM tbl_delivery_fee_levy_log WHERE df_id='".$mfee_info->df_id."'";
							$this->db->query($sql);
						}
					}
					$sql = "DELETE FROM tbl_delivery_fee WHERE ct_id='".$data['ct_id']."' and df_month='".$data['df_month']."'";
					$this->db->query($sql);
				} else if($data['calculate_progress'] == "through_coops") {

				}
			}
		}
			//

/*
			//기존데이터 삭제
			if (!empty($df_month) && !empty($ct_id)) {
	            $cwhere = array('df_month' => $df_month,'ct_id' => $ct_id);
                $this->cowork_model->_table_name = 'tbl_delivery_fee';
                $this->cowork_model->delete_multiple($cwhere);
			}

            // get all designation info by Department id
            $this->cowork_model->_table_name = 'tbl_designations';
            $this->cowork_model->_order_by = 'designations_id';
            $designation_info = $this->cowork_model->get_by(array('departments_id' => $data['departments_id']), FALSE);
            if (!empty($designation_info)) {
                foreach ($designation_info as $v_designatio) {
                    $data['employee_info'][] = $this->cowork_model->get_emp_salary_list('', $v_designatio->designations_id);
                    $employee_info = $this->cowork_model->get_emp_salary_list('', $v_designatio->designations_id);
                    foreach ($employee_info as $value) {
                        // get all allowance info by salary template id
                        if (!empty($value->salary_template_id)) {
                            $data['allowance_info'][$value->user_id] = $this->get_allowance_info_by_id($value->salary_template_id);
                            // get all deduction info by salary template id
                            $data['deduction_info'][$value->user_id] = $this->get_deduction_info_by_id($value->salary_template_id);
                            // get all overtime info by month and employee id
                            $data['overtime_info'][$value->user_id] = $this->get_overtime_info_by_id($value->user_id, $data['payment_month']);
                        }
                        // get all advance salary info by month and employee id
                        $data['advance_salary'][$value->user_id] = $this->get_advance_salary_info_by_id($value->user_id, $data['payment_month']);
                        // get award info by employee id and payment month
                        $data['award_info'][$value->user_id] = $this->get_award_info_by_id($value->user_id, $data['payment_month']);
                        // check hourly payment info
                        // if exist count total hours in a month
                        // get hourly payment info by id
                        if (!empty($value->hourly_rate_id)) {
                            $data['total_hours'][$value->user_id] = $this->get_total_hours_in_month($value->user_id, $data['payment_month']);
                        }
                    }
                }
            }
*/

		// get contract list
        $this->cowork_model->_table_name = 'tbl_contract';
        $this->cowork_model->_order_by = 'idx';
        $this->cowork_model->db->where('tbl_contract.ct_status <>', '계약만료');
        $data['all_contract_info'] = $this->cowork_model->get();

		// get all customer list
        $this->cowork_model->_table_name = 'tbl_members';
        $this->cowork_model->_order_by = 'co_name';
        $this->cowork_model->db->where('tbl_members.mb_type', 'customer');
        $data['all_customer_info'] = $this->cowork_model->get();

        $data['subview'] = $this->load->view('admin/cowork/generate_mfee', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }


    public function set_rgongje($id,$dp_id)
    {
        $data['assign_user'] = $this->cowork_model->allowad_user('57');
        $data['df_info'] = $this->cowork_model->check_by(array('df_id' => $id), 'tbl_delivery_fee');
        $data['rgongje_info'] = $this->cowork_model->check_by(array('dp_id' => $dp_id,'master_yn' => 'Y'), 'tbl_delivery_fee_refund_gongje_set');
        
		$this->cowork_model->_table_name = 'tbl_delivery_fee_added_items'; //table name
		$this->cowork_model->db->where('df_month', $df_month)->where('ws_co_id', $ws_co_id)->where('add_type', 'GR');
		$this->cowork_model->_order_by = 'title';
		$data['all_rgongje_group'] = $this->cowork_model->get();
		
		$data['modal_subview'] = $this->load->view('admin/cowork/_modal_rgongje', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_rgongje($df_month = null, $gongje_req_co = null)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $df_id = $this->input->post('df_id', true);
            $df_month = $this->input->post('df_month', true);
            $gongje_req_co = $this->input->post('gongje_req_co', true);
            $dp_id = $this->input->post('dp_id', true);
            $ct_id = $this->input->post('ct_id', true);

            $this->cowork_model->_table_name = 'tbl_delivery_fee_refund_gongje'; //table name
            $result = $this->cowork_model->get_by(array('df_id' => $df_id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_delivery_fee_refund_gongje SET ";
				$sql .= "gj_termination_mortgage='".$this->input->post('gj_termination_mortgage', true)."'";
				$sql .= " WHERE df_id = '".$df_id."'";
				$this->db->query($sql);
				$exsql = $sql;
			} else {
				$sql = "INSERT INTO tbl_delivery_fee_refund_gongje SET ";
				$sql .= "dp_id='".$dp_id."'";
				$sql .= ",df_id='".$df_id."'";
				$sql .= ",df_month='".$df_month."'";
				$sql .= ",ws_co_id='".$gongje_req_co."'";
				$sql .= ",ct_id='".$ct_id."'";
				$sql .= ",gj_termination_mortgage='".$this->input->post('gj_termination_mortgage', true)."'";
				$this->db->query($sql);
			}

			$sql = "UPDATE tbl_delivery_fee_refund_gongje_set SET master_yn = 'N' WHERE dp_id = '".$dp_id."'";
			$this->db->query($sql);

			$sql = "INSERT INTO tbl_delivery_fee_refund_gongje_set SET ";
			$sql .= "dp_id='".$dp_id."'";
			$sql .= ",master_yn='Y'";
			$sql .= ",ct_id='".$ct_id."'";
			$sql .= ",gj_termination_mortgage='".$this->input->post('gj_termination_mortgage', true)."'";
			$this->db->query($sql);

			$message = "환급형공제가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co.'/'.$exsql); //redirect page
    }

    public function set_ngongje($id,$dp_id,$df_month,$ws_co_id)
    {
        $data['df_month'] = $df_month;
        $data['dp_id'] = $dp_id;
        $data['assign_user'] = $this->cowork_model->allowad_user('57');
        $data['df_info'] = $this->cowork_model->check_by(array('df_id' => $id), 'tbl_delivery_fee');
        $data['ngongje_info'] = $this->cowork_model->check_by(array('dp_id' => $dp_id,'master_yn' => 'Y'), 'tbl_delivery_fee_gongje_set');

		$this->cowork_model->_table_name = 'tbl_delivery_fee_added_items'; //table name
		$this->cowork_model->db->where('df_month', $df_month)->where('ws_co_id', $ws_co_id)->where('add_type', 'GN');
		$this->cowork_model->_order_by = 'title';
		$data['all_ngongje_group'] = $this->cowork_model->get();


        $data['modal_subview'] = $this->load->view('admin/cowork/_modal_ngongje', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_ngongje($df_month = null, $gongje_req_co = null)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $df_id = $this->input->post('df_id', true);
            $df_month = $this->input->post('df_month', true);
            $gongje_req_co = $this->input->post('gongje_req_co', true);
            $dp_id = $this->input->post('dp_id', true);
            $ct_id = $this->input->post('ct_id', true);

			//과태료 첨부파일
			$sql_file = "";
            if (!empty($_FILES['fine']['name'])) {
                $old_path = $this->input->post('fine_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->cowork_model->uploadAllType('fine','asset/fine/');
                $val == TRUE || redirect('admin/cowork/jiip_gongje/');
                $fine_data['fine_filename'] = $val['fileName'];
                $fine_data['fine'] = $val['path'];
                $fine_data['fine_path'] = $val['fullPath'];
				$sql_file .= ",fine_filename='".$fine_data['fine_filename']."'";
				$sql_file .= ",fine='".$fine_data['fine']."'";
				$sql_file .= ",fine_path='".$fine_data['fine_path']."'";
            }

            $this->cowork_model->_table_name = 'tbl_delivery_fee_gongje'; //table name
            $result = $this->cowork_model->get_by(array('df_id' => $df_id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_delivery_fee_gongje SET ";
				$sql .= "car_tax='".$this->input->post('car_tax', true)."'";
				$sql .= ",env_fee='".$this->input->post('env_fee', true)."'";
				$sql .= ",fine_fee='".$this->input->post('fine_fee', true)."'";
				$sql .= ",fine_fee_memo='".$this->input->post('fine_fee_memo', true)."'";
				$sql .= $sql_file;
				$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
				$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
				$sql .= " WHERE df_id = '".$df_id."'";
				$this->db->query($sql);
				$exsql = $sql;
			} else {
				$sql = "INSERT INTO tbl_delivery_fee_gongje SET ";
				$sql .= "dp_id='".$dp_id."'";
				$sql .= ",df_id='".$df_id."'";
				$sql .= ",df_month='".$df_month."'";
				$sql .= ",ws_co_id='".$gongje_req_co."'";
				$sql .= ",ct_id='".$ct_id."'";
				$sql .= ",car_tax='".$this->input->post('car_tax', true)."'";
				$sql .= ",env_fee='".$this->input->post('env_fee', true)."'";
				$sql .= ",fine_fee='".$this->input->post('fine_fee', true)."'";
				$sql .= ",fine_fee_memo='".$this->input->post('fine_fee_memo', true)."'";
				$sql .= $sql_file;
				$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
				$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
				$this->db->query($sql);
			}

			$sql = "UPDATE tbl_delivery_fee_gongje_set SET master_yn = 'N' WHERE dp_id = '".$dp_id."'";
			$this->db->query($sql);

			$sql = "INSERT INTO tbl_delivery_fee_gongje_set SET ";
			$sql .= "dp_id='".$dp_id."'";
			$sql .= ",master_yn='Y'";
			$sql .= ",ct_id='".$ct_id."'";
			$sql .= ",fine_fee='".$this->input->post('fine_fee', true)."'";
			$sql .= ",fine_fee_memo='".$this->input->post('fine_fee_memo', true)."'";
			$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
			$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
			$this->db->query($sql);

			//추가 항목 저장
			for ($i=0; $i<count($this->input->post('chk[]', true)); $i++) 
			{
				$k = $this->input->post('chk['.$i.']', true);
				$item_pidx = $this->input->post('item_pidx['.$i.']', true);
				$item_idx = $this->input->post('item_idx['.$i.']', true);
				$item_amount = $this->input->post('item_amount['.$i.']', true);
				$memo = $this->input->post('memo['.$i.']', true);

				if(!empty($item_idx)) $chk = $this->db->where('idx', $item_idx)->get('tbl_delivery_fee_add')->row();
				if(!empty($item_pidx)) $pitem = $this->db->where('idx', $item_pidx)->get('tbl_delivery_fee_added_items')->row();
				if(!empty($chk->idx)) {
					$sql = "UPDATE tbl_delivery_fee_add SET pid='$item_pidx',df_id='$df_id',add_type='".$pitem->add_type."',df_month='$df_month',dp_id='$dp_id',amount='$item_amount',memo='$memo',ws_co_id='$gongje_req_co',apply_yn='Y' where idx='".$chk->idx."'";
				} else {
					if(!empty($item_pidx)) {
						$sql = "INSERT INTO tbl_delivery_fee_add SET pid='$item_pidx',df_id='$df_id',add_type='".$pitem->add_type."',df_month='$df_month',dp_id='$dp_id',title='".$pitem->title."',amount='$item_amount',memo='$memo',ws_co_id='$gongje_req_co',apply_yn='Y'";
					}
				}
				$this->db->query($sql);
			}


			$message = "공제가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co.'/'.$exsql); //redirect page
    }

	public function set_mfee($df_id, $dp_id, $df_month, $ws_co_id)
    {
        $data['assign_user'] = $this->cowork_model->allowad_user('57');
        //$data['df_info'] = $this->cowork_model->check_by(array('df_id' => $df_id), 'tbl_delivery_fee');
$data['df_info'] = $this->db->where('df_id', $df_id)->get('tbl_delivery_fee')->row();
        $data['mfee_info'] = $this->cowork_model->check_by(array('dp_id' => $dp_id,'master_yn' => 'Y'), 'tbl_delivery_fee_fixmfee_set');
        
		$this->cowork_model->_table_name = 'tbl_delivery_fee_added_items'; //table name
		$this->cowork_model->db->where('df_month', $df_month)->where('ws_co_id', $ws_co_id)->where('add_type', 'GW');
		$this->cowork_model->_order_by = 'title';
		$data['all_wgongje_group'] = $this->cowork_model->get();

		$data['modal_subview'] = $this->load->view('admin/cowork/_modal_mfee', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_mfee($df_month = null, $gongje_req_co = null)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $df_id = $this->input->post('df_id', true);
            $df_month = $this->input->post('df_month', true);
            $gongje_req_co = $this->input->post('gongje_req_co', true);
            $dp_id = $this->input->post('dp_id', true);
            $ct_id = $this->input->post('ct_id', true);

            $this->cowork_model->_table_name = 'tbl_delivery_fee_fixmfee'; //table name
            $result = $this->cowork_model->get_by(array('df_id' => $df_id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_delivery_fee_fixmfee SET ";
				$sql .= "wst_mfee='".$this->input->post('wst_mfee', true)."'";
				$sql .= ",mfee_vat='".$this->input->post('mfee_vat', true)."'";
				$sql .= ",org_fee='".$this->input->post('org_fee', true)."'";
				//$sql .= ",org_fee_vat='".$this->input->post('org_fee_vat', true)."'";
				//$sql .= ",env_fee='".$this->input->post('env_fee', true)."'";
				//$sql .= ",env_fee_vat='".$this->input->post('env_fee_vat', true)."'";
				//$sql .= ",car_tax='".$this->input->post('car_tax', true)."'";
				//$sql .= ",car_tax_vat='".$this->input->post('car_tax_vat', true)."'";
				$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
				$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
				//$sql .= ",etc='".$this->input->post('etc', true)."'";
				$sql .= " WHERE df_id = '".$df_id."'";
				$this->db->query($sql);
				$exsql = $sql;

				$sql = "UPDATE tbl_delivery_fee_fixmfee_set SET master_yn = 'N' WHERE dp_id = '".$dp_id."'";
				$this->db->query($sql);

				$sql = "INSERT INTO tbl_delivery_fee_fixmfee_set SET ";
				$sql .= "dp_id='".$dp_id."'";
				$sql .= ",master_yn='Y'";
				$sql .= ",ct_id='".$ct_id."'";
				$sql .= ",wst_mfee='".$this->input->post('wst_mfee', true)."'";
				$sql .= ",mfee_vat='".$this->input->post('mfee_vat', true)."'";
				$sql .= ",org_fee='".$this->input->post('org_fee', true)."'";
				//$sql .= ",org_fee_vat='".$this->input->post('org_fee_vat', true)."'";
				//$sql .= ",env_fee='".$this->input->post('env_fee', true)."'";
				//$sql .= ",env_fee_vat='".$this->input->post('env_fee_vat', true)."'";
				//$sql .= ",car_tax='".$this->input->post('car_tax', true)."'";
				//$sql .= ",car_tax_vat='".$this->input->post('car_tax_vat', true)."'";
				$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
				$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
				//$sql .= ",etc='".$this->input->post('etc', true)."'";
				$this->db->query($sql);
			} 
			/*
			else {
				$sql = "INSERT INTO tbl_delivery_fee_fixmfee SET ";
				$sql .= "df_id='".$df_id."'";
				$sql .= ",ws_co_id ='".$gongje_req_co."'";
				$sql .= ",df_month ='".$df_month."'";
				$sql .= ",dp_id='".$dp_id."'";
				$sql .= ",ct_id='".$ct_id."'";
				$sql .= ",wst_mfee='".$this->input->post('wst_mfee', true)."'";
				$sql .= ",mfee_vat='".$this->input->post('mfee_vat', true)."'";
				$sql .= ",org_fee='".$this->input->post('org_fee', true)."'";
				$sql .= ",org_fee_vat='".$this->input->post('org_fee_vat', true)."'";
				$sql .= ",env_fee='".$this->input->post('env_fee', true)."'";
				$sql .= ",env_fee_vat='".$this->input->post('env_fee_vat', true)."'";
				$sql .= ",car_tax='".$this->input->post('car_tax', true)."'";
				$sql .= ",car_tax_vat='".$this->input->post('car_tax_vat', true)."'";
				$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
				$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
				$sql .= ",etc='".$this->input->post('etc', true)."'";
				$this->db->query($sql);
			}
			*/

			$message = "관리비가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co.'/'.$exsql); //redirect page
    }

    public function set_misu($id)
    {
        $data['assign_user'] = $this->cowork_model->allowad_user('57');
        $data['mfee_info'] = $this->cowork_model->check_by(array('df_id' => $id), 'tbl_delivery_fee');
        $data['modal_subview'] = $this->load->view('admin/cowork/_modal_misu', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_misu($id = null)
    {
        $df_id = $this->input->post('df_id', true);
        $df_month = $this->input->post('df_month', true);
        $gongje_req_co = $this->input->post('gongje_req_co', true);

		$created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $df_data['remark'] = $this->input->post('remark', true);
            $this->cowork_model->_table_name = 'tbl_delivery_fee';
            $this->cowork_model->_primary_key = 'df_id';
            $id = $this->cowork_model->save($df_data, $df_id);

            $this->cowork_model->_table_name = 'tbl_delivery_fee_gongje'; //table name
            $result = $this->cowork_model->get_by(array('df_id' => $df_id), true);
            if (!empty($result)) {
				$sql = "UPDATE tbl_delivery_fee_gongje SET ";
				$sql .= "not_paid='".$this->input->post('not_paid', true)."'";
				$sql .= " WHERE df_id = '".$df_id."'";
				$this->db->query($sql);
				$exsql = $sql;
			} else {
				$sql = "INSERT INTO tbl_delivery_fee_gongje SET ";
				$sql .= "df_id='".$df_id."'";
				$sql .= ",not_paid='".$this->input->post('not_paid', true)."'";
				$this->db->query($sql);
			}

			$message = "초기미수액이 수정되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

    public function set_car_insur($id)
    {
        $data['assign_user'] = $this->cowork_model->allowad_user('57');
        $data['car_info'] = $this->cowork_model->check_by(array('idx' => $id), 'tbl_asset_truck');
        $data['modal_subview'] = $this->load->view('admin/cowork/_modal_set_car', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function close_mfee_all($df_month = NULL, $gongje_req_co = NULL, $is_closed = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
			$sql = "UPDATE tbl_delivery_fee SET is_closed='".$is_closed."' WHERE df_month = ?";
			$this->db->query($sql, array($df_month));
			$message = "처리 되었습니다.";
            $type = 'success';
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

    public function close_mfee($df_month = NULL, $gongje_req_co = NULL, $is_closed = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
			$sql = "UPDATE tbl_delivery_fee SET is_closed='".$is_closed."' WHERE df_month = ? AND gongje_req_co = ?";
			$this->db->query($sql, array($df_month, $gongje_req_co));
			$message = "처리 되었습니다.";
            $type = 'success';
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

    public function close_mfee_id($df_month = NULL, $gongje_req_co = NULL, $id = NULL, $is_closed = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
			$sql = "UPDATE tbl_delivery_fee SET is_closed='".$is_closed."' WHERE df_id = ?";
			$this->db->query($sql, array($id));
			$message = "처리 되었습니다.";
            $type = 'success';
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

    public function close_payment($df_month = NULL, $gongje_req_co = NULL, $id = NULL, $is_closed = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
			$sql = "UPDATE tbl_delivery_fee SET is_pay_closed='".$is_closed."' WHERE df_id = ?";
			$this->db->query($sql, array($id));
			$message = "처리 되었습니다.";
            $type = 'success';
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

	// 위수탁관리비 출력
    public function jiip_gongje($df_month = NULL, $gongje_req_co = NULL, $action = NULL, $id = NULL)
    {
        $data['single_df_id'] = $id;
        //receive form input by post
		if (!empty($gongje_req_co)) $data['gongje_req_co'] = $gongje_req_co;
        else $data['gongje_req_co'] = $this->input->post('gongje_req_co', true);
        if (empty($data['gongje_req_co'])) $data['gongje_req_co'] = "1413";
        $data['gongje_req_co_name'] = $this->input->post('gongje_req_co_name', true);
        if (empty($data['gongje_req_co_name'])) $data['gongje_req_co_name'] = "케이티지엘에스(주)";
        $data['ws_mode'] = $this->input->post('ws_mode', true);
        $data['close_yn'] = $this->input->post('close_yn', true);
		$data['search_keyword'] = $this->input->post('search_keyword', true);

        if (!empty($df_month)) $data['df_month'] = $df_month;
        else $data['df_month'] = $this->input->post('df_month', true);
        if (empty($data['df_month'])) $data['df_month'] = date('Y-m');

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_member', 'edit', array('asset_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('member_id', $asset_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }

	    $data['title'] = '지입공제내역';

		$this->cowork_model->_table_name = ' tbl_delivery_fee df, tbl_members pt, tbl_asset_truck tr'; //table name
		$this->cowork_model->db->select('df.*, pt.co_name, pt.ceo, pt.bs_number, pt.N, pt.O, pt.ws_co_id, pt.driver, tr.car_1');
		$this->cowork_model->db->where(" ( df.dp_id = pt.dp_id and df.tr_id = tr.idx) ");
        $this->cowork_model->db->where('df.df_month', $data['df_month']);
		if($data['gongje_req_co'] == "etc") {
			$this->cowork_model->db->where_not_in('df.gongje_req_co', '1413')->where_not_in('df.gongje_req_co', '1414')->where_not_in('df.gongje_req_co', '1415')->where_not_in('df.gongje_req_co', '1903');
		} else {
			$this->cowork_model->db->where('df.gongje_req_co', $data['gongje_req_co']);
		}
		if(!empty($data['search_keyword'])) {
			$this->cowork_model->db->where("(pt.co_name like '%".$data['search_keyword']."%' OR pt.ceo like '%".$data['search_keyword']."%' OR pt.reg_number like '%".$data['search_keyword']."%' OR pt.bs_number like '%".$data['search_keyword']."%' OR pt.driver like '%".$data['search_keyword']."%' )");
		}
        $this->cowork_model->_order_by = 'df.C';
        $data['all_delivery_fee_info'] = $this->cowork_model->get();
        $data['total_count'] = count($data['all_delivery_fee_info']);

		if(!empty($data['gongje_req_co'])) {
			//수당
			$this->cowork_model->_table_name = 'tbl_delivery_fee_added_items'; //table name
			$this->cowork_model->db->where('df_month', $data['df_month'])->where('ws_co_id', $data['gongje_req_co'])->where('add_type', 'S');
			$this->cowork_model->_order_by = 'title';
			$data['all_sudang_group'] = $this->cowork_model->get();
			
			//공제
			$this->cowork_model->_table_name = 'tbl_delivery_fee_added_items'; //table name
			$this->cowork_model->db->where('df_month', $data['df_month'])->where('ws_co_id', $data['gongje_req_co'])->where('add_type<>', 'S');
			$this->cowork_model->_order_by = 'title';
			$data['all_gongje_group'] = $this->cowork_model->get();
		}

        if ($action == 'print') {
			$data['subview'] = $this->load->view('admin/cowork/jiip_gongje_print', $data, true);
			$this->load->view('admin/_layout_print', $data);
        } else if ($action == 'excel') {
			$data['subview'] = $this->load->view('admin/cowork/jiip_gongje_excel', $data, true);
			$this->load->view('admin/_layout_excel', $data);
        } else if ($action == 'excel_list') {
			$data['subview'] = $this->load->view('admin/cowork/jiip_gongje_list_excel', $data, true);
			$this->load->view('admin/_layout_excel', $data);
		} else {
			$data['subview'] = $this->load->view('admin/cowork/jiip_gongje', $data, true);
			$this->load->view('admin/_layout_main', $data);
		}
	}
	
    public function jiip_gongje_all($df_month = NULL, $gongje_req_co = NULL, $action = NULL, $id = NULL, $page = 1)
    {
		$data['page'] = $page;
        //receive form input by post
		if (!empty($gongje_req_co)) $data['gongje_req_co'] = $gongje_req_co;
        else $data['gongje_req_co'] = $this->input->post('gongje_req_co', true);
        if (empty($data['gongje_req_co'])) $data['gongje_req_co'] = "1413";
        $data['gongje_req_co_name'] = $this->input->post('gongje_req_co_name', true);
		$data['search_field'] = $this->input->post('search_field', true);
		$data['search_keyword'] = $this->input->post('search_keyword', true);
        $data['ws_mode'] = $this->input->post('ws_mode', true);
        $data['close_yn'] = $this->input->post('close_yn', true);

        if (!empty($df_month)) $data['df_month'] = $df_month;
        else $data['df_month'] = $this->input->post('df_month', true);
        if (empty($data['df_month'])) $data['df_month'] = date('Y-m');

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->cowork_model->can_action('tbl_member', 'edit', array('asset_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('member_id', $asset_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }
		
		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($limit)) $limit = 20;

		$this->cowork_model->_table_name = ' tbl_delivery_fee df, tbl_members pt, tbl_asset_truck tr'; //table name
		$this->cowork_model->db->select('df.*');
		$this->cowork_model->db->where(" ( df.dp_id = pt.dp_id and df.tr_id = tr.idx) ");
        $this->cowork_model->db->where('df.df_month', $data['df_month']);
		if(!empty($data['search_keyword'])) {
			$this->cowork_model->db->where("(pt.co_name like '%".$data['search_keyword']."%' OR pt.ceo like '%".$data['search_keyword']."%' OR pt.reg_number like '%".$data['search_keyword']."%' OR pt.bs_number like '%".$data['search_keyword']."%' OR pt.driver like '%".$data['search_keyword']."%' OR df.D like '%".$data['search_keyword']."%' OR tr.car_1 like '%".$data['search_keyword']."%' )");
		}
		$data['total_count'] = count($this->cowork_model->get());

		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함

	    $data['title'] = '지입공제내역';

		$this->cowork_model->_table_name = ' tbl_delivery_fee df, tbl_members pt, tbl_asset_truck tr'; //table name
		$this->cowork_model->db->select('df.*, pt.co_name, pt.ceo, pt.bs_number, pt.N, pt.O, pt.ws_co_id, pt.driver, tr.car_1');
		$this->cowork_model->db->where(" ( df.dp_id = pt.dp_id and df.tr_id = tr.idx) ");
        $this->cowork_model->db->where('df.df_month', $data['df_month']);
		if(!empty($data['search_keyword'])) { //D
			$this->cowork_model->db->where("(pt.co_name like '%".$data['search_keyword']."%' OR pt.ceo like '%".$data['search_keyword']."%' OR pt.reg_number like '%".$data['search_keyword']."%' OR pt.bs_number like '%".$data['search_keyword']."%' OR pt.driver like '%".$data['search_keyword']."%' OR df.D like '%".$data['search_keyword']."%' OR tr.car_1 like '%".$data['search_keyword']."%' )");
		}
        $this->cowork_model->_order_by = 'df.C';

		$this->cowork_model->db->limit($limit, $data['from_record']);
		$data['all_delivery_fee_info'] = $this->cowork_model->get();

        $data['subview'] = $this->load->view('admin/cowork/jiip_gongje_all', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}
	
	//거래등록현황
    public function jiip_logis($df_month = NULL, $gongje_req_co = NULL, $action = NULL, $id = NULL)
    {
        $member_id = $id;
        //receive form input by post
        $data['gongje_req_co'] = $this->input->post('gongje_req_co', true);
        if (empty($data['gongje_req_co'])) $data['gongje_req_co'] = "1413";

        $data['df_month'] = $this->input->post('df_month', true);
        if (empty($data['df_month'])) $data['df_month'] = date('Y-m');

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_member', 'edit', array('asset_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('member_id', $asset_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }

	    $data['title'] = '지입공제내역';

        $this->cowork_model->_table_name = 'tbl_delivery_fee'; //table name
        $this->cowork_model->_order_by = 'C';
        $this->cowork_model->db->where('tbl_delivery_fee.df_month', '2018-09');
        $this->cowork_model->db->where('tbl_delivery_fee.gongje_req_co', $data['gongje_req_co']);

/*
		$this->cowork_model->db->select('tbl_delivery_fee.*', false);
        $this->cowork_model->db->select('tbl_members.*', false);
        $this->cowork_model->db->from('tbl_user_role');
        $this->cowork_model->db->join('tbl_menu', 'tbl_user_role.menu_id = tbl_menu.menu_id', 'left');
        $this->cowork_model->db->where('tbl_user_role.user_id', $user_id);
*/
        $data['all_delivery_fee_info'] = $this->cowork_model->get();

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->basic_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->basic_model->get_permission('tbl_member');

        $data['all_designation_info'] = $this->basic_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/cowork/jiip_logis', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

    public function jiip_transfer($action = NULL, $id = NULL)
    {
        $data['title'] = '지입송금완료';
          $data['active'] = 1;
        $data['subview'] = $this->load->view('admin/cowork/jiip_transfer', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

    public function mfee_reg($action = NULL, $id = NULL)
    {
        $data['title'] = '지입송금완료';
          $data['active'] = 1;
        $data['subview'] = $this->load->view('admin/cowork/mfee_reg', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

    public function jiip_gongje_rq($action = NULL, $id = NULL)
    {
        $data['title'] = '지입료공제입금요청';
          $data['active'] = 1;
        $data['subview'] = $this->load->view('admin/cowork/jiip_gongje_rq', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

    public function jiip_pay_finish($action = NULL, $id = NULL)
    {
        $data['title'] = '지입료수납완료';
          $data['active'] = 1;
        $data['subview'] = $this->load->view('admin/cowork/jiip_pay_finish', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

    public function cowork_list($action = NULL, $id = NULL)
    {

        $cowork_id = $id;

        if ($action == 'edit_cowork') {
            $data['active'] = 2;
            $can_edit = $this->cowork_model->can_action('tbl_coworks', 'edit', array('cowork_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('cowork_id', $cowork_id)->get('tbl_coworks')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'cowork List';

        $this->cowork_model->_table_name = 'tbl_client'; //table name
        $this->cowork_model->_order_by = 'client_id';
        $data['all_client_info'] = $this->cowork_model->get();

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_cowork'] = $this->cowork_model->all_permission_cowork('24');
        $data['all_cowork_info'] = $this->cowork_model->get_permission('tbl_coworks');

        $data['all_designation_info'] = $this->cowork_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/cowork/cowork_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function cowork_details($id, $active = null)
    {
        $data['title'] = lang('cowork_details');
        $data['id'] = $id;
        if (!empty($active)) {
            $data['active'] = $active;
        } else {
            $data['active'] = 1;
        }
        $date = $this->input->post('date', true);
        if (!empty($date)) {
            $data['date'] = $date;
        } else {
            $data['date'] = date('Y-m');
        }
        $data['attendace_info'] = $this->get_report($id, $data['date']);

        $data['my_leave_report'] = leave_report($id);

        //
        if ($this->input->post('year', TRUE)) { // if input year
            $data['year'] = $this->input->post('year', TRUE);
        } else { // else current year
            $data['year'] = date('Y'); // get current year
        }
        // get all expense list by year and month
        $data['provident_fund_info'] = $this->get_provident_fund_info($data['year'], $id);

        if ($this->input->post('overtime_year', TRUE)) { // if input year
            $data['overtime_year'] = $this->input->post('overtime_year', TRUE);
        } else { // else current year
            $data['overtime_year'] = date('Y'); // get current year
        }
        // get all expense list by year and month
        $data['all_overtime_info'] = $this->get_overtime_info($data['overtime_year'], $id);
        $data['profile_info'] = $this->db->where('cowork_id', $id)->get('tbl_account_details')->row();

        $data['total_attendance'] = count($this->total_attendace_in_month($id));

        $data['total_absent'] = count($this->total_attendace_in_month($id, 'absent'));

        $data['total_leave'] = count($this->total_attendace_in_month($id, 'leave'));
        //award received
        $data['total_award'] = count($this->db->where('cowork_id', $id)->get('tbl_employee_award')->result());

        // get working days holiday
        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday

        $num = cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y'));
        $working_holiday = 0;
        for ($i = 1; $i <= $num; $i++) {
            $day_name = date('l', strtotime("+0 days", strtotime(date('Y') . '-' . date('n') . '-' . $i)));

            if (!empty($holidays)) {
                foreach ($holidays as $v_holiday) {
                    if ($v_holiday->day == $day_name) {
                        $working_holiday += count($day_name);
                    }
                }
            }
        }
        // get public holiday
        $public_holiday = count($this->total_attendace_in_month($id, TRUE));

        // get total days in a month
        $month = date('m');
        $year = date('Y');
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        // total attend days in a month without public holiday and working days
        $data['total_days'] = $days - $working_holiday - $public_holiday;

        $data['all_working_hour'] = $this->all_attendance_id_by_date($id, true);

        $data['this_month_working_hour'] = $this->all_attendance_id_by_date($id);

        $data['subview'] = $this->load->view('admin/cowork/cowork_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function all_attendance_id_by_date($cowork_id, $flag = null)
    {
        if (!empty($flag)) {
            $get_total_attendance = $this->db->where(array('cowork_id' => $cowork_id, 'attendance_status' => '1'))->get('tbl_attendance')->result();
            if (!empty($get_total_attendance)) {
                foreach ($get_total_attendance as $v_attendance_id) {
                    $aresult[] = $this->global_model->get_total_working_hours($v_attendance_id->attendance_id);
                }
                return $aresult;
            }
        } else {

            $month = date('n');
            $year = date('Y');
            if ($month >= 1 && $month <= 9) {
                $yymm = $year . '-' . '0' . $month;
            } else {
                $yymm = $year . '-' . $month;
            }
            $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            for ($i = 1; $i <= $num; $i++) {
                if ($i >= 1 && $i <= 9) {
                    $sdate = $yymm . '-' . '0' . $i;
                } else {
                    $sdate = $yymm . '-' . $i;
                }
                $get_total_attendance = $this->global_model->get_total_attendace_by_date($sdate, $sdate, $cowork_id); // get all attendace by start date and in date
                if (!empty($get_total_attendance)) {
                    foreach ($get_total_attendance as $v_attendance_id) {
                        $result[] = $this->global_model->get_total_working_hours($v_attendance_id->attendance_id);
                    }
                }
            }
            if (!empty($result)) {
                return $result;
            }
        }
    }

    public function total_attendace_in_month($cowork_id, $flag = NULL)
    {
        $month = date('m');
        $year = date('Y');

        if ($month >= 1 && $month <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
            $start_date = $year . "-" . '0' . $month . '-' . '01';
            $end_date = $year . "-" . '0' . $month . '-' . '31';
        } else {
            $start_date = $year . "-" . $month . '-' . '01';
            $end_date = $year . "-" . $month . '-' . '31';
        }
        if (!empty($flag) && $flag == 1) { // if flag is not empty that means get pulic holiday
            $get_public_holiday = $this->global_model->get_holiday_list_by_date($start_date, $end_date);

            if (!empty($get_public_holiday)) { // if not empty the public holiday
                foreach ($get_public_holiday as $v_holiday) {
                    if ($v_holiday->start_date == $v_holiday->end_date) { // if start date and end date is equal return one data
                        $total_holiday[] = $v_holiday->start_date;
                    } else { // if start date and end date not equan return all date
                        for ($j = $v_holiday->start_date; $j <= $v_holiday->end_date; $j++) {
                            $total_holiday[] = $j;
                        }
                    }
                }
                return $total_holiday;
            }
        } elseif (!empty($flag)) { // if flag is not empty that means get pulic holiday
            $get_total_absent = $this->global_model->get_total_attendace_by_date($start_date, $end_date, $cowork_id, $flag); // get all attendace by start date and in date
            return $get_total_absent;
        } else {
            $get_total_attendance = $this->global_model->get_total_attendace_by_date($start_date, $end_date, $cowork_id); // get all attendace by start date and in date
            return $get_total_attendance;
        }
    }

    public function get_overtime_info($year, $cowork_id)
    {// this function is to create get monthy recap report

        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $i . '-' . '01';
                $end_date = $year . "-" . '0' . $i . '-' . '31';
            } else {
                $start_date = $year . "-" . $i . '-' . '01';
                $end_date = $year . "-" . $i . '-' . '31';
            }
            $get_expense_list[$i] = $this->utilities_model->get_overtime_info_by_date($start_date, $end_date, $cowork_id); // get all report by start date and in date
        }

        return $get_expense_list; // return the result
    }

    public function overtime_report_pdf($year, $cowork_id)
    {
        $data['all_overtime_info'] = $this->get_overtime_info($year, $cowork_id);

        $data['monthyaer'] = $year;
        $data['cowork_info'] = $this->db->where('cowork_id', $cowork_id)->get('tbl_account_details')->row();
        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/cowork/overtime_report_pdf', $data, TRUE);
        pdf_create($viewfile, 'Overtime Report  - ' . $data['monthyaer']);
    }

    public function get_provident_fund_info($year, $cowork_id)
    {// this function is to create get monthy recap report

        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $i;
                $end_date = $year . "-" . '0' . $i;
            } else {
                $start_date = $year . "-" . $i;
                $end_date = $year . "-" . $i;
            }
            $provident_fund_info[$i] = $this->payroll_model->get_provident_fund_info_by_date($start_date, $end_date, $cowork_id); // get all report by start date and in date
        }

        return $provident_fund_info; // return the result
    }

    public function provident_fund_pdf($year, $cowork_id)
    {

        $data['provident_fund_info'] = $this->get_provident_fund_info($year, $cowork_id);
        $data['monthyaer'] = $year;

        $data['cowork_info'] = $this->db->where('cowork_id', $cowork_id)->get('tbl_account_details')->row();

        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/cowork/provident_fund_pdf', $data, TRUE);
        pdf_create($viewfile, lang('provident_found_report') . ' - ' . $data['monthyaer']);
    }

    public function timecard_details_pdf($id, $date)
    {
        $data['profile_info'] = $this->db->where('cowork_id', $id)->get('tbl_account_details')->row();
        $data['date'] = $date;
        $data['attendace_info'] = $this->get_report($id, $date);

        $viewfile = $this->load->view('admin/cowork/timecard_details_pdf', $data, TRUE);

        $this->load->helper('dompdf');
        pdf_create($viewfile, lang('timecard_details') . '- ' . $data['profile_info']->fullname);
    }

    public function get_report($cowork_id, $date)
    {
        $month = date('n', strtotime($date));
        $year = date('Y', strtotime($date));
        $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday

        if ($month >= 1 && $month <= 9) {
            $yymm = $year . '-' . '0' . $month;
        } else {
            $yymm = $year . '-' . $month;
        }

        $public_holiday = $this->global_model->get_public_holidays($yymm);

        //tbl a_calendar Days Holiday
        if (!empty($public_holiday)) {
            foreach ($public_holiday as $p_holiday) {
                $p_hday = $this->attendance_model->GetDays($p_holiday->start_date, $p_holiday->end_date);
            }
        }

        $key = 1;
        $x = 0;
        for ($i = 1; $i <= $num; $i++) {

            if ($i >= 1 && $i <= 9) {
                $sdate = $yymm . '-' . '0' . $i;
            } else {
                $sdate = $yymm . '-' . $i;
            }
            $day_name = date('l', strtotime("+$x days", strtotime($year . '-' . $month . '-' . $key)));

            $data['week_info'][date('W', strtotime($sdate))][$sdate] = $sdate;

            // get leave info
            if (!empty($holidays)) {
                foreach ($holidays as $v_holiday) {
                    if ($v_holiday->day == $day_name) {
                        $flag = 'H';
                    }
                }
            }
            if (!empty($p_hday)) {
                foreach ($p_hday as $v_hday) {
                    if ($v_hday == $sdate) {
                        $flag = 'H';
                    }
                }
            }
            if (!empty($flag)) {
                $attendace_info[date('W', strtotime($sdate))][$sdate] = $this->attendance_model->attendance_report_by_empid($cowork_id, $sdate, $flag);
            } else {
                $attendace_info[date('W', strtotime($sdate))][$sdate] = $this->attendance_model->attendance_report_by_empid($cowork_id, $sdate);
            }
            $key++;
            $flag = '';
        }
        return $attendace_info;

    }

    public function update_contact($update = null, $id = null)
    {
        $data['title'] = lang('update_contact');
        $data['update'] = $update;
        $data['profile_info'] = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
        $data['modal_subview'] = $this->load->view('admin/cowork/update_contact', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_details($id)
    {
        $data = $this->cowork_model->array_from_post(array('joining_date', 'gender', 'date_of_birth', 'maratial_status', 'father_name', 'mother_name', 'phone', 'mobile', 'skype', 'present_address', 'passport'));

        $this->cowork_model->_table_name = 'tbl_account_details'; // table name
        $this->cowork_model->_primary_key = 'account_details_id'; // $id
        $this->cowork_model->save($data, $id);

        $profile_info = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
        $activities = array(
            'cowork' => $this->session->coworkdata('cowork_id'),
            'module' => 'cowork',
            'module_field_id' => $id,
            'activity' => 'activity_update_cowork',
            'icon' => 'fa-cowork',
            'value1' => $profile_info->fullname
        );
        $this->cowork_model->_table_name = 'tbl_activities';
        $this->cowork_model->_primary_key = "activities_id";
        $this->cowork_model->save($activities);

        $message = lang('update_cowork_info');
        $type = 'success';
        set_message($type, $message);
        redirect('admin/cowork/cowork_details/' . $profile_info->cowork_id); //redirect page
    }

    public function cowork_documents($id)
    {
        $data['title'] = lang('update_documents');
        $data['profile_info'] = $this->db->where('cowork_id', $id)->get('tbl_account_details')->row();
        $data['document_info'] = $this->db->where('cowork_id', $id)->get('tbl_employee_document')->row();
        $data['modal_subview'] = $this->load->view('admin/cowork/cowork_documents', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_documents($id)
    {
        $edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $profile_info = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
            // ** Employee Document Information Save and Update Start  **
            // Resume File upload

            if (!empty($_FILES['resume']['name'])) {
                $old_path = $this->input->post('resume_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->cowork_model->uploadAllType('resume');
                $val == TRUE || redirect('admin/cowork/cowork_details/' . $profile_info->cowork_id);
                $document_data['resume_filename'] = $val['fileName'];
                $document_data['resume'] = $val['path'];
                $document_data['resume_path'] = $val['fullPath'];
            }
            // offer_letter File upload
            if (!empty($_FILES['offer_letter']['name'])) {
                $old_path = $this->input->post('offer_letter_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->cowork_model->uploadAllType('offer_letter');
                $val == TRUE || redirect('admin/cowork/cowork_details/' . $profile_info->cowork_id);
                $document_data['offer_letter_filename'] = $val['fileName'];
                $document_data['offer_letter'] = $val['path'];
                $document_data['offer_letter_path'] = $val['fullPath'];
            }
            // joining_letter File upload
            if (!empty($_FILES['joining_letter']['name'])) {
                $old_path = $this->input->post('joining_letter_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->cowork_model->uploadAllType('joining_letter');
                $val == TRUE || redirect('admin/cowork/cowork_details/' . $profile_info->cowork_id);
                $document_data['joining_letter_filename'] = $val['fileName'];
                $document_data['joining_letter'] = $val['path'];
                $document_data['joining_letter_path'] = $val['fullPath'];
            }

            // contract_paper File upload
            if (!empty($_FILES['contract_paper']['name'])) {
                $old_path = $this->input->post('contract_paper_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->cowork_model->uploadAllType('contract_paper');
                $val == TRUE || redirect('admin/cowork/cowork_details/' . $profile_info->cowork_id);
                $document_data['contract_paper_filename'] = $val['fileName'];
                $document_data['contract_paper'] = $val['path'];
                $document_data['contract_paper_path'] = $val['fullPath'];
            }
            // id_proff File upload
            if (!empty($_FILES['id_proff']['name'])) {
                $old_path = $this->input->post('id_proff_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->cowork_model->uploadAllType('id_proff');
                $val == TRUE || redirect('admin/cowork/cowork_details/' . $profile_info->cowork_id);
                $document_data['id_proff_filename'] = $val['fileName'];
                $document_data['id_proff'] = $val['path'];
                $document_data['id_proff_path'] = $val['fullPath'];
            }

            $fileName = $this->input->post('fileName');
            $path = $this->input->post('path');
            $fullPath = $this->input->post('fullPath');
            $size = $this->input->post('size');
            $is_image = $this->input->post('is_image');

            if (!empty($fileName)) {
                foreach ($fileName as $key => $name) {
                    $old['fileName'] = $name;
                    $old['path'] = $path[$key];
                    $old['fullPath'] = $fullPath[$key];
                    $old['size'] = $size[$key];
                    $old['is_image'] = $is_image[$key];
                    $result[] = $old;
                }

                $document_data['other_document'] = json_encode($result);
            }

            if (!empty($_FILES['other_document']['name']['0'])) {
                $old_path_info = $this->input->post('upload_path');
                if (!empty($old_path_info)) {
                    foreach ($old_path_info as $old_path) {
                        unlink($old_path);
                    }
                }
                $mul_val = $this->cowork_model->multi_uploadAllType('other_document');
                $document_data['other_document'] = json_encode($mul_val);
            }

            if (!empty($result) && !empty($mul_val)) {
                $file = array_merge($result, $mul_val);
                $document_data['other_document'] = json_encode($file);
            }

            $document_data['cowork_id'] = $profile_info->cowork_id;

            $this->cowork_model->_table_name = "tbl_employee_document"; // table name
            $this->cowork_model->_primary_key = "document_id"; // $id
            $document_id = $this->input->post('document_id', TRUE);
            if (!empty($document_id)) {
                $this->cowork_model->save($document_data, $document_id);
            } else {
                $this->cowork_model->save($document_data);
            }

            $activities = array(
                'cowork' => $this->session->coworkdata('cowork_id'),
                'module' => 'cowork',
                'module_field_id' => $id,
                'activity' => 'activity_documents_update',
                'icon' => 'fa-cowork',
                'value1' => $profile_info->fullname
            );
            $this->cowork_model->_table_name = 'tbl_activities';
            $this->cowork_model->_primary_key = "activities_id";
            $this->cowork_model->save($activities);

            $message = lang('emplyee_documents_update');
            $type = 'success';
            set_message($type, $message);
            redirect('admin/cowork/cowork_details/' . $profile_info->cowork_id . '/' . '4'); //redirect page
        } else {
            redirect('admin/cowork/cowork_list');
        }
    }

    public function new_bank($cowork_id, $bank_id = null)
    {
        $data['title'] = lang('new_bank');

        $data['profile_info'] = $this->db->where('cowork_id', $cowork_id)->get('tbl_account_details')->row();
        if (!empty($bank_id)) {
            $data['bank_info'] = $this->db->where('employee_bank_id', $bank_id)->get('tbl_employee_bank')->row();
        }
        $data['modal_subview'] = $this->load->view('admin/cowork/new_bank', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_bank_info($cowork_id, $bank_id = null)
    {
        $edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $bank_data = $this->cowork_model->array_from_post(array('bank_name', 'routing_number', 'account_name', 'account_number','type_of_account'));
            $bank_data['cowork_id'] = $cowork_id;
            $this->cowork_model->_table_name = "tbl_employee_bank"; // table name
            $this->cowork_model->_primary_key = "employee_bank_id"; // $id

            if (!empty($bank_id)) {
                $activity = 'activity_update_cowork_bank';
                $msg = lang('update_bank_info');
                $this->cowork_model->save($bank_data, $bank_id);
            } else {
                $activity = 'activity_new_cowork_bank';
                $msg = lang('save_bank_info');
                $bank_id = $this->cowork_model->save($bank_data);
            }
            $profile_info = $this->db->where('cowork_id', $cowork_id)->get('tbl_account_details')->row();
            $activities = array(
                'cowork' => $this->session->coworkdata('cowork_id'),
                'module' => 'cowork',
                'module_field_id' => $bank_id,
                'activity' => $activity,
                'icon' => 'fa-cowork',
                'value1' => $profile_info->fullname
            );
            $this->cowork_model->_table_name = 'tbl_activities';
            $this->cowork_model->_primary_key = "activities_id";
            $this->cowork_model->save($activities);

            $type = 'success';
            set_message($type, $msg);
            redirect('admin/cowork/cowork_details/' . $profile_info->cowork_id . '/' . '3'); //redirect page
        } else {
            redirect('admin/cowork/cowork_list');
        }
    }

    public function delete_cowork_bank($cowork_id, $bank_id)
    {
        $this->cowork_model->_table_name = "tbl_employee_bank"; // table name
        $this->cowork_model->_primary_key = "employee_bank_id"; // $id
        $this->cowork_model->delete($bank_id);

        $profile_info = $this->db->where('cowork_id', $cowork_id)->get('tbl_account_details')->row();
        $activities = array(
            'cowork' => $this->session->coworkdata('cowork_id'),
            'module' => 'cowork',
            'module_field_id' => $bank_id,
            'activity' => 'activity_delete_cowork_bank',
            'icon' => 'fa-cowork',
            'value1' => $profile_info->fullname
        );
        $this->cowork_model->_table_name = 'tbl_activities';
        $this->cowork_model->_primary_key = "activities_id";
        $this->cowork_model->save($activities);
        $type = 'success';
        $msg = lang('delete_cowork_bank');
        set_message($type, $msg);
        redirect('admin/cowork/cowork_details/' . $profile_info->cowork_id);
    }

    /*     * * Save New cowork ** */
    public function save_cowork()
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
            $login_data = $this->cowork_model->array_from_post(array('coworkname', 'email', 'role_id'));
            $cowork_id = $this->input->post('cowork_id', true);
            // update root category
            $where = array('coworkname' => $login_data['coworkname']);
            $email = array('email' => $login_data['email']);
            // duplicate value check in DB
            if (!empty($cowork_id)) { // if id exist in db update data
                $check_id = array('cowork_id !=' => $cowork_id);
            } else { // if id is not exist then set id as null
                $check_id = null;
            }
            // check whether this input data already exist or not
            $check_cowork = $this->cowork_model->check_update('tbl_coworks', $where, $check_id);
            $check_email = $this->cowork_model->check_update('tbl_coworks', $email, $check_id);
            if (!empty($check_cowork) || !empty($check_email)) { // if input data already exist show error alert
                if (!empty($check_cowork)) {
                    $error = $login_data['coworkname'];
                } else {
                    $error = $login_data['email'];
                }

                // massage for cowork
                $type = 'error';
                $message = "<strong style='color:#000'>" . $error . '</strong>  ' . lang('already_exist');

                $password = $this->input->post('password', TRUE);
                $confirm_password = $this->input->post('confirm_password', TRUE);
                if ($password != $confirm_password) {
                    $type = 'error';
                    $message = lang('password_does_not_match');
                }
            } else { // save and update query
                $login_data['last_ip'] = $this->input->ip_address();

                if (empty($cowork_id)) {
                    $password = $this->input->post('password', TRUE);
                    $login_data['password'] = $this->hash($password);
                }
                $permission = $this->input->post('permission', true);
                if (!empty($permission)) {
                    if ($permission == 'everyone') {
                        $assigned = 'all';
                    } else {
                        $assigned_to = $this->cowork_model->array_from_post(array('assigned_to'));
                        if (!empty($assigned_to['assigned_to'])) {
                            foreach ($assigned_to['assigned_to'] as $assign_cowork) {
                                $assigned[$assign_cowork] = $this->input->post('action_' . $assign_cowork, true);
                            }
                        }
                    }
                    if (!empty($assigned)) {
                        if ($assigned != 'all') {
                            $assigned = json_encode($assigned);
                        }
                    } else {
                        $assigned = 'all';
                    }
                    $login_data['permission'] = $assigned;
                } else {
                    set_message('error', lang('assigned_to') . ' Field is required');
                    redirect($_SERVER['HTTP_REFERER']);
                }
                $this->cowork_model->_table_name = 'tbl_coworks'; // table name
                $this->cowork_model->_primary_key = 'cowork_id'; // $id
                if (!empty($cowork_id)) {
                    $id = $this->cowork_model->save($login_data, $cowork_id);
                } else {
                    $login_data['activated'] = '1';
                    $id = $this->cowork_model->save($login_data);
                }
                save_custom_field(13, $id);
                // save into tbl_account details
                $profile_data = $this->cowork_model->array_from_post(array('fullname', 'employment_id', 'company', 'locale', 'language', 'phone', 'mobile', 'skype', 'designations_id', 'direction'));
                if ($login_data['role_id'] != 2) {
                    $profile_data['company'] = 0;
                }

                $account_details_id = $this->input->post('account_details_id', TRUE);
                if (!empty($_FILES['avatar']['name'])) {
                    $val = $this->cowork_model->uploadImage('avatar');
                    $val == TRUE || redirect('admin/cowork/cowork_list');
                    $profile_data['avatar'] = $val['path'];
                }

                $profile_data['cowork_id'] = $id;

                $this->cowork_model->_table_name = 'tbl_account_details'; // table name
                $this->cowork_model->_primary_key = 'account_details_id'; // $id
                if (!empty($account_details_id)) {
                    $this->cowork_model->save($profile_data, $account_details_id);
                } else {
                    $this->cowork_model->save($profile_data);
                }
                if (!empty($profile_data['designations_id'])) {
                    $desig = $this->db->where('designations_id', $profile_data['designations_id'])->get('tbl_designations')->row();
                    $department_head_id = $this->input->post('department_head_id', true);
                    if (!empty($department_head_id)) {
                        $head['department_head_id'] = $id;
                    } else {
                        $dep_head = $this->cowork_model->check_by(array('departments_id' => $desig->departments_id), 'tbl_departments');

                        if (empty($dep_head->department_head_id)) {
                            $head['department_head_id'] = $id;
                        }
                    }
                    if (!empty($desig->departments_id) && !empty($head)) {
                        $this->cowork_model->_table_name = "tbl_departments"; //table name
                        $this->cowork_model->_primary_key = "departments_id";
                        $this->cowork_model->save($head, $desig->departments_id);
                    }
                }

                $activities = array(
                    'cowork' => $this->session->coworkdata('cowork_id'),
                    'module' => 'cowork',
                    'module_field_id' => $id,
                    'activity' => 'activity_added_new_cowork',
                    'icon' => 'fa-cowork',
                    'value1' => $login_data['coworkname']
                );
                $this->cowork_model->_table_name = 'tbl_activities';
                $this->cowork_model->_primary_key = "activities_id";
                $this->cowork_model->save($activities);
                if (!empty($id)) {
                    $this->cowork_model->_table_name = 'tbl_client_role'; //table name
                    $this->cowork_model->delete_multiple(array('cowork_id' => $id));
                    $all_client_menu = $this->db->get('tbl_client_menu')->result();
                    foreach ($all_client_menu as $v_client_menu) {
                        $client_role_data['menu_id'] = $this->input->post($v_client_menu->label, true);
                        if (!empty($client_role_data['menu_id'])) {
                            $client_role_data['cowork_id'] = $id;
                            $this->cowork_model->_table_name = 'tbl_client_role';
                            $this->cowork_model->_primary_key = 'client_role_id';
                            $this->cowork_model->save($client_role_data);
                        }
                    }
                }

                if (!empty($cowork_id)) {
                    $message = lang('update_cowork_info');
                } else {
                    $message = lang('save_cowork_info');
                }
                $type = 'success';
            }
            set_message($type, $message);
        }
        redirect('admin/cowork/cowork_list'); //redirect page
    }

    public function send_welcome_email($id)
    {
        $cowork_info = $this->db->where('cowork_id', $id)->get('tbl_coworks')->row();
        $profile_info = $this->db->where('cowork_id', $id)->get('tbl_account_details')->row();
        $email_template = $this->cowork_model->check_by(array('email_group' => 'wellcome_email'), 'tbl_email_templates');

        $message = $email_template->template_body;
        $subject = $email_template->subject;
        $NAME = str_replace("{NAME}", $profile_info->fullname, $message);
        $URL = str_replace("{COMPANY_URL}", base_url(), $NAME);
        $message = str_replace("{COMPANY_NAME}", config_item('company_name'), $URL);

        $data['message'] = $message;
        $message = $this->load->view('email_template', $data, TRUE);

        $params['subject'] = $subject;
        $params['message'] = $message;
        $params['resourceed_file'] = '';
        $params['recipient'] = $cowork_info->email;

        $this->cowork_model->send_email($params);

        $type = 'success';
        $message = lang('welcome_email_success');
        set_message($type, $message);
        redirect('admin/cowork/cowork_list'); //redirect page
    }

    /*     * * Delete cowork ** */

    public function delete_cowork($id = null)
    {
        $deleted = can_action('24', 'deleted');
        if (!empty($deleted)) {
            if (!empty($id)) {
                $id = $id;
                $cowork_id = $this->session->coworkdata('cowork_id');

                //checking login cowork trying delete his own account
                if ($id == $cowork_id) {
                    //same cowork can not delete his own account
                    // redirect with error msg
                    $type = 'error';
                    $message = 'Sorry You can not delete your own account!';
                    set_message($type, $message);
                    redirect('admin/cowork/cowork_list'); //redirect page
                } else {
                    $sbtn = $this->input->post('submit', true);

                    if (!empty($sbtn)) {
                        //delete procedure run
                        // Check cowork in db or not
                        $this->cowork_model->_table_name = 'tbl_coworks'; //table name
                        $this->cowork_model->_order_by = 'cowork_id';
                        $result = $this->cowork_model->get_by(array('cowork_id' => $id), true);

                        if (!empty($result)) {
                            //delete cowork roll id
                            $this->cowork_model->_table_name = 'tbl_account_details';
                            $this->cowork_model->delete_multiple(array('cowork_id' => $id));//delete cowork roll id

                            $cwhere = array('cowork_id' => $id);
                            $this->cowork_model->_table_name = 'tbl_private_chat';
                            $this->cowork_model->delete_multiple($cwhere);

                            $this->cowork_model->_table_name = 'tbl_private_chat_coworks';
                            $this->cowork_model->delete_multiple($cwhere);

                            $this->cowork_model->_table_name = 'tbl_private_chat_messages';
                            $this->cowork_model->delete_multiple($cwhere);

                            $this->cowork_model->_table_name = 'tbl_activities';
                            $this->cowork_model->delete_multiple(array('cowork' => $id));

                            $this->cowork_model->_table_name = 'tbl_payments';
                            $this->cowork_model->delete_multiple(array('paid_by' => $id));

                            // delete all tbl_quotations by id
                            $this->cowork_model->_table_name = 'tbl_quotations';
                            $this->cowork_model->_order_by = 'cowork_id';
                            $quotations_info = $this->cowork_model->get_by(array('cowork_id' => $id), FALSE);

                            if (!empty($quotations_info)) {
                                foreach ($quotations_info as $v_quotations) {
                                    $this->cowork_model->_table_name = 'tbl_quotation_details';
                                    $this->cowork_model->delete_multiple(array('quotations_id' => $v_quotations->quotations_id));
                                }
                            }

                            $this->cowork_model->_table_name = 'tbl_quotations';
                            $this->cowork_model->delete_multiple(array('cowork_id' => $id));

                            $this->cowork_model->_table_name = 'tbl_quotationforms';
                            $this->cowork_model->delete_multiple(array('quotations_created_by_id' => $id));

                            $this->cowork_model->_table_name = 'tbl_coworks';
                            $this->cowork_model->delete_multiple(array('cowork_id' => $id));

                            $this->cowork_model->_table_name = 'tbl_cowork_role';
                            $this->cowork_model->delete_multiple(array('designations_id' => $id));

                            $this->cowork_model->_table_name = 'tbl_inbox';
                            $this->cowork_model->delete_multiple(array('cowork_id' => $id));

                            $this->cowork_model->_table_name = 'tbl_sent';
                            $this->cowork_model->delete_multiple(array('cowork_id' => $id));

                            $this->cowork_model->_table_name = 'tbl_draft';
                            $this->cowork_model->delete_multiple(array('cowork_id' => $id));

                            $tickets_info = $this->db->get('tbl_tickets')->result();
                            if (!empty($tickets_info)) {
                                foreach ($tickets_info as $v_tickets) {
                                    if (!empty($v_tickets->permission) && $v_tickets->permission != 'all') {
                                        $allowad_cowork = json_decode($v_tickets->permission);
                                        if (!empty($allowad_cowork)) {
                                            foreach ($allowad_cowork as $op_cowork_id => $v_cowork) {
                                                if ($op_cowork_id == $id || $v_tickets->reporter == $id) {
                                                    $this->cowork_model->_table_name = 'tbl_tickets';
                                                    $this->cowork_model->delete_multiple(array('tickets_id' => $v_tickets->tickets_id));
                                                    $this->cowork_model->_table_name = 'tbl_tickets_replies';
                                                    $this->cowork_model->delete_multiple(array('tickets_id' => $v_tickets->tickets_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // xpm crm
                            // delete all leads by id
                            $leads_info = $this->db->get('tbl_leads')->result();
                            if (!empty($leads_info)) {
                                foreach ($leads_info as $v_leads) {
                                    if (!empty($v_leads->permission) && $v_leads->permission != 'all') {
                                        $allowad_cowork = json_decode($v_leads->permission);
                                        if (!empty($allowad_cowork)) {
                                            foreach ($allowad_cowork as $op_cowork_id => $v_cowork) {
                                                if ($op_cowork_id == $id) {
                                                    //delete data into table.
                                                    $this->cowork_model->_table_name = "tbl_calls"; // table name
                                                    $this->cowork_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    //delete data into table.
                                                    $this->cowork_model->_table_name = "tbl_mettings"; // table name
                                                    $this->cowork_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    //delete data into table.
                                                    $this->cowork_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->cowork_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    $this->cowork_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->cowork_model->_order_by = "leads_id";
                                                    $files_info = $this->cowork_model->get_by(array('leads_id' => $v_leads->leads_id), FALSE);

                                                    if (!empty($files_info)) {
                                                        foreach ($files_info as $v_files) {
                                                            //save data into table.
                                                            $this->cowork_model->_table_name = "tbl_task_uploaded_files"; // table name
                                                            $this->cowork_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                        }
                                                    }
                                                    //save data into table.
                                                    $this->cowork_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->cowork_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    $this->cowork_model->_table_name = "tbl_task"; // table name
                                                    $this->cowork_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    $this->cowork_model->_table_name = 'tbl_leads';
                                                    $this->cowork_model->_primary_key = 'leads_id';
                                                    $this->cowork_model->delete($v_leads->leads_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //save data into table.
                            $this->cowork_model->_table_name = "tbl_milestones"; // table name
                            $this->cowork_model->delete_multiple(array('cowork_id' => $id));
                            // todo
                            $this->cowork_model->_table_name = "tbl_todo"; // table name
                            $this->cowork_model->delete_multiple(array('cowork_id' => $id));

                            // opportunity
                            $oppurtunity_info = $this->db->get('tbl_opportunities')->result();
                            if (!empty($oppurtunity_info)) {
                                foreach ($oppurtunity_info as $v_oppurtunity) {
                                    if (!empty($v_oppurtunity->permission) && $v_oppurtunity->permission != 'all') {
                                        $allowad_cowork = json_decode($v_oppurtunity->permission);
                                        if (!empty($allowad_cowork)) {
                                            foreach ($allowad_cowork as $op_cowork_id => $v_cowork) {
                                                if ($op_cowork_id == $id)
                                                    //delete data into table.
                                                    $this->cowork_model->_table_name = "tbl_calls"; // table name
                                                $this->cowork_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //delete data into table.
                                                $this->cowork_model->_table_name = "tbl_mettings"; // table name
                                                $this->cowork_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //delete data into table.
                                                $this->cowork_model->_table_name = "tbl_task_comment"; // table name
                                                $this->cowork_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                $this->cowork_model->_table_name = "tbl_task_attachment"; //table name
                                                $this->cowork_model->_order_by = "task_id";
                                                $files_info = $this->cowork_model->get_by(array('opportunities_id' => $v_oppurtunity->opportunities_id), FALSE);
                                                if (!empty($files_info)) {
                                                    foreach ($files_info as $v_files) {
                                                        //save data into table.
                                                        $this->cowork_model->_table_name = "tbl_task_uploaded_files"; // table name
                                                        $this->cowork_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                    }
                                                }
                                                //save data into table.
                                                $this->cowork_model->_table_name = "tbl_task_attachment"; // table name
                                                $this->cowork_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //save data into table.
                                                $this->cowork_model->_table_name = "tbl_task"; // table name
                                                $this->cowork_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //save data into table.
                                                $this->cowork_model->_table_name = "tbl_bug"; // table name
                                                $this->cowork_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                $this->cowork_model->_table_name = 'tbl_opportunities';
                                                $this->cowork_model->_primary_key = 'opportunities_id';
                                                $this->cowork_model->delete($v_oppurtunity->opportunities_id);
                                            }
                                        }
                                    }
                                }
                            }
                            // project
                            $project_info = $this->db->get('tbl_project')->result();
                            if (!empty($project_info)) {
                                foreach ($project_info as $v_project) {
                                    if (!empty($v_project->permission) && $v_project->permission != 'all') {
                                        $allowad_cowork = json_decode($v_project->permission);
                                        if (!empty($allowad_cowork)) {
                                            foreach ($allowad_cowork as $cowork_id => $v_cowork) {
                                                if ($cowork_id == $id) {
                                                    //delete data into table.
                                                    $this->cowork_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->cowork_model->delete_multiple(array('project_id' => $v_project->project_id));

                                                    $this->cowork_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->cowork_model->_order_by = "task_id";
                                                    $files_info = $this->cowork_model->get_by(array('project_id' => $v_project->project_id), FALSE);
                                                    if (!empty($files_info)) {
                                                        foreach ($files_info as $v_files) {
                                                            //save data into table.
                                                            $this->cowork_model->_table_name = "tbl_task_uploaded_files"; // table name
                                                            $this->cowork_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                        }
                                                    }
                                                    //save data into table.
                                                    $this->cowork_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->cowork_model->delete_multiple(array('project_id' => $v_project->project_id));

                                                    //save data into table.
                                                    $this->cowork_model->_table_name = "tbl_milestones"; // table name
                                                    $this->cowork_model->delete_multiple(array('project_id' => $v_project->project_id));

                                                    // tasks
                                                    $taskss_info = $this->db->where('project_id', $v_project->project_id)->get('tbl_task')->result();
                                                    if (!empty($taskss_info)) {
                                                        foreach ($taskss_info as $v_taskss) {
                                                            if (!empty($v_taskss->permission) && $v_taskss->permission != 'all') {
                                                                $allowad_cowork = json_decode($v_taskss->permission);
                                                                if (!empty($allowad_cowork)) {
                                                                    foreach ($allowad_cowork as $task_cowork_id => $v_cowork) {
                                                                        if ($task_cowork_id == $id) {

                                                                            $this->cowork_model->_table_name = "tbl_task_attachment"; //table name
                                                                            $this->cowork_model->_order_by = "task_id";
                                                                            $files_info = $this->cowork_model->get_by(array('task_id' => $v_taskss->task_id), FALSE);
                                                                            foreach ($files_info as $v_files) {
                                                                                $this->cowork_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                                                $this->cowork_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                                            }
                                                                            //delete into table.
                                                                            $this->cowork_model->_table_name = "tbl_task_attachment"; // table name
                                                                            $this->cowork_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                                            //delete data into table.
                                                                            $this->cowork_model->_table_name = "tbl_task_comment"; // table name
                                                                            $this->cowork_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                                            $this->cowork_model->_table_name = "tbl_task"; // table name
                                                                            $this->cowork_model->_primary_key = "task_id"; // $id
                                                                            $this->cowork_model->delete($v_taskss->task_id);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    // Bugs
                                                    $bugs_info = $this->db->where('project_id', $v_project->project_id)->get('tbl_bug')->result();
                                                    if (!empty($bugs_info)) {
                                                        foreach ($bugs_info as $v_bugs) {
                                                            if (!empty($v_bugs->permission) && $v_bugs->permission != 'all') {
                                                                $allowad_cowork = json_decode($v_bugs->permission);
                                                                if (!empty($allowad_cowork)) {
                                                                    foreach ($allowad_cowork as $bugs_cowork_id => $v_cowork) {
                                                                        if ($bugs_cowork_id == $id) {

                                                                            $this->cowork_model->_table_name = "tbl_task_attachment"; //table name
                                                                            $this->cowork_model->_order_by = "bug_id";
                                                                            $files_info = $this->cowork_model->get_by(array('bug_id' => $v_bugs->bug_id), FALSE);
                                                                            foreach ($files_info as $v_files) {
                                                                                $this->cowork_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                                                $this->cowork_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                                            }
                                                                            //delete into table.
                                                                            $this->cowork_model->_table_name = "tbl_task_attachment"; // table name
                                                                            $this->cowork_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                                            //delete data into table.
                                                                            $this->cowork_model->_table_name = "tbl_task_comment"; // table name
                                                                            $this->cowork_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                                            //delete data into table.
                                                                            $this->cowork_model->_table_name = "tbl_task"; // table name
                                                                            $this->cowork_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                                            $this->cowork_model->_table_name = "tbl_bug"; // table name
                                                                            $this->cowork_model->_primary_key = "bug_id"; // $id
                                                                            $this->cowork_model->delete($v_bugs->bug_id);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    $this->cowork_model->_table_name = 'tbl_project';
                                                    $this->cowork_model->_primary_key = 'project_id';
                                                    $this->cowork_model->delete($v_project->project_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // tasks
                            $taskss_info = $this->db->get('tbl_task')->result();
                            if (!empty($taskss_info)) {
                                foreach ($taskss_info as $v_taskss) {
                                    if (!empty($v_taskss->permission) && $v_taskss->permission != 'all') {
                                        $allowad_cowork = json_decode($v_taskss->permission);
                                        if (!empty($allowad_cowork)) {
                                            foreach ($allowad_cowork as $task_cowork_id => $v_cowork) {
                                                if ($task_cowork_id == $id) {

                                                    $this->cowork_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->cowork_model->_order_by = "task_id";
                                                    $files_info = $this->cowork_model->get_by(array('task_id' => $v_taskss->task_id), FALSE);
                                                    foreach ($files_info as $v_files) {
                                                        $this->cowork_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                        $this->cowork_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                    }
                                                    //delete into table.
                                                    $this->cowork_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->cowork_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                    //delete data into table.
                                                    $this->cowork_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->cowork_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                    $this->cowork_model->_table_name = "tbl_task"; // table name
                                                    $this->cowork_model->_primary_key = "task_id"; // $id
                                                    $this->cowork_model->delete($v_taskss->task_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // Bugs
                            $bugs_info = $this->db->get('tbl_bug')->result();
                            if (!empty($bugs_info)) {
                                foreach ($bugs_info as $v_bugs) {
                                    if (!empty($v_bugs->permission) && $v_bugs->permission != 'all') {
                                        $allowad_cowork = json_decode($v_bugs->permission);
                                        if (!empty($allowad_cowork)) {
                                            foreach ($allowad_cowork as $bugs_cowork_id => $v_cowork) {
                                                if ($bugs_cowork_id == $id) {

                                                    $this->cowork_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->cowork_model->_order_by = "bug_id";
                                                    $files_info = $this->cowork_model->get_by(array('bug_id' => $v_bugs->bug_id), FALSE);
                                                    foreach ($files_info as $v_files) {
                                                        $this->cowork_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                        $this->cowork_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                    }
                                                    //delete into table.
                                                    $this->cowork_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->cowork_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                    //delete data into table.
                                                    $this->cowork_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->cowork_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                    //delete data into table.
                                                    $this->cowork_model->_table_name = "tbl_task"; // table name
                                                    $this->cowork_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                    $this->cowork_model->_table_name = "tbl_bug"; // table name
                                                    $this->cowork_model->_primary_key = "bug_id"; // $id
                                                    $this->cowork_model->delete($v_bugs->bug_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // tbl_invoices
                            $invoices_info = $this->db->get('tbl_invoices')->result();
                            if (!empty($invoices_info)) {
                                foreach ($invoices_info as $v_invoices) {
                                    if (!empty($v_invoices->permission) && $v_invoices->permission != 'all') {
                                        $allowad_cowork = json_decode($v_invoices->permission);
                                        if (!empty($allowad_cowork)) {
                                            foreach ($allowad_cowork as $invoice_cowork_id => $v_cowork) {
                                                if ($invoice_cowork_id == $id) {
                                                    $this->cowork_model->_table_name = "tbl_invoices"; // table name
                                                    $this->cowork_model->_primary_key = "invoices_id"; // $id
                                                    $this->cowork_model->delete($v_invoices->invoices_id);

                                                    $this->cowork_model->_table_name = "tbl_items"; // table name
                                                    $this->cowork_model->delete_multiple(array('invoices_id' => $v_invoices->invoices_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // tbl_estimates
                            $estimate_info = $this->db->get('tbl_estimates')->result();
                            if (!empty($estimate_info)) {
                                foreach ($estimate_info as $v_estimate) {
                                    if (!empty($v_estimate->permission) && $v_estimate->permission != 'all') {
                                        $allowad_cowork = json_decode($v_estimate->permission);
                                        if (!empty($allowad_cowork)) {
                                            foreach ($allowad_cowork as $estimate_cowork_id => $v_cowork) {
                                                if ($estimate_cowork_id == $id) {
                                                    $this->cowork_model->_table_name = "tbl_estimates"; // table name
                                                    $this->cowork_model->_primary_key = "estimates_id"; // $id
                                                    $this->cowork_model->delete($v_estimate->estimates_id);

                                                    $this->cowork_model->_table_name = "tbl_estimate_items"; // table name
                                                    $this->cowork_model->delete_multiple(array('estimates_id' => $v_estimate->estimates_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // 	tbl_tax_rates
                            $tax_rate_info = $this->db->get('tbl_tax_rates')->result();
                            if (!empty($tax_rate_info)) {
                                foreach ($tax_rate_info as $v_tax_rat) {
                                    if (!empty($v_tax_rat->permission) && $v_tax_rat->permission != 'all') {
                                        $allowad_cowork = json_decode($v_tax_rat->permission);
                                        if (!empty($allowad_cowork)) {
                                            foreach ($allowad_cowork as $tax_rate_cowork_id => $v_cowork) {
                                                if ($tax_rate_cowork_id == $id) {
                                                    $this->cowork_model->_table_name = "tbl_tax_rates"; // table name
                                                    $this->cowork_model->_primary_key = "tax_rates_id"; // $id
                                                    $this->cowork_model->delete($v_tax_rat->tax_rates_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $transactions_info = $this->db->get('tbl_transactions')->result();
                            if (!empty($transactions_info)) {
                                foreach ($transactions_info as $v_transactions) {
                                    if (!empty($v_transactions->permission) && $v_transactions->permission != 'all') {
                                        $allowad_cowork = json_decode($v_transactions->permission);
                                        if (!empty($allowad_cowork)) {
                                            foreach ($allowad_cowork as $trsn_cowork_id => $v_cowork) {
                                                if ($trsn_cowork_id == $id) {
                                                    $this->cowork_model->_table_name = 'tbl_transactions';
                                                    $this->cowork_model->delete_multiple(array('transactions_id' => $v_transactions->transactions_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $transfer_info = $this->db->get('tbl_transfer')->result();
                            if (!empty($transfer_info)) {
                                foreach ($transfer_info as $v_transfer) {
                                    if (!empty($v_transfer->permission) && $v_transfer->permission != 'all') {
                                        $allowad_cowork = json_decode($v_transfer->permission);
                                        if (!empty($allowad_cowork)) {
                                            foreach ($allowad_cowork as $trfr_cowork_id => $v_cowork) {
                                                if ($trfr_cowork_id == $id) {
                                                    $this->cowork_model->_table_name = 'tbl_transfer';
                                                    $this->cowork_model->delete_multiple(array('transfer_id' => $v_transfer->transfer_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //redirect successful msg
                            $type = 'success';
                            $message = 'cowork Delete Successfully!';
                        } else {
                            //redirect error msg
                            $type = 'error';
                            $message = 'Sorry this cowork not find in database!';

                        }
                        set_message($type, $message);
                        redirect('admin/cowork/cowork_list'); //redirect page
                    } else {
                        $data['title'] = "Delete coworks"; //Page title
                        $data['cowork_info'] = $this->db->where('cowork_id', $id)->get('tbl_account_details')->row();
                        $data['subview'] = $this->load->view('admin/cowork/delete_cowork', $data, TRUE);
                        $this->load->view('admin/_layout_main', $data); //page load
                    }
                }
            } else {
                redirect('admin/cowork/cowork_list'); //redirect page
            }
        }
    }

    public function change_status($flag, $id)
    {
        $can_edit = $this->cowork_model->can_action('tbl_coworks', 'edit', array('cowork_id' => $id));
        $edited = can_action('24', 'edited');
        if (!empty($can_edit) && !empty($edited)) {
            $cowork_info = $this->db->where('cowork_id', $id)->get('tbl_coworks')->row();
            // if flag == 1 it is active cowork else deactive cowork
            if ($flag == 1) {
                $msg = 'Active';
            } else {
                $msg = 'Deactive';
            }
            $where = array('cowork_id' => $id);
            $action = array('activated' => $flag);
            $this->cowork_model->set_action($where, $action, 'tbl_coworks');

            $activities = array(
                'cowork' => $this->session->coworkdata('cowork_id'),
                'module' => 'cowork',
                'module_field_id' => $id,
                'activity' => 'activity_change_status',
                'icon' => 'fa-cowork',
                'value1' => $cowork_info->coworkname . ' ' . $msg,
            );
            $this->cowork_model->_table_name = 'tbl_activities';
            $this->cowork_model->_primary_key = "activities_id";
            $this->cowork_model->save($activities);

            $type = "success";
            $message = "cowork " . $msg . " Successfully!";
        } else {
            $type = 'error';
            $message = lang('there_in_no_value');
        }
        echo json_encode(array("status" => $type, "message" => $message));
        exit();
    }

    public function set_banned($flag, $id)
    {
        $can_edit = $this->cowork_model->can_action('tbl_coworks', 'edit', array('cowork_id' => $id));
        $edited = can_action('24', 'edited');
        if (!empty($can_edit) && !empty($edited)) {
            if ($flag == 1) {
                $msg = lang('banned');
                $action = array('activated' => 0, 'banned' => $flag, 'ban_reason' => $this->input->post('ban_reason', TRUE));
            } else {
                $msg = lang('unbanned');
                $action = array('activated' => 1, 'banned' => $flag);
            }
            $where = array('cowork_id' => $id);

            $this->cowork_model->set_action($where, $action, 'tbl_coworks');

            $activities = array(
                'cowork' => $this->session->coworkdata('cowork_id'),
                'module' => 'cowork',
                'module_field_id' => $id,
                'activity' => 'activity_change_status',
                'icon' => 'fa-cowork',
                'value1' => $flag,
            );
            $this->cowork_model->_table_name = 'tbl_activities';
            $this->cowork_model->_primary_key = "activities_id";
            $this->cowork_model->save($activities);

            $type = "success";
            $message = "cowork " . $msg . " Successfully!";
        } else {
            $type = 'error';
            $message = lang('there_in_no_value');
        }
        set_message($type, $message);
        redirect('admin/cowork/cowork_list'); //redirect page
    }

    public function change_banned($id)
    {

        $data['cowork_id'] = $id;
        $data['modal_subview'] = $this->load->view('admin/cowork/_modal_banned_reson', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);

    }

    public function hash($string)
    {
        return hash('sha512', $string . config_item('encryption_key'));
    }

// crud for sidebar todo list
    function todo($task = '', $todo_id = '', $swap_with = '')
    {
        if ($task == 'add') {
            $this->add_todo();
        }
        if ($task == 'reload_incomplete_todo') {
            $this->get_incomplete_todo();
        }
        if ($task == 'mark_as_done') {
            $this->mark_todo_as_done($todo_id);
        }
        if ($task == 'mark_as_undone') {
            $this->mark_todo_as_undone($todo_id);
        }
        if ($task == 'swap') {

            $this->swap_todo($todo_id, $swap_with);
        }
        if ($task == 'delete') {
            $this->delete_todo($todo_id);
        }
        $todo['opened'] = 1;
        $this->session->set_coworkdata($todo);
        redirect('admin/dashboard/');
    }

    function add_todo()
    {
        $data['title'] = $this->input->post('title');
        $data['cowork_id'] = $this->session->coworkdata('cowork_id');

        $this->db->insert('tbl_todo', $data);
        $todo_id = $this->db->insert_id();

        $data['order'] = $todo_id;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function mark_todo_as_done($todo_id = '')
    {
        $data['status'] = 1;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function mark_todo_as_undone($todo_id = '')
    {
        $data['status'] = 0;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function swap_todo($todo_id = '', $swap_with = '')
    {
        $counter = 0;
        $temp_order = $this->db->get_where('tbl_todo', array('todo_id' => $todo_id))->row()->order;
        $cowork = $this->session->coworkdata('cowork_id');

        // Move current todo up.
        if ($swap_with == 'up') {
            // Fetch all todo lists of current cowork in ascending order.
            $this->db->order_by('order', 'ASC');
            $todo_lists = $this->db->get_where('tbl_todo', array('cowork_id' => $cowork))->result_array();
            $array_length = count($todo_lists);

            // Create separate array for orders and todo_id's from above array.
            foreach ($todo_lists as $todo_list) {
                $id_list[] = $todo_list['todo_id'];
                $order_list[] = $todo_list['order'];
            }
        }

        // Move current todo down.
        if ($swap_with == 'down') {

            // Fetch all todo lists of current cowork in descending order.
            $this->db->order_by('order', 'DESC');
            $todo_lists = $this->db->get_where('tbl_todo', array('cowork_id' => $cowork))->result_array();
            $array_length = count($todo_lists);

            // Create separate array for orders and todo_id's from above array.
            foreach ($todo_lists as $todo_list) {
                $id_list[] = $todo_list['todo_id'];
                $order_list[] = $todo_list['order'];
            }
        }

        // Swap orders between current and next/previous todo.
        for ($i = 0; $i < $array_length; $i++) {
            if ($temp_order == $order_list[$i]) {
                if ($counter > 0) {
                    $swap_order = $order_list[$i - 1];
                    $swap_id = $id_list[$i - 1];

                    // Update order of current todo.
                    $data['order'] = $swap_order;
                    $this->db->where('todo_id', $todo_id);
                    $this->db->update('tbl_todo', $data);

                    // Update order of next/previous todo.
                    $data['order'] = $temp_order;
                    $this->db->where('todo_id', $swap_id);
                    $this->db->update('tbl_todo', $data);
                }
            } else
                $counter++;
        }
    }

    function delete_todo($todo_id = '')
    {
        $this->db->where('todo_id', $todo_id);
        $this->db->delete('tbl_todo');
    }

    function get_incomplete_todo()
    {
        $cowork = $this->session->coworkdata('cowork_id');
        $this->db->where('cowork_id', $cowork);
        $this->db->where('status', 0);
        $query = $this->db->get('tbl_todo');

        $incomplete_todo_number = $query->num_rows();
        if ($incomplete_todo_number > 0) {
            echo '<span class="badge badge-secondary">';
            echo $incomplete_todo_number;
            echo '</span>';
        }
    }

    public function reset_password($id)
    {
        if ($this->session->coworkdata('cowork_type') == 1) {
            $new_password = $this->input->post('password', true);
            $old_password = $this->input->post('old_password', true);
            if (!empty($new_password)) {
                $email = $this->session->coworkdata('email');
                $cowork_info = $this->db->where('cowork_id', $id)->get('tbl_coworks')->row();
                $old_password = $this->cowork_model->hash($old_password);
                if ($cowork_info->password == $old_password) {
                    $where = array('cowork_id' => $id);
                    $action = array('password' => $this->cowork_model->hash($new_password));
                    $this->cowork_model->set_action($where, $action, 'tbl_coworks');
                    $login_details = $this->db->where('cowork_id', $id)->get('tbl_coworks')->row();
                    $activities = array(
                        'cowork' => $this->session->coworkdata('cowork_id'),
                        'module' => 'cowork',
                        'module_field_id' => $id,
                        'activity' => 'activity_reset_password',
                        'icon' => 'fa-cowork',
                        'value1' => $login_details->coworkname,
                    );

                    $this->cowork_model->_table_name = 'tbl_activities';
                    $this->cowork_model->_primary_key = "activities_id";
                    $this->cowork_model->save($activities);

                    $this->send_email_reset_password($email, $cowork_info, $new_password);

                    $type = "success";
                    $message = lang('message_new_password_sent');
                } else {
                    $type = "error";
                    $message = lang('password_does_not_match');
                }
                set_message($type, $message);
                redirect('admin/cowork/cowork_details/' . $id); //redirect page

            } else {
                $data['title'] = lang('see_password');
                $data['cowork_info'] = $this->db->where('cowork_id', $id)->get('tbl_coworks')->row();
                $data['subview'] = $this->load->view('admin/settings/reset_password', $data, FALSE);
                $this->load->view('admin/_layout_modal', $data);
            }

        } else {
            $type = 'error';
            $message = lang('there_in_no_value');
            set_message($type, $message);
            redirect('admin/cowork/cowork_list'); //redirect page
        }


    }

    function send_email_reset_password($email, $cowork_info, $password)
    {
        $email_template = $this->cowork_model->check_by(array('email_group' => 'reset_password'), 'tbl_email_templates');
        $message = $email_template->template_body;
        $subject = $email_template->subject;

        $coworkname = str_replace("{coworkNAME}", $cowork_info->coworkname, $message);
        $cowork_email = str_replace("{EMAIL}", $cowork_info->email, $coworkname);
        $cowork_password = str_replace("{NEW_PASSWORD}", $password, $cowork_email);
        $message = str_replace("{SITE_NAME}", config_item('company_name'), $cowork_password);
        $params['recipient'] = $email;
        $params['subject'] = '[ ' . config_item('company_name') . ' ]' . $subject;
        $params['message'] = $message;
        $params['resourceed_file'] = '';
        $this->cowork_model->send_email($params);
    }

}
