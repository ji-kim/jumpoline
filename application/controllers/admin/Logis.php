<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Logis extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('logis_model');
        $this->load->model('utilities_model');
    }

    public function buildChild($parent, $menu)
    {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

    public function transaction_ready($action = NULL, $id = NULL)
    {

        $logis_id = $id;

        if ($action == 'edit_logis') {
            $data['active'] = 2;
            $can_edit = $this->logis_model->can_action('tbl_logiss', 'edit', array('logis_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('logis_id', $logis_id)->get('tbl_logiss')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'logis List';
        $this->logis_model->_table_name = 'tbl_contract ct'; //table name          
		$this->logis_model->db->join('tbl_members rco', 'ct.r_co_id = rco.userid');

		if(!empty($this->input->post('baecha_co_id', true))) $this->logis_model->db->where('tr.baecha_co', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->logis_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\' OR tr.car_1 like \'%'.$data['search_keyword'].'%\' OR tr.car_7 like \'%'.$data['search_keyword'].'%\' OR tr.ws_co_name like \'%'.$data['search_keyword'].'%\' OR tr.inv_co_name like \'%'.$data['search_keyword'].'%\' )' );
		}
		if(!empty($data['ws_co_id'])) {
			$this->logis_model->db->or_where('tr.ws_co_id', $data['ws_co_id'] );
		}
        $this->logis_model->db->where('ct.is_contract', 'N')->where(' (TRIM(ct.ct_status) <> \'계약만료\' and TRIM(ct.ct_status) <> \'계약취소\')')->order_by('ct.ct_title', 'ASC');//->where('ct.is_subcontract','Y')//현규씨가 계약이 N으로 해놓은 이유는?

        $data['all_contract_info'] = $this->logis_model->get();

        $data['subview'] = $this->load->view('admin/logis/transaction_ready', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function transaction_finish($action = NULL, $id = NULL)
    {

        $logis_id = $id;

        if ($action == 'edit_logis') {
            $data['active'] = 2;
            $can_edit = $this->logis_model->can_action('tbl_logiss', 'edit', array('logis_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('logis_id', $logis_id)->get('tbl_logiss')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'logis List';

        $this->logis_model->_table_name = 'tbl_client'; //table name
        $this->logis_model->_order_by = 'client_id';
        $data['all_client_info'] = $this->logis_model->get();

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_logis'] = $this->logis_model->all_permission_logis('24');
        $data['all_logis_info'] = $this->logis_model->get_permission('tbl_logiss');

        $data['all_designation_info'] = $this->logis_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/logis/transaction_finish', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function stransaction_ready($action = NULL, $id = NULL)
    {
        $logis_id = $id;

        if ($action == 'edit_logis') {
            $data['active'] = 2;
            $can_edit = $this->logis_model->can_action('tbl_logiss', 'edit', array('logis_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('logis_id', $logis_id)->get('tbl_logiss')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'logis List';
        $this->logis_model->_table_name = 'tbl_contract ct'; //table name          
		$this->logis_model->db->join('tbl_members rco', 'ct.r_co_id = rco.userid');

		if(!empty($this->input->post('baecha_co_id', true))) $this->logis_model->db->where('tr.baecha_co', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->logis_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\' OR tr.car_1 like \'%'.$data['search_keyword'].'%\' OR tr.car_7 like \'%'.$data['search_keyword'].'%\' OR tr.ws_co_name like \'%'.$data['search_keyword'].'%\' OR tr.inv_co_name like \'%'.$data['search_keyword'].'%\' )' );
		}
		if(!empty($data['ws_co_id'])) {
			$this->logis_model->db->or_where('tr.ws_co_id', $data['ws_co_id'] );
		}
        $this->logis_model->db->where('ct.is_contract', 'N')->where(' (TRIM(ct.ct_status) <> \'계약만료\')')->where('ct.is_subcontract','Y')->order_by('ct.ct_title', 'ASC');////현규씨가 계약이 N으로 해놓은 이유는?

        $data['all_scontract_info'] = $this->logis_model->get();

        $data['subview'] = $this->load->view('admin/logis/stransaction_ready', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function stransaction_finish($action = NULL)
    {
        $data['df_month'] = $this->input->post('df_month', true);
        if (empty($data['df_month'])) $data['df_month'] = date('Y-m');

        if ($action == 'detail_view') {
            $data['active'] = 2;
        } else {
            $data['active'] = 1;
        }
		
		$data['title'] = '하도급 거래등록 완료';
        $this->logis_model->_table_name = 'tbl_delivery_fee df'; //table name          
		$this->logis_model->db->join('tbl_members dp', 'dp.dp_id = df.dp_id');
		$this->logis_model->db->join('tbl_members ct', 'ct.dp_id = df.dp_id');
		$this->logis_model->db->where('df.status', '마감')->where('df.df_month', $data['df_month']);
		if(!empty($this->input->post('co_code', true))) $this->logis_model->db->where('ct.mb_type', 'customer')->where('ct.code', $this->input->post('co_code', true));

        $this->logis_model->_table_name = 'tbl_delivery_fee df'; //table name          
		$sql4 = " select * from az_contract_detail where ct_id='$pct_id' and depth='1' order by idx asc";
$result4 = sql_query($sql4);


		if(!empty($data['search_keyword'])) {
			$this->logis_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\' OR tr.car_1 like \'%'.$data['search_keyword'].'%\' OR tr.car_7 like \'%'.$data['search_keyword'].'%\' OR tr.ws_co_name like \'%'.$data['search_keyword'].'%\' OR tr.inv_co_name like \'%'.$data['search_keyword'].'%\' )' );
		}

        $data['all_stransaction_info'] = $this->logis_model->get();

        $data['subview'] = $this->load->view('admin/logis/stransaction_finish', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }


    public function stransaction_write($action = NULL, $id = NULL)
    {

        $logis_id = $id;

        if ($action == 'detail_view') {
            $data['active'] = 2;
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'logis List';

       // $this->logis_model->_table_name = 'tbl_client'; //table name
       // $this->logis_model->_order_by = 'client_id';
        //$data['all_client_info'] = $this->logis_model->get();

        $data['subview'] = $this->load->view('admin/logis/stransaction_write', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function logis_list($action = NULL, $id = NULL)
    {

        $logis_id = $id;

        if ($action == 'edit_logis') {
            $data['active'] = 2;
            $can_edit = $this->logis_model->can_action('tbl_logiss', 'edit', array('logis_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('logis_id', $logis_id)->get('tbl_logiss')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'logis List';

        $this->logis_model->_table_name = 'tbl_client'; //table name
        $this->logis_model->_order_by = 'client_id';
        $data['all_client_info'] = $this->logis_model->get();

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_logis'] = $this->logis_model->all_permission_logis('24');
        $data['all_logis_info'] = $this->logis_model->get_permission('tbl_logiss');

        $data['all_designation_info'] = $this->logis_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/logis/logis_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function logis_details($id, $active = null)
    {
        $data['title'] = lang('logis_details');
        $data['id'] = $id;
        if (!empty($active)) {
            $data['active'] = $active;
        } else {
            $data['active'] = 1;
        }
        $date = $this->input->post('date', true);
        if (!empty($date)) {
            $data['date'] = $date;
        } else {
            $data['date'] = date('Y-m');
        }
        $data['attendace_info'] = $this->get_report($id, $data['date']);

        $data['my_leave_report'] = leave_report($id);

        //
        if ($this->input->post('year', TRUE)) { // if input year
            $data['year'] = $this->input->post('year', TRUE);
        } else { // else current year
            $data['year'] = date('Y'); // get current year
        }
        // get all expense list by year and month
        $data['provident_fund_info'] = $this->get_provident_fund_info($data['year'], $id);

        if ($this->input->post('overtime_year', TRUE)) { // if input year
            $data['overtime_year'] = $this->input->post('overtime_year', TRUE);
        } else { // else current year
            $data['overtime_year'] = date('Y'); // get current year
        }
        // get all expense list by year and month
        $data['all_overtime_info'] = $this->get_overtime_info($data['overtime_year'], $id);
        $data['profile_info'] = $this->db->where('logis_id', $id)->get('tbl_account_details')->row();

        $data['total_attendance'] = count($this->total_attendace_in_month($id));

        $data['total_absent'] = count($this->total_attendace_in_month($id, 'absent'));

        $data['total_leave'] = count($this->total_attendace_in_month($id, 'leave'));
        //award received
        $data['total_award'] = count($this->db->where('logis_id', $id)->get('tbl_employee_award')->result());

        // get working days holiday
        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday

        $num = cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y'));
        $working_holiday = 0;
        for ($i = 1; $i <= $num; $i++) {
            $day_name = date('l', strtotime("+0 days", strtotime(date('Y') . '-' . date('n') . '-' . $i)));

            if (!empty($holidays)) {
                foreach ($holidays as $v_holiday) {
                    if ($v_holiday->day == $day_name) {
                        $working_holiday += count($day_name);
                    }
                }
            }
        }
        // get public holiday
        $public_holiday = count($this->total_attendace_in_month($id, TRUE));

        // get total days in a month
        $month = date('m');
        $year = date('Y');
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        // total attend days in a month without public holiday and working days
        $data['total_days'] = $days - $working_holiday - $public_holiday;

        $data['all_working_hour'] = $this->all_attendance_id_by_date($id, true);

        $data['this_month_working_hour'] = $this->all_attendance_id_by_date($id);

        $data['subview'] = $this->load->view('admin/logis/logis_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function all_attendance_id_by_date($logis_id, $flag = null)
    {
        if (!empty($flag)) {
            $get_total_attendance = $this->db->where(array('logis_id' => $logis_id, 'attendance_status' => '1'))->get('tbl_attendance')->result();
            if (!empty($get_total_attendance)) {
                foreach ($get_total_attendance as $v_attendance_id) {
                    $aresult[] = $this->global_model->get_total_working_hours($v_attendance_id->attendance_id);
                }
                return $aresult;
            }
        } else {

            $month = date('n');
            $year = date('Y');
            if ($month >= 1 && $month <= 9) {
                $yymm = $year . '-' . '0' . $month;
            } else {
                $yymm = $year . '-' . $month;
            }
            $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            for ($i = 1; $i <= $num; $i++) {
                if ($i >= 1 && $i <= 9) {
                    $sdate = $yymm . '-' . '0' . $i;
                } else {
                    $sdate = $yymm . '-' . $i;
                }
                $get_total_attendance = $this->global_model->get_total_attendace_by_date($sdate, $sdate, $logis_id); // get all attendace by start date and in date
                if (!empty($get_total_attendance)) {
                    foreach ($get_total_attendance as $v_attendance_id) {
                        $result[] = $this->global_model->get_total_working_hours($v_attendance_id->attendance_id);
                    }
                }
            }
            if (!empty($result)) {
                return $result;
            }
        }
    }

    public function total_attendace_in_month($logis_id, $flag = NULL)
    {
        $month = date('m');
        $year = date('Y');

        if ($month >= 1 && $month <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
            $start_date = $year . "-" . '0' . $month . '-' . '01';
            $end_date = $year . "-" . '0' . $month . '-' . '31';
        } else {
            $start_date = $year . "-" . $month . '-' . '01';
            $end_date = $year . "-" . $month . '-' . '31';
        }
        if (!empty($flag) && $flag == 1) { // if flag is not empty that means get pulic holiday
            $get_public_holiday = $this->global_model->get_holiday_list_by_date($start_date, $end_date);

            if (!empty($get_public_holiday)) { // if not empty the public holiday
                foreach ($get_public_holiday as $v_holiday) {
                    if ($v_holiday->start_date == $v_holiday->end_date) { // if start date and end date is equal return one data
                        $total_holiday[] = $v_holiday->start_date;
                    } else { // if start date and end date not equan return all date
                        for ($j = $v_holiday->start_date; $j <= $v_holiday->end_date; $j++) {
                            $total_holiday[] = $j;
                        }
                    }
                }
                return $total_holiday;
            }
        } elseif (!empty($flag)) { // if flag is not empty that means get pulic holiday
            $get_total_absent = $this->global_model->get_total_attendace_by_date($start_date, $end_date, $logis_id, $flag); // get all attendace by start date and in date
            return $get_total_absent;
        } else {
            $get_total_attendance = $this->global_model->get_total_attendace_by_date($start_date, $end_date, $logis_id); // get all attendace by start date and in date
            return $get_total_attendance;
        }
    }

    public function get_overtime_info($year, $logis_id)
    {// this function is to create get monthy recap report

        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $i . '-' . '01';
                $end_date = $year . "-" . '0' . $i . '-' . '31';
            } else {
                $start_date = $year . "-" . $i . '-' . '01';
                $end_date = $year . "-" . $i . '-' . '31';
            }
            $get_expense_list[$i] = $this->utilities_model->get_overtime_info_by_date($start_date, $end_date, $logis_id); // get all report by start date and in date
        }

        return $get_expense_list; // return the result
    }

    public function overtime_report_pdf($year, $logis_id)
    {
        $data['all_overtime_info'] = $this->get_overtime_info($year, $logis_id);

        $data['monthyaer'] = $year;
        $data['logis_info'] = $this->db->where('logis_id', $logis_id)->get('tbl_account_details')->row();
        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/logis/overtime_report_pdf', $data, TRUE);
        pdf_create($viewfile, 'Overtime Report  - ' . $data['monthyaer']);
    }

    public function get_provident_fund_info($year, $logis_id)
    {// this function is to create get monthy recap report

        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $i;
                $end_date = $year . "-" . '0' . $i;
            } else {
                $start_date = $year . "-" . $i;
                $end_date = $year . "-" . $i;
            }
            $provident_fund_info[$i] = $this->payroll_model->get_provident_fund_info_by_date($start_date, $end_date, $logis_id); // get all report by start date and in date
        }

        return $provident_fund_info; // return the result
    }

    public function provident_fund_pdf($year, $logis_id)
    {

        $data['provident_fund_info'] = $this->get_provident_fund_info($year, $logis_id);
        $data['monthyaer'] = $year;

        $data['logis_info'] = $this->db->where('logis_id', $logis_id)->get('tbl_account_details')->row();

        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/logis/provident_fund_pdf', $data, TRUE);
        pdf_create($viewfile, lang('provident_found_report') . ' - ' . $data['monthyaer']);
    }

    public function timecard_details_pdf($id, $date)
    {
        $data['profile_info'] = $this->db->where('logis_id', $id)->get('tbl_account_details')->row();
        $data['date'] = $date;
        $data['attendace_info'] = $this->get_report($id, $date);

        $viewfile = $this->load->view('admin/logis/timecard_details_pdf', $data, TRUE);

        $this->load->helper('dompdf');
        pdf_create($viewfile, lang('timecard_details') . '- ' . $data['profile_info']->fullname);
    }

    public function get_report($logis_id, $date)
    {
        $month = date('n', strtotime($date));
        $year = date('Y', strtotime($date));
        $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday

        if ($month >= 1 && $month <= 9) {
            $yymm = $year . '-' . '0' . $month;
        } else {
            $yymm = $year . '-' . $month;
        }

        $public_holiday = $this->global_model->get_public_holidays($yymm);

        //tbl a_calendar Days Holiday
        if (!empty($public_holiday)) {
            foreach ($public_holiday as $p_holiday) {
                $p_hday = $this->attendance_model->GetDays($p_holiday->start_date, $p_holiday->end_date);
            }
        }

        $key = 1;
        $x = 0;
        for ($i = 1; $i <= $num; $i++) {

            if ($i >= 1 && $i <= 9) {
                $sdate = $yymm . '-' . '0' . $i;
            } else {
                $sdate = $yymm . '-' . $i;
            }
            $day_name = date('l', strtotime("+$x days", strtotime($year . '-' . $month . '-' . $key)));

            $data['week_info'][date('W', strtotime($sdate))][$sdate] = $sdate;

            // get leave info
            if (!empty($holidays)) {
                foreach ($holidays as $v_holiday) {
                    if ($v_holiday->day == $day_name) {
                        $flag = 'H';
                    }
                }
            }
            if (!empty($p_hday)) {
                foreach ($p_hday as $v_hday) {
                    if ($v_hday == $sdate) {
                        $flag = 'H';
                    }
                }
            }
            if (!empty($flag)) {
                $attendace_info[date('W', strtotime($sdate))][$sdate] = $this->attendance_model->attendance_report_by_empid($logis_id, $sdate, $flag);
            } else {
                $attendace_info[date('W', strtotime($sdate))][$sdate] = $this->attendance_model->attendance_report_by_empid($logis_id, $sdate);
            }
            $key++;
            $flag = '';
        }
        return $attendace_info;

    }

    public function update_contact($update = null, $id = null)
    {
        $data['title'] = lang('update_contact');
        $data['update'] = $update;
        $data['profile_info'] = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
        $data['modal_subview'] = $this->load->view('admin/logis/update_contact', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_details($id)
    {
        $data = $this->logis_model->array_from_post(array('joining_date', 'gender', 'date_of_birth', 'maratial_status', 'father_name', 'mother_name', 'phone', 'mobile', 'skype', 'present_address', 'passport'));

        $this->logis_model->_table_name = 'tbl_account_details'; // table name
        $this->logis_model->_primary_key = 'account_details_id'; // $id
        $this->logis_model->save($data, $id);

        $profile_info = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
        $activities = array(
            'logis' => $this->session->logisdata('logis_id'),
            'module' => 'logis',
            'module_field_id' => $id,
            'activity' => 'activity_update_logis',
            'icon' => 'fa-logis',
            'value1' => $profile_info->fullname
        );
        $this->logis_model->_table_name = 'tbl_activities';
        $this->logis_model->_primary_key = "activities_id";
        $this->logis_model->save($activities);

        $message = lang('update_logis_info');
        $type = 'success';
        set_message($type, $message);
        redirect('admin/logis/logis_details/' . $profile_info->logis_id); //redirect page
    }

    public function logis_documents($id)
    {
        $data['title'] = lang('update_documents');
        $data['profile_info'] = $this->db->where('logis_id', $id)->get('tbl_account_details')->row();
        $data['document_info'] = $this->db->where('logis_id', $id)->get('tbl_employee_document')->row();
        $data['modal_subview'] = $this->load->view('admin/logis/logis_documents', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_documents($id)
    {
        $edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $profile_info = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
            // ** Employee Document Information Save and Update Start  **
            // Resume File upload

            if (!empty($_FILES['resume']['name'])) {
                $old_path = $this->input->post('resume_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->logis_model->uploadAllType('resume');
                $val == TRUE || redirect('admin/logis/logis_details/' . $profile_info->logis_id);
                $document_data['resume_filename'] = $val['fileName'];
                $document_data['resume'] = $val['path'];
                $document_data['resume_path'] = $val['fullPath'];
            }
            // offer_letter File upload
            if (!empty($_FILES['offer_letter']['name'])) {
                $old_path = $this->input->post('offer_letter_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->logis_model->uploadAllType('offer_letter');
                $val == TRUE || redirect('admin/logis/logis_details/' . $profile_info->logis_id);
                $document_data['offer_letter_filename'] = $val['fileName'];
                $document_data['offer_letter'] = $val['path'];
                $document_data['offer_letter_path'] = $val['fullPath'];
            }
            // joining_letter File upload
            if (!empty($_FILES['joining_letter']['name'])) {
                $old_path = $this->input->post('joining_letter_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->logis_model->uploadAllType('joining_letter');
                $val == TRUE || redirect('admin/logis/logis_details/' . $profile_info->logis_id);
                $document_data['joining_letter_filename'] = $val['fileName'];
                $document_data['joining_letter'] = $val['path'];
                $document_data['joining_letter_path'] = $val['fullPath'];
            }

            // contract_paper File upload
            if (!empty($_FILES['contract_paper']['name'])) {
                $old_path = $this->input->post('contract_paper_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->logis_model->uploadAllType('contract_paper');
                $val == TRUE || redirect('admin/logis/logis_details/' . $profile_info->logis_id);
                $document_data['contract_paper_filename'] = $val['fileName'];
                $document_data['contract_paper'] = $val['path'];
                $document_data['contract_paper_path'] = $val['fullPath'];
            }
            // id_proff File upload
            if (!empty($_FILES['id_proff']['name'])) {
                $old_path = $this->input->post('id_proff_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->logis_model->uploadAllType('id_proff');
                $val == TRUE || redirect('admin/logis/logis_details/' . $profile_info->logis_id);
                $document_data['id_proff_filename'] = $val['fileName'];
                $document_data['id_proff'] = $val['path'];
                $document_data['id_proff_path'] = $val['fullPath'];
            }

            $fileName = $this->input->post('fileName');
            $path = $this->input->post('path');
            $fullPath = $this->input->post('fullPath');
            $size = $this->input->post('size');
            $is_image = $this->input->post('is_image');

            if (!empty($fileName)) {
                foreach ($fileName as $key => $name) {
                    $old['fileName'] = $name;
                    $old['path'] = $path[$key];
                    $old['fullPath'] = $fullPath[$key];
                    $old['size'] = $size[$key];
                    $old['is_image'] = $is_image[$key];
                    $result[] = $old;
                }

                $document_data['other_document'] = json_encode($result);
            }

            if (!empty($_FILES['other_document']['name']['0'])) {
                $old_path_info = $this->input->post('upload_path');
                if (!empty($old_path_info)) {
                    foreach ($old_path_info as $old_path) {
                        unlink($old_path);
                    }
                }
                $mul_val = $this->logis_model->multi_uploadAllType('other_document');
                $document_data['other_document'] = json_encode($mul_val);
            }

            if (!empty($result) && !empty($mul_val)) {
                $file = array_merge($result, $mul_val);
                $document_data['other_document'] = json_encode($file);
            }

            $document_data['logis_id'] = $profile_info->logis_id;

            $this->logis_model->_table_name = "tbl_employee_document"; // table name
            $this->logis_model->_primary_key = "document_id"; // $id
            $document_id = $this->input->post('document_id', TRUE);
            if (!empty($document_id)) {
                $this->logis_model->save($document_data, $document_id);
            } else {
                $this->logis_model->save($document_data);
            }

            $activities = array(
                'logis' => $this->session->logisdata('logis_id'),
                'module' => 'logis',
                'module_field_id' => $id,
                'activity' => 'activity_documents_update',
                'icon' => 'fa-logis',
                'value1' => $profile_info->fullname
            );
            $this->logis_model->_table_name = 'tbl_activities';
            $this->logis_model->_primary_key = "activities_id";
            $this->logis_model->save($activities);

            $message = lang('emplyee_documents_update');
            $type = 'success';
            set_message($type, $message);
            redirect('admin/logis/logis_details/' . $profile_info->logis_id . '/' . '4'); //redirect page
        } else {
            redirect('admin/logis/logis_list');
        }
    }

    public function new_bank($logis_id, $bank_id = null)
    {
        $data['title'] = lang('new_bank');

        $data['profile_info'] = $this->db->where('logis_id', $logis_id)->get('tbl_account_details')->row();
        if (!empty($bank_id)) {
            $data['bank_info'] = $this->db->where('employee_bank_id', $bank_id)->get('tbl_employee_bank')->row();
        }
        $data['modal_subview'] = $this->load->view('admin/logis/new_bank', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_bank_info($logis_id, $bank_id = null)
    {
        $edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $bank_data = $this->logis_model->array_from_post(array('bank_name', 'routing_number', 'account_name', 'account_number','type_of_account'));
            $bank_data['logis_id'] = $logis_id;
            $this->logis_model->_table_name = "tbl_employee_bank"; // table name
            $this->logis_model->_primary_key = "employee_bank_id"; // $id

            if (!empty($bank_id)) {
                $activity = 'activity_update_logis_bank';
                $msg = lang('update_bank_info');
                $this->logis_model->save($bank_data, $bank_id);
            } else {
                $activity = 'activity_new_logis_bank';
                $msg = lang('save_bank_info');
                $bank_id = $this->logis_model->save($bank_data);
            }
            $profile_info = $this->db->where('logis_id', $logis_id)->get('tbl_account_details')->row();
            $activities = array(
                'logis' => $this->session->logisdata('logis_id'),
                'module' => 'logis',
                'module_field_id' => $bank_id,
                'activity' => $activity,
                'icon' => 'fa-logis',
                'value1' => $profile_info->fullname
            );
            $this->logis_model->_table_name = 'tbl_activities';
            $this->logis_model->_primary_key = "activities_id";
            $this->logis_model->save($activities);

            $type = 'success';
            set_message($type, $msg);
            redirect('admin/logis/logis_details/' . $profile_info->logis_id . '/' . '3'); //redirect page
        } else {
            redirect('admin/logis/logis_list');
        }
    }

    public function delete_logis_bank($logis_id, $bank_id)
    {
        $this->logis_model->_table_name = "tbl_employee_bank"; // table name
        $this->logis_model->_primary_key = "employee_bank_id"; // $id
        $this->logis_model->delete($bank_id);

        $profile_info = $this->db->where('logis_id', $logis_id)->get('tbl_account_details')->row();
        $activities = array(
            'logis' => $this->session->logisdata('logis_id'),
            'module' => 'logis',
            'module_field_id' => $bank_id,
            'activity' => 'activity_delete_logis_bank',
            'icon' => 'fa-logis',
            'value1' => $profile_info->fullname
        );
        $this->logis_model->_table_name = 'tbl_activities';
        $this->logis_model->_primary_key = "activities_id";
        $this->logis_model->save($activities);
        $type = 'success';
        $msg = lang('delete_logis_bank');
        set_message($type, $msg);
        redirect('admin/logis/logis_details/' . $profile_info->logis_id);
    }

    /*     * * Save New logis ** */
    public function save_logis()
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
            $login_data = $this->logis_model->array_from_post(array('logisname', 'email', 'role_id'));
            $logis_id = $this->input->post('logis_id', true);
            // update root category
            $where = array('logisname' => $login_data['logisname']);
            $email = array('email' => $login_data['email']);
            // duplicate value check in DB
            if (!empty($logis_id)) { // if id exist in db update data
                $check_id = array('logis_id !=' => $logis_id);
            } else { // if id is not exist then set id as null
                $check_id = null;
            }
            // check whether this input data already exist or not
            $check_logis = $this->logis_model->check_update('tbl_logiss', $where, $check_id);
            $check_email = $this->logis_model->check_update('tbl_logiss', $email, $check_id);
            if (!empty($check_logis) || !empty($check_email)) { // if input data already exist show error alert
                if (!empty($check_logis)) {
                    $error = $login_data['logisname'];
                } else {
                    $error = $login_data['email'];
                }

                // massage for logis
                $type = 'error';
                $message = "<strong style='color:#000'>" . $error . '</strong>  ' . lang('already_exist');

                $password = $this->input->post('password', TRUE);
                $confirm_password = $this->input->post('confirm_password', TRUE);
                if ($password != $confirm_password) {
                    $type = 'error';
                    $message = lang('password_does_not_match');
                }
            } else { // save and update query
                $login_data['last_ip'] = $this->input->ip_address();

                if (empty($logis_id)) {
                    $password = $this->input->post('password', TRUE);
                    $login_data['password'] = $this->hash($password);
                }
                $permission = $this->input->post('permission', true);
                if (!empty($permission)) {
                    if ($permission == 'everyone') {
                        $assigned = 'all';
                    } else {
                        $assigned_to = $this->logis_model->array_from_post(array('assigned_to'));
                        if (!empty($assigned_to['assigned_to'])) {
                            foreach ($assigned_to['assigned_to'] as $assign_logis) {
                                $assigned[$assign_logis] = $this->input->post('action_' . $assign_logis, true);
                            }
                        }
                    }
                    if (!empty($assigned)) {
                        if ($assigned != 'all') {
                            $assigned = json_encode($assigned);
                        }
                    } else {
                        $assigned = 'all';
                    }
                    $login_data['permission'] = $assigned;
                } else {
                    set_message('error', lang('assigned_to') . ' Field is required');
                    redirect($_SERVER['HTTP_REFERER']);
                }
                $this->logis_model->_table_name = 'tbl_logiss'; // table name
                $this->logis_model->_primary_key = 'logis_id'; // $id
                if (!empty($logis_id)) {
                    $id = $this->logis_model->save($login_data, $logis_id);
                } else {
                    $login_data['activated'] = '1';
                    $id = $this->logis_model->save($login_data);
                }
                save_custom_field(13, $id);
                // save into tbl_account details
                $profile_data = $this->logis_model->array_from_post(array('fullname', 'employment_id', 'company', 'locale', 'language', 'phone', 'mobile', 'skype', 'designations_id', 'direction'));
                if ($login_data['role_id'] != 2) {
                    $profile_data['company'] = 0;
                }

                $account_details_id = $this->input->post('account_details_id', TRUE);
                if (!empty($_FILES['avatar']['name'])) {
                    $val = $this->logis_model->uploadImage('avatar');
                    $val == TRUE || redirect('admin/logis/logis_list');
                    $profile_data['avatar'] = $val['path'];
                }

                $profile_data['logis_id'] = $id;

                $this->logis_model->_table_name = 'tbl_account_details'; // table name
                $this->logis_model->_primary_key = 'account_details_id'; // $id
                if (!empty($account_details_id)) {
                    $this->logis_model->save($profile_data, $account_details_id);
                } else {
                    $this->logis_model->save($profile_data);
                }
                if (!empty($profile_data['designations_id'])) {
                    $desig = $this->db->where('designations_id', $profile_data['designations_id'])->get('tbl_designations')->row();
                    $department_head_id = $this->input->post('department_head_id', true);
                    if (!empty($department_head_id)) {
                        $head['department_head_id'] = $id;
                    } else {
                        $dep_head = $this->logis_model->check_by(array('departments_id' => $desig->departments_id), 'tbl_departments');

                        if (empty($dep_head->department_head_id)) {
                            $head['department_head_id'] = $id;
                        }
                    }
                    if (!empty($desig->departments_id) && !empty($head)) {
                        $this->logis_model->_table_name = "tbl_departments"; //table name
                        $this->logis_model->_primary_key = "departments_id";
                        $this->logis_model->save($head, $desig->departments_id);
                    }
                }

                $activities = array(
                    'logis' => $this->session->logisdata('logis_id'),
                    'module' => 'logis',
                    'module_field_id' => $id,
                    'activity' => 'activity_added_new_logis',
                    'icon' => 'fa-logis',
                    'value1' => $login_data['logisname']
                );
                $this->logis_model->_table_name = 'tbl_activities';
                $this->logis_model->_primary_key = "activities_id";
                $this->logis_model->save($activities);
                if (!empty($id)) {
                    $this->logis_model->_table_name = 'tbl_client_role'; //table name
                    $this->logis_model->delete_multiple(array('logis_id' => $id));
                    $all_client_menu = $this->db->get('tbl_client_menu')->result();
                    foreach ($all_client_menu as $v_client_menu) {
                        $client_role_data['menu_id'] = $this->input->post($v_client_menu->label, true);
                        if (!empty($client_role_data['menu_id'])) {
                            $client_role_data['logis_id'] = $id;
                            $this->logis_model->_table_name = 'tbl_client_role';
                            $this->logis_model->_primary_key = 'client_role_id';
                            $this->logis_model->save($client_role_data);
                        }
                    }
                }

                if (!empty($logis_id)) {
                    $message = lang('update_logis_info');
                } else {
                    $message = lang('save_logis_info');
                }
                $type = 'success';
            }
            set_message($type, $message);
        }
        redirect('admin/logis/logis_list'); //redirect page
    }

    public function send_welcome_email($id)
    {
        $logis_info = $this->db->where('logis_id', $id)->get('tbl_logiss')->row();
        $profile_info = $this->db->where('logis_id', $id)->get('tbl_account_details')->row();
        $email_template = $this->logis_model->check_by(array('email_group' => 'wellcome_email'), 'tbl_email_templates');

        $message = $email_template->template_body;
        $subject = $email_template->subject;
        $NAME = str_replace("{NAME}", $profile_info->fullname, $message);
        $URL = str_replace("{COMPANY_URL}", base_url(), $NAME);
        $message = str_replace("{COMPANY_NAME}", config_item('company_name'), $URL);

        $data['message'] = $message;
        $message = $this->load->view('email_template', $data, TRUE);

        $params['subject'] = $subject;
        $params['message'] = $message;
        $params['resourceed_file'] = '';
        $params['recipient'] = $logis_info->email;

        $this->logis_model->send_email($params);

        $type = 'success';
        $message = lang('welcome_email_success');
        set_message($type, $message);
        redirect('admin/logis/logis_list'); //redirect page
    }

    /*     * * Delete logis ** */

    public function delete_logis($id = null)
    {
        $deleted = can_action('24', 'deleted');
        if (!empty($deleted)) {
            if (!empty($id)) {
                $id = $id;
                $logis_id = $this->session->logisdata('logis_id');

                //checking login logis trying delete his own account
                if ($id == $logis_id) {
                    //same logis can not delete his own account
                    // redirect with error msg
                    $type = 'error';
                    $message = 'Sorry You can not delete your own account!';
                    set_message($type, $message);
                    redirect('admin/logis/logis_list'); //redirect page
                } else {
                    $sbtn = $this->input->post('submit', true);

                    if (!empty($sbtn)) {
                        //delete procedure run
                        // Check logis in db or not
                        $this->logis_model->_table_name = 'tbl_logiss'; //table name
                        $this->logis_model->_order_by = 'logis_id';
                        $result = $this->logis_model->get_by(array('logis_id' => $id), true);

                        if (!empty($result)) {
                            //delete logis roll id
                            $this->logis_model->_table_name = 'tbl_account_details';
                            $this->logis_model->delete_multiple(array('logis_id' => $id));//delete logis roll id

                            $cwhere = array('logis_id' => $id);
                            $this->logis_model->_table_name = 'tbl_private_chat';
                            $this->logis_model->delete_multiple($cwhere);

                            $this->logis_model->_table_name = 'tbl_private_chat_logiss';
                            $this->logis_model->delete_multiple($cwhere);

                            $this->logis_model->_table_name = 'tbl_private_chat_messages';
                            $this->logis_model->delete_multiple($cwhere);

                            $this->logis_model->_table_name = 'tbl_activities';
                            $this->logis_model->delete_multiple(array('logis' => $id));

                            $this->logis_model->_table_name = 'tbl_payments';
                            $this->logis_model->delete_multiple(array('paid_by' => $id));

                            // delete all tbl_quotations by id
                            $this->logis_model->_table_name = 'tbl_quotations';
                            $this->logis_model->_order_by = 'logis_id';
                            $quotations_info = $this->logis_model->get_by(array('logis_id' => $id), FALSE);

                            if (!empty($quotations_info)) {
                                foreach ($quotations_info as $v_quotations) {
                                    $this->logis_model->_table_name = 'tbl_quotation_details';
                                    $this->logis_model->delete_multiple(array('quotations_id' => $v_quotations->quotations_id));
                                }
                            }

                            $this->logis_model->_table_name = 'tbl_quotations';
                            $this->logis_model->delete_multiple(array('logis_id' => $id));

                            $this->logis_model->_table_name = 'tbl_quotationforms';
                            $this->logis_model->delete_multiple(array('quotations_created_by_id' => $id));

                            $this->logis_model->_table_name = 'tbl_logiss';
                            $this->logis_model->delete_multiple(array('logis_id' => $id));

                            $this->logis_model->_table_name = 'tbl_logis_role';
                            $this->logis_model->delete_multiple(array('designations_id' => $id));

                            $this->logis_model->_table_name = 'tbl_inbox';
                            $this->logis_model->delete_multiple(array('logis_id' => $id));

                            $this->logis_model->_table_name = 'tbl_sent';
                            $this->logis_model->delete_multiple(array('logis_id' => $id));

                            $this->logis_model->_table_name = 'tbl_draft';
                            $this->logis_model->delete_multiple(array('logis_id' => $id));

                            $tickets_info = $this->db->get('tbl_tickets')->result();
                            if (!empty($tickets_info)) {
                                foreach ($tickets_info as $v_tickets) {
                                    if (!empty($v_tickets->permission) && $v_tickets->permission != 'all') {
                                        $allowad_logis = json_decode($v_tickets->permission);
                                        if (!empty($allowad_logis)) {
                                            foreach ($allowad_logis as $op_logis_id => $v_logis) {
                                                if ($op_logis_id == $id || $v_tickets->reporter == $id) {
                                                    $this->logis_model->_table_name = 'tbl_tickets';
                                                    $this->logis_model->delete_multiple(array('tickets_id' => $v_tickets->tickets_id));
                                                    $this->logis_model->_table_name = 'tbl_tickets_replies';
                                                    $this->logis_model->delete_multiple(array('tickets_id' => $v_tickets->tickets_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // xpm crm
                            // delete all leads by id
                            $leads_info = $this->db->get('tbl_leads')->result();
                            if (!empty($leads_info)) {
                                foreach ($leads_info as $v_leads) {
                                    if (!empty($v_leads->permission) && $v_leads->permission != 'all') {
                                        $allowad_logis = json_decode($v_leads->permission);
                                        if (!empty($allowad_logis)) {
                                            foreach ($allowad_logis as $op_logis_id => $v_logis) {
                                                if ($op_logis_id == $id) {
                                                    //delete data into table.
                                                    $this->logis_model->_table_name = "tbl_calls"; // table name
                                                    $this->logis_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    //delete data into table.
                                                    $this->logis_model->_table_name = "tbl_mettings"; // table name
                                                    $this->logis_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    //delete data into table.
                                                    $this->logis_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->logis_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    $this->logis_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->logis_model->_order_by = "leads_id";
                                                    $files_info = $this->logis_model->get_by(array('leads_id' => $v_leads->leads_id), FALSE);

                                                    if (!empty($files_info)) {
                                                        foreach ($files_info as $v_files) {
                                                            //save data into table.
                                                            $this->logis_model->_table_name = "tbl_task_uploaded_files"; // table name
                                                            $this->logis_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                        }
                                                    }
                                                    //save data into table.
                                                    $this->logis_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->logis_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    $this->logis_model->_table_name = "tbl_task"; // table name
                                                    $this->logis_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    $this->logis_model->_table_name = 'tbl_leads';
                                                    $this->logis_model->_primary_key = 'leads_id';
                                                    $this->logis_model->delete($v_leads->leads_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //save data into table.
                            $this->logis_model->_table_name = "tbl_milestones"; // table name
                            $this->logis_model->delete_multiple(array('logis_id' => $id));
                            // todo
                            $this->logis_model->_table_name = "tbl_todo"; // table name
                            $this->logis_model->delete_multiple(array('logis_id' => $id));

                            // opportunity
                            $oppurtunity_info = $this->db->get('tbl_opportunities')->result();
                            if (!empty($oppurtunity_info)) {
                                foreach ($oppurtunity_info as $v_oppurtunity) {
                                    if (!empty($v_oppurtunity->permission) && $v_oppurtunity->permission != 'all') {
                                        $allowad_logis = json_decode($v_oppurtunity->permission);
                                        if (!empty($allowad_logis)) {
                                            foreach ($allowad_logis as $op_logis_id => $v_logis) {
                                                if ($op_logis_id == $id)
                                                    //delete data into table.
                                                    $this->logis_model->_table_name = "tbl_calls"; // table name
                                                $this->logis_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //delete data into table.
                                                $this->logis_model->_table_name = "tbl_mettings"; // table name
                                                $this->logis_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //delete data into table.
                                                $this->logis_model->_table_name = "tbl_task_comment"; // table name
                                                $this->logis_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                $this->logis_model->_table_name = "tbl_task_attachment"; //table name
                                                $this->logis_model->_order_by = "task_id";
                                                $files_info = $this->logis_model->get_by(array('opportunities_id' => $v_oppurtunity->opportunities_id), FALSE);
                                                if (!empty($files_info)) {
                                                    foreach ($files_info as $v_files) {
                                                        //save data into table.
                                                        $this->logis_model->_table_name = "tbl_task_uploaded_files"; // table name
                                                        $this->logis_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                    }
                                                }
                                                //save data into table.
                                                $this->logis_model->_table_name = "tbl_task_attachment"; // table name
                                                $this->logis_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //save data into table.
                                                $this->logis_model->_table_name = "tbl_task"; // table name
                                                $this->logis_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //save data into table.
                                                $this->logis_model->_table_name = "tbl_bug"; // table name
                                                $this->logis_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                $this->logis_model->_table_name = 'tbl_opportunities';
                                                $this->logis_model->_primary_key = 'opportunities_id';
                                                $this->logis_model->delete($v_oppurtunity->opportunities_id);
                                            }
                                        }
                                    }
                                }
                            }
                            // project
                            $project_info = $this->db->get('tbl_project')->result();
                            if (!empty($project_info)) {
                                foreach ($project_info as $v_project) {
                                    if (!empty($v_project->permission) && $v_project->permission != 'all') {
                                        $allowad_logis = json_decode($v_project->permission);
                                        if (!empty($allowad_logis)) {
                                            foreach ($allowad_logis as $logis_id => $v_logis) {
                                                if ($logis_id == $id) {
                                                    //delete data into table.
                                                    $this->logis_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->logis_model->delete_multiple(array('project_id' => $v_project->project_id));

                                                    $this->logis_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->logis_model->_order_by = "task_id";
                                                    $files_info = $this->logis_model->get_by(array('project_id' => $v_project->project_id), FALSE);
                                                    if (!empty($files_info)) {
                                                        foreach ($files_info as $v_files) {
                                                            //save data into table.
                                                            $this->logis_model->_table_name = "tbl_task_uploaded_files"; // table name
                                                            $this->logis_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                        }
                                                    }
                                                    //save data into table.
                                                    $this->logis_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->logis_model->delete_multiple(array('project_id' => $v_project->project_id));

                                                    //save data into table.
                                                    $this->logis_model->_table_name = "tbl_milestones"; // table name
                                                    $this->logis_model->delete_multiple(array('project_id' => $v_project->project_id));

                                                    // tasks
                                                    $taskss_info = $this->db->where('project_id', $v_project->project_id)->get('tbl_task')->result();
                                                    if (!empty($taskss_info)) {
                                                        foreach ($taskss_info as $v_taskss) {
                                                            if (!empty($v_taskss->permission) && $v_taskss->permission != 'all') {
                                                                $allowad_logis = json_decode($v_taskss->permission);
                                                                if (!empty($allowad_logis)) {
                                                                    foreach ($allowad_logis as $task_logis_id => $v_logis) {
                                                                        if ($task_logis_id == $id) {

                                                                            $this->logis_model->_table_name = "tbl_task_attachment"; //table name
                                                                            $this->logis_model->_order_by = "task_id";
                                                                            $files_info = $this->logis_model->get_by(array('task_id' => $v_taskss->task_id), FALSE);
                                                                            foreach ($files_info as $v_files) {
                                                                                $this->logis_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                                                $this->logis_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                                            }
                                                                            //delete into table.
                                                                            $this->logis_model->_table_name = "tbl_task_attachment"; // table name
                                                                            $this->logis_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                                            //delete data into table.
                                                                            $this->logis_model->_table_name = "tbl_task_comment"; // table name
                                                                            $this->logis_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                                            $this->logis_model->_table_name = "tbl_task"; // table name
                                                                            $this->logis_model->_primary_key = "task_id"; // $id
                                                                            $this->logis_model->delete($v_taskss->task_id);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    // Bugs
                                                    $bugs_info = $this->db->where('project_id', $v_project->project_id)->get('tbl_bug')->result();
                                                    if (!empty($bugs_info)) {
                                                        foreach ($bugs_info as $v_bugs) {
                                                            if (!empty($v_bugs->permission) && $v_bugs->permission != 'all') {
                                                                $allowad_logis = json_decode($v_bugs->permission);
                                                                if (!empty($allowad_logis)) {
                                                                    foreach ($allowad_logis as $bugs_logis_id => $v_logis) {
                                                                        if ($bugs_logis_id == $id) {

                                                                            $this->logis_model->_table_name = "tbl_task_attachment"; //table name
                                                                            $this->logis_model->_order_by = "bug_id";
                                                                            $files_info = $this->logis_model->get_by(array('bug_id' => $v_bugs->bug_id), FALSE);
                                                                            foreach ($files_info as $v_files) {
                                                                                $this->logis_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                                                $this->logis_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                                            }
                                                                            //delete into table.
                                                                            $this->logis_model->_table_name = "tbl_task_attachment"; // table name
                                                                            $this->logis_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                                            //delete data into table.
                                                                            $this->logis_model->_table_name = "tbl_task_comment"; // table name
                                                                            $this->logis_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                                            //delete data into table.
                                                                            $this->logis_model->_table_name = "tbl_task"; // table name
                                                                            $this->logis_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                                            $this->logis_model->_table_name = "tbl_bug"; // table name
                                                                            $this->logis_model->_primary_key = "bug_id"; // $id
                                                                            $this->logis_model->delete($v_bugs->bug_id);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    $this->logis_model->_table_name = 'tbl_project';
                                                    $this->logis_model->_primary_key = 'project_id';
                                                    $this->logis_model->delete($v_project->project_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // tasks
                            $taskss_info = $this->db->get('tbl_task')->result();
                            if (!empty($taskss_info)) {
                                foreach ($taskss_info as $v_taskss) {
                                    if (!empty($v_taskss->permission) && $v_taskss->permission != 'all') {
                                        $allowad_logis = json_decode($v_taskss->permission);
                                        if (!empty($allowad_logis)) {
                                            foreach ($allowad_logis as $task_logis_id => $v_logis) {
                                                if ($task_logis_id == $id) {

                                                    $this->logis_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->logis_model->_order_by = "task_id";
                                                    $files_info = $this->logis_model->get_by(array('task_id' => $v_taskss->task_id), FALSE);
                                                    foreach ($files_info as $v_files) {
                                                        $this->logis_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                        $this->logis_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                    }
                                                    //delete into table.
                                                    $this->logis_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->logis_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                    //delete data into table.
                                                    $this->logis_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->logis_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                    $this->logis_model->_table_name = "tbl_task"; // table name
                                                    $this->logis_model->_primary_key = "task_id"; // $id
                                                    $this->logis_model->delete($v_taskss->task_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // Bugs
                            $bugs_info = $this->db->get('tbl_bug')->result();
                            if (!empty($bugs_info)) {
                                foreach ($bugs_info as $v_bugs) {
                                    if (!empty($v_bugs->permission) && $v_bugs->permission != 'all') {
                                        $allowad_logis = json_decode($v_bugs->permission);
                                        if (!empty($allowad_logis)) {
                                            foreach ($allowad_logis as $bugs_logis_id => $v_logis) {
                                                if ($bugs_logis_id == $id) {

                                                    $this->logis_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->logis_model->_order_by = "bug_id";
                                                    $files_info = $this->logis_model->get_by(array('bug_id' => $v_bugs->bug_id), FALSE);
                                                    foreach ($files_info as $v_files) {
                                                        $this->logis_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                        $this->logis_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                    }
                                                    //delete into table.
                                                    $this->logis_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->logis_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                    //delete data into table.
                                                    $this->logis_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->logis_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                    //delete data into table.
                                                    $this->logis_model->_table_name = "tbl_task"; // table name
                                                    $this->logis_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                    $this->logis_model->_table_name = "tbl_bug"; // table name
                                                    $this->logis_model->_primary_key = "bug_id"; // $id
                                                    $this->logis_model->delete($v_bugs->bug_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // tbl_invoices
                            $invoices_info = $this->db->get('tbl_invoices')->result();
                            if (!empty($invoices_info)) {
                                foreach ($invoices_info as $v_invoices) {
                                    if (!empty($v_invoices->permission) && $v_invoices->permission != 'all') {
                                        $allowad_logis = json_decode($v_invoices->permission);
                                        if (!empty($allowad_logis)) {
                                            foreach ($allowad_logis as $invoice_logis_id => $v_logis) {
                                                if ($invoice_logis_id == $id) {
                                                    $this->logis_model->_table_name = "tbl_invoices"; // table name
                                                    $this->logis_model->_primary_key = "invoices_id"; // $id
                                                    $this->logis_model->delete($v_invoices->invoices_id);

                                                    $this->logis_model->_table_name = "tbl_items"; // table name
                                                    $this->logis_model->delete_multiple(array('invoices_id' => $v_invoices->invoices_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // tbl_estimates
                            $estimate_info = $this->db->get('tbl_estimates')->result();
                            if (!empty($estimate_info)) {
                                foreach ($estimate_info as $v_estimate) {
                                    if (!empty($v_estimate->permission) && $v_estimate->permission != 'all') {
                                        $allowad_logis = json_decode($v_estimate->permission);
                                        if (!empty($allowad_logis)) {
                                            foreach ($allowad_logis as $estimate_logis_id => $v_logis) {
                                                if ($estimate_logis_id == $id) {
                                                    $this->logis_model->_table_name = "tbl_estimates"; // table name
                                                    $this->logis_model->_primary_key = "estimates_id"; // $id
                                                    $this->logis_model->delete($v_estimate->estimates_id);

                                                    $this->logis_model->_table_name = "tbl_estimate_items"; // table name
                                                    $this->logis_model->delete_multiple(array('estimates_id' => $v_estimate->estimates_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // 	tbl_tax_rates
                            $tax_rate_info = $this->db->get('tbl_tax_rates')->result();
                            if (!empty($tax_rate_info)) {
                                foreach ($tax_rate_info as $v_tax_rat) {
                                    if (!empty($v_tax_rat->permission) && $v_tax_rat->permission != 'all') {
                                        $allowad_logis = json_decode($v_tax_rat->permission);
                                        if (!empty($allowad_logis)) {
                                            foreach ($allowad_logis as $tax_rate_logis_id => $v_logis) {
                                                if ($tax_rate_logis_id == $id) {
                                                    $this->logis_model->_table_name = "tbl_tax_rates"; // table name
                                                    $this->logis_model->_primary_key = "tax_rates_id"; // $id
                                                    $this->logis_model->delete($v_tax_rat->tax_rates_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $transactions_info = $this->db->get('tbl_transactions')->result();
                            if (!empty($transactions_info)) {
                                foreach ($transactions_info as $v_transactions) {
                                    if (!empty($v_transactions->permission) && $v_transactions->permission != 'all') {
                                        $allowad_logis = json_decode($v_transactions->permission);
                                        if (!empty($allowad_logis)) {
                                            foreach ($allowad_logis as $trsn_logis_id => $v_logis) {
                                                if ($trsn_logis_id == $id) {
                                                    $this->logis_model->_table_name = 'tbl_transactions';
                                                    $this->logis_model->delete_multiple(array('transactions_id' => $v_transactions->transactions_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $transfer_info = $this->db->get('tbl_transfer')->result();
                            if (!empty($transfer_info)) {
                                foreach ($transfer_info as $v_transfer) {
                                    if (!empty($v_transfer->permission) && $v_transfer->permission != 'all') {
                                        $allowad_logis = json_decode($v_transfer->permission);
                                        if (!empty($allowad_logis)) {
                                            foreach ($allowad_logis as $trfr_logis_id => $v_logis) {
                                                if ($trfr_logis_id == $id) {
                                                    $this->logis_model->_table_name = 'tbl_transfer';
                                                    $this->logis_model->delete_multiple(array('transfer_id' => $v_transfer->transfer_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //redirect successful msg
                            $type = 'success';
                            $message = 'logis Delete Successfully!';
                        } else {
                            //redirect error msg
                            $type = 'error';
                            $message = 'Sorry this logis not find in database!';

                        }
                        set_message($type, $message);
                        redirect('admin/logis/logis_list'); //redirect page
                    } else {
                        $data['title'] = "Delete logiss"; //Page title
                        $data['logis_info'] = $this->db->where('logis_id', $id)->get('tbl_account_details')->row();
                        $data['subview'] = $this->load->view('admin/logis/delete_logis', $data, TRUE);
                        $this->load->view('admin/_layout_main', $data); //page load
                    }
                }
            } else {
                redirect('admin/logis/logis_list'); //redirect page
            }
        }
    }

    public function change_status($flag, $id)
    {
        $can_edit = $this->logis_model->can_action('tbl_logiss', 'edit', array('logis_id' => $id));
        $edited = can_action('24', 'edited');
        if (!empty($can_edit) && !empty($edited)) {
            $logis_info = $this->db->where('logis_id', $id)->get('tbl_logiss')->row();
            // if flag == 1 it is active logis else deactive logis
            if ($flag == 1) {
                $msg = 'Active';
            } else {
                $msg = 'Deactive';
            }
            $where = array('logis_id' => $id);
            $action = array('activated' => $flag);
            $this->logis_model->set_action($where, $action, 'tbl_logiss');

            $activities = array(
                'logis' => $this->session->logisdata('logis_id'),
                'module' => 'logis',
                'module_field_id' => $id,
                'activity' => 'activity_change_status',
                'icon' => 'fa-logis',
                'value1' => $logis_info->logisname . ' ' . $msg,
            );
            $this->logis_model->_table_name = 'tbl_activities';
            $this->logis_model->_primary_key = "activities_id";
            $this->logis_model->save($activities);

            $type = "success";
            $message = "logis " . $msg . " Successfully!";
        } else {
            $type = 'error';
            $message = lang('there_in_no_value');
        }
        echo json_encode(array("status" => $type, "message" => $message));
        exit();
    }

    public function set_banned($flag, $id)
    {
        $can_edit = $this->logis_model->can_action('tbl_logiss', 'edit', array('logis_id' => $id));
        $edited = can_action('24', 'edited');
        if (!empty($can_edit) && !empty($edited)) {
            if ($flag == 1) {
                $msg = lang('banned');
                $action = array('activated' => 0, 'banned' => $flag, 'ban_reason' => $this->input->post('ban_reason', TRUE));
            } else {
                $msg = lang('unbanned');
                $action = array('activated' => 1, 'banned' => $flag);
            }
            $where = array('logis_id' => $id);

            $this->logis_model->set_action($where, $action, 'tbl_logiss');

            $activities = array(
                'logis' => $this->session->logisdata('logis_id'),
                'module' => 'logis',
                'module_field_id' => $id,
                'activity' => 'activity_change_status',
                'icon' => 'fa-logis',
                'value1' => $flag,
            );
            $this->logis_model->_table_name = 'tbl_activities';
            $this->logis_model->_primary_key = "activities_id";
            $this->logis_model->save($activities);

            $type = "success";
            $message = "logis " . $msg . " Successfully!";
        } else {
            $type = 'error';
            $message = lang('there_in_no_value');
        }
        set_message($type, $message);
        redirect('admin/logis/logis_list'); //redirect page
    }

    public function change_banned($id)
    {

        $data['logis_id'] = $id;
        $data['modal_subview'] = $this->load->view('admin/logis/_modal_banned_reson', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);

    }

    public function hash($string)
    {
        return hash('sha512', $string . config_item('encryption_key'));
    }

// crud for sidebar todo list
    function todo($task = '', $todo_id = '', $swap_with = '')
    {
        if ($task == 'add') {
            $this->add_todo();
        }
        if ($task == 'reload_incomplete_todo') {
            $this->get_incomplete_todo();
        }
        if ($task == 'mark_as_done') {
            $this->mark_todo_as_done($todo_id);
        }
        if ($task == 'mark_as_undone') {
            $this->mark_todo_as_undone($todo_id);
        }
        if ($task == 'swap') {

            $this->swap_todo($todo_id, $swap_with);
        }
        if ($task == 'delete') {
            $this->delete_todo($todo_id);
        }
        $todo['opened'] = 1;
        $this->session->set_logisdata($todo);
        redirect('admin/dashboard/');
    }

    function add_todo()
    {
        $data['title'] = $this->input->post('title');
        $data['logis_id'] = $this->session->logisdata('logis_id');

        $this->db->insert('tbl_todo', $data);
        $todo_id = $this->db->insert_id();

        $data['order'] = $todo_id;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function mark_todo_as_done($todo_id = '')
    {
        $data['status'] = 1;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function mark_todo_as_undone($todo_id = '')
    {
        $data['status'] = 0;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function swap_todo($todo_id = '', $swap_with = '')
    {
        $counter = 0;
        $temp_order = $this->db->get_where('tbl_todo', array('todo_id' => $todo_id))->row()->order;
        $logis = $this->session->logisdata('logis_id');

        // Move current todo up.
        if ($swap_with == 'up') {
            // Fetch all todo lists of current logis in ascending order.
            $this->db->order_by('order', 'ASC');
            $todo_lists = $this->db->get_where('tbl_todo', array('logis_id' => $logis))->result_array();
            $array_length = count($todo_lists);

            // Create separate array for orders and todo_id's from above array.
            foreach ($todo_lists as $todo_list) {
                $id_list[] = $todo_list['todo_id'];
                $order_list[] = $todo_list['order'];
            }
        }

        // Move current todo down.
        if ($swap_with == 'down') {

            // Fetch all todo lists of current logis in descending order.
            $this->db->order_by('order', 'DESC');
            $todo_lists = $this->db->get_where('tbl_todo', array('logis_id' => $logis))->result_array();
            $array_length = count($todo_lists);

            // Create separate array for orders and todo_id's from above array.
            foreach ($todo_lists as $todo_list) {
                $id_list[] = $todo_list['todo_id'];
                $order_list[] = $todo_list['order'];
            }
        }

        // Swap orders between current and next/previous todo.
        for ($i = 0; $i < $array_length; $i++) {
            if ($temp_order == $order_list[$i]) {
                if ($counter > 0) {
                    $swap_order = $order_list[$i - 1];
                    $swap_id = $id_list[$i - 1];

                    // Update order of current todo.
                    $data['order'] = $swap_order;
                    $this->db->where('todo_id', $todo_id);
                    $this->db->update('tbl_todo', $data);

                    // Update order of next/previous todo.
                    $data['order'] = $temp_order;
                    $this->db->where('todo_id', $swap_id);
                    $this->db->update('tbl_todo', $data);
                }
            } else
                $counter++;
        }
    }

    function delete_todo($todo_id = '')
    {
        $this->db->where('todo_id', $todo_id);
        $this->db->delete('tbl_todo');
    }

    function get_incomplete_todo()
    {
        $logis = $this->session->logisdata('logis_id');
        $this->db->where('logis_id', $logis);
        $this->db->where('status', 0);
        $query = $this->db->get('tbl_todo');

        $incomplete_todo_number = $query->num_rows();
        if ($incomplete_todo_number > 0) {
            echo '<span class="badge badge-secondary">';
            echo $incomplete_todo_number;
            echo '</span>';
        }
    }

    public function reset_password($id)
    {
        if ($this->session->logisdata('logis_type') == 1) {
            $new_password = $this->input->post('password', true);
            $old_password = $this->input->post('old_password', true);
            if (!empty($new_password)) {
                $email = $this->session->logisdata('email');
                $logis_info = $this->db->where('logis_id', $id)->get('tbl_logiss')->row();
                $old_password = $this->logis_model->hash($old_password);
                if ($logis_info->password == $old_password) {
                    $where = array('logis_id' => $id);
                    $action = array('password' => $this->logis_model->hash($new_password));
                    $this->logis_model->set_action($where, $action, 'tbl_logiss');
                    $login_details = $this->db->where('logis_id', $id)->get('tbl_logiss')->row();
                    $activities = array(
                        'logis' => $this->session->logisdata('logis_id'),
                        'module' => 'logis',
                        'module_field_id' => $id,
                        'activity' => 'activity_reset_password',
                        'icon' => 'fa-logis',
                        'value1' => $login_details->logisname,
                    );

                    $this->logis_model->_table_name = 'tbl_activities';
                    $this->logis_model->_primary_key = "activities_id";
                    $this->logis_model->save($activities);

                    $this->send_email_reset_password($email, $logis_info, $new_password);

                    $type = "success";
                    $message = lang('message_new_password_sent');
                } else {
                    $type = "error";
                    $message = lang('password_does_not_match');
                }
                set_message($type, $message);
                redirect('admin/logis/logis_details/' . $id); //redirect page

            } else {
                $data['title'] = lang('see_password');
                $data['logis_info'] = $this->db->where('logis_id', $id)->get('tbl_logiss')->row();
                $data['subview'] = $this->load->view('admin/settings/reset_password', $data, FALSE);
                $this->load->view('admin/_layout_modal', $data);
            }

        } else {
            $type = 'error';
            $message = lang('there_in_no_value');
            set_message($type, $message);
            redirect('admin/logis/logis_list'); //redirect page
        }


    }

    function send_email_reset_password($email, $logis_info, $password)
    {
        $email_template = $this->logis_model->check_by(array('email_group' => 'reset_password'), 'tbl_email_templates');
        $message = $email_template->template_body;
        $subject = $email_template->subject;

        $logisname = str_replace("{logisNAME}", $logis_info->logisname, $message);
        $logis_email = str_replace("{EMAIL}", $logis_info->email, $logisname);
        $logis_password = str_replace("{NEW_PASSWORD}", $password, $logis_email);
        $message = str_replace("{SITE_NAME}", config_item('company_name'), $logis_password);
        $params['recipient'] = $email;
        $params['subject'] = '[ ' . config_item('company_name') . ' ]' . $subject;
        $params['message'] = $message;
        $params['resourceed_file'] = '';
        $this->logis_model->send_email($params);
    }

}
