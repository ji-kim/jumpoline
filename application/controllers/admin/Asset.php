<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Asset extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('asset_model');
        $this->load->model('utilities_model');
    }

    public function buildChild($parent, $menu)
    {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

	// 등록대기차량
	public function car_ready_list($action = NULL, $id = NULL)
	{
        $tr_id = $id;
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
        //if (empty($data['ws_co_id'])) $data['ws_co_id'] = "1413";
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
       // if (empty($data['ws_co_name'])) $data['ws_co_name'] = "케이티지엘에스(주)";
		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);

        if ($action == 'add') {
            $data['active'] = 2;
            $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '차량정보등록대기현황';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at, tbl_members mb'; //table name
        $this->asset_model->db->select('at.*, mb.co_name, mb.ceo, mb.driver');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('TRIM(at.baecha_co)', TRIM($this->input->post('baecha_co_id', true)));
        $this->asset_model->db->where('mb.dp_id = at.owner_id');
        $this->asset_model->db->where('at.is_deleted', 'N')->where('at.car_status', 'R');
		$this->asset_model->_order_by = 'car_1';
		if(!empty($data['ws_co_id'])) {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}

		if($data['ws_co_id'] == "etc") {
			//$this->asset_model->db->where_not_in('at.ws_co_id', '1413')->where_not_in('at.ws_co_id', '1414')->where_not_in('at.ws_co_id', '1415')->where_not_in('at.ws_co_id', '1903');
		//} else {
		}
        $data['all_car_info'] = $this->asset_model->get();
        $data['total_cnt'] = count($data['all_car_info']);

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();

        $data['subview'] = $this->load->view('admin/asset/car_ready_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

	public function car_gongt_list($action = NULL, $id = NULL)
	{
        $tr_id = $id;
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
        //if (empty($data['ws_co_id'])) $data['ws_co_id'] = "1413";
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
       // if (empty($data['ws_co_name'])) $data['ws_co_name'] = "케이티지엘에스(주)";
		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);

        if ($action == 'add') {
            $data['active'] = 2;
            $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '공T현황';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at, tbl_members mb'; //table name
        $this->asset_model->db->select('at.*, mb.co_name, mb.ceo, mb.driver');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('TRIM(at.baecha_co)', TRIM($this->input->post('baecha_co_id', true)));
        $this->asset_model->db->where('mb.dp_id = at.owner_id');
        $this->asset_model->db->where('at.is_deleted', 'N')->where('at.car_status', 'G');
		$this->asset_model->_order_by = 'car_1';
		if(!empty($data['ws_co_id'])) {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}

		if($data['ws_co_id'] == "etc") {
			//$this->asset_model->db->where_not_in('at.ws_co_id', '1413')->where_not_in('at.ws_co_id', '1414')->where_not_in('at.ws_co_id', '1415')->where_not_in('at.ws_co_id', '1903');
		//} else {
		}
        $data['all_car_info'] = $this->asset_model->get();
        $data['total_cnt'] = count($data['all_car_info']);

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();

        $data['subview'] = $this->load->view('admin/asset/car_gongt_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

	public function car_return_list($action = NULL, $id = NULL)
	{
        $tr_id = $id;
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
        if (empty($data['ws_co_id'])) $data['ws_co_id'] = "1413";
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
        if (empty($data['ws_co_name'])) $data['ws_co_name'] = "케이티지엘에스(주)";
		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '번호반납요청현황';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at, tbl_members mb'; //table name
        $this->asset_model->db->select('at.*, mb.co_name, mb.ceo, mb.driver');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('TRIM(at.baecha_co)', TRIM($this->input->post('baecha_co_id', true)));
        $this->asset_model->db->where('mb.dp_id = at.owner_id');
        $this->asset_model->db->where('at.is_deleted', 'N')->where('at.return_ready', 'Y');
		$this->asset_model->_order_by = 'car_1';
		if($data['ws_co_id'] == "etc") {
			$this->asset_model->db->where_not_in('at.ws_co_id', '1413')->where_not_in('at.ws_co_id', '1414')->where_not_in('at.ws_co_id', '1415')->where_not_in('at.ws_co_id', '1903');
		} else {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}
        $data['all_car_info'] = $this->asset_model->get();

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();

        $data['subview'] = $this->load->view('admin/asset/car_return_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
	}

	//공티설정
    public function set_gongT($id)
    {
        $data['assign_user'] = $this->asset_model->allowad_user('57');
        $data['tr_info'] = $this->asset_model->check_by(array('idx' => $id), 'tbl_asset_truck');
        $data['modal_subview'] = $this->load->view('admin/asset/_modal_gongT', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

	//번호반납설정
    public function set_rtplate($id)
    {
        $data['assign_user'] = $this->asset_model->allowad_user('57');
        $data['tr_info'] = $this->asset_model->check_by(array('idx' => $id), 'tbl_asset_truck');
        $data['modal_subview'] = $this->load->view('admin/asset/_modal_rtplate', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

	//팝업 차량 선택
    public function select_truck($params = NULL, $page = NULL)
    {

		$data['params'] = $params;
		$data['page'] = $page;
		$data['search_field']		= $this->input->post('search_field', true);
		$data['search_keyword']		= $this->input->post('search_keyword', true);
		$data['car_status']			= $this->input->post('car_status', true);
		if(empty($data['car_status'])) $data['car_status'] = "A";

		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($limit)) $limit = 20;

        $this->asset_model->_table_name = 'tbl_asset_truck'; //table name
        $this->asset_model->db->where('tbl_asset_truck.is_deleted', 'N');
        
		if(!empty($data['search_field']) && !empty($data['search_keyword']))
			$this->asset_model->db->where('tbl_asset_truck.'.$data['search_field'].' like ', '%'.$data['search_keyword'].'%');
		if(!empty($data['car_status']))	$this->asset_model->db->where('tbl_asset_truck.car_status', $data['car_status']);
		$data['total_count'] = count($this->asset_model->get());

		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함

        $this->asset_model->_table_name = 'tbl_asset_truck'; //table name
        $this->asset_model->_order_by = 'idx';
        $this->asset_model->db->where('tbl_asset_truck.is_deleted', 'N')->where('tbl_asset_truck.car_status <> \'L\'');

		if(!empty($data['search_field']) && !empty($data['search_keyword']))
			$this->asset_model->db->where('tbl_asset_truck.'.$data['search_field'].' like ', '%'.$data['search_keyword'].'%');
		if(!empty($data['car_status']))	$this->asset_model->db->where('tbl_asset_truck.car_status', $data['car_status']);
		$this->asset_model->db->limit($limit, $data['from_record']);
        $data['all_truck_group'] = $this->asset_model->get();

		$data['title'] = '차량 선택';
		$data['subview'] = $this->load->view('admin/asset/pop_truck_list', $data, true);
        $this->load->view('admin/_layout_pop_truck_list', $data);
    }


	//차량정보 업데이트 창
    public function pop_carinfo($md = 'car_info', $tr_id, $action = NULL, $page = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
		$data['md'] = $md;
		$data['tr_id'] = $tr_id;
		$data['page'] = $page;

		$this->asset_model->_table_name = 'tbl_asset_truck'; //table name
		$this->asset_model->db->where('tbl_asset_truck.idx', $tr_id);
		$data['tr'] = $this->asset_model->get();


/*
		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($limit)) $limit = 20;

		//거래처
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->db->where('tbl_members.mb_type', 'customer');
        $this->asset_model->_order_by = 'co_name';
        $data['all_customer_group'] = $this->asset_model->get();

		//지입회사
        $this->asset_model->db->select('distinct(X)');
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->db->where('tbl_members.mb_type', 'partner');
        $this->asset_model->_order_by = 'X';
        $data['all_jiip_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->db->where('tbl_members.mb_type', $co_type);
		$data['total_count'] = count($this->asset_model->get());

		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함

        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'E';
        $this->asset_model->db->where('tbl_members.mb_type', $co_type);
		$this->asset_model->db->limit($limit, $data['from_record']);

        $data['all_company_info'] = $this->asset_model->get();
*/
		if($md == "car_info") {
			if ($action == 'edit_car_info') {
				$data['active'] = 2;
			} else {
				$data['active'] = 1;
			}
			// 최초 데이터 히스토리 삽입

			$this->asset_model->_table_name = 'tbl_asset_truck_info'; //table name
			$this->asset_model->db->where('tbl_asset_truck_info.tr_id', $tr_id);
			$chk = count($this->asset_model->get());
			if($chk==0) { // 이력이 등록된게 없을때 최초가 기본이력으로 등록
                $master = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();

				$sql_add = "";
				if(!empty($master->owner_id)) $sql_add .= ", owner_id = '".$master->owner_id."'";
				if(!empty($master->owner_name)) $sql_add .= ", owner_name = '".$master->owner_name."'";
				if(!empty($master->driver_name)) $sql_add .= ", driver_name = '".$master->driver_name."'";
				if(!empty($master->car_1)) $sql_add .= ", car_1 = '".$master->car_1."'";
				if(!empty($master->car_7)) $sql_add .= ", car_7 = '".$master->car_7."'";
				if(!empty($master->car_3)) $sql_add .= ", car_3 = '".$master->car_3."'";
				if(!empty($master->car_4)) $sql_add .= ", car_4 = '".$master->car_4."'";
				if(!empty($master->mode)) $sql_add .= ", mode = '".$master->mode."'";
				if(!empty($master->car_5)) $sql_add .= ", car_5 = '".$master->car_5."'";
				if(!empty($master->type)) $sql_add .= ", type = '".$master->type."'";
				if(!empty($master->motor_mode)) $sql_add .= ", motor_mode = '".$master->motor_mode."'";
				if(!empty($master->headquarter)) $sql_add .= ", headquarter = '".$master->headquarter."'";
				if(!empty($master->length)) $sql_add .= ", length = '".$master->length."'";
				if(!empty($master->width)) $sql_add .= ", width = '".$master->width."'";
				if(!empty($master->height)) $sql_add .= ", height = '".$master->height."'";
				if(!empty($master->max_load)) $sql_add .= ", max_load = '".$master->max_load."'";
				if(!empty($master->fuel_type)) $sql_add .= ", fuel_type = '".$master->fuel_type."'";
				if(!empty($master->carinfo_9)) $sql_add .= ", carinfo_9 = '".$master->carinfo_9."'";
				if(!empty($master->carinfo_11)) $sql_add .= ", carinfo_11 = '".$master->carinfo_11."'";
				if(!empty($master->baecha_co_id)) $sql_add .= ", baecha_co_id = '".$master->baecha_co_id."'";
				if(!empty($master->baecha_co_name)) $sql_add .= ", baecha_co_name = '".$master->baecha_co_name."'";
				if(!empty($master->ws_co_id)) $sql_add .= ", ws_co_id = '".$master->ws_co_id."'";
				if(!empty($master->ws_co_name)) $sql_add .= ", ws_co_name = '".$master->ws_co_name."'";
				if(!empty($master->inv_co_id)) $sql_add .= ", inv_co_id = '".$master->inv_co_id."'";
				if(!empty($master->inv_co_name)) $sql_add .= ", inv_co_name = '".$master->inv_co_name."'";
			
				$sql = "INSERT INTO tbl_asset_truck_info  SET tr_id='$tr_id', reason = '최초등록'".$sql_add;
				$sql .= ",reg_datetime=now(), is_master='Y'";
				$this->db->query($sql);
				$data['qry'] = $sql;
			}

       // $this->asset_model->_table_name = 'tbl_asset_truck'; //table name
	//	$this->asset_model->db->where('tbl_asset_truck.idx', $tr_id);
     //   $this->asset_model->_order_by = 'idx';
        //$data['cur_info'] = $this->asset_model->get();
			//$data['test'] = $cur_info['car_1'].'차랑정보관리';
			$data['title'] = '차랑정보관리';
			$this->asset_model->_table_name = 'tbl_asset_truck_info'; //table name
			$this->asset_model->db->where('tbl_asset_truck_info.tr_id', $tr_id); //->where('tbl_asset_truck_info.isq_master', 'Y');
			$this->asset_model->_order_by = 'change_date';
			$data['all_info_group'] = $this->asset_model->get();
		} else if($md == "car_ins") { 
			if ($action == 'edit_ins_info') {
				$data['spay_cnt']		= $this->input->post('spay_cnt', true);
				if(!empty($data['spay_cnt'])) {
					$data['ins_co_id']		= $this->input->post('ins_co_id', true);
				} 
				
				$data['ins_id']		= $this->input->post('ins_id', true);
				if(!empty($data['ins_id'])) {
					$data['ins'] = $this->db->where('idx', $data['ins_id'])->get('tbl_asset_truck_insur')->row();
					$data['ins_co_id']		= $data['ins']->ins_co_id;
				}

				$data['active'] = 2;
			} else {
				$data['active'] = 1;
			}

			$data['title'] = '자동차보험관리';
			$this->asset_model->_table_name = 'tbl_asset_truck_insur'; //table name
			$this->asset_model->db->where('tbl_asset_truck_insur.tr_id', $tr_id)->where('tbl_asset_truck_insur.type', '자동차');
			$this->asset_model->_order_by = 'end_date desc';
			$data['all_ins_group'] = $this->asset_model->get();
		} else if($md == "car_lins") {

			if ($action == 'car_lins_edit') {
				$data['spay_cnt']		= $this->input->post('spay_cnt', true);
				if(!empty($data['spay_cnt'])) {
					$data['ins_co_id']		= $this->input->post('ins_co_id', true);
				} 
				
				$data['lins_id']		= $this->input->post('lins_id', true);
				if(!empty($data['lins_id'])) {
					$data['lins'] = $this->db->where('idx', $data['lins_id'])->get('tbl_asset_truck_insur')->row();
					$data['ins_co_id']		= $data['lins']->ins_co_id;
				}

				$data['active'] = 2;
			} else {
				$data['active'] = 1;
			}

			$data['title'] = '적재물보험관리';
			$this->asset_model->_table_name = 'tbl_asset_truck_insur'; //table name
			$this->asset_model->db->where('tbl_asset_truck_insur.tr_id', $tr_id)->where('tbl_asset_truck_insur.type', '적재물');
			$this->asset_model->_order_by = 'end_date desc';
			$data['all_lins_group'] = $this->asset_model->get();
		} else if($md == "car_pch") {
			$data['title'] = '구매관리';

			$data['idx'] = $this->input->post('idx', true);
		} else if($md == "car_sell") $data['title'] = '매각정보';
		else if($md == "car_chk") {
			if ($action == 'edit_lins_info') {
				$data['spay_cnt']		= $this->input->post('spay_cnt', true);
				$data['ins_co_id']		= $this->input->post('ins_co_id', true);
				$data['closing']		= $this->input->post('closing', true);
				$data['cls_pay_date']	= $this->input->post('cls_pay_date', true);
				$data['cls_gj_year']	= $this->input->post('cls_gj_year', true);
				$data['cls_gj_month']	= $this->input->post('cls_gj_month', true);

				//대인배상 Ⅰ
				$data['person1'] = $this->input->post('person1', true);
				$data['person2'] = $this->input->post('person2', true);
				$data['object'] = $this->input->post('object', true);
				$data['self_body'] = $this->input->post('self_body', true);
				$data['self_car'] = $this->input->post('self_car', true);
				$data['call_svc'] = $this->input->post('call_svc', true);
				$data['start_date'] = $this->input->post('start_date', true);
				$data['end_date'] = $this->input->post('end_date', true);

				$data['active'] = 2;
			} else {
				$data['active'] = 1;
			}
			$data['title'] = '정기(정밀)검사';
			$this->asset_model->_table_name = 'tbl_asset_truck_check'; //table name
			$this->asset_model->db->where('tbl_asset_truck_check.tr_id', $tr_id);
			$this->asset_model->_order_by = 'change_date';
			$data['all_check_group'] = $this->asset_model->get();
		} else if($md == "car_own") $data['title'] = '차주정보관리';
		
		$data['subview'] = $this->load->view('admin/asset/pop_'.$md, $data, true);
        $this->load->view('admin/_layout_pop_car', $data);
    }

    public function pop_save_carinfo_($id = NULL)
    {
		$tr_id		= $id;
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');

		$sql_keep = "";
		if(!empty($this->input->post('keep_car_1', true))) $sql_keep .= ", keep_car_1 = '".$this->input->post('keep_car_1')."'";
		if(!empty($this->input->post('keep_owner', true))) $sql_keep .= ", keep_owner = '".$this->input->post('keep_owner')."'";
		if(!empty($this->input->post('keep_type', true))) $sql_keep .= ", keep_type = '".$this->input->post('keep_type')."'";
		if(!empty($this->input->post('keep_car_3', true))) $sql_keep .= ", keep_car_3 = '".$this->input->post('keep_car_3')."'";
		if(!empty($this->input->post('keep_car_4', true))) $sql_keep .= ", keep_car_4 = '".$this->input->post('keep_car_4')."'";
		if(!empty($this->input->post('keep_mode', true))) $sql_keep .= ", keep_mode = '".$this->input->post('keep_mode')."'";
		if(!empty($this->input->post('keep_car_5', true))) $sql_keep .= ", keep_car_5 = '".$this->input->post('keep_car_5')."'";
		if(!empty($this->input->post('keep_car_7', true))) $sql_keep .= ", keep_car_7 = '".$this->input->post('keep_car_7')."'";
		if(!empty($this->input->post('keep_motor_mode', true))) $sql_keep .= ", keep_motor_mode = '".$this->input->post('keep_motor_mode')."'";
		if(!empty($this->input->post('keep_headquarter', true))) $sql_keep .= ", keep_headquarter = '".$this->input->post('keep_headquarter')."'";
		if(!empty($this->input->post('keep_carinfo', true))) $sql_keep .= ", keep_carinfo = '".$this->input->post('keep_carinfo')."'";
		if(!empty($this->input->post('keep_baecha_co_id', true))) $sql_keep .= ", keep_baecha_co_id = '".$this->input->post('keep_baecha_co_id')."'";
		if(!empty($this->input->post('keep_inv_co_id', true))) $sql_keep .= ", keep_inv_co_id = '".$this->input->post('keep_inv_co_id')."'";
		if(!empty($this->input->post('keep_ws_co_id', true))) $sql_keep .= ", keep_ws_co_id = '".$this->input->post('keep_ws_co_id')."'";
		if(!empty($this->input->post('keep_carinfo_9', true))) $sql_keep .= ", keep_carinfo_9 = '".$this->input->post('keep_carinfo_9')."'";
		if(!empty($this->input->post('keep_carinfo_11', true))) $sql_keep .= ", keep_carinfo_11 = '".$this->input->post('keep_carinfo_11')."'";

		$sql_add = "";
		if(!empty($this->input->post('owner_id', true))) $sql_add .= ", owner_id = '".$this->input->post('owner_id')."'";
		if(!empty($this->input->post('owner_name', true))) $sql_add .= ", owner_name = '".$this->input->post('owner_name')."'";
		if(!empty($this->input->post('driver_name', true))) $sql_add .= ", driver_name = '".$this->input->post('driver_name')."'";
		if(!empty($this->input->post('car_1', true))) $sql_add .= ", car_1 = '".$this->input->post('car_1')."'";
		if(!empty($this->input->post('car_7', true))) $sql_add .= ", car_7 = '".$this->input->post('car_7')."'";
		if(!empty($this->input->post('car_3', true))) $sql_add .= ", car_3 = '".$this->input->post('car_3')."'";
		if(!empty($this->input->post('car_4', true))) $sql_add .= ", car_4 = '".$this->input->post('car_4')."'";
		if(!empty($this->input->post('mode', true))) $sql_add .= ", mode = '".$this->input->post('mode')."'";
		if(!empty($this->input->post('car_5', true))) $sql_add .= ", car_5 = '".$this->input->post('car_5')."'";
		if(!empty($this->input->post('type', true))) $sql_add .= ", type = '".$this->input->post('type')."'";
		if(!empty($this->input->post('motor_mode', true))) $sql_add .= ", motor_mode = '".$this->input->post('motor_mode')."'";
		if(!empty($this->input->post('headquarter', true))) $sql_add .= ", headquarter = '".$this->input->post('headquarter')."'";
		if(!empty($this->input->post('length', true))) $sql_add .= ", length = '".$this->input->post('length')."'";
		if(!empty($this->input->post('width', true))) $sql_add .= ", width = '".$this->input->post('width')."'";
		if(!empty($this->input->post('height', true))) $sql_add .= ", height = '".$this->input->post('height')."'";
		if(!empty($this->input->post('max_load', true))) $sql_add .= ", max_load = '".$this->input->post('max_load')."'";
		if(!empty($this->input->post('fuel_type', true))) $sql_add .= ", fuel_type = '".$this->input->post('fuel_type')."'";
		if(!empty($this->input->post('carinfo_9', true))) $sql_add .= ", carinfo_9 = '".$this->input->post('carinfo_9')."'";
		if(!empty($this->input->post('carinfo_11', true))) $sql_add .= ", carinfo_11 = '".$this->input->post('carinfo_11')."'";
		if(!empty($this->input->post('baecha_co_id', true))) $sql_add .= ", baecha_co_id = '".$this->input->post('baecha_co_id')."'";
		if(!empty($this->input->post('baecha_co_name', true))) $sql_add .= ", baecha_co_name = '".$this->input->post('baecha_co_name')."'";
		if(!empty($this->input->post('ws_co_id', true))) $sql_add .= ", ws_co_id = '".$this->input->post('ws_co_id')."'";
		if(!empty($this->input->post('ws_co_name', true))) $sql_add .= ", ws_co_name = '".$this->input->post('ws_co_name')."'";
		if(!empty($this->input->post('inv_co_id', true))) $sql_add .= ", inv_co_id = '".$this->input->post('inv_co_id')."'";
		if(!empty($this->input->post('inv_co_name', true))) $sql_add .= ", inv_co_name = '".$this->input->post('inv_co_name')."'";
		
		//반납예정관련
		if(!empty($this->input->post('return_ready', true))) $sql_add .= ", return_ready = '".$this->input->post('return_ready')."'";
		if(!empty($this->input->post('return_due_date', true))) $sql_add .= ", return_due_date = '".$this->input->post('return_due_date')."'";
		if(!empty($this->input->post('return_cls_date', true))) $sql_add .= ", return_cls_date = '".$this->input->post('return_cls_date')."'";

	
        if (!empty($created) || !empty($edited) || !empty($tr_id)) {
			// 1. 기존이력 is_master = '0'
			$sql = "update tbl_asset_truck_info  SET is_master='0' where is_master='2' and tr_id='$tr_id'";
			$this->db->query($sql);
			$sql = "update tbl_asset_truck_info  SET is_master='2' where is_master='1' and tr_id='$tr_id'";//마지막 정보
			$this->db->query($sql);

			// 2. 새 이력 삽입 $sql_keep
			$sql = "INSERT INTO tbl_asset_truck_info  SET tr_id='$tr_id'".$sql_add;
			$sql .= ",reg_datetime=now(), is_master='1'";
			if(!empty($this->input->post('reason', true))) $sql .= ", reason = '".$this->input->post('reason')."'";
			if(!empty($this->input->post('remark', true))) $sql .= ", remark = '".$this->input->post('remark')."'";
			$this->db->query($sql);

			// 3. 마스터 업데이트
			$sql = "UPDATE tbl_asset_truck  SET car_1='$car_1'".$sql_add;
			$sql .= " WHERE idx='$tr_id'";
			$this->db->query($sql);

			$message = "자동차 정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('pop_carinfo/car_info/'.$tr_id); //redirect page
	}

	// 팝업 자동차보험 정보 저장
    public function pop_save_carinfo_ins($id = NULL)
    {
		$tr_id		= $id;
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
		$ins_id = $this->input->post('ins_id', true);
        if (!empty($created) || !empty($edited)) {
			$sql_upload = "";
            $this->asset_model->_table_name = 'tbl_asset_truck_insur'; //table name
            if(!empty($ins_id)) $result = $this->asset_model->get_by(array('idx' => $ins_id), true);

            if (!empty($_FILES['cert_file1']['name'])) {
                $val = $this->asset_model->customUploadFile('cert_file1','car_ins');
                $val == TRUE || redirect('pop_carinfo/car_ins/'.$tr_id.'/edit_ins_info');
                $ins_data['cert_file1'] = $val['path'];
				$sql_upload .= ", cert_file1 = '".$ins_data['cert_file1']."'";
            }
            if (!empty($_FILES['cert_file2']['name'])) {
                $val = $this->asset_model->customUploadFile('cert_file2','car_ins');
                $val == TRUE || redirect('pop_carinfo/car_ins/'.$tr_id.'/edit_ins_info');
                $ins_data['cert_file2'] = $val['path'];
				$sql_upload .= ", cert_file2 = '".$ins_data['cert_file2']."'";
            }
            if (!empty($_FILES['cert_file3']['name'])) {
                $val = $this->asset_model->customUploadFile('cert_file3','car_ins');
                $val == TRUE || redirect('pop_carinfo/car_ins/'.$tr_id.'/edit_ins_info');
                $ins_data['cert_file3'] = $val['path'];
				$sql_upload .= ", cert_file3 = '".$ins_data['cert_file3']."'";
            }
			
			if (!empty($result)) {
				$sql = "UPDATE tbl_asset_truck_insur SET ";
				$sql .= "type = '자동차'";
				$sql .= ",tr_id		= '".$tr_id."'";
				$sql .= ",ins_co_id	= '".$this->input->post('ins_co_id', true)."'";
				$sql .= ",pay_cnt	= '".$this->input->post('spay_cnt', true)."'";
				$sql .= ",person1	= '".$this->input->post('person1', true)."'";
				$sql .= ",person2	= '".$this->input->post('person2', true)."'";
				$sql .= ",closing	= '".$this->input->post('closing', true)."'";
				$sql .= ",cls_pay_date = '".$this->input->post('cls_pay_date', true)."'";
				$sql .= ",cls_gj_date = '".$this->input->post('cls_gj_date', true)."'";
				$sql .= ",exp_date	= '".$this->input->post('exp_date', true)."'";
				$sql .= ",total		= '".$this->input->post('tot_amount', true)."'";
				$sql .= ",object	= '".$this->input->post('object', true)."'";
				$sql .= ",self_body = '".$this->input->post('self_body', true)."'";
				$sql .= ",self_car	= '".$this->input->post('self_car', true)."'";
				$sql .= ",call_svc	= '".$this->input->post('call_svc', true)."'";
				$sql .= ",etc		= '".$this->input->post('etc', true)."'";
				$sql .= ",start_date = '".$this->input->post('start_date', true)."'";
				$sql .= ",end_date	= '".$this->input->post('end_date', true)."'";
				$sql .= ",co_pay	= '".$this->input->post('co_pay', true)."'" . $sql_upload;
				$sql .= " WHERE idx = '".$ins_id."'";
				$this->db->query($sql);
			} else {
				$sql = "UPDATE tbl_asset_truck_insur  SET active_yn ='N' WHERE tr_id='$tr_id' and type='자동차'";
				$this->db->query($sql);

				//,closing='$closing',cls_pay_date='$cls_pay_date',cls_gj_date='{$cls_gj_year}-{$cls_gj_month}',exp_date='$exp_date'
				$sql = "INSERT INTO tbl_asset_truck_insur  SET type='자동차'";
				$sql .= ",tr_id	= '".$tr_id."'";
				$sql .= ",ins_co_id	= '".$this->input->post('ins_co_id', true)."'";
				$sql .= ",pay_cnt	= '".$this->input->post('spay_cnt', true)."'";
				$sql .= ",person1	= '".$this->input->post('person1', true)."'";
				$sql .= ",person2	= '".$this->input->post('person2', true)."'";
				$sql .= ",closing	= '".$this->input->post('closing', true)."'";
				$sql .= ",cls_pay_date = '".$this->input->post('cls_pay_date', true)."'";
				$sql .= ",cls_gj_date = '".$this->input->post('cls_gj_date', true)."'";
				$sql .= ",exp_date	= '".$this->input->post('exp_date', true)."'";
				$sql .= ",total		= '".$this->input->post('tot_amount', true)."'";
				$sql .= ",object	= '".$this->input->post('object', true)."'";
				$sql .= ",self_body = '".$this->input->post('self_body', true)."'";
				$sql .= ",self_car	= '".$this->input->post('self_car', true)."'";
				$sql .= ",call_svc	= '".$this->input->post('call_svc', true)."'";
				$sql .= ",etc		= '".$this->input->post('etc', true)."'";
				$sql .= ",start_date = '".$this->input->post('start_date', true)."'";
				$sql .= ",end_date	= '".$this->input->post('end_date', true)."'";
				$sql .= ",active_yn = 'Y'";
				$sql .= ",co_pay	= '".$this->input->post('co_pay', true)."'" . $sql_upload;
				$this->db->query($sql);
				$ins_id = $this->db->insert_id(); 
			}
			$data['qry'] = $sql;
			$data['cnt'] = count($this->input->post('chk[]', true));


			//하위정보 저장
			$this->db->query("delete from tbl_asset_truck_insur_sub where pid='{$ins_id}'");
			//$data['subdata'] = "";
			//$subdatas = "";
			for ($i=0; $i<count($this->input->post('chk[]', true)); $i++) 
			{
				// 실제 번호를 넘김 
				$k = $this->input->post('chk['.$i.']', true);
				//echo ("$i <br/>");

				$pay_cnt = $this->input->post('pay_cnt['.$i.']', true);
				$pay_amt = $this->input->post('pay_amt['.$i.']', true);
				//$pay_year = $this->input->post('pay_year['.$i.']', true);
				//$pay_month = $this->input->post('pay_month['.$i.']', true);

				$pay_date = $this->input->post('pay_date['.$i.']', true);
				$gj_month = $this->input->post('gj_month['.$i.']', true);
				$cls_date = $this->input->post('cls_date['.$i.']', true);

				$sql = "INSERT INTO tbl_asset_truck_insur_sub SET pid='$ins_id',tr_id='$tr_id',pay_amt='$pay_amt',pay_cnt='$pay_cnt',gj_month='$gj_month',pay_date='$pay_date',cls_date='$cls_date'";
				$this->db->query($sql);
				//$subdatas .= "<br/>".$sql;
			}
			//$data['subdata'] = $subdatas;

			//echo $sql."<br/>";

			$message = "자동차보험 정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
		
		//$data['qry'] = $sql."<br/>";
        redirect('admin/asset/pop_carinfo/car_ins/'.$tr_id); //redirect page
		//$data['subview'] = $this->load->view('admin/asset/test', $data, true);
        //$this->load->view('admin/_layout_pop_car', $data);
    }

    public function save_car($id = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

			$sql_files = "";
            if (!empty($_FILES['attach1']['name'])) {
                $val = $this->asset_model->customUploadFile('attach1','car_file');
                $val == TRUE || redirect('admin/basic/car_list/'.$ws_co_id);
				$sql_files .= ", attach1 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach2']['name'])) {
                $val = $this->asset_model->customUploadFile('attach2','car_file');
                $val == TRUE || redirect('admin/basic/car_list/'.$ws_co_id);
				$sql_files .= ", attach2 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach3']['name'])) {
                $val = $this->asset_model->customUploadFile('attach3','car_file');
                $val == TRUE || redirect('admin/basic/car_list/'.$ws_co_id);
				$sql_files .= ", attach3 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach4']['name'])) {
                $val = $this->asset_model->customUploadFile('attach4','car_file');
                $val == TRUE || redirect('admin/basic/car_list/'.$ws_co_id);
				$sql_files .= ", attach4 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach5']['name'])) {
                $val = $this->asset_model->customUploadFile('attach1','car_file');
                $val == TRUE || redirect('admin/basic/car_list/'.$ws_co_id);
				$sql_files .= ", attach5 = '".$val['path']."'";
            }

            $this->asset_model->_table_name = 'tbl_asset_truck'; //table name
            $result = $this->asset_model->get_by(array('idx' => $id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_asset_truck SET ";
				$sql .= "gr_id='".$this->input->post('gr_id', true)."'";
				$sql .= ",p_tr_id='".$id."'";
				$sql .= ",car_5='".$this->input->post('car_5', true)."'";
				$sql .= ",car_4='".$this->input->post('car_4', true)."'";
				$sql .= ",car_1='".$this->input->post('car_1', true)."'";
				$sql .= ",baecha_co='".$this->input->post('baecha_co', true)."'";
				$sql .= ",baecha_co_name='".$this->input->post('baecha_co_name', true)."'";
				$sql .= ",max_load='".$this->input->post('max_load', true)."'";
				$sql .= ",car_7='".$this->input->post('car_7', true)."'";
				$sql .= ",length='".$this->input->post('length', true)."'";
				$sql .= ",width='".$this->input->post('width', true)."'";
				$sql .= ",type='".$this->input->post('type', true)."'";
				$sql .= ",carinfo_8='".$this->input->post('carinfo_8', true)."'";
				$sql .= ",car_3='".$this->input->post('car_3', true)."'";
				$sql .= ",carinfo_11='".$this->input->post('carinfo_11', true)."'";
				$sql .= ",carinfo_9='".$this->input->post('carinfo_9', true)."'";
				$sql .= ",mode='".$this->input->post('mode', true)."'";
				$sql .= ",motor_mode='".$this->input->post('motor_mode', true)."'";
				$sql .= ",headquarter='".$this->input->post('headquarter', true)."'";
				$sql .= ",inv_co_id='".$this->input->post('inv_co_id', true)."'";
				$sql .= ",inv_co_name='".$this->input->post('inv_co_name', true)."'";
				$sql .= ",inv_reg_no='".$this->input->post('inv_reg_no', true)."'";
				$sql .= ",inv_location='".$this->input->post('inv_location', true)."'";
				$sql .= ",inv_tel='".$this->input->post('inv_tel', true)."'";
				$sql .= ",inv_ceo='".$this->input->post('inv_ceo', true)."'";
				$sql .= ",inv_fax='".$this->input->post('inv_fax', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",ws_co_name='".$this->input->post('ws_co_name', true)."'";
				$sql .= ",ws_reg_no='".$this->input->post('ws_reg_no', true)."'";
				$sql .= ",ws_location='".$this->input->post('ws_location', true)."'";
				$sql .= ",ws_tel='".$this->input->post('ws_tel', true)."'";
				$sql .= ",ws_ceo='".$this->input->post('ws_ceo', true)."'";
				$sql .= ",ws_fax='".$this->input->post('ws_fax', true)."'";
				$sql .= ",height='".$this->input->post('height', true)."'";
				$sql .= ",max_on='".$this->input->post('max_on', true)."'";
				$sql .= ",fuel_type='".$this->input->post('fuel_type', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$sql .= ",reg_datetime='".date('Y-m-d h:i:s')."'";
				$sql .= " WHERE idx = '".$id."'";
				$this->db->query($sql);
			} else {
				$sql = "INSERT INTO tbl_asset_truck SET ";
				$sql .= "gr_id='".$this->input->post('gr_id', true)."'";
				$sql .= ",car_5='".$this->input->post('car_5', true)."'";
				$sql .= ",car_4='".$this->input->post('car_4', true)."'";
				$sql .= ",car_1='".$this->input->post('car_1', true)."'";
				$sql .= ",baecha_co='".$this->input->post('baecha_co', true)."'";
				$sql .= ",baecha_co_name='".$this->input->post('baecha_co_name', true)."'";
				$sql .= ",max_load='".$this->input->post('max_load', true)."'";
				$sql .= ",car_7='".$this->input->post('car_7', true)."'";
				$sql .= ",length='".$this->input->post('length', true)."'";
				$sql .= ",width='".$this->input->post('width', true)."'";
				$sql .= ",type='".$this->input->post('type', true)."'";
				$sql .= ",carinfo_8='".$this->input->post('carinfo_8', true)."'";
				$sql .= ",car_3='".$this->input->post('car_3', true)."'";
				$sql .= ",carinfo_11='".$this->input->post('carinfo_11', true)."'";
				$sql .= ",carinfo_9='".$this->input->post('carinfo_9', true)."'";
				$sql .= ",mode='".$this->input->post('mode', true)."'";
				$sql .= ",motor_mode='".$this->input->post('motor_mode', true)."'";
				$sql .= ",headquarter='".$this->input->post('headquarter', true)."'";
				$sql .= ",inv_co_id='".$this->input->post('inv_co_id', true)."'";
				$sql .= ",inv_co_name='".$this->input->post('inv_co_name', true)."'";
				$sql .= ",inv_reg_no='".$this->input->post('inv_reg_no', true)."'";
				$sql .= ",inv_location='".$this->input->post('inv_location', true)."'";
				$sql .= ",inv_tel='".$this->input->post('inv_tel', true)."'";
				$sql .= ",inv_ceo='".$this->input->post('inv_ceo', true)."'";
				$sql .= ",inv_fax='".$this->input->post('inv_fax', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",ws_co_name='".$this->input->post('ws_co_name', true)."'";
				$sql .= ",ws_reg_no='".$this->input->post('ws_reg_no', true)."'";
				$sql .= ",ws_location='".$this->input->post('ws_location', true)."'";
				$sql .= ",ws_tel='".$this->input->post('ws_tel', true)."'";
				$sql .= ",ws_ceo='".$this->input->post('ws_ceo', true)."'";
				$sql .= ",ws_fax='".$this->input->post('ws_fax', true)."'";
				$sql .= ",height='".$this->input->post('height', true)."'";
				$sql .= ",max_on='".$this->input->post('max_on', true)."'";
				$sql .= ",fuel_type='".$this->input->post('fuel_type', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$this->db->query($sql);
				$id = $this->db->insert_id();

				$sql = "UPDATE tbl_asset_truck SET p_tr_id='".$id."' WHERE tr_id = '".$id."'";
				$this->db->query($sql);
			}

            $this->asset_model->_table_name = 'tbl_asset_truck_files';
            $result = $this->asset_model->get_by(array('tr_id' => $id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_asset_truck_files SET ";
				$sql .= "tr_id='".$id."'";
				$sql .= $sql_files;
				$sql .= " WHERE tr_id = '".$id."'";
			} else {
				$sql = "INSERT INTO tbl_asset_truck_files SET ";
				$sql .= "tr_id='".$id."'" . $sql_files;
			}
			$this->db->query($sql);

			$message = "차량정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/asset/car_list/'.$ws_co_id); //redirect page
    } 
    public function save_GTcar($id = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

			$pidx = $this->input->post('pidx', true);
			$sql_files = "";
            if (!empty($_FILES['attach1']['name'])) {
                $val = $this->asset_model->customUploadFile('attach1','car_file');
                $val == TRUE || redirect('admin/basic/car_list/'.$ws_co_id);
				$sql_files .= ", attach1 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach2']['name'])) {
                $val = $this->asset_model->customUploadFile('attach2','car_file');
                $val == TRUE || redirect('admin/basic/car_list/'.$ws_co_id);
				$sql_files .= ", attach2 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach3']['name'])) {
                $val = $this->asset_model->customUploadFile('attach3','car_file');
                $val == TRUE || redirect('admin/basic/car_list/'.$ws_co_id);
				$sql_files .= ", attach3 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach4']['name'])) {
                $val = $this->asset_model->customUploadFile('attach4','car_file');
                $val == TRUE || redirect('admin/basic/car_list/'.$ws_co_id);
				$sql_files .= ", attach4 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach5']['name'])) {
                $val = $this->asset_model->customUploadFile('attach1','car_file');
                $val == TRUE || redirect('admin/basic/car_list/'.$ws_co_id);
				$sql_files .= ", attach5 = '".$val['path']."'";
            }

            $this->asset_model->_table_name = 'tbl_asset_truck'; //table name
            $result = $this->asset_model->get_by(array('idx' => $id), true);

				$sql = "INSERT INTO tbl_asset_truck SET ";
				$sql .= "gr_id='".$this->input->post('gr_id', true)."'";
				$sql .= ",car_5='".$this->input->post('car_5', true)."'";
				$sql .= ",car_4='".$this->input->post('car_4', true)."'";
				$sql .= ",car_1='".$this->input->post('car_1', true)."'";
				$sql .= ",baecha_co='".$this->input->post('baecha_co', true)."'";
				$sql .= ",baecha_co_name='".$this->input->post('baecha_co_name', true)."'";
				$sql .= ",max_load='".$this->input->post('max_load', true)."'";
				$sql .= ",car_7='".$this->input->post('car_7', true)."'";
				$sql .= ",length='".$this->input->post('length', true)."'";
				$sql .= ",width='".$this->input->post('width', true)."'";
				$sql .= ",type='".$this->input->post('type', true)."'";
				$sql .= ",carinfo_8='".$this->input->post('carinfo_8', true)."'";
				$sql .= ",car_3='".$this->input->post('car_3', true)."'";
				$sql .= ",carinfo_11='".$this->input->post('carinfo_11', true)."'";
				$sql .= ",carinfo_9='".$this->input->post('carinfo_9', true)."'";
				$sql .= ",mode='".$this->input->post('mode', true)."'";
				$sql .= ",motor_mode='".$this->input->post('motor_mode', true)."'";
				$sql .= ",headquarter='".$this->input->post('headquarter', true)."'";
				$sql .= ",inv_co_id='".$this->input->post('inv_co_id', true)."'";
				$sql .= ",inv_co_name='".$this->input->post('inv_co_name', true)."'";
				$sql .= ",inv_reg_no='".$this->input->post('inv_reg_no', true)."'";
				$sql .= ",inv_location='".$this->input->post('inv_location', true)."'";
				$sql .= ",inv_tel='".$this->input->post('inv_tel', true)."'";
				$sql .= ",inv_ceo='".$this->input->post('inv_ceo', true)."'";
				$sql .= ",inv_fax='".$this->input->post('inv_fax', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",ws_co_name='".$this->input->post('ws_co_name', true)."'";
				$sql .= ",ws_reg_no='".$this->input->post('ws_reg_no', true)."'";
				$sql .= ",ws_location='".$this->input->post('ws_location', true)."'";
				$sql .= ",ws_tel='".$this->input->post('ws_tel', true)."'";
				$sql .= ",ws_ceo='".$this->input->post('ws_ceo', true)."'";
				$sql .= ",ws_fax='".$this->input->post('ws_fax', true)."'";
				$sql .= ",height='".$this->input->post('height', true)."'";
				$sql .= ",max_on='".$this->input->post('max_on', true)."'";
				$sql .= ",fuel_type='".$this->input->post('fuel_type', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$this->db->query($sql);
				$id = $this->db->insert_id();

            $this->asset_model->_table_name = 'tbl_asset_truck_files';
            $result = $this->asset_model->get_by(array('tr_id' => $id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_asset_truck_files SET ";
				$sql .= "tr_id='".$id."'";
				$sql .= $sql_files;
				$sql .= " WHERE tr_id = '".$id."'";
			} else {
				$sql = "INSERT INTO tbl_asset_truck_files SET ";
				$sql .= "tr_id='".$id."'" . $sql_files;
			}
			$this->db->query($sql);

			//기존 공티이 차량 해제
			if(!empty($pidx)) {
			//	$sql = "UPDATE tbl_asset_truck SET gongT_yn='L' WHERE idx = '".$pidx."'";
			//	$this->db->query($sql);
			}

			$message = "차량정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/asset/car_gongt_list/'.$ws_co_id); //redirect page
    } 

    public function car_registration($action = NULL, $id = NULL)
    {
        $data['title'] = '자동차등록증재교부';
        $data['subview'] = $this->load->view('admin/asset/car_registration', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 

    public function car_daepae($action = NULL, $id = NULL)
    {
        $data['title'] = '자동차대패신청';
        $data['subview'] = $this->load->view('admin/asset/car_daepae', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 

    public function car_transfer($action = NULL, $id = NULL)
    {
        $data['title'] = '자동차이전등록';
        $data['subview'] = $this->load->view('admin/asset/car_transfer', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 

    public function car_info($action = NULL, $id = NULL)
    {
        $data['title'] = '자동차정보변경';
        $data['subview'] = $this->load->view('admin/asset/car_info', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 

    public function car_insur($action = NULL, $id = NULL)
    {
        $tr_id = $id;
        $data['df_month'] = $this->input->post('df_month', true);
        if (empty($data['df_month'])) $data['df_month'] = date('Y-m');
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
        if (empty($data['ws_co_id'])) $data['ws_co_id'] = "1413";
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
        if (empty($data['ws_co_name'])) $data['ws_co_name'] = "케이티지엘에스(주)";

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '만기 보험 현황';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at, tbl_members mb'; //table name
        $this->asset_model->db->select('at.*, mb.co_name, mb.ceo, mb.driver');
        $this->asset_model->db->where('mb.dp_id = at.owner_id');
        $this->asset_model->db->where('at.is_deleted', 'N');
		$this->asset_model->_order_by = 'car_1';
		if($data['ws_co_id'] == "etc") {
			$this->asset_model->db->where_not_in('at.ws_co_id', '1413')->where_not_in('at.ws_co_id', '1414')->where_not_in('at.ws_co_id', '1415')->where_not_in('at.ws_co_id', '1903');
		} else {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}
        $data['all_car_info'] = $this->asset_model->get();

        $data['subview'] = $this->load->view('admin/asset/car_insur', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 

    public function car_inspection($action = NULL, $id = NULL)
    {
        $data['title'] = '자동차검사 목록';
        $data['subview'] = $this->load->view('admin/asset/car_inspection', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 

    public function car_garage($action = NULL, $id = NULL)
    {
        $member_id = $id;

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->asset_model->can_action('tbl_members', 'edit', array('asset_id' => $id));
			$this->db->where('member_id', $asset_id)->get('tbl_member')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('member_id', $asset_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }

		$data['title'] = '차고지 목록';

        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_id';
        $data['all_garage_info'] = $this->asset_model->get();

        $data['subview'] = $this->load->view('admin/asset/car_garage', $data, true);
        $this->load->view('admin/_layout_main', $data);
    } 
	
    public function car_assign_ready($action = NULL, $id = NULL)
    {

        $tr_id = $id;
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '차주할당대기차량 목록';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at'; //table name
        $this->asset_model->db->select('at.*');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('TRIM(at.baecha_co)', TRIM($this->input->post('baecha_co_id', true)));
        $this->asset_model->db->where('at.is_deleted', 'N')->where('at.car_status', 'N');
		$this->asset_model->_order_by = 'car_1';
		if(!empty($data['ws_co_id'])) {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}
        $data['all_car_info'] = $this->asset_model->get();

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();

        // get all language
//        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->asset_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->asset_model->get_permission('tbl_assets');

        $data['all_designation_info'] = $this->asset_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/asset/car_assign_ready', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }    
	
    public function car_list($action = NULL, $id = NULL)
    {

        $tr_id = $id;
		if (!empty($ws_co_id)) $data['ws_co_id'] = $ws_co_id;
        else $data['ws_co_id'] = $this->input->post('ws_co_id', true);
        if (empty($data['ws_co_id'])) $data['ws_co_id'] = "1413";
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
        if (empty($data['ws_co_name'])) $data['ws_co_name'] = "케이티지엘에스(주)";
		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $data['car_info'] = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '차량자산 목록';

		//모든 현물출자자 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('tbl_members.mb_type<>', 'partner');
        $this->asset_model->db->where('tbl_members.co_name<>', '');
        $data['all_inv_group'] = $this->asset_model->get();

		//차고지
        $this->asset_model->_table_name = 'tbl_garage'; //table name
        $this->asset_model->_order_by = 'gr_name';
        $data['all_garage_group'] = $this->asset_model->get();

        $this->asset_model->_table_name = 'tbl_asset_truck at, tbl_members mb'; //table name
        $this->asset_model->db->select('at.*, mb.co_name, mb.ceo, mb.driver');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('TRIM(at.baecha_co)', TRIM($this->input->post('baecha_co_id', true)));
        $this->asset_model->db->where('mb.dp_id = at.owner_id');
        $this->asset_model->db->where('at.is_deleted', 'N')->where('at.gongT_yn', 'N')->where('at.car_status', 'A');
		$this->asset_model->_order_by = 'car_1';
		if($data['ws_co_id'] == "etc") {
			$this->asset_model->db->where_not_in('at.ws_co_id', '1413')->where_not_in('at.ws_co_id', '1414')->where_not_in('at.ws_co_id', '1415')->where_not_in('at.ws_co_id', '1903');
		} else {
			$this->asset_model->db->where('at.ws_co_id', $data['ws_co_id']);
		}
        $data['all_car_info'] = $this->asset_model->get();

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();

        // get all language
//        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->asset_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->asset_model->get_permission('tbl_assets');

        $data['all_designation_info'] = $this->asset_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/asset/car_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }    


    public function car_list_all($action = NULL, $id = NULL)
    {
		$data['page'] = $this->input->post('page', true);
		if(empty($data['page']) || $data['page'] < 1) $data['page'] =  1;
		$data['limit'] = $this->input->post('limit', true);
		if(empty($data['limit']) || $data['limit']=="") $data['limit'] =  20;
		$data['belongs'] = $this->input->post('belongs', true);
		if(empty($data['belongs'])) $data['belongs'] =  "W"; // W-자사, O -타사

		$data['ws_co_id'] = $this->input->post('ws_co_id', true);
		$data['ws_co_name'] = $this->input->post('ws_co_name', true);
		$data['search_field'] = $this->input->post('search_field', true);
		$data['search_keyword'] = $this->input->post('search_keyword', true);
		$data['baecha_co_id'] = $this->input->post('baecha_co_id', true);

        if ($action == 'edit_partner') {
            $data['active'] = 2;
            $can_edit = $this->asset_model->can_action('tbl_asset_truck', 'edit', array('dp_id' => $dp_id));
			$this->db->where('dp_id', $dp_id)->get('tbl_asset_truck')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['car_info'] = $this->db->where('dp_id', $dp_id)->get('tbl_asset_truck')->row();
            }
        } else {
            $data['active'] = 1;
        }
		
		if(empty($data['from_record'])) $data['from_record'] = 0;
        $this->asset_model->_table_name = 'tbl_asset_truck tr'; //table name          
		$this->asset_model->db->join('tbl_members dp', 'dp.tr_id = tr.idx', 'right');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('tr.baecha_co', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->asset_model->db->where('(car_1 like \'%'.$data['search_keyword'].'%\' OR car_7 like \'%'.$data['search_keyword'].'%\' OR ws_co_name like \'%'.$data['search_keyword'].'%\' OR dp.L like \'%'.$data['search_keyword'].'%\' OR dp.M like \'%'.$data['search_keyword'].'%\' OR dp.bs_number like \'%'.$data['search_keyword'].'%\' OR inv_co_name like \'%'.$data['search_keyword'].'%\')' );
		}
		if(!empty($data['ws_co_id'])) $this->asset_model->db->or_where('tr.ws_co_id', $data['ws_co_id'] );
		if(!empty($data['belongs']) && $data['belongs']<>"A") $this->asset_model->db->where('tr.belongs', $data['belongs'] );
        $this->asset_model->db->where('tr.is_deleted', 'N')->where('tr.car_status', 'A');

/*
        $this->asset_model->_table_name = 'tbl_asset_truck'; //table name          
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('baecha_co', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->asset_model->db->or_where('car_1 like', '%'.$data['search_keyword'].'%' )->or_where('car_7 like', '%'.$data['search_keyword'].'%' )->or_where('ws_co_name like', '%'.$data['search_keyword'].'%' )->or_where('inv_co_name like', '%'.$data['search_keyword'].'%' );
		}
		if(!empty($data['ws_co_id'])) {
			$this->asset_model->db->or_where('ws_co_id', $data['ws_co_id'] );
		}
        $this->asset_model->db->where('is_deleted', 'N')->where('gongT_yn', 'N');
*/
		$data['total_count'] = count($this->asset_model->get());

		$data['total_page']  = ceil($data['total_count'] / $data['limit']);  // 전체 페이지 계산
		$data['from_record'] = ($data['page'] - 1) * $data['limit']; // 시작 열을 구함

        $this->asset_model->_table_name = 'tbl_asset_truck tr'; //table name          
		$this->asset_model->db->select('tr.*'); //, dp.ceo, bc.co_name as bc_co_name');
		$this->asset_model->db->join('tbl_members dp', 'dp.tr_id = tr.idx', 'right');
		//$this->asset_model->db->join('tbl_members bc', '(dp.code = bc.code and bc.mb_type = \'customer\')', 'right');
		if(!empty($this->input->post('baecha_co_id', true))) $this->asset_model->db->where('tr.baecha_co', $this->input->post('baecha_co_id', true));
		if(!empty($data['search_keyword'])) {
			$this->asset_model->db->where('(car_1 like \'%'.$data['search_keyword'].'%\' OR car_7 like \'%'.$data['search_keyword'].'%\' OR ws_co_name like \'%'.$data['search_keyword'].'%\' OR dp.L like \'%'.$data['search_keyword'].'%\' OR dp.M like \'%'.$data['search_keyword'].'%\' OR dp.bs_number like \'%'.$data['search_keyword'].'%\' OR inv_co_name like \'%'.$data['search_keyword'].'%\')' );

		}
		if(!empty($data['ws_co_id'])) {
			$this->asset_model->db->where('tr.ws_co_id', $data['ws_co_id'] );
		}
        $this->asset_model->db->where('tr.is_deleted', 'N')->where('tr.car_status', 'A');

        $this->asset_model->_order_by = 'tr.car_1';
		$this->asset_model->db->limit($data['limit'], $data['from_record']);
        $data['all_car_info'] = $this->asset_model->get();
        $data['title'] = '차량 목록';

		//배차지 목록
        $this->asset_model->_table_name = 'tbl_members'; //table name
        $this->asset_model->_order_by = 'co_name';
        $this->asset_model->db->where('mb_type','customer');
        $data['all_baecha_info'] = $this->asset_model->get();

        $data['subview'] = $this->load->view('admin/asset/car_list_all', $data, true);
        $this->load->view('admin/_layout_main_nd', $data);
    }

	
	public function asset_list($action = NULL, $id = NULL)
    {

        $asset_id = $id;

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->asset_model->can_action('tbl_assets', 'edit', array('asset_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('asset_id', $asset_id)->get('tbl_assets')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'asset List';

        $this->asset_model->_table_name = 'tbl_client'; //table name
        $this->asset_model->_order_by = 'client_id';
        $data['all_client_info'] = $this->asset_model->get();

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->asset_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->asset_model->get_permission('tbl_assets');

        $data['all_designation_info'] = $this->asset_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/asset/asset_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function asset_details($id, $active = null)
    {
        $data['title'] = lang('asset_details');
        $data['id'] = $id;
        if (!empty($active)) {
            $data['active'] = $active;
        } else {
            $data['active'] = 1;
        }
        $date = $this->input->post('date', true);
        if (!empty($date)) {
            $data['date'] = $date;
        } else {
            $data['date'] = date('Y-m');
        }
        $data['attendace_info'] = $this->get_report($id, $data['date']);

        $data['my_leave_report'] = leave_report($id);

        //
        if ($this->input->post('year', TRUE)) { // if input year
            $data['year'] = $this->input->post('year', TRUE);
        } else { // else current year
            $data['year'] = date('Y'); // get current year
        }
        // get all expense list by year and month
        $data['provident_fund_info'] = $this->get_provident_fund_info($data['year'], $id);

        if ($this->input->post('overtime_year', TRUE)) { // if input year
            $data['overtime_year'] = $this->input->post('overtime_year', TRUE);
        } else { // else current year
            $data['overtime_year'] = date('Y'); // get current year
        }
        // get all expense list by year and month
        $data['all_overtime_info'] = $this->get_overtime_info($data['overtime_year'], $id);
        $data['profile_info'] = $this->db->where('asset_id', $id)->get('tbl_account_details')->row();

        $data['total_attendance'] = count($this->total_attendace_in_month($id));

        $data['total_absent'] = count($this->total_attendace_in_month($id, 'absent'));

        $data['total_leave'] = count($this->total_attendace_in_month($id, 'leave'));
        //award received
        $data['total_award'] = count($this->db->where('asset_id', $id)->get('tbl_employee_award')->result());

        // get working days holiday
        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday

        $num = cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y'));
        $working_holiday = 0;
        for ($i = 1; $i <= $num; $i++) {
            $day_name = date('l', strtotime("+0 days", strtotime(date('Y') . '-' . date('n') . '-' . $i)));

            if (!empty($holidays)) {
                foreach ($holidays as $v_holiday) {
                    if ($v_holiday->day == $day_name) {
                        $working_holiday += count($day_name);
                    }
                }
            }
        }
        // get public holiday
        $public_holiday = count($this->total_attendace_in_month($id, TRUE));

        // get total days in a month
        $month = date('m');
        $year = date('Y');
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        // total attend days in a month without public holiday and working days
        $data['total_days'] = $days - $working_holiday - $public_holiday;

        $data['all_working_hour'] = $this->all_attendance_id_by_date($id, true);

        $data['this_month_working_hour'] = $this->all_attendance_id_by_date($id);

        $data['subview'] = $this->load->view('admin/asset/asset_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function all_attendance_id_by_date($asset_id, $flag = null)
    {
        if (!empty($flag)) {
            $get_total_attendance = $this->db->where(array('asset_id' => $asset_id, 'attendance_status' => '1'))->get('tbl_attendance')->result();
            if (!empty($get_total_attendance)) {
                foreach ($get_total_attendance as $v_attendance_id) {
                    $aresult[] = $this->global_model->get_total_working_hours($v_attendance_id->attendance_id);
                }
                return $aresult;
            }
        } else {

            $month = date('n');
            $year = date('Y');
            if ($month >= 1 && $month <= 9) {
                $yymm = $year . '-' . '0' . $month;
            } else {
                $yymm = $year . '-' . $month;
            }
            $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            for ($i = 1; $i <= $num; $i++) {
                if ($i >= 1 && $i <= 9) {
                    $sdate = $yymm . '-' . '0' . $i;
                } else {
                    $sdate = $yymm . '-' . $i;
                }
                $get_total_attendance = $this->global_model->get_total_attendace_by_date($sdate, $sdate, $asset_id); // get all attendace by start date and in date
                if (!empty($get_total_attendance)) {
                    foreach ($get_total_attendance as $v_attendance_id) {
                        $result[] = $this->global_model->get_total_working_hours($v_attendance_id->attendance_id);
                    }
                }
            }
            if (!empty($result)) {
                return $result;
            }
        }
    }

    public function total_attendace_in_month($asset_id, $flag = NULL)
    {
        $month = date('m');
        $year = date('Y');

        if ($month >= 1 && $month <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
            $start_date = $year . "-" . '0' . $month . '-' . '01';
            $end_date = $year . "-" . '0' . $month . '-' . '31';
        } else {
            $start_date = $year . "-" . $month . '-' . '01';
            $end_date = $year . "-" . $month . '-' . '31';
        }
        if (!empty($flag) && $flag == 1) { // if flag is not empty that means get pulic holiday
            $get_public_holiday = $this->global_model->get_holiday_list_by_date($start_date, $end_date);

            if (!empty($get_public_holiday)) { // if not empty the public holiday
                foreach ($get_public_holiday as $v_holiday) {
                    if ($v_holiday->start_date == $v_holiday->end_date) { // if start date and end date is equal return one data
                        $total_holiday[] = $v_holiday->start_date;
                    } else { // if start date and end date not equan return all date
                        for ($j = $v_holiday->start_date; $j <= $v_holiday->end_date; $j++) {
                            $total_holiday[] = $j;
                        }
                    }
                }
                return $total_holiday;
            }
        } elseif (!empty($flag)) { // if flag is not empty that means get pulic holiday
            $get_total_absent = $this->global_model->get_total_attendace_by_date($start_date, $end_date, $asset_id, $flag); // get all attendace by start date and in date
            return $get_total_absent;
        } else {
            $get_total_attendance = $this->global_model->get_total_attendace_by_date($start_date, $end_date, $asset_id); // get all attendace by start date and in date
            return $get_total_attendance;
        }
    }

    public function get_overtime_info($year, $asset_id)
    {// this function is to create get monthy recap report

        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $i . '-' . '01';
                $end_date = $year . "-" . '0' . $i . '-' . '31';
            } else {
                $start_date = $year . "-" . $i . '-' . '01';
                $end_date = $year . "-" . $i . '-' . '31';
            }
            $get_expense_list[$i] = $this->utilities_model->get_overtime_info_by_date($start_date, $end_date, $asset_id); // get all report by start date and in date
        }

        return $get_expense_list; // return the result
    }

    public function overtime_report_pdf($year, $asset_id)
    {
        $data['all_overtime_info'] = $this->get_overtime_info($year, $asset_id);

        $data['monthyaer'] = $year;
        $data['asset_info'] = $this->db->where('asset_id', $asset_id)->get('tbl_account_details')->row();
        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/asset/overtime_report_pdf', $data, TRUE);
        pdf_create($viewfile, 'Overtime Report  - ' . $data['monthyaer']);
    }

    public function get_provident_fund_info($year, $asset_id)
    {// this function is to create get monthy recap report

        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $i;
                $end_date = $year . "-" . '0' . $i;
            } else {
                $start_date = $year . "-" . $i;
                $end_date = $year . "-" . $i;
            }
            $provident_fund_info[$i] = $this->payroll_model->get_provident_fund_info_by_date($start_date, $end_date, $asset_id); // get all report by start date and in date
        }

        return $provident_fund_info; // return the result
    }

    public function provident_fund_pdf($year, $asset_id)
    {

        $data['provident_fund_info'] = $this->get_provident_fund_info($year, $asset_id);
        $data['monthyaer'] = $year;

        $data['asset_info'] = $this->db->where('asset_id', $asset_id)->get('tbl_account_details')->row();

        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/asset/provident_fund_pdf', $data, TRUE);
        pdf_create($viewfile, lang('provident_found_report') . ' - ' . $data['monthyaer']);
    }

    public function timecard_details_pdf($id, $date)
    {
        $data['profile_info'] = $this->db->where('asset_id', $id)->get('tbl_account_details')->row();
        $data['date'] = $date;
        $data['attendace_info'] = $this->get_report($id, $date);

        $viewfile = $this->load->view('admin/asset/timecard_details_pdf', $data, TRUE);

        $this->load->helper('dompdf');
        pdf_create($viewfile, lang('timecard_details') . '- ' . $data['profile_info']->fullname);
    }

    public function get_report($asset_id, $date)
    {
        $month = date('n', strtotime($date));
        $year = date('Y', strtotime($date));
        $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday

        if ($month >= 1 && $month <= 9) {
            $yymm = $year . '-' . '0' . $month;
        } else {
            $yymm = $year . '-' . $month;
        }

        $public_holiday = $this->global_model->get_public_holidays($yymm);

        //tbl a_calendar Days Holiday
        if (!empty($public_holiday)) {
            foreach ($public_holiday as $p_holiday) {
                $p_hday = $this->attendance_model->GetDays($p_holiday->start_date, $p_holiday->end_date);
            }
        }

        $key = 1;
        $x = 0;
        for ($i = 1; $i <= $num; $i++) {

            if ($i >= 1 && $i <= 9) {
                $sdate = $yymm . '-' . '0' . $i;
            } else {
                $sdate = $yymm . '-' . $i;
            }
            $day_name = date('l', strtotime("+$x days", strtotime($year . '-' . $month . '-' . $key)));

            $data['week_info'][date('W', strtotime($sdate))][$sdate] = $sdate;

            // get leave info
            if (!empty($holidays)) {
                foreach ($holidays as $v_holiday) {
                    if ($v_holiday->day == $day_name) {
                        $flag = 'H';
                    }
                }
            }
            if (!empty($p_hday)) {
                foreach ($p_hday as $v_hday) {
                    if ($v_hday == $sdate) {
                        $flag = 'H';
                    }
                }
            }
            if (!empty($flag)) {
                $attendace_info[date('W', strtotime($sdate))][$sdate] = $this->attendance_model->attendance_report_by_empid($asset_id, $sdate, $flag);
            } else {
                $attendace_info[date('W', strtotime($sdate))][$sdate] = $this->attendance_model->attendance_report_by_empid($asset_id, $sdate);
            }
            $key++;
            $flag = '';
        }
        return $attendace_info;

    }

    public function update_contact($update = null, $id = null)
    {
        $data['title'] = lang('update_contact');
        $data['update'] = $update;
        $data['profile_info'] = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
        $data['modal_subview'] = $this->load->view('admin/asset/update_contact', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_details($id)
    {
        $data = $this->asset_model->array_from_post(array('joining_date', 'gender', 'date_of_birth', 'maratial_status', 'father_name', 'mother_name', 'phone', 'mobile', 'skype', 'present_address', 'passport'));

        $this->asset_model->_table_name = 'tbl_account_details'; // table name
        $this->asset_model->_primary_key = 'account_details_id'; // $id
        $this->asset_model->save($data, $id);

        $profile_info = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
        $activities = array(
            'asset' => $this->session->assetdata('asset_id'),
            'module' => 'asset',
            'module_field_id' => $id,
            'activity' => 'activity_update_asset',
            'icon' => 'fa-asset',
            'value1' => $profile_info->fullname
        );
        $this->asset_model->_table_name = 'tbl_activities';
        $this->asset_model->_primary_key = "activities_id";
        $this->asset_model->save($activities);

        $message = lang('update_asset_info');
        $type = 'success';
        set_message($type, $message);
        redirect('admin/asset/asset_details/' . $profile_info->asset_id); //redirect page
    }

    public function asset_documents($id)
    {
        $data['title'] = lang('update_documents');
        $data['profile_info'] = $this->db->where('asset_id', $id)->get('tbl_account_details')->row();
        $data['document_info'] = $this->db->where('asset_id', $id)->get('tbl_employee_document')->row();
        $data['modal_subview'] = $this->load->view('admin/asset/asset_documents', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_documents($id)
    {
        $edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $profile_info = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
            // ** Employee Document Information Save and Update Start  **
            // Resume File upload

            if (!empty($_FILES['resume']['name'])) {
                $old_path = $this->input->post('resume_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->asset_model->uploadAllType('resume');
                $val == TRUE || redirect('admin/asset/asset_details/' . $profile_info->asset_id);
                $document_data['resume_filename'] = $val['fileName'];
                $document_data['resume'] = $val['path'];
                $document_data['resume_path'] = $val['fullPath'];
            }
            // offer_letter File upload
            if (!empty($_FILES['offer_letter']['name'])) {
                $old_path = $this->input->post('offer_letter_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->asset_model->uploadAllType('offer_letter');
                $val == TRUE || redirect('admin/asset/asset_details/' . $profile_info->asset_id);
                $document_data['offer_letter_filename'] = $val['fileName'];
                $document_data['offer_letter'] = $val['path'];
                $document_data['offer_letter_path'] = $val['fullPath'];
            }
            // joining_letter File upload
            if (!empty($_FILES['joining_letter']['name'])) {
                $old_path = $this->input->post('joining_letter_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->asset_model->uploadAllType('joining_letter');
                $val == TRUE || redirect('admin/asset/asset_details/' . $profile_info->asset_id);
                $document_data['joining_letter_filename'] = $val['fileName'];
                $document_data['joining_letter'] = $val['path'];
                $document_data['joining_letter_path'] = $val['fullPath'];
            }

            // contract_paper File upload
            if (!empty($_FILES['contract_paper']['name'])) {
                $old_path = $this->input->post('contract_paper_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->asset_model->uploadAllType('contract_paper');
                $val == TRUE || redirect('admin/asset/asset_details/' . $profile_info->asset_id);
                $document_data['contract_paper_filename'] = $val['fileName'];
                $document_data['contract_paper'] = $val['path'];
                $document_data['contract_paper_path'] = $val['fullPath'];
            }
            // id_proff File upload
            if (!empty($_FILES['id_proff']['name'])) {
                $old_path = $this->input->post('id_proff_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->asset_model->uploadAllType('id_proff');
                $val == TRUE || redirect('admin/asset/asset_details/' . $profile_info->asset_id);
                $document_data['id_proff_filename'] = $val['fileName'];
                $document_data['id_proff'] = $val['path'];
                $document_data['id_proff_path'] = $val['fullPath'];
            }

            $fileName = $this->input->post('fileName');
            $path = $this->input->post('path');
            $fullPath = $this->input->post('fullPath');
            $size = $this->input->post('size');
            $is_image = $this->input->post('is_image');

            if (!empty($fileName)) {
                foreach ($fileName as $key => $name) {
                    $old['fileName'] = $name;
                    $old['path'] = $path[$key];
                    $old['fullPath'] = $fullPath[$key];
                    $old['size'] = $size[$key];
                    $old['is_image'] = $is_image[$key];
                    $result[] = $old;
                }

                $document_data['other_document'] = json_encode($result);
            }

            if (!empty($_FILES['other_document']['name']['0'])) {
                $old_path_info = $this->input->post('upload_path');
                if (!empty($old_path_info)) {
                    foreach ($old_path_info as $old_path) {
                        unlink($old_path);
                    }
                }
                $mul_val = $this->asset_model->multi_uploadAllType('other_document');
                $document_data['other_document'] = json_encode($mul_val);
            }

            if (!empty($result) && !empty($mul_val)) {
                $file = array_merge($result, $mul_val);
                $document_data['other_document'] = json_encode($file);
            }

            $document_data['asset_id'] = $profile_info->asset_id;

            $this->asset_model->_table_name = "tbl_employee_document"; // table name
            $this->asset_model->_primary_key = "document_id"; // $id
            $document_id = $this->input->post('document_id', TRUE);
            if (!empty($document_id)) {
                $this->asset_model->save($document_data, $document_id);
            } else {
                $this->asset_model->save($document_data);
            }

            $activities = array(
                'asset' => $this->session->assetdata('asset_id'),
                'module' => 'asset',
                'module_field_id' => $id,
                'activity' => 'activity_documents_update',
                'icon' => 'fa-asset',
                'value1' => $profile_info->fullname
            );
            $this->asset_model->_table_name = 'tbl_activities';
            $this->asset_model->_primary_key = "activities_id";
            $this->asset_model->save($activities);

            $message = lang('emplyee_documents_update');
            $type = 'success';
            set_message($type, $message);
            redirect('admin/asset/asset_details/' . $profile_info->asset_id . '/' . '4'); //redirect page
        } else {
            redirect('admin/asset/asset_list');
        }
    }

    public function new_bank($asset_id, $bank_id = null)
    {
        $data['title'] = lang('new_bank');

        $data['profile_info'] = $this->db->where('asset_id', $asset_id)->get('tbl_account_details')->row();
        if (!empty($bank_id)) {
            $data['bank_info'] = $this->db->where('employee_bank_id', $bank_id)->get('tbl_employee_bank')->row();
        }
        $data['modal_subview'] = $this->load->view('admin/asset/new_bank', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_bank_info($asset_id, $bank_id = null)
    {
        $edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $bank_data = $this->asset_model->array_from_post(array('bank_name', 'routing_number', 'account_name', 'account_number','type_of_account'));
            $bank_data['asset_id'] = $asset_id;
            $this->asset_model->_table_name = "tbl_employee_bank"; // table name
            $this->asset_model->_primary_key = "employee_bank_id"; // $id

            if (!empty($bank_id)) {
                $activity = 'activity_update_asset_bank';
                $msg = lang('update_bank_info');
                $this->asset_model->save($bank_data, $bank_id);
            } else {
                $activity = 'activity_new_asset_bank';
                $msg = lang('save_bank_info');
                $bank_id = $this->asset_model->save($bank_data);
            }
            $profile_info = $this->db->where('asset_id', $asset_id)->get('tbl_account_details')->row();
            $activities = array(
                'asset' => $this->session->assetdata('asset_id'),
                'module' => 'asset',
                'module_field_id' => $bank_id,
                'activity' => $activity,
                'icon' => 'fa-asset',
                'value1' => $profile_info->fullname
            );
            $this->asset_model->_table_name = 'tbl_activities';
            $this->asset_model->_primary_key = "activities_id";
            $this->asset_model->save($activities);

            $type = 'success';
            set_message($type, $msg);
            redirect('admin/asset/asset_details/' . $profile_info->asset_id . '/' . '3'); //redirect page
        } else {
            redirect('admin/asset/asset_list');
        }
    }

    public function delete_asset_bank($asset_id, $bank_id)
    {
        $this->asset_model->_table_name = "tbl_employee_bank"; // table name
        $this->asset_model->_primary_key = "employee_bank_id"; // $id
        $this->asset_model->delete($bank_id);

        $profile_info = $this->db->where('asset_id', $asset_id)->get('tbl_account_details')->row();
        $activities = array(
            'asset' => $this->session->assetdata('asset_id'),
            'module' => 'asset',
            'module_field_id' => $bank_id,
            'activity' => 'activity_delete_asset_bank',
            'icon' => 'fa-asset',
            'value1' => $profile_info->fullname
        );
        $this->asset_model->_table_name = 'tbl_activities';
        $this->asset_model->_primary_key = "activities_id";
        $this->asset_model->save($activities);
        $type = 'success';
        $msg = lang('delete_asset_bank');
        set_message($type, $msg);
        redirect('admin/asset/asset_details/' . $profile_info->asset_id);
    }

    /*     * * Save New asset ** */
    public function save_asset()
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
            $login_data = $this->asset_model->array_from_post(array('assetname', 'email', 'role_id'));
            $asset_id = $this->input->post('asset_id', true);
            // update root category
            $where = array('assetname' => $login_data['assetname']);
            $email = array('email' => $login_data['email']);
            // duplicate value check in DB
            if (!empty($asset_id)) { // if id exist in db update data
                $check_id = array('asset_id !=' => $asset_id);
            } else { // if id is not exist then set id as null
                $check_id = null;
            }
            // check whether this input data already exist or not
            $check_asset = $this->asset_model->check_update('tbl_assets', $where, $check_id);
            $check_email = $this->asset_model->check_update('tbl_assets', $email, $check_id);
            if (!empty($check_asset) || !empty($check_email)) { // if input data already exist show error alert
                if (!empty($check_asset)) {
                    $error = $login_data['assetname'];
                } else {
                    $error = $login_data['email'];
                }

                // massage for asset
                $type = 'error';
                $message = "<strong style='color:#000'>" . $error . '</strong>  ' . lang('already_exist');

                $password = $this->input->post('password', TRUE);
                $confirm_password = $this->input->post('confirm_password', TRUE);
                if ($password != $confirm_password) {
                    $type = 'error';
                    $message = lang('password_does_not_match');
                }
            } else { // save and update query
                $login_data['last_ip'] = $this->input->ip_address();

                if (empty($asset_id)) {
                    $password = $this->input->post('password', TRUE);
                    $login_data['password'] = $this->hash($password);
                }
                $permission = $this->input->post('permission', true);
                if (!empty($permission)) {
                    if ($permission == 'everyone') {
                        $assigned = 'all';
                    } else {
                        $assigned_to = $this->asset_model->array_from_post(array('assigned_to'));
                        if (!empty($assigned_to['assigned_to'])) {
                            foreach ($assigned_to['assigned_to'] as $assign_asset) {
                                $assigned[$assign_asset] = $this->input->post('action_' . $assign_asset, true);
                            }
                        }
                    }
                    if (!empty($assigned)) {
                        if ($assigned != 'all') {
                            $assigned = json_encode($assigned);
                        }
                    } else {
                        $assigned = 'all';
                    }
                    $login_data['permission'] = $assigned;
                } else {
                    set_message('error', lang('assigned_to') . ' Field is required');
                    redirect($_SERVER['HTTP_REFERER']);
                }
                $this->asset_model->_table_name = 'tbl_assets'; // table name
                $this->asset_model->_primary_key = 'asset_id'; // $id
                if (!empty($asset_id)) {
                    $id = $this->asset_model->save($login_data, $asset_id);
                } else {
                    $login_data['activated'] = '1';
                    $id = $this->asset_model->save($login_data);
                }
                save_custom_field(13, $id);
                // save into tbl_account details
                $profile_data = $this->asset_model->array_from_post(array('fullname', 'employment_id', 'company', 'locale', 'language', 'phone', 'mobile', 'skype', 'designations_id', 'direction'));
                if ($login_data['role_id'] != 2) {
                    $profile_data['company'] = 0;
                }

                $account_details_id = $this->input->post('account_details_id', TRUE);
                if (!empty($_FILES['avatar']['name'])) {
                    $val = $this->asset_model->uploadImage('avatar');
                    $val == TRUE || redirect('admin/asset/asset_list');
                    $profile_data['avatar'] = $val['path'];
                }

                $profile_data['asset_id'] = $id;

                $this->asset_model->_table_name = 'tbl_account_details'; // table name
                $this->asset_model->_primary_key = 'account_details_id'; // $id
                if (!empty($account_details_id)) {
                    $this->asset_model->save($profile_data, $account_details_id);
                } else {
                    $this->asset_model->save($profile_data);
                }
                if (!empty($profile_data['designations_id'])) {
                    $desig = $this->db->where('designations_id', $profile_data['designations_id'])->get('tbl_designations')->row();
                    $department_head_id = $this->input->post('department_head_id', true);
                    if (!empty($department_head_id)) {
                        $head['department_head_id'] = $id;
                    } else {
                        $dep_head = $this->asset_model->check_by(array('departments_id' => $desig->departments_id), 'tbl_departments');

                        if (empty($dep_head->department_head_id)) {
                            $head['department_head_id'] = $id;
                        }
                    }
                    if (!empty($desig->departments_id) && !empty($head)) {
                        $this->asset_model->_table_name = "tbl_departments"; //table name
                        $this->asset_model->_primary_key = "departments_id";
                        $this->asset_model->save($head, $desig->departments_id);
                    }
                }

                $activities = array(
                    'asset' => $this->session->assetdata('asset_id'),
                    'module' => 'asset',
                    'module_field_id' => $id,
                    'activity' => 'activity_added_new_asset',
                    'icon' => 'fa-asset',
                    'value1' => $login_data['assetname']
                );
                $this->asset_model->_table_name = 'tbl_activities';
                $this->asset_model->_primary_key = "activities_id";
                $this->asset_model->save($activities);
                if (!empty($id)) {
                    $this->asset_model->_table_name = 'tbl_client_role'; //table name
                    $this->asset_model->delete_multiple(array('asset_id' => $id));
                    $all_client_menu = $this->db->get('tbl_client_menu')->result();
                    foreach ($all_client_menu as $v_client_menu) {
                        $client_role_data['menu_id'] = $this->input->post($v_client_menu->label, true);
                        if (!empty($client_role_data['menu_id'])) {
                            $client_role_data['asset_id'] = $id;
                            $this->asset_model->_table_name = 'tbl_client_role';
                            $this->asset_model->_primary_key = 'client_role_id';
                            $this->asset_model->save($client_role_data);
                        }
                    }
                }

                if (!empty($asset_id)) {
                    $message = lang('update_asset_info');
                } else {
                    $message = lang('save_asset_info');
                }
                $type = 'success';
            }
            set_message($type, $message);
        }
        redirect('admin/asset/asset_list'); //redirect page
    }

    public function send_welcome_email($id)
    {
        $asset_info = $this->db->where('asset_id', $id)->get('tbl_assets')->row();
        $profile_info = $this->db->where('asset_id', $id)->get('tbl_account_details')->row();
        $email_template = $this->asset_model->check_by(array('email_group' => 'wellcome_email'), 'tbl_email_templates');

        $message = $email_template->template_body;
        $subject = $email_template->subject;
        $NAME = str_replace("{NAME}", $profile_info->fullname, $message);
        $URL = str_replace("{COMPANY_URL}", base_url(), $NAME);
        $message = str_replace("{COMPANY_NAME}", config_item('company_name'), $URL);

        $data['message'] = $message;
        $message = $this->load->view('email_template', $data, TRUE);

        $params['subject'] = $subject;
        $params['message'] = $message;
        $params['resourceed_file'] = '';
        $params['recipient'] = $asset_info->email;

        $this->asset_model->send_email($params);

        $type = 'success';
        $message = lang('welcome_email_success');
        set_message($type, $message);
        redirect('admin/asset/asset_list'); //redirect page
    }

    /*     * * Delete asset ** */

    public function delete_asset($id = null)
    {
        $deleted = can_action('24', 'deleted');
        if (!empty($deleted)) {
            if (!empty($id)) {
                $id = $id;
                $asset_id = $this->session->assetdata('asset_id');

                //checking login asset trying delete his own account
                if ($id == $asset_id) {
                    //same asset can not delete his own account
                    // redirect with error msg
                    $type = 'error';
                    $message = 'Sorry You can not delete your own account!';
                    set_message($type, $message);
                    redirect('admin/asset/asset_list'); //redirect page
                } else {
                    $sbtn = $this->input->post('submit', true);

                    if (!empty($sbtn)) {
                        //delete procedure run
                        // Check asset in db or not
                        $this->asset_model->_table_name = 'tbl_assets'; //table name
                        $this->asset_model->_order_by = 'asset_id';
                        $result = $this->asset_model->get_by(array('asset_id' => $id), true);

                        if (!empty($result)) {
                            //delete asset roll id
                            $this->asset_model->_table_name = 'tbl_account_details';
                            $this->asset_model->delete_multiple(array('asset_id' => $id));//delete asset roll id

                            $cwhere = array('asset_id' => $id);
                            $this->asset_model->_table_name = 'tbl_private_chat';
                            $this->asset_model->delete_multiple($cwhere);

                            $this->asset_model->_table_name = 'tbl_private_chat_assets';
                            $this->asset_model->delete_multiple($cwhere);

                            $this->asset_model->_table_name = 'tbl_private_chat_messages';
                            $this->asset_model->delete_multiple($cwhere);

                            $this->asset_model->_table_name = 'tbl_activities';
                            $this->asset_model->delete_multiple(array('asset' => $id));

                            $this->asset_model->_table_name = 'tbl_payments';
                            $this->asset_model->delete_multiple(array('paid_by' => $id));

                            // delete all tbl_quotations by id
                            $this->asset_model->_table_name = 'tbl_quotations';
                            $this->asset_model->_order_by = 'asset_id';
                            $quotations_info = $this->asset_model->get_by(array('asset_id' => $id), FALSE);

                            if (!empty($quotations_info)) {
                                foreach ($quotations_info as $v_quotations) {
                                    $this->asset_model->_table_name = 'tbl_quotation_details';
                                    $this->asset_model->delete_multiple(array('quotations_id' => $v_quotations->quotations_id));
                                }
                            }

                            $this->asset_model->_table_name = 'tbl_quotations';
                            $this->asset_model->delete_multiple(array('asset_id' => $id));

                            $this->asset_model->_table_name = 'tbl_quotationforms';
                            $this->asset_model->delete_multiple(array('quotations_created_by_id' => $id));

                            $this->asset_model->_table_name = 'tbl_assets';
                            $this->asset_model->delete_multiple(array('asset_id' => $id));

                            $this->asset_model->_table_name = 'tbl_asset_role';
                            $this->asset_model->delete_multiple(array('designations_id' => $id));

                            $this->asset_model->_table_name = 'tbl_inbox';
                            $this->asset_model->delete_multiple(array('asset_id' => $id));

                            $this->asset_model->_table_name = 'tbl_sent';
                            $this->asset_model->delete_multiple(array('asset_id' => $id));

                            $this->asset_model->_table_name = 'tbl_draft';
                            $this->asset_model->delete_multiple(array('asset_id' => $id));

                            $tickets_info = $this->db->get('tbl_tickets')->result();
                            if (!empty($tickets_info)) {
                                foreach ($tickets_info as $v_tickets) {
                                    if (!empty($v_tickets->permission) && $v_tickets->permission != 'all') {
                                        $allowad_asset = json_decode($v_tickets->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $op_asset_id => $v_asset) {
                                                if ($op_asset_id == $id || $v_tickets->reporter == $id) {
                                                    $this->asset_model->_table_name = 'tbl_tickets';
                                                    $this->asset_model->delete_multiple(array('tickets_id' => $v_tickets->tickets_id));
                                                    $this->asset_model->_table_name = 'tbl_tickets_replies';
                                                    $this->asset_model->delete_multiple(array('tickets_id' => $v_tickets->tickets_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // xpm crm
                            // delete all leads by id
                            $leads_info = $this->db->get('tbl_leads')->result();
                            if (!empty($leads_info)) {
                                foreach ($leads_info as $v_leads) {
                                    if (!empty($v_leads->permission) && $v_leads->permission != 'all') {
                                        $allowad_asset = json_decode($v_leads->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $op_asset_id => $v_asset) {
                                                if ($op_asset_id == $id) {
                                                    //delete data into table.
                                                    $this->asset_model->_table_name = "tbl_calls"; // table name
                                                    $this->asset_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    //delete data into table.
                                                    $this->asset_model->_table_name = "tbl_mettings"; // table name
                                                    $this->asset_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    //delete data into table.
                                                    $this->asset_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->asset_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    $this->asset_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->asset_model->_order_by = "leads_id";
                                                    $files_info = $this->asset_model->get_by(array('leads_id' => $v_leads->leads_id), FALSE);

                                                    if (!empty($files_info)) {
                                                        foreach ($files_info as $v_files) {
                                                            //save data into table.
                                                            $this->asset_model->_table_name = "tbl_task_uploaded_files"; // table name
                                                            $this->asset_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                        }
                                                    }
                                                    //save data into table.
                                                    $this->asset_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->asset_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    $this->asset_model->_table_name = "tbl_task"; // table name
                                                    $this->asset_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    $this->asset_model->_table_name = 'tbl_leads';
                                                    $this->asset_model->_primary_key = 'leads_id';
                                                    $this->asset_model->delete($v_leads->leads_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //save data into table.
                            $this->asset_model->_table_name = "tbl_milestones"; // table name
                            $this->asset_model->delete_multiple(array('asset_id' => $id));
                            // todo
                            $this->asset_model->_table_name = "tbl_todo"; // table name
                            $this->asset_model->delete_multiple(array('asset_id' => $id));

                            // opportunity
                            $oppurtunity_info = $this->db->get('tbl_opportunities')->result();
                            if (!empty($oppurtunity_info)) {
                                foreach ($oppurtunity_info as $v_oppurtunity) {
                                    if (!empty($v_oppurtunity->permission) && $v_oppurtunity->permission != 'all') {
                                        $allowad_asset = json_decode($v_oppurtunity->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $op_asset_id => $v_asset) {
                                                if ($op_asset_id == $id)
                                                    //delete data into table.
                                                    $this->asset_model->_table_name = "tbl_calls"; // table name
                                                $this->asset_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //delete data into table.
                                                $this->asset_model->_table_name = "tbl_mettings"; // table name
                                                $this->asset_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //delete data into table.
                                                $this->asset_model->_table_name = "tbl_task_comment"; // table name
                                                $this->asset_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                $this->asset_model->_table_name = "tbl_task_attachment"; //table name
                                                $this->asset_model->_order_by = "task_id";
                                                $files_info = $this->asset_model->get_by(array('opportunities_id' => $v_oppurtunity->opportunities_id), FALSE);
                                                if (!empty($files_info)) {
                                                    foreach ($files_info as $v_files) {
                                                        //save data into table.
                                                        $this->asset_model->_table_name = "tbl_task_uploaded_files"; // table name
                                                        $this->asset_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                    }
                                                }
                                                //save data into table.
                                                $this->asset_model->_table_name = "tbl_task_attachment"; // table name
                                                $this->asset_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //save data into table.
                                                $this->asset_model->_table_name = "tbl_task"; // table name
                                                $this->asset_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //save data into table.
                                                $this->asset_model->_table_name = "tbl_bug"; // table name
                                                $this->asset_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                $this->asset_model->_table_name = 'tbl_opportunities';
                                                $this->asset_model->_primary_key = 'opportunities_id';
                                                $this->asset_model->delete($v_oppurtunity->opportunities_id);
                                            }
                                        }
                                    }
                                }
                            }
                            // project
                            $project_info = $this->db->get('tbl_project')->result();
                            if (!empty($project_info)) {
                                foreach ($project_info as $v_project) {
                                    if (!empty($v_project->permission) && $v_project->permission != 'all') {
                                        $allowad_asset = json_decode($v_project->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $asset_id => $v_asset) {
                                                if ($asset_id == $id) {
                                                    //delete data into table.
                                                    $this->asset_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->asset_model->delete_multiple(array('project_id' => $v_project->project_id));

                                                    $this->asset_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->asset_model->_order_by = "task_id";
                                                    $files_info = $this->asset_model->get_by(array('project_id' => $v_project->project_id), FALSE);
                                                    if (!empty($files_info)) {
                                                        foreach ($files_info as $v_files) {
                                                            //save data into table.
                                                            $this->asset_model->_table_name = "tbl_task_uploaded_files"; // table name
                                                            $this->asset_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                        }
                                                    }
                                                    //save data into table.
                                                    $this->asset_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->asset_model->delete_multiple(array('project_id' => $v_project->project_id));

                                                    //save data into table.
                                                    $this->asset_model->_table_name = "tbl_milestones"; // table name
                                                    $this->asset_model->delete_multiple(array('project_id' => $v_project->project_id));

                                                    // tasks
                                                    $taskss_info = $this->db->where('project_id', $v_project->project_id)->get('tbl_task')->result();
                                                    if (!empty($taskss_info)) {
                                                        foreach ($taskss_info as $v_taskss) {
                                                            if (!empty($v_taskss->permission) && $v_taskss->permission != 'all') {
                                                                $allowad_asset = json_decode($v_taskss->permission);
                                                                if (!empty($allowad_asset)) {
                                                                    foreach ($allowad_asset as $task_asset_id => $v_asset) {
                                                                        if ($task_asset_id == $id) {

                                                                            $this->asset_model->_table_name = "tbl_task_attachment"; //table name
                                                                            $this->asset_model->_order_by = "task_id";
                                                                            $files_info = $this->asset_model->get_by(array('task_id' => $v_taskss->task_id), FALSE);
                                                                            foreach ($files_info as $v_files) {
                                                                                $this->asset_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                                                $this->asset_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                                            }
                                                                            //delete into table.
                                                                            $this->asset_model->_table_name = "tbl_task_attachment"; // table name
                                                                            $this->asset_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                                            //delete data into table.
                                                                            $this->asset_model->_table_name = "tbl_task_comment"; // table name
                                                                            $this->asset_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                                            $this->asset_model->_table_name = "tbl_task"; // table name
                                                                            $this->asset_model->_primary_key = "task_id"; // $id
                                                                            $this->asset_model->delete($v_taskss->task_id);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    // Bugs
                                                    $bugs_info = $this->db->where('project_id', $v_project->project_id)->get('tbl_bug')->result();
                                                    if (!empty($bugs_info)) {
                                                        foreach ($bugs_info as $v_bugs) {
                                                            if (!empty($v_bugs->permission) && $v_bugs->permission != 'all') {
                                                                $allowad_asset = json_decode($v_bugs->permission);
                                                                if (!empty($allowad_asset)) {
                                                                    foreach ($allowad_asset as $bugs_asset_id => $v_asset) {
                                                                        if ($bugs_asset_id == $id) {

                                                                            $this->asset_model->_table_name = "tbl_task_attachment"; //table name
                                                                            $this->asset_model->_order_by = "bug_id";
                                                                            $files_info = $this->asset_model->get_by(array('bug_id' => $v_bugs->bug_id), FALSE);
                                                                            foreach ($files_info as $v_files) {
                                                                                $this->asset_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                                                $this->asset_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                                            }
                                                                            //delete into table.
                                                                            $this->asset_model->_table_name = "tbl_task_attachment"; // table name
                                                                            $this->asset_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                                            //delete data into table.
                                                                            $this->asset_model->_table_name = "tbl_task_comment"; // table name
                                                                            $this->asset_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                                            //delete data into table.
                                                                            $this->asset_model->_table_name = "tbl_task"; // table name
                                                                            $this->asset_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                                            $this->asset_model->_table_name = "tbl_bug"; // table name
                                                                            $this->asset_model->_primary_key = "bug_id"; // $id
                                                                            $this->asset_model->delete($v_bugs->bug_id);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    $this->asset_model->_table_name = 'tbl_project';
                                                    $this->asset_model->_primary_key = 'project_id';
                                                    $this->asset_model->delete($v_project->project_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // tasks
                            $taskss_info = $this->db->get('tbl_task')->result();
                            if (!empty($taskss_info)) {
                                foreach ($taskss_info as $v_taskss) {
                                    if (!empty($v_taskss->permission) && $v_taskss->permission != 'all') {
                                        $allowad_asset = json_decode($v_taskss->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $task_asset_id => $v_asset) {
                                                if ($task_asset_id == $id) {

                                                    $this->asset_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->asset_model->_order_by = "task_id";
                                                    $files_info = $this->asset_model->get_by(array('task_id' => $v_taskss->task_id), FALSE);
                                                    foreach ($files_info as $v_files) {
                                                        $this->asset_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                        $this->asset_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                    }
                                                    //delete into table.
                                                    $this->asset_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->asset_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                    //delete data into table.
                                                    $this->asset_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->asset_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                    $this->asset_model->_table_name = "tbl_task"; // table name
                                                    $this->asset_model->_primary_key = "task_id"; // $id
                                                    $this->asset_model->delete($v_taskss->task_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // Bugs
                            $bugs_info = $this->db->get('tbl_bug')->result();
                            if (!empty($bugs_info)) {
                                foreach ($bugs_info as $v_bugs) {
                                    if (!empty($v_bugs->permission) && $v_bugs->permission != 'all') {
                                        $allowad_asset = json_decode($v_bugs->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $bugs_asset_id => $v_asset) {
                                                if ($bugs_asset_id == $id) {

                                                    $this->asset_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->asset_model->_order_by = "bug_id";
                                                    $files_info = $this->asset_model->get_by(array('bug_id' => $v_bugs->bug_id), FALSE);
                                                    foreach ($files_info as $v_files) {
                                                        $this->asset_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                        $this->asset_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                    }
                                                    //delete into table.
                                                    $this->asset_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->asset_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                    //delete data into table.
                                                    $this->asset_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->asset_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                    //delete data into table.
                                                    $this->asset_model->_table_name = "tbl_task"; // table name
                                                    $this->asset_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                    $this->asset_model->_table_name = "tbl_bug"; // table name
                                                    $this->asset_model->_primary_key = "bug_id"; // $id
                                                    $this->asset_model->delete($v_bugs->bug_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // tbl_invoices
                            $invoices_info = $this->db->get('tbl_invoices')->result();
                            if (!empty($invoices_info)) {
                                foreach ($invoices_info as $v_invoices) {
                                    if (!empty($v_invoices->permission) && $v_invoices->permission != 'all') {
                                        $allowad_asset = json_decode($v_invoices->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $invoice_asset_id => $v_asset) {
                                                if ($invoice_asset_id == $id) {
                                                    $this->asset_model->_table_name = "tbl_invoices"; // table name
                                                    $this->asset_model->_primary_key = "invoices_id"; // $id
                                                    $this->asset_model->delete($v_invoices->invoices_id);

                                                    $this->asset_model->_table_name = "tbl_items"; // table name
                                                    $this->asset_model->delete_multiple(array('invoices_id' => $v_invoices->invoices_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // tbl_estimates
                            $estimate_info = $this->db->get('tbl_estimates')->result();
                            if (!empty($estimate_info)) {
                                foreach ($estimate_info as $v_estimate) {
                                    if (!empty($v_estimate->permission) && $v_estimate->permission != 'all') {
                                        $allowad_asset = json_decode($v_estimate->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $estimate_asset_id => $v_asset) {
                                                if ($estimate_asset_id == $id) {
                                                    $this->asset_model->_table_name = "tbl_estimates"; // table name
                                                    $this->asset_model->_primary_key = "estimates_id"; // $id
                                                    $this->asset_model->delete($v_estimate->estimates_id);

                                                    $this->asset_model->_table_name = "tbl_estimate_items"; // table name
                                                    $this->asset_model->delete_multiple(array('estimates_id' => $v_estimate->estimates_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // 	tbl_tax_rates
                            $tax_rate_info = $this->db->get('tbl_tax_rates')->result();
                            if (!empty($tax_rate_info)) {
                                foreach ($tax_rate_info as $v_tax_rat) {
                                    if (!empty($v_tax_rat->permission) && $v_tax_rat->permission != 'all') {
                                        $allowad_asset = json_decode($v_tax_rat->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $tax_rate_asset_id => $v_asset) {
                                                if ($tax_rate_asset_id == $id) {
                                                    $this->asset_model->_table_name = "tbl_tax_rates"; // table name
                                                    $this->asset_model->_primary_key = "tax_rates_id"; // $id
                                                    $this->asset_model->delete($v_tax_rat->tax_rates_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $transactions_info = $this->db->get('tbl_transactions')->result();
                            if (!empty($transactions_info)) {
                                foreach ($transactions_info as $v_transactions) {
                                    if (!empty($v_transactions->permission) && $v_transactions->permission != 'all') {
                                        $allowad_asset = json_decode($v_transactions->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $trsn_asset_id => $v_asset) {
                                                if ($trsn_asset_id == $id) {
                                                    $this->asset_model->_table_name = 'tbl_transactions';
                                                    $this->asset_model->delete_multiple(array('transactions_id' => $v_transactions->transactions_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $transfer_info = $this->db->get('tbl_transfer')->result();
                            if (!empty($transfer_info)) {
                                foreach ($transfer_info as $v_transfer) {
                                    if (!empty($v_transfer->permission) && $v_transfer->permission != 'all') {
                                        $allowad_asset = json_decode($v_transfer->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $trfr_asset_id => $v_asset) {
                                                if ($trfr_asset_id == $id) {
                                                    $this->asset_model->_table_name = 'tbl_transfer';
                                                    $this->asset_model->delete_multiple(array('transfer_id' => $v_transfer->transfer_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //redirect successful msg
                            $type = 'success';
                            $message = 'asset Delete Successfully!';
                        } else {
                            //redirect error msg
                            $type = 'error';
                            $message = 'Sorry this asset not find in database!';

                        }
                        set_message($type, $message);
                        redirect('admin/asset/asset_list'); //redirect page
                    } else {
                        $data['title'] = "Delete assets"; //Page title
                        $data['asset_info'] = $this->db->where('asset_id', $id)->get('tbl_account_details')->row();
                        $data['subview'] = $this->load->view('admin/asset/delete_asset', $data, TRUE);
                        $this->load->view('admin/_layout_main', $data); //page load
                    }
                }
            } else {
                redirect('admin/asset/asset_list'); //redirect page
            }
        }
    }

    public function change_status($flag, $id)
    {
        $can_edit = $this->asset_model->can_action('tbl_assets', 'edit', array('asset_id' => $id));
        $edited = can_action('24', 'edited');
        if (!empty($can_edit) && !empty($edited)) {
            $asset_info = $this->db->where('asset_id', $id)->get('tbl_assets')->row();
            // if flag == 1 it is active asset else deactive asset
            if ($flag == 1) {
                $msg = 'Active';
            } else {
                $msg = 'Deactive';
            }
            $where = array('asset_id' => $id);
            $action = array('activated' => $flag);
            $this->asset_model->set_action($where, $action, 'tbl_assets');

            $activities = array(
                'asset' => $this->session->assetdata('asset_id'),
                'module' => 'asset',
                'module_field_id' => $id,
                'activity' => 'activity_change_status',
                'icon' => 'fa-asset',
                'value1' => $asset_info->assetname . ' ' . $msg,
            );
            $this->asset_model->_table_name = 'tbl_activities';
            $this->asset_model->_primary_key = "activities_id";
            $this->asset_model->save($activities);

            $type = "success";
            $message = "asset " . $msg . " Successfully!";
        } else {
            $type = 'error';
            $message = lang('there_in_no_value');
        }
        echo json_encode(array("status" => $type, "message" => $message));
        exit();
    }

    public function set_banned($flag, $id)
    {
        $can_edit = $this->asset_model->can_action('tbl_assets', 'edit', array('asset_id' => $id));
        $edited = can_action('24', 'edited');
        if (!empty($can_edit) && !empty($edited)) {
            if ($flag == 1) {
                $msg = lang('banned');
                $action = array('activated' => 0, 'banned' => $flag, 'ban_reason' => $this->input->post('ban_reason', TRUE));
            } else {
                $msg = lang('unbanned');
                $action = array('activated' => 1, 'banned' => $flag);
            }
            $where = array('asset_id' => $id);

            $this->asset_model->set_action($where, $action, 'tbl_assets');

            $activities = array(
                'asset' => $this->session->assetdata('asset_id'),
                'module' => 'asset',
                'module_field_id' => $id,
                'activity' => 'activity_change_status',
                'icon' => 'fa-asset',
                'value1' => $flag,
            );
            $this->asset_model->_table_name = 'tbl_activities';
            $this->asset_model->_primary_key = "activities_id";
            $this->asset_model->save($activities);

            $type = "success";
            $message = "asset " . $msg . " Successfully!";
        } else {
            $type = 'error';
            $message = lang('there_in_no_value');
        }
        set_message($type, $message);
        redirect('admin/asset/asset_list'); //redirect page
    }

    public function change_banned($id)
    {

        $data['asset_id'] = $id;
        $data['modal_subview'] = $this->load->view('admin/asset/_modal_banned_reson', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);

    }

    public function hash($string)
    {
        return hash('sha512', $string . config_item('encryption_key'));
    }

// crud for sidebar todo list
    function todo($task = '', $todo_id = '', $swap_with = '')
    {
        if ($task == 'add') {
            $this->add_todo();
        }
        if ($task == 'reload_incomplete_todo') {
            $this->get_incomplete_todo();
        }
        if ($task == 'mark_as_done') {
            $this->mark_todo_as_done($todo_id);
        }
        if ($task == 'mark_as_undone') {
            $this->mark_todo_as_undone($todo_id);
        }
        if ($task == 'swap') {

            $this->swap_todo($todo_id, $swap_with);
        }
        if ($task == 'delete') {
            $this->delete_todo($todo_id);
        }
        $todo['opened'] = 1;
        $this->session->set_assetdata($todo);
        redirect('admin/dashboard/');
    }

    function add_todo()
    {
        $data['title'] = $this->input->post('title');
        $data['asset_id'] = $this->session->assetdata('asset_id');

        $this->db->insert('tbl_todo', $data);
        $todo_id = $this->db->insert_id();

        $data['order'] = $todo_id;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function mark_todo_as_done($todo_id = '')
    {
        $data['status'] = 1;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function mark_todo_as_undone($todo_id = '')
    {
        $data['status'] = 0;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function swap_todo($todo_id = '', $swap_with = '')
    {
        $counter = 0;
        $temp_order = $this->db->get_where('tbl_todo', array('todo_id' => $todo_id))->row()->order;
        $asset = $this->session->assetdata('asset_id');

        // Move current todo up.
        if ($swap_with == 'up') {
            // Fetch all todo lists of current asset in ascending order.
            $this->db->order_by('order', 'ASC');
            $todo_lists = $this->db->get_where('tbl_todo', array('asset_id' => $asset))->result_array();
            $array_length = count($todo_lists);

            // Create separate array for orders and todo_id's from above array.
            foreach ($todo_lists as $todo_list) {
                $id_list[] = $todo_list['todo_id'];
                $order_list[] = $todo_list['order'];
            }
        }

        // Move current todo down.
        if ($swap_with == 'down') {

            // Fetch all todo lists of current asset in descending order.
            $this->db->order_by('order', 'DESC');
            $todo_lists = $this->db->get_where('tbl_todo', array('asset_id' => $asset))->result_array();
            $array_length = count($todo_lists);

            // Create separate array for orders and todo_id's from above array.
            foreach ($todo_lists as $todo_list) {
                $id_list[] = $todo_list['todo_id'];
                $order_list[] = $todo_list['order'];
            }
        }

        // Swap orders between current and next/previous todo.
        for ($i = 0; $i < $array_length; $i++) {
            if ($temp_order == $order_list[$i]) {
                if ($counter > 0) {
                    $swap_order = $order_list[$i - 1];
                    $swap_id = $id_list[$i - 1];

                    // Update order of current todo.
                    $data['order'] = $swap_order;
                    $this->db->where('todo_id', $todo_id);
                    $this->db->update('tbl_todo', $data);

                    // Update order of next/previous todo.
                    $data['order'] = $temp_order;
                    $this->db->where('todo_id', $swap_id);
                    $this->db->update('tbl_todo', $data);
                }
            } else
                $counter++;
        }
    }

    function delete_todo($todo_id = '')
    {
        $this->db->where('todo_id', $todo_id);
        $this->db->delete('tbl_todo');
    }

    function get_incomplete_todo()
    {
        $asset = $this->session->assetdata('asset_id');
        $this->db->where('asset_id', $asset);
        $this->db->where('status', 0);
        $query = $this->db->get('tbl_todo');

        $incomplete_todo_number = $query->num_rows();
        if ($incomplete_todo_number > 0) {
            echo '<span class="badge badge-secondary">';
            echo $incomplete_todo_number;
            echo '</span>';
        }
    }

    public function reset_password($id)
    {
        if ($this->session->assetdata('asset_type') == 1) {
            $new_password = $this->input->post('password', true);
            $old_password = $this->input->post('old_password', true);
            if (!empty($new_password)) {
                $email = $this->session->assetdata('email');
                $asset_info = $this->db->where('asset_id', $id)->get('tbl_assets')->row();
                $old_password = $this->asset_model->hash($old_password);
                if ($asset_info->password == $old_password) {
                    $where = array('asset_id' => $id);
                    $action = array('password' => $this->asset_model->hash($new_password));
                    $this->asset_model->set_action($where, $action, 'tbl_assets');
                    $login_details = $this->db->where('asset_id', $id)->get('tbl_assets')->row();
                    $activities = array(
                        'asset' => $this->session->assetdata('asset_id'),
                        'module' => 'asset',
                        'module_field_id' => $id,
                        'activity' => 'activity_reset_password',
                        'icon' => 'fa-asset',
                        'value1' => $login_details->assetname,
                    );

                    $this->asset_model->_table_name = 'tbl_activities';
                    $this->asset_model->_primary_key = "activities_id";
                    $this->asset_model->save($activities);

                    $this->send_email_reset_password($email, $asset_info, $new_password);

                    $type = "success";
                    $message = lang('message_new_password_sent');
                } else {
                    $type = "error";
                    $message = lang('password_does_not_match');
                }
                set_message($type, $message);
                redirect('admin/asset/asset_details/' . $id); //redirect page

            } else {
                $data['title'] = lang('see_password');
                $data['asset_info'] = $this->db->where('asset_id', $id)->get('tbl_assets')->row();
                $data['subview'] = $this->load->view('admin/settings/reset_password', $data, FALSE);
                $this->load->view('admin/_layout_modal', $data);
            }

        } else {
            $type = 'error';
            $message = lang('there_in_no_value');
            set_message($type, $message);
            redirect('admin/asset/asset_list'); //redirect page
        }


    }

    function send_email_reset_password($email, $asset_info, $password)
    {
        $email_template = $this->asset_model->check_by(array('email_group' => 'reset_password'), 'tbl_email_templates');
        $message = $email_template->template_body;
        $subject = $email_template->subject;

        $assetname = str_replace("{assetNAME}", $asset_info->assetname, $message);
        $asset_email = str_replace("{EMAIL}", $asset_info->email, $assetname);
        $asset_password = str_replace("{NEW_PASSWORD}", $password, $asset_email);
        $message = str_replace("{SITE_NAME}", config_item('company_name'), $asset_password);
        $params['recipient'] = $email;
        $params['subject'] = '[ ' . config_item('company_name') . ' ]' . $subject;
        $params['message'] = $message;
        $params['resourceed_file'] = '';
        $this->asset_model->send_email($params);
    }

}
