<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Basic extends Admin_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('basic_model');
        $this->load->model('utilities_model');
    }

    public function buildChild($parent, $menu)
    {
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $ItemID) {
                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

    public function save_partner_____($id = NULL)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $this->basic_model->_table_name = 'tbl_asset_truck'; //table name
            $result = $this->basic_model->get_by(array('tr_id' => $id), true);

            if (!empty($result)) {
				$sql = "UPDATE tbl_asset_truck SET ";
				$sql .= "gr_id='".$this->input->post('gr_id', true)."'";
				$sql .= ",car_5='".$this->input->post('car_5', true)."'";
				$sql .= ",car_4='".$this->input->post('car_4', true)."'";
				$sql .= ",car_1='".$this->input->post('car_1', true)."'";
				$sql .= ",baecha_co='".$this->input->post('baecha_co', true)."'";
				$sql .= ",baecha_co_name='".$this->input->post('baecha_co_name', true)."'";
				$sql .= ",max_load='".$this->input->post('max_load', true)."'";
				$sql .= ",car_7='".$this->input->post('car_7', true)."'";
				$sql .= ",length='".$this->input->post('length', true)."'";
				$sql .= ",width='".$this->input->post('width', true)."'";
				$sql .= ",type='".$this->input->post('type', true)."'";
				$sql .= ",carinfo_8='".$this->input->post('carinfo_8', true)."'";
				$sql .= ",car_3='".$this->input->post('car_3', true)."'";
				$sql .= ",carinfo_11='".$this->input->post('carinfo_11', true)."'";
				$sql .= ",carinfo_9='".$this->input->post('carinfo_9', true)."'";
				$sql .= ",mode='".$this->input->post('mode', true)."'";
				$sql .= ",motor_mode='".$this->input->post('motor_mode', true)."'";
				$sql .= ",headquarter='".$this->input->post('headquarter', true)."'";
				$sql .= ",inv_co_id='".$this->input->post('inv_co_id', true)."'";
				$sql .= ",inv_co_name='".$this->input->post('inv_co_name', true)."'";
				$sql .= ",inv_reg_no='".$this->input->post('inv_reg_no', true)."'";
				$sql .= ",inv_location='".$this->input->post('inv_location', true)."'";
				$sql .= ",inv_tel='".$this->input->post('inv_tel', true)."'";
				$sql .= ",inv_ceo='".$this->input->post('inv_ceo', true)."'";
				$sql .= ",inv_fax='".$this->input->post('inv_fax', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",ws_co_name='".$this->input->post('ws_co_name', true)."'";
				$sql .= ",ws_reg_no='".$this->input->post('ws_reg_no', true)."'";
				$sql .= ",ws_location='".$this->input->post('ws_location', true)."'";
				$sql .= ",ws_tel='".$this->input->post('ws_tel', true)."'";
				$sql .= ",ws_ceo='".$this->input->post('ws_ceo', true)."'";
				$sql .= ",ws_fax='".$this->input->post('ws_fax', true)."'";
				$sql .= ",height='".$this->input->post('height', true)."'";
				$sql .= ",max_on='".$this->input->post('max_on', true)."'";
				$sql .= ",fuel_type='".$this->input->post('fuel_type', true)."'";
				$sql .= ",car_remark='".$this->input->post('car_remark', true)."'";
				$sql .= ",reg_datetime='".date('Y-m-d h:i:s')."'";
				$sql .= " WHERE tr_id = '".$id."'";
				$this->db->query($sql);
			} else {
				$sql = "INSERT INTO tbl_asset_truck SET ";
				$sql .= "gr_id='".$this->input->post('gr_id', true)."'";
				$sql .= ",car_5='".$this->input->post('car_5', true)."'";
				$sql .= ",car_4='".$this->input->post('car_4', true)."'";
				$sql .= ",car_1='".$this->input->post('car_1', true)."'";
				$sql .= ",baecha_co='".$this->input->post('baecha_co', true)."'";
				$sql .= ",baecha_co_name='".$this->input->post('baecha_co_name', true)."'";
				$sql .= ",max_load='".$this->input->post('max_load', true)."'";
				$sql .= ",car_7='".$this->input->post('car_7', true)."'";
				$sql .= ",length='".$this->input->post('length', true)."'";
				$sql .= ",width='".$this->input->post('width', true)."'";
				$sql .= ",type='".$this->input->post('type', true)."'";
				$sql .= ",carinfo_8='".$this->input->post('carinfo_8', true)."'";
				$sql .= ",car_3='".$this->input->post('car_3', true)."'";
				$sql .= ",carinfo_11='".$this->input->post('carinfo_11', true)."'";
				$sql .= ",carinfo_9='".$this->input->post('carinfo_9', true)."'";
				$sql .= ",mode='".$this->input->post('mode', true)."'";
				$sql .= ",motor_mode='".$this->input->post('motor_mode', true)."'";
				$sql .= ",headquarter='".$this->input->post('headquarter', true)."'";
				$sql .= ",inv_co_id='".$this->input->post('inv_co_id', true)."'";
				$sql .= ",inv_co_name='".$this->input->post('inv_co_name', true)."'";
				$sql .= ",inv_reg_no='".$this->input->post('inv_reg_no', true)."'";
				$sql .= ",inv_location='".$this->input->post('inv_location', true)."'";
				$sql .= ",inv_tel='".$this->input->post('inv_tel', true)."'";
				$sql .= ",inv_ceo='".$this->input->post('inv_ceo', true)."'";
				$sql .= ",inv_fax='".$this->input->post('inv_fax', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",ws_co_name='".$this->input->post('ws_co_name', true)."'";
				$sql .= ",ws_reg_no='".$this->input->post('ws_reg_no', true)."'";
				$sql .= ",ws_location='".$this->input->post('ws_location', true)."'";
				$sql .= ",ws_tel='".$this->input->post('ws_tel', true)."'";
				$sql .= ",ws_ceo='".$this->input->post('ws_ceo', true)."'";
				$sql .= ",ws_fax='".$this->input->post('ws_fax', true)."'";
				$sql .= ",height='".$this->input->post('height', true)."'";
				$sql .= ",max_on='".$this->input->post('max_on', true)."'";
				$sql .= ",fuel_type='".$this->input->post('fuel_type', true)."'";
				$sql .= ",car_remark='".$this->input->post('car_remark', true)."'";
				$this->db->query($sql);
			}

			$message = "차량정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }


    public function set_mfee($id)
    {
        $data['assign_user'] = $this->cowork_model->allowad_user('57');
        $data['mfee_info'] = $this->cowork_model->check_by(array('df_id' => $id), 'tbl_delivery_fee');
        $data['modal_subview'] = $this->load->view('admin/cowork/_modal_mfee', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_mfee($df_month = null, $gongje_req_co = null)
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {

            $mfeeset_id = $this->input->post('mfeeset_id', true);
            $dp_id = $this->input->post('dp_id', true);
            $df_month = $this->input->post('df_month', true);
            $gongje_req_co = $this->input->post('gongje_req_co', true);

/*			$data['wst_mfee'] = $this->input->post('wst_mfee', true);
			$data['mfee_vat'] = $this->input->post('mfee_vat', true);
			$data['org_fee'] = $this->input->post('org_fee', true);
			$data['org_fee_vat'] = $this->input->post('org_fee_vat', true);
			$data['env_fee'] = $this->input->post('env_fee', true);
			$data['env_fee_vat'] = $this->input->post('env_fee_vat', true);
			$data['car_tax'] = $this->input->post('car_tax', true);
			$data['car_tax_vat'] = $this->input->post('car_tax_vat', true);
			$data['grg_fee'] = $this->input->post('grg_fee', true);
			$data['grg_fee_vat'] = $this->input->post('grg_fee_vat', true);
			$data['etc'] = $this->input->post('etc', true);
*/
			//기존등록정보 is_master = 'N'
			$sql = "UPDATE tbl_delivery_fee_fixmfee_set SET master_yn='N' WHERE dp_id = ?";
			$this->db->query($sql, array($dp_id));

			//신규 insert
			$sql = "INSERT INTO tbl_delivery_fee_fixmfee_set SET ";
			$sql .= "dp_id='".$dp_id."'";
			$sql .= ",wst_mfee='".$this->input->post('wst_mfee', true)."'";
			$sql .= ",mfee_vat='".$this->input->post('mfee_vat', true)."'";
			$sql .= ",org_fee='".$this->input->post('org_fee', true)."'";
			$sql .= ",org_fee_vat='".$this->input->post('org_fee_vat', true)."'";
			$sql .= ",env_fee='".$this->input->post('env_fee', true)."'";
			$sql .= ",env_fee_vat='".$this->input->post('env_fee_vat', true)."'";
			$sql .= ",car_tax='".$this->input->post('car_tax', true)."'";
			$sql .= ",car_tax_vat='".$this->input->post('car_tax_vat', true)."'";
			$sql .= ",grg_fee='".$this->input->post('grg_fee', true)."'";
			$sql .= ",grg_fee_vat='".$this->input->post('grg_fee_vat', true)."'";
			$sql .= ",etc='".$this->input->post('etc', true)."'";
			$sql .= ",master_yn = 'Y'";
			$sql .= ",reg_date = '".date('Y-m-d', time())."'";
			$this->db->query($sql);

/*			$this->cowork_model->_table_name = 'tbl_delivery_fee_fixmfee_set'; // table name
			$this->cowork_model->_primary_key = 'idx'; // $id
			$this->cowork_model->save($data, $mfeeset_id);*/

			//$delivery_fee_fixmfee_set_info = $this->db->where('idx', $mfeeset_id)->get('tbl_delivery_fee_fixmfee_set')->row();
			$activities = array(
				'user' => $this->session->userdata('user_id'),
				'module' => 'delivery_fee_fixmfee_set',
				'module_field_id' => $mfeeset_id,
				'activity' => 'activity_update_delivery_fee_fixmfee_set',
				'icon' => 'fa-user',
				'value1' => 'update'
			);
			$this->cowork_model->_table_name = 'tbl_activities';
			$this->cowork_model->_primary_key = "activities_id";
			$this->cowork_model->save($activities);

/*
            $wst_mfee = $this->input->post('wst_mfee', true);
            $mfee_vat = $this->input->post('mfee_vat', true);
            $org_fee = $this->input->post('org_fee', true);
            $org_fee_vat = $this->input->post('org_fee_vat', true);
            $env_fee = $this->input->post('env_fee', true);
            $env_fee_vat = $this->input->post('env_fee_vat', true);
            $car_tax = $this->input->post('car_tax', true);
            $car_tax_vat = $this->input->post('car_tax_vat', true);
            $grg_fee = $this->input->post('grg_fee', true);
            $grg_fee_vat = $this->input->post('grg_fee_vat', true);
            $etc = $this->input->post('etc', true);

			$activities = array(
				'wst_mfee' => $wst_mfee,
				'mfee_vat' => $mfee_vat,
				'org_fee' => $org_fee,
				'org_fee_vat' => $org_fee_vat,
				'env_fee' => $env_fee,
				'env_fee_vat' => $env_fee_vat,
				'car_tax' => $car_tax,
				'car_tax_vat' => $car_tax_vat,
				'grg_fee' => $grg_fee,
				'grg_fee_vat' => $grg_fee_vat,
				'etc' => $etc
			);
			$this->db->where('idx', $mfeeset_id);
			$this->cowork_model->_table_name = 'tbl_delivery_fee_fixmfee_set';
			$this->cowork_model->_primary_key = "idx";
			$this->cowork_model->save($activities);
*/
			$message = "관리비 설정이 수정되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/cowork/jiip_gongje/'.$df_month.'/'.$gongje_req_co); //redirect page
    }

    public function logis_settings()
    {
        $settings = $this->input->get('settings', TRUE) ? $this->input->get('settings', TRUE) : 'settings_mfee';
        $data['title'] = "기초데이터설정"; //Page title
        $can_do = can_do(111);
        if (!empty($can_do)) {
            $data['load_setting'] = $settings;
        } else {
            $data['load_setting'] = 'not_found';
        }
        $data['page'] = "기초데이터설정";
      //  $this->basic_model->_table_name = "tbl_logis_setting"; //table name
      //  $this->basic_model->_order_by = "id";

        // get default settings
      //  $data['default_settings'] = $this->basic_model->get('tbl_setting')->row();

        $data['subview'] = $this->load->view('admin/basic/settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
	}

    public function customer_list($action = NULL, $id = NULL)
    {

        $member_id = $id;

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_members', 'edit', array('asset_id' => $id));
			$this->db->where('member_id', $asset_id)->get('tbl_member')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('member_id', $asset_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '거래처 목록';

        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'dp_id';
        $this->basic_model->db->where('tbl_members.mb_type', 'customer');
        $data['all_customer_info'] = $this->basic_model->get();

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->basic_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->basic_model->get_permission('tbl_member');

        $data['all_designation_info'] = $this->basic_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/basic/customer_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function customer_details($id, $action = null)
    {
        if ($action == 'add_contacts') {
            // get all language
            $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
            // get all location
            $this->basic_model->_table_name = 'tbl_locales';
            $this->basic_model->_order_by = 'name';
            $data['locales'] = $this->basic_model->get();
            $data['company'] = $id;
            $user_id = $this->uri->segment(6);
            if (!empty($user_id)) {
                // get all user_info by user id
                $data['account_details'] = $this->basic_model->check_by(array('user_id' => $user_id), 'tbl_account_details');

                $data['user_info'] = $this->basic_model->check_by(array('user_id' => $user_id), 'tbl_users');
            }

        }
        $data['title'] = "거래처 상세 정보"; //Page title
        // get all client details
        $this->basic_model->_table_name = "tbl_members"; //table name
        $this->basic_model->_order_by = "dp_id";
        $data['customer_details'] = $this->basic_model->get_by(array('dp_id' => $id), TRUE);

/*
        // get all invoice by client id
        $this->basic_model->_table_name = "tbl_invoices"; //table name
        $this->basic_model->_order_by = "dp_id";
        $data['customer_invoices'] = $this->customer_model->get_by(array('dp_id' => $id), FALSE);

        // get all estimates by customer id
        $this->customer_model->_table_name = "tbl_estimates"; //table name
        $this->customer_model->_order_by = "dp_id";
        $data['customer_estimates'] = $this->customer_model->get_by(array('dp_id' => $id), FALSE);

        // get customer contatc by customer id
        $data['customer_contacts'] = $this->customer_model->get_customer_contacts($id);
*/
        $data['subview'] = $this->load->view('admin/basic/customer_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

	public function set_partner_status($action = NULL, $id = NULL, $page, $list_mode, $co_code)
    {
		$data['page'] = $page;
		$data['list_mode'] = $list_mode;
		$data['co_code'] = $co_code;
		$data['dp_id'] = $id;

		$data['dp'] = $this->db->where('dp_id', $id)->get('tbl_members')->row();
		if(!empty($data['dp']->tr_id)) $data['tr'] = $this->db->where('idx', $data['dp']->tr_id)->get('tbl_asset_truck')->row();
		$data['modal_subview'] = $this->load->view('admin/basic/_modal_partner_set_contract', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_partner_status($id = NULL, $status = NULL)
    {
		$data['page'] = $this->input->post('page', true);
		$data['list_mode'] = $this->input->post('list_mode', true);
		$data['co_code'] = $this->input->post('co_code', true);
		$data['tr_id'] = $this->input->post('tr_id', true);
		$data['out_date'] = $this->input->post('out_date', true);
		$data['gongT_date'] = $this->input->post('gongT_date', true);
		$data['return_cls_date'] = $this->input->post('return_cls_date', true);

		if(!empty($id)) {
			// 로그정보
			$dp = $this->basic_model->db->query("select * from tbl_members where dp_id = '".$id."'")->row();
			//현규씨작업
			if(!empty($dp->tr_id)) $tr = $this->basic_model->db->query("select car_1 from tbl_asset_truck where idx = '".$dp->tr_id."' ")->row();

			$sql = "INSERT INTO tbl_members_history SET dp_id='".$id."'";
			$sql .= ",ct_fdate = '".$dp->N."'";
			$sql .= ",ct_tdate = '".$dp->O."'";
			$sql .= ",rco_code = '".$dp->code."'";
			if(!empty($dp->tr_id)) $sql .= ",tr_id = '".$dp->tr_id."'";
			if(!empty($tr->car_1)) $sql .= ",car_no = '".$tr->car_1."'";
			$sql .= ",out_date = '" . $data['out_date'] . "'";
			$sql .= ",reg_datetime = '" . date('Y-m-d h:i:s') . "'";
			$this->db->query($sql);

			// 정보 변경
			$sql = "UPDATE tbl_members SET partner_status='".$status."' WHERE dp_id = '".$id."'";
			$this->db->query($sql);

			//차량 상태 변경
			$this->db->query($sql);
			if($status == "N") {
				if(!empty($dp->tr_id)) {
					$sql = "UPDATE tbl_asset_truck SET gongT_yn ='Y',gongT_date ='".$data['gongT_date']."',return_cls_date ='".$data['return_cls_date']."' WHERE idx = '".$dp->tr_id."'";
					$this->db->query($sql);
				}
			}

			$message = "파트너 상태정보가 변경되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }

		if(empty($ws_co_id) || $ws_co_id == "") 
			redirect('admin/basic/partner_list_all/'.$ws_co_id); //redirect page
		else
			redirect('admin/basic/partner_list/'.$ws_co_id); //redirect page
    }

    public function partner_list($action = NULL, $id = NULL)
    {
		$dp_id = $id;
		$data['page'] = $this->input->post('page', true);
		if(empty($data['page']) || $data['page'] < 1) $data['page'] =  1;
		$data['list_mode'] = $this->input->post('list_mode', true);
        if (empty($data['list_mode'])) $data['list_mode'] = "ALL";

		$data['co_code'] = $this->input->post('co_code', true);
        if (empty($data['co_code'])) $data['co_code'] = "kn4021";

		$data['vX'] = $this->input->post('vX', true);

        if ($action == 'edit_partner') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_members', 'edit', array('dp_id' => $dp_id));
			$this->db->where('dp_id', $dp_id)->get('tbl_members')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {

				//$this->basic_model->_table_name = ' tbl_members dp'; //table name
				//$this->basic_model->db->select('dp.*, tr.car_7 ');
				//$this->basic_model->db->join('tbl_asset_truck tr', 'dp.tr_id = tr.idx');
				//$this->basic_model->db->where("dp.dp_id = '".$dp_id."'");
				//$data['partner_info'] = $this->basic_model->get()->row();
				$qry = "select dp.*, tr.car_7 from tbl_members dp left join tbl_asset_truck tr on dp.tr_id = tr.idx";
				$qry .= " where dp.dp_id = '".$dp_id."'";
				$data['partner_info'] = $this->basic_model->db->query($qry)->row();
               // $data['partner_info'] = $this->db->where('dp_id', $dp_id)->get('tbl_members')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '파트너 목록';

		//지입사 목록
        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'co_name';
        $this->basic_model->db->where("mb_type='group' or mb_type='coop'");
        $data['all_jiip_info'] = $this->basic_model->get();

        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'dp_id';
        $this->basic_model->db->where('tbl_members.code', $data['co_code']);
		if(!empty($this->input->post('vX', true))) $this->basic_model->db->where('tbl_members.X', $this->input->post('vX', true));
        $this->basic_model->db->where('tbl_members.mb_type', 'partner');
		
		if($data['list_mode'] == "IN") { //계약중 파트너
			$this->basic_model->db->where("(partner_status = 'Y' and O >= '".date('Y-m-d', time())."')");
		} else if($data['list_mode'] == "WT" || $data['list_mode'] == "WTU") { //계약중료대기 파트너 
			$this->basic_model->db->where("(partner_status = 'Y' and O <= '".date('Y-m-d', time())."')");
		} else if($data['list_mode'] == "EP") { //계약중료 파트너
			$this->basic_model->db->where('partner_status', 'N');
		}

        $data['all_partner_info'] = $this->basic_model->get();

/*
		//계약중료대기 파트너 차량업데이트
		if($data['list_mode'] == "WTU") { 
			if (!empty($data['all_partner_info'])) {
				foreach ($data['all_partner_info'] as $pd) {

					$tr_id = $pd->tr_id;
					//기존 현규씨 데이터 보정(차량정보가 제데로 연결되지 않았음)
					if(empty($pd->tr_id) && !empty($pd->U)) {
						$tr = $this->db->where('car_1', TRIM($pd->U))->get('tbl_asset_truck')->row();
						if(!empty($tr->idx)) {
							$sql = "UPDATE tbl_members SET tr_id='".$tr->idx."' WHERE dp_id = '".$pd->dp_id."'";
							$this->db->query($sql);
							$tr_id = $tr->idx;
						}
					}


					// 1.  (요청일 : 파트너계약종료일) 차량정보 동시적으로  파트너 및 해당 파트너의 차량번호등 종료대기현황 > 번호반납예정등록현황 동시발생
					if(!empty($tr_id)) {
						$return_ready = "Y";
						$return_due_date = $pd->O;

						$sql = "UPDATE tbl_asset_truck SET return_ready='Y', return_due_date='$return_due_date'";
						$sql .= " WHERE idx='$tr_id'";
						$this->db->query($sql);
					}


				}
			}
		}
*/

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->basic_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->basic_model->get_permission('tbl_member');

        $data['all_designation_info'] = $this->basic_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/basic/partner_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function partner_list_all($action = NULL, $id = NULL)
    {
		$data['page'] = $this->input->post('page', true);
		if(empty($data['page']) || $data['page'] < 1) $data['page'] =  1;
		$data['list_mode'] = $this->input->post('list_mode', true);
        if (empty($data['list_mode'])) $data['list_mode'] = "ALL";

		$data['co_code'] = $this->input->post('co_code', true);
		$data['search_field'] = $this->input->post('search_field', true);
		$data['search_keyword'] = $this->input->post('search_keyword', true);
		$data['vX'] = $this->input->post('vX', true);

        if ($action == 'edit_partner') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_members', 'edit', array('dp_id' => $dp_id));
			$this->db->where('dp_id', $dp_id)->get('tbl_members')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['partner_info'] = $this->db->where('dp_id', $dp_id)->get('tbl_members')->row();
            }
        } else {
            $data['active'] = 1;
        }
		
		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($limit)) $limit = 20;

        $this->basic_model->_table_name = 'tbl_members dp'; //table name          
		if(!empty($this->input->post('vX', true))) $this->basic_model->db->where('dp.X', $this->input->post('vX', true));
		if(!empty($data['search_keyword'])) {
			$this->basic_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.reg_number like \'%'.$data['search_keyword'].'%\' OR dp.L like \'%'.$data['search_keyword'].'%\' OR dp.M like \'%'.$data['search_keyword'].'%\' OR dp.bs_number like \'%'.$data['search_keyword'].'%\' OR dp.U like \'%'.$data['search_keyword'].'%\' OR dp.W like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\')' );
		}
        $this->basic_model->db->where('dp.mb_type', 'partner');
		if($data['list_mode'] == "IN") { //계약중 파트너
			$this->basic_model->db->where("(dp.partner_status = 'Y' and dp.O >= '".date('Y-m-d', time())."')");
		} else if($data['list_mode'] == "WT" || $data['list_mode'] == "WTU") { //계약중료대기 파트너  date('Y-m-d', 
			$this->basic_model->db->where("(dp.partner_status = 'Y' and dp.O <= '".date('Y-m-d', time())."')");
		} else if($data['list_mode'] == "EP") { //계약중료 파트너
			$this->basic_model->db->where('dp.partner_status', 'N');
		}
		$data['total_count'] = count($this->basic_model->get());

		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함

        $this->basic_model->_table_name = 'tbl_members dp'; //table name
		$this->basic_model->db->select('dp.*, tr.car_1, tr.car_7, tr.car_3');
		$this->basic_model->db->join('tbl_asset_truck tr', 'dp.tr_id = tr.idx', 'left');
        $this->basic_model->_order_by = 'dp.co_name';
		if(!empty($this->input->post('vX', true))) $this->basic_model->db->where('X', $this->input->post('vX', true));
		if(!empty($data['search_keyword'])) {
			$this->basic_model->db->or_where('(dp.co_name like \'%'.$data['search_keyword'].'%\' OR dp.ceo like \'%'.$data['search_keyword'].'%\' OR dp.reg_number like \'%'.$data['search_keyword'].'%\' OR dp.L like \'%'.$data['search_keyword'].'%\' OR dp.M like \'%'.$data['search_keyword'].'%\' OR dp.bs_number like \'%'.$data['search_keyword'].'%\' OR dp.U like \'%'.$data['search_keyword'].'%\' OR dp.W like \'%'.$data['search_keyword'].'%\' OR dp.driver like \'%'.$data['search_keyword'].'%\')' );
		//	$this->basic_model->db->or_where('co_name like', '%'.$data['search_keyword'].'%' )->or_where('ceo like', '%'.$data['search_keyword'].'%' )->or_where('reg_number like', '%'.$data['search_keyword'].'%' )->or_where('L like', '%'.$data['search_keyword'].'%' )->or_where('M like', '%'.$data['search_keyword'].'%' )->or_where('bs_number like', '%'.$data['search_keyword'].'%' )->or_where('U like', '%'.$data['search_keyword'].'%' )->or_where('W like', '%'.$data['search_keyword'].'%' )->or_where('driver like', '%'.$data['search_keyword'].'%' );
		}
        $this->basic_model->db->where('dp.mb_type', 'partner');
		if($data['list_mode'] == "IN") { //계약중 파트너
			$this->basic_model->db->where("(dp.partner_status = 'Y' and dp.O >= '".date('Y-m-d', time())."')");
		} else if($data['list_mode'] == "WT" || $data['list_mode'] == "WTU") { //계약중료대기 파트너 
			$this->basic_model->db->where("(dp.partner_status = 'Y' and dp.O <= '".date('Y-m-d', time())."')");
		} else if($data['list_mode'] == "EP") { //계약중료 파트너
			$this->basic_model->db->where('dp.partner_status', 'N');
		}
		$this->basic_model->db->limit($limit, $data['from_record']);
		//if($data['list_mode'] <> "WTU") $this->basic_model->db->limit($limit, $data['from_record']);
        $data['all_partner_info'] = $this->basic_model->get();

/*
		//계약중료대기 파트너 차량업데이트
		if($data['list_mode'] == "WTU") { 
			if (!empty($data['all_partner_info'])) {
				foreach ($data['all_partner_info'] as $pd) {

					$tr_id = $pd->tr_id;
					//기존 현규씨 데이터 보정(차량정보가 제데로 연결되지 않았음)
					if(empty($pd->tr_id) && !empty($pd->U)) {
						$tr = $this->db->where('car_1', TRIM($pd->U))->get('tbl_asset_truck')->row();
						if(!empty($tr->idx)) {
							$sql = "UPDATE tbl_members SET tr_id='".$tr->idx."' WHERE dp_id = '".$pd->dp_id."'";
							$this->db->query($sql);
							$tr_id = $tr->idx;
						}
					}


					// 1.  (요청일 : 파트너계약종료일) 차량정보 동시적으로  파트너 및 해당 파트너의 차량번호등 종료대기현황 > 번호반납예정등록현황 동시발생
					if(!empty($tr_id)) {
						$return_ready = "Y";
						$return_due_date = $pd->O;

						$sql = "UPDATE tbl_asset_truck SET return_ready='Y', return_due_date='$return_due_date'";
						$sql .= " WHERE idx='$tr_id'";
						$this->db->query($sql);
					}


				}
			}
		}
*/

		//지입사 목록
        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'co_name';
        $this->basic_model->db->where("mb_type='group' or mb_type='coop'");
        $data['all_jiip_info'] = $this->basic_model->get();
        $data['title'] = '파트너 목록';

        $data['subview'] = $this->load->view('admin/basic/partner_list_all', $data, true);
        $this->load->view('admin/_layout_main_nd', $data);
    }

	// 파트너저장
    public function save_partner($id = NULL)
    {
		$dp_id = $id;
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
			$sql_upload = "";
            if (!empty($_FILES['mb_pic']['name'])) {
                $val = $this->basic_model->uploadImage('mb_pic','partner_avatar');
                $val == TRUE || redirect('admin/basic/partner_list/'.$ws_co_id);
				$sql_upload .= ", mb_pic = '".$val['path']."'";
            }
            if (!empty($_FILES['attach1']['name'])) {
                $val = $this->basic_model->customUploadFile('attach1','partner_file');
                $val == TRUE || redirect('admin/basic/partner_list/'.$ws_co_id);
				$sql_upload .= ", attach1 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach2']['name'])) {
                $val = $this->basic_model->customUploadFile('attach2','partner_file');
                $val == TRUE || redirect('admin/basic/partner_list/'.$ws_co_id);
				$sql_upload .= ", attach2 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach3']['name'])) {
                $val = $this->basic_model->customUploadFile('attach3','partner_file');
                $val == TRUE || redirect('admin/basic/partner_list/'.$ws_co_id);
				$sql_upload .= ", attach3 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach4']['name'])) {
                $val = $this->basic_model->customUploadFile('attach4','partner_file');
                $val == TRUE || redirect('admin/basic/partner_list/'.$ws_co_id);
				$sql_upload .= ", attach4 = '".$val['path']."'";
            }
            if (!empty($_FILES['attach5']['name'])) {
                $val = $this->basic_model->customUploadFile('attach1','partner_file');
                $val == TRUE || redirect('admin/basic/partner_list/'.$ws_co_id);
				$sql_upload .= ", attach5 = '".$val['path']."'";
            }
			
			$this->basic_model->_table_name = 'tbl_members'; //table name
            $result = $this->basic_model->get_by(array('dp_id' => $dp_id), true);

            if (!empty($result)) {
				// 기존 선택차량이 있고 변경 되었을 경우 - 기사없음 상태로 변경
                $ptr = $this->db->where('idx', $this->input->post('tr_id', true))->get('tbl_asset_truck')->row();
				if(   (!empty($ptr->tr_id) && !empty($this->input->post('tr_id', true))) && ($ptr->tr_id != $this->input->post('tr_id', true))  
					|| (!empty($ptr->tr_id) && empty($this->input->post('tr_id', true)))  		) {
					$sql = "UPDATE tbl_asset_truck SET car_status='N' WHERE idx = '".$this->input->post('tr_id', true)."'";
					$this->db->query($sql);
				}

				$sql = "UPDATE tbl_members SET ";
				$sql .= "code='".$this->input->post('code', true)."'";
				$sql .= ",userid='".$this->input->post('userid', true)."'";
				$sql .= ",userpwd='".$this->input->post('userpwd', true)."'";
				$sql .= ",co_name='".$this->input->post('co_name', true)."'";
				$sql .= ",ceo='".$this->input->post('ceo', true)."'";
				$sql .= ",driver='".$this->input->post('driver', true)."'";
				$sql .= ",G='".$this->input->post('G', true)."'";
				$sql .= ",reg_number='".$this->input->post('reg_number', true)."'";
				$sql .= ",L='".$this->input->post('L', true)."'";
				$sql .= ",M='".$this->input->post('M', true)."'";
				$sql .= ",N='".$this->input->post('N', true)."'";
				$sql .= ",O='".$this->input->post('O', true)."'";
				$sql .= ",tax_yn='".$this->input->post('tax_yn', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$sql .= ",bs_number='".$this->input->post('bs_number', true)."'";
				$sql .= ",T='".$this->input->post('T', true)."'";
				$sql .= ",bs_type1='".$this->input->post('bs_type1', true)."'";
				$sql .= ",bs_type2='".$this->input->post('bs_type2', true)."'";
				$sql .= ",tr_id='".$this->input->post('tr_id', true)."'";
				$sql .= ",U='".$this->input->post('U', true)."'";
				$sql .= ",V='".$this->input->post('V', true)."'";
				$sql .= ",W='".$this->input->post('W', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",X='".$this->input->post('X', true)."'";
				$sql .= ",co_tel='".$this->input->post('co_tel', true)."'";
				$sql .= ",Z='".$this->input->post('Z', true)."'";
				$sql .= ",AA='".$this->input->post('AA', true)."'";
				$sql .= ",AB='".$this->input->post('AB', true)."'";
				$sql .= ",AC='".$this->input->post('AC', true)."'";
				$sql .= ",AD='".$this->input->post('AD', true)."'";
				$sql .= ",email='".$this->input->post('email', true)."'";
				$sql .= ",co_address='".$this->input->post('co_address', true)."'";
				$sql .= ",remark ='".$this->input->post('remark', true)."'";
				$sql .= ",attach1_text='".$this->input->post('attach1_text', true)."'";
				$sql .= ",attach2_text='".$this->input->post('attach2_text', true)."'";
				$sql .= ",attach3_text='".$this->input->post('attach3_text', true)."'";
				$sql .= ",attach4_text='".$this->input->post('attach4_text', true)."'";
				$sql .= ",attach5_text='".$this->input->post('attach5_text', true)."'". $sql_upload ;
				$sql .= " WHERE dp_id = '".$dp_id."'";
				$this->db->query($sql);
			} else { 
				//아이디 정보가 없을경우 1. 주민등록번호 없으면 2. 사업자등록번호


				$sql = "INSERT INTO tbl_members SET ";
				$sql .= "mb_type='partner'";
				$sql .= ",code='".$this->input->post('code', true)."'";
				$sql .= ",userid='".$this->input->post('userid', true)."'";
				$sql .= ",userpwd='1111'";
				$sql .= ",co_name='".$this->input->post('co_name', true)."'";
				$sql .= ",ceo='".$this->input->post('ceo', true)."'";
				$sql .= ",driver='".$this->input->post('driver', true)."'";
				$sql .= ",G='".$this->input->post('G', true)."'";
				$sql .= ",reg_number='".$this->input->post('reg_number', true)."'";
				$sql .= ",L='".$this->input->post('L', true)."'";
				$sql .= ",M='".$this->input->post('M', true)."'";
				$sql .= ",N='".$this->input->post('N', true)."'";
				$sql .= ",O='".$this->input->post('O', true)."'";
				$sql .= ",tax_yn='".$this->input->post('tax_yn', true)."'";
				$sql .= ",bs_number='".$this->input->post('bs_number', true)."'";
				$sql .= ",T='".$this->input->post('T', true)."'";
				$sql .= ",bs_type1='".$this->input->post('bs_type1', true)."'";
				$sql .= ",bs_type2='".$this->input->post('bs_type2', true)."'";
				$sql .= ",tr_id='".$this->input->post('tr_id', true)."'";
				$sql .= ",U='".$this->input->post('U', true)."'";
				$sql .= ",V='".$this->input->post('V', true)."'";
				$sql .= ",W='".$this->input->post('W', true)."'";
				$sql .= ",ws_co_id='".$this->input->post('ws_co_id', true)."'";
				$sql .= ",X='".$this->input->post('X', true)."'";
				$sql .= ",co_tel='".$this->input->post('co_tel', true)."'";
				$sql .= ",Z='".$this->input->post('Z', true)."'";
				$sql .= ",AA='".$this->input->post('AA', true)."'";
				$sql .= ",AB='".$this->input->post('AB', true)."'";
				$sql .= ",AC='".$this->input->post('AC', true)."'";
				$sql .= ",AD='".$this->input->post('AD', true)."'";
				$sql .= ",email='".$this->input->post('email', true)."'";
				$sql .= ",co_address='".$this->input->post('co_address', true)."'";
				$sql .= ",remark='".$this->input->post('remark', true)."'";
				$sql .= ",attach1_text='".$this->input->post('attach1_text', true)."'";
				$sql .= ",attach2_text='".$this->input->post('attach2_text', true)."'";
				$sql .= ",attach3_text='".$this->input->post('attach3_text', true)."'";
				$sql .= ",attach4_text='".$this->input->post('attach4_text', true)."'";
				$sql .= ",attach5_text='".$this->input->post('attach5_text', true)."'". $sql_upload ;
				$this->db->query($sql);
				$dp_id = $this->db->insert_id();
			}

			//공티이 차량일경우 대기로 이동
			if(!empty($this->input->post('tr_id', true))) { 
                $tr = $this->db->where('idx', $this->input->post('tr_id', true))->get('tbl_asset_truck')->row();

				if($tr->car_status=="G") { //변경 차량이 공티이 차량일경우 이력으로 넘기고 새 차량 삽입(번호/소속), 변경된 차량 키 저장
					$sql = "UPDATE tbl_asset_truck SET car_status='H' WHERE idx = '".$this->input->post('tr_id', true)."'";
					$this->db->query($sql);

					$sql = "INSERT INTO tbl_asset_truck SET ";
					$sql .= "p_tr_id='".$this->input->post('tr_id', true)."'";
					$sql .= ",gr_id='".$tr->gr_id."'";
					$sql .= ",car_1='".$tr->car_1."'";
					$sql .= ",ws_co_id='".$tr->ws_co_id."'";
					$sql .= ",car_status='R'";
					$this->db->query($sql);
					$new_tr_id = $this->db->insert_id();

					$sql = "UPDATE tbl_members SET tr_id='".$new_tr_id."' WHERE dp_id = '".$dp_id."'";
					$this->db->query($sql);
				} else { //공티이 차량이 아닐경우
					$sql = "UPDATE tbl_asset_truck SET car_status='A' WHERE idx = '".$this->input->post('tr_id', true)."'";
					$this->db->query($sql);
				}
			}


			$message = "파트너정보가 저장되었습니다.";
            $type = 'success';
            
            set_message($type, $message);
        }
        redirect('admin/basic/partner_list/'.$ws_co_id); //redirect page
    } 


    public function set_item($ws_co_id = NULL, $type = 'sudang', $cat = '일반', $action = NULL, $id = NULL)
    {
        $created = can_action('123', 'created');
        $edited = can_action('123', 'edited');

		$data['ws_co_id'] = $ws_co_id;
		$data['type'] = $type;
		$data['cat'] = $cat;

		$data['edit_item_info'] = "";
        if ($action == 'edit_item') {
            $data['active'] = 2;
            if (!empty($id) && !empty($edited)) {
                $data['edit_item_info'] = $this->basic_model->check_by(array('idx' => $id), 'tbl_dp_assign_item');
            }
        } else {
            $data['active'] = 1;
        }


        if ($action == 'update') {
            if (!empty($created) || !empty($edited)) {
                $this->basic_model->_table_name = 'tbl_dp_assign_item';
                $this->basic_model->_primary_key = 'idx';

                $item_data['ws_co_id'] = $ws_co_id;
                $item_data['p_category'] = $cat;
                $item_data['type'] = $type;
                $item_data['item'] = $this->input->post('item', TRUE);
                $item_data['remark'] = $this->input->post('remark', TRUE);

				if (!empty($id)) { 
                    $item_id = array('idx !=' => $id);
                } else {  
                    $item_id = null;
                }
                $where = array('ws_co_id' => $ws_co_id, 'item' => $item_data['item']);
                $check_item = $this->basic_model->check_update('tbl_dp_assign_item', $where, $item_id);
                if (!empty($check_item)) { 
                    // massage for user
                  //  $type = 'error';
                  //  $msg = "<strong style='color:#000'>" . $item_data['item'] . '</strong> 은 이미 존재합니다 ';
                } else { // save and update query
                    $id = $this->basic_model->save($item_data, $id);

                    $activity = array(
                        'user' => $this->session->userdata('user_id'),
                        'module' => 'basic',
                        'module_field_id' => $id,
                        'activity' => ('activity_added_a_dp_item'),
                        'value1' => $item_data['item']
                    );
                    $this->basic_model->_table_name = 'tbl_activities';
                    $this->basic_model->_primary_key = 'activities_id';
                    $this->basic_model->save($activity);

                    // messages for user
                   // $type = "success";
                   // $msg = "항목이 추가되었습니다.";
                }
               // $message = $msg;
                //set_message($type, $message);
            }
            redirect('admin/basic/set_item/'.$ws_co_id.'/'.$type.'/');
       // } else {
       //     $data['title'] = lang('income_category'); //Page title
       //     $data['load_setting'] = 'income_category';
        }
        $this->basic_model->_table_name = 'tbl_dp_assign_item'; //table name
        $this->basic_model->db->where('tbl_dp_assign_item.type', $type);
        $this->basic_model->db->where('tbl_dp_assign_item.p_category', $cat);
        $this->basic_model->db->where('tbl_dp_assign_item.ws_co_id', $ws_co_id);
        $this->basic_model->_order_by = 'item';
        $data['all_item_group'] = $this->basic_model->get();

		if($type == "sudang") {
			$data['title'] = '수당항목 설정';
			if($cat == "01") $data['title'] = "<font style='font-weight:bold;color:blue;'>각종</font>".$data['title'];
			$data['subview'] = $this->load->view('admin/basic/pop_set_item', $data, true);
		} else if($type == "gongje") {
			$data['title'] = '공제항목 설정';
			if($cat == "01") $data['title'] = "<font style='font-weight:bold;color:red;'>위.수탁관리비</font>".$data['title'];
			else if($cat == "02") $data['title'] = "<font style='font-weight:bold;color:red;'>일반</font>".$data['title'];
			else if($cat == "03") $data['title'] = "<font style='font-weight:bold;color:red;'>환급형</font>".$data['title'];
			$data['subview'] = $this->load->view('admin/basic/pop_set_item', $data, true);
		}
        $this->load->view('admin/_layout_pop_set_item', $data);
	}

    public function delete_item($ws_co_id = NULL, $type = 'sudang', $id)
    {
        $deleted = can_action('122', 'deleted');
        if (!empty($deleted)) {
            $item_info = $this->settings_model->check_by(array('idx' => $id), 'tbl_dp_assign_item');
            $activity = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'basic',
                'module_field_id' => $id,
                'activity' => ('activity_delete_a_dp_item'),
                'value1' => $item_info->item,
            );
            $this->settings_model->_table_name = 'tbl_activities';
            $this->settings_model->_primary_key = 'activities_id';
            $this->settings_model->save($activity);

            $this->settings_model->_table_name = 'tbl_dp_assign_item';
            $this->settings_model->_primary_key = 'idx';
            $this->settings_model->delete($id);
            // messages for user
            $type = "success";
            $message = "항목이 삭제되었습니다";
            // messages for user
            echo json_encode(array("status" => $type, 'message' => $message));
            exit();
        } else {
            echo json_encode(array("status" => 'error', 'message' => lang('there_in_no_value')));
            exit();
        }
    }

    public function select_company($co_type = NULL, $params = NULL, $page = NULL)
    {

		$data['co_type'] = $co_type;
		$data['params'] = $params;
		$data['page'] = $page;

		$data['search_field'] = $this->input->post('search_field', true);
		$data['search_keyword'] = $this->input->post('search_keyword', true);
		
		if(empty($data['from_record'])) $data['from_record'] = 0;
		if(empty($limit)) $limit = 20;

        $this->basic_model->_table_name = 'tbl_members'; //table name
		if(!empty($data['search_field']) && !empty($data['search_keyword'])) {
			$this->basic_model->db->where('tbl_members.'.$data['search_field'].' like', '%'.$data['search_keyword'].'%' );
		}
        if($co_type != "all") $this->basic_model->db->where('tbl_members.mb_type', $co_type);
		$data['total_count'] = count($this->basic_model->get());

		$data['total_page']  = ceil($data['total_count'] / $limit);  // 전체 페이지 계산
		if (empty($data['page'])) $data['page'] = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
		$data['from_record'] = ($data['page'] - 1) * $limit; // 시작 열을 구함

        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'co_name';
		if(!empty($data['search_field']) && !empty($data['search_keyword'])) {
			$this->basic_model->db->where('tbl_members.'.$data['search_field'].' like', '%'.$data['search_keyword'].'%' );
		}
        if($co_type != "all") $this->basic_model->db->where('tbl_members.mb_type', $co_type);
		$this->basic_model->db->limit($limit, $data['from_record']);

        $data['all_company_info'] = $this->basic_model->get();
		if($co_type == "partner") {
			$data['title'] = '파트너 선택';
			$data['subview'] = $this->load->view('admin/basic/pop_partner_list', $data, true);
		} else if($co_type == "customer") {
			$data['title'] = '거래처 선택';
			$data['subview'] = $this->load->view('admin/basic/pop_customer_list', $data, true);
		} else if($co_type == "coop") {
			$data['title'] = '협력사 선택';
			$data['subview'] = $this->load->view('admin/basic/pop_coop_list', $data, true);
		} else if($co_type == "group") {
			$data['title'] = '그룹사 선택';
			$data['subview'] = $this->load->view('admin/basic/pop_group_list', $data, true);
		} else if($co_type == "all") {
			$data['title'] = '전체 관계사';
			$data['subview'] = $this->load->view('admin/basic/pop_allco_list', $data, true);
		}
        $this->load->view('admin/_layout_pop', $data);
    }

    public function coop_list($action = NULL, $id = NULL)
    {

        $member_id = $id;

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_members', 'edit', array('asset_id' => $id));
			$this->db->where('member_id', $asset_id)->get('tbl_member')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('member_id', $asset_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '협력사 목록';

        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'dp_id';
        $this->basic_model->db->where('tbl_members.mb_type', 'coop');
        $data['all_coop_info'] = $this->basic_model->get();

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->basic_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->basic_model->get_permission('tbl_member');

        $data['all_designation_info'] = $this->basic_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/basic/coop_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function coop_details($id, $action = null)
    {
        if ($action == 'add_contacts') {
            // get all language
            $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
            // get all location
            $this->basic_model->_table_name = 'tbl_locales';
            $this->basic_model->_order_by = 'name';
            $data['locales'] = $this->basic_model->get();
            $data['company'] = $id;
            $user_id = $this->uri->segment(6);
            if (!empty($user_id)) {
                // get all user_info by user id
                $data['account_details'] = $this->basic_model->check_by(array('user_id' => $user_id), 'tbl_account_details');

                $data['user_info'] = $this->basic_model->check_by(array('user_id' => $user_id), 'tbl_users');
            }

        }
        $data['title'] = "협력사 상세 정보"; //Page title
        // get all coop details
        $this->basic_model->_table_name = "tbl_members"; //table name
        $this->basic_model->_order_by = "dp_id";
        $data['coop_details'] = $this->basic_model->get_by(array('dp_id' => $id), TRUE);

/*
        // get all invoice by client id
        $this->basic_model->_table_name = "tbl_invoices"; //table name
        $this->basic_model->_order_by = "dp_id";
        $data['customer_invoices'] = $this->customer_model->get_by(array('dp_id' => $id), FALSE);

        // get all estimates by customer id
        $this->customer_model->_table_name = "tbl_estimates"; //table name
        $this->customer_model->_order_by = "dp_id";
        $data['customer_estimates'] = $this->customer_model->get_by(array('dp_id' => $id), FALSE);

        // get customer contatc by customer id
        $data['customer_contacts'] = $this->customer_model->get_customer_contacts($id);
*/
        $data['subview'] = $this->load->view('admin/basic/coop_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function group_list($action = NULL, $id = NULL)
    {
        $dp_id = $id;

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_members', 'edit', array('asset_id' => $dp_id));
			$this->db->where('dp_id', $dp_id)->get('tbl_member')->row();

            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('dp_id', $dp_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = '그룹사 목록';

        $this->basic_model->_table_name = 'tbl_members'; //table name
        $this->basic_model->_order_by = 'dp_id';
        $this->basic_model->db->where('tbl_members.mb_type', 'group');
        $data['all_group_info'] = $this->basic_model->get();

        $data['subview'] = $this->load->view('admin/basic/group_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function branch_list($action = NULL, $id = NULL)
    {

        $member_id = $id;

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_member', 'edit', array('asset_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('member_id', $asset_id)->get('tbl_member')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'asset List';

        $this->basic_model->_table_name = 'tbl_client'; //table name
        $this->basic_model->_order_by = 'client_id';
        $data['all_client_info'] = $this->basic_model->get();

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->basic_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->basic_model->get_permission('tbl_member');

        $data['all_designation_info'] = $this->basic_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/basic/branch_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }


    public function asset_list($action = NULL, $id = NULL)
    {

        $asset_id = $id;

        if ($action == 'edit_asset') {
            $data['active'] = 2;
            $can_edit = $this->basic_model->can_action('tbl_assets', 'edit', array('asset_id' => $id));
            $edited = can_action('24', 'edited');
            if (!empty($can_edit) || !empty($edited)) {
                $data['login_info'] = $this->db->where('asset_id', $asset_id)->get('tbl_assets')->row();
            }
        } else {
            $data['active'] = 1;
        }

        $data['title'] = 'asset List';

        $this->basic_model->_table_name = 'tbl_client'; //table name
        $this->basic_model->_order_by = 'client_id';
        $data['all_client_info'] = $this->basic_model->get();

        // get all language
        $data['languages'] = $this->db->where('active', 1)->order_by('name', 'ASC')->get('tbl_languages')->result();
/*
        $data['permission_asset'] = $this->basic_model->all_permission_asset('24');
        $data['all_asset_info'] = $this->basic_model->get_permission('tbl_assets');

        $data['all_designation_info'] = $this->basic_model->all_designation();
*/
        $data['subview'] = $this->load->view('admin/asset/asset_list', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }

    public function asset_details($id, $active = null)
    {
        $data['title'] = lang('asset_details');
        $data['id'] = $id;
        if (!empty($active)) {
            $data['active'] = $active;
        } else {
            $data['active'] = 1;
        }
        $date = $this->input->post('date', true);
        if (!empty($date)) {
            $data['date'] = $date;
        } else {
            $data['date'] = date('Y-m');
        }
        $data['attendace_info'] = $this->get_report($id, $data['date']);

        $data['my_leave_report'] = leave_report($id);

        //
        if ($this->input->post('year', TRUE)) { // if input year
            $data['year'] = $this->input->post('year', TRUE);
        } else { // else current year
            $data['year'] = date('Y'); // get current year
        }
        // get all expense list by year and month
        $data['provident_fund_info'] = $this->get_provident_fund_info($data['year'], $id);

        if ($this->input->post('overtime_year', TRUE)) { // if input year
            $data['overtime_year'] = $this->input->post('overtime_year', TRUE);
        } else { // else current year
            $data['overtime_year'] = date('Y'); // get current year
        }
        // get all expense list by year and month
        $data['all_overtime_info'] = $this->get_overtime_info($data['overtime_year'], $id);
        $data['profile_info'] = $this->db->where('asset_id', $id)->get('tbl_account_details')->row();

        $data['total_attendance'] = count($this->total_attendace_in_month($id));

        $data['total_absent'] = count($this->total_attendace_in_month($id, 'absent'));

        $data['total_leave'] = count($this->total_attendace_in_month($id, 'leave'));
        //award received
        $data['total_award'] = count($this->db->where('asset_id', $id)->get('tbl_employee_award')->result());

        // get working days holiday
        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday

        $num = cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y'));
        $working_holiday = 0;
        for ($i = 1; $i <= $num; $i++) {
            $day_name = date('l', strtotime("+0 days", strtotime(date('Y') . '-' . date('n') . '-' . $i)));

            if (!empty($holidays)) {
                foreach ($holidays as $v_holiday) {
                    if ($v_holiday->day == $day_name) {
                        $working_holiday += count($day_name);
                    }
                }
            }
        }
        // get public holiday
        $public_holiday = count($this->total_attendace_in_month($id, TRUE));

        // get total days in a month
        $month = date('m');
        $year = date('Y');
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        // total attend days in a month without public holiday and working days
        $data['total_days'] = $days - $working_holiday - $public_holiday;

        $data['all_working_hour'] = $this->all_attendance_id_by_date($id, true);

        $data['this_month_working_hour'] = $this->all_attendance_id_by_date($id);

        $data['subview'] = $this->load->view('admin/asset/asset_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function all_attendance_id_by_date($asset_id, $flag = null)
    {
        if (!empty($flag)) {
            $get_total_attendance = $this->db->where(array('asset_id' => $asset_id, 'attendance_status' => '1'))->get('tbl_attendance')->result();
            if (!empty($get_total_attendance)) {
                foreach ($get_total_attendance as $v_attendance_id) {
                    $aresult[] = $this->global_model->get_total_working_hours($v_attendance_id->attendance_id);
                }
                return $aresult;
            }
        } else {

            $month = date('n');
            $year = date('Y');
            if ($month >= 1 && $month <= 9) {
                $yymm = $year . '-' . '0' . $month;
            } else {
                $yymm = $year . '-' . $month;
            }
            $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            for ($i = 1; $i <= $num; $i++) {
                if ($i >= 1 && $i <= 9) {
                    $sdate = $yymm . '-' . '0' . $i;
                } else {
                    $sdate = $yymm . '-' . $i;
                }
                $get_total_attendance = $this->global_model->get_total_attendace_by_date($sdate, $sdate, $asset_id); // get all attendace by start date and in date
                if (!empty($get_total_attendance)) {
                    foreach ($get_total_attendance as $v_attendance_id) {
                        $result[] = $this->global_model->get_total_working_hours($v_attendance_id->attendance_id);
                    }
                }
            }
            if (!empty($result)) {
                return $result;
            }
        }
    }

    public function total_attendace_in_month($asset_id, $flag = NULL)
    {
        $month = date('m');
        $year = date('Y');

        if ($month >= 1 && $month <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
            $start_date = $year . "-" . '0' . $month . '-' . '01';
            $end_date = $year . "-" . '0' . $month . '-' . '31';
        } else {
            $start_date = $year . "-" . $month . '-' . '01';
            $end_date = $year . "-" . $month . '-' . '31';
        }
        if (!empty($flag) && $flag == 1) { // if flag is not empty that means get pulic holiday
            $get_public_holiday = $this->global_model->get_holiday_list_by_date($start_date, $end_date);

            if (!empty($get_public_holiday)) { // if not empty the public holiday
                foreach ($get_public_holiday as $v_holiday) {
                    if ($v_holiday->start_date == $v_holiday->end_date) { // if start date and end date is equal return one data
                        $total_holiday[] = $v_holiday->start_date;
                    } else { // if start date and end date not equan return all date
                        for ($j = $v_holiday->start_date; $j <= $v_holiday->end_date; $j++) {
                            $total_holiday[] = $j;
                        }
                    }
                }
                return $total_holiday;
            }
        } elseif (!empty($flag)) { // if flag is not empty that means get pulic holiday
            $get_total_absent = $this->global_model->get_total_attendace_by_date($start_date, $end_date, $asset_id, $flag); // get all attendace by start date and in date
            return $get_total_absent;
        } else {
            $get_total_attendance = $this->global_model->get_total_attendace_by_date($start_date, $end_date, $asset_id); // get all attendace by start date and in date
            return $get_total_attendance;
        }
    }

    public function get_overtime_info($year, $asset_id)
    {// this function is to create get monthy recap report

        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $i . '-' . '01';
                $end_date = $year . "-" . '0' . $i . '-' . '31';
            } else {
                $start_date = $year . "-" . $i . '-' . '01';
                $end_date = $year . "-" . $i . '-' . '31';
            }
            $get_expense_list[$i] = $this->utilities_model->get_overtime_info_by_date($start_date, $end_date, $asset_id); // get all report by start date and in date
        }

        return $get_expense_list; // return the result
    }

    public function overtime_report_pdf($year, $asset_id)
    {
        $data['all_overtime_info'] = $this->get_overtime_info($year, $asset_id);

        $data['monthyaer'] = $year;
        $data['asset_info'] = $this->db->where('asset_id', $asset_id)->get('tbl_account_details')->row();
        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/asset/overtime_report_pdf', $data, TRUE);
        pdf_create($viewfile, 'Overtime Report  - ' . $data['monthyaer']);
    }

    public function get_provident_fund_info($year, $asset_id)
    {// this function is to create get monthy recap report

        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $i;
                $end_date = $year . "-" . '0' . $i;
            } else {
                $start_date = $year . "-" . $i;
                $end_date = $year . "-" . $i;
            }
            $provident_fund_info[$i] = $this->payroll_model->get_provident_fund_info_by_date($start_date, $end_date, $asset_id); // get all report by start date and in date
        }

        return $provident_fund_info; // return the result
    }

    public function provident_fund_pdf($year, $asset_id)
    {

        $data['provident_fund_info'] = $this->get_provident_fund_info($year, $asset_id);
        $data['monthyaer'] = $year;

        $data['asset_info'] = $this->db->where('asset_id', $asset_id)->get('tbl_account_details')->row();

        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/asset/provident_fund_pdf', $data, TRUE);
        pdf_create($viewfile, lang('provident_found_report') . ' - ' . $data['monthyaer']);
    }

    public function timecard_details_pdf($id, $date)
    {
        $data['profile_info'] = $this->db->where('asset_id', $id)->get('tbl_account_details')->row();
        $data['date'] = $date;
        $data['attendace_info'] = $this->get_report($id, $date);

        $viewfile = $this->load->view('admin/asset/timecard_details_pdf', $data, TRUE);

        $this->load->helper('dompdf');
        pdf_create($viewfile, lang('timecard_details') . '- ' . $data['profile_info']->fullname);
    }

    public function get_report($asset_id, $date)
    {
        $month = date('n', strtotime($date));
        $year = date('Y', strtotime($date));
        $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday

        if ($month >= 1 && $month <= 9) {
            $yymm = $year . '-' . '0' . $month;
        } else {
            $yymm = $year . '-' . $month;
        }

        $public_holiday = $this->global_model->get_public_holidays($yymm);

        //tbl a_calendar Days Holiday
        if (!empty($public_holiday)) {
            foreach ($public_holiday as $p_holiday) {
                $p_hday = $this->attendance_model->GetDays($p_holiday->start_date, $p_holiday->end_date);
            }
        }

        $key = 1;
        $x = 0;
        for ($i = 1; $i <= $num; $i++) {

            if ($i >= 1 && $i <= 9) {
                $sdate = $yymm . '-' . '0' . $i;
            } else {
                $sdate = $yymm . '-' . $i;
            }
            $day_name = date('l', strtotime("+$x days", strtotime($year . '-' . $month . '-' . $key)));

            $data['week_info'][date('W', strtotime($sdate))][$sdate] = $sdate;

            // get leave info
            if (!empty($holidays)) {
                foreach ($holidays as $v_holiday) {
                    if ($v_holiday->day == $day_name) {
                        $flag = 'H';
                    }
                }
            }
            if (!empty($p_hday)) {
                foreach ($p_hday as $v_hday) {
                    if ($v_hday == $sdate) {
                        $flag = 'H';
                    }
                }
            }
            if (!empty($flag)) {
                $attendace_info[date('W', strtotime($sdate))][$sdate] = $this->attendance_model->attendance_report_by_empid($asset_id, $sdate, $flag);
            } else {
                $attendace_info[date('W', strtotime($sdate))][$sdate] = $this->attendance_model->attendance_report_by_empid($asset_id, $sdate);
            }
            $key++;
            $flag = '';
        }
        return $attendace_info;

    }

    public function update_contact($update = null, $id = null)
    {
        $data['title'] = lang('update_contact');
        $data['update'] = $update;
        $data['profile_info'] = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
        $data['modal_subview'] = $this->load->view('admin/asset/update_contact', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_details($id)
    {
        $data = $this->basic_model->array_from_post(array('joining_date', 'gender', 'date_of_birth', 'maratial_status', 'father_name', 'mother_name', 'phone', 'mobile', 'skype', 'present_address', 'passport'));

        $this->basic_model->_table_name = 'tbl_account_details'; // table name
        $this->basic_model->_primary_key = 'account_details_id'; // $id
        $this->basic_model->save($data, $id);

        $profile_info = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
        $activities = array(
            'asset' => $this->session->assetdata('asset_id'),
            'module' => 'asset',
            'module_field_id' => $id,
            'activity' => 'activity_update_asset',
            'icon' => 'fa-asset',
            'value1' => $profile_info->fullname
        );
        $this->basic_model->_table_name = 'tbl_activities';
        $this->basic_model->_primary_key = "activities_id";
        $this->basic_model->save($activities);

        $message = lang('update_asset_info');
        $type = 'success';
        set_message($type, $message);
        redirect('admin/asset/asset_details/' . $profile_info->asset_id); //redirect page
    }

    public function asset_documents($id)
    {
        $data['title'] = lang('update_documents');
        $data['profile_info'] = $this->db->where('asset_id', $id)->get('tbl_account_details')->row();
        $data['document_info'] = $this->db->where('asset_id', $id)->get('tbl_employee_document')->row();
        $data['modal_subview'] = $this->load->view('admin/asset/asset_documents', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_documents($id)
    {
        $edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $profile_info = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
            // ** Employee Document Information Save and Update Start  **
            // Resume File upload

            if (!empty($_FILES['resume']['name'])) {
                $old_path = $this->input->post('resume_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->basic_model->uploadAllType('resume');
                $val == TRUE || redirect('admin/asset/asset_details/' . $profile_info->asset_id);
                $document_data['resume_filename'] = $val['fileName'];
                $document_data['resume'] = $val['path'];
                $document_data['resume_path'] = $val['fullPath'];
            }
            // offer_letter File upload
            if (!empty($_FILES['offer_letter']['name'])) {
                $old_path = $this->input->post('offer_letter_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->basic_model->uploadAllType('offer_letter');
                $val == TRUE || redirect('admin/asset/asset_details/' . $profile_info->asset_id);
                $document_data['offer_letter_filename'] = $val['fileName'];
                $document_data['offer_letter'] = $val['path'];
                $document_data['offer_letter_path'] = $val['fullPath'];
            }
            // joining_letter File upload
            if (!empty($_FILES['joining_letter']['name'])) {
                $old_path = $this->input->post('joining_letter_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->basic_model->uploadAllType('joining_letter');
                $val == TRUE || redirect('admin/asset/asset_details/' . $profile_info->asset_id);
                $document_data['joining_letter_filename'] = $val['fileName'];
                $document_data['joining_letter'] = $val['path'];
                $document_data['joining_letter_path'] = $val['fullPath'];
            }

            // contract_paper File upload
            if (!empty($_FILES['contract_paper']['name'])) {
                $old_path = $this->input->post('contract_paper_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->basic_model->uploadAllType('contract_paper');
                $val == TRUE || redirect('admin/asset/asset_details/' . $profile_info->asset_id);
                $document_data['contract_paper_filename'] = $val['fileName'];
                $document_data['contract_paper'] = $val['path'];
                $document_data['contract_paper_path'] = $val['fullPath'];
            }
            // id_proff File upload
            if (!empty($_FILES['id_proff']['name'])) {
                $old_path = $this->input->post('id_proff_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->basic_model->uploadAllType('id_proff');
                $val == TRUE || redirect('admin/asset/asset_details/' . $profile_info->asset_id);
                $document_data['id_proff_filename'] = $val['fileName'];
                $document_data['id_proff'] = $val['path'];
                $document_data['id_proff_path'] = $val['fullPath'];
            }

            $fileName = $this->input->post('fileName');
            $path = $this->input->post('path');
            $fullPath = $this->input->post('fullPath');
            $size = $this->input->post('size');
            $is_image = $this->input->post('is_image');

            if (!empty($fileName)) {
                foreach ($fileName as $key => $name) {
                    $old['fileName'] = $name;
                    $old['path'] = $path[$key];
                    $old['fullPath'] = $fullPath[$key];
                    $old['size'] = $size[$key];
                    $old['is_image'] = $is_image[$key];
                    $result[] = $old;
                }

                $document_data['other_document'] = json_encode($result);
            }

            if (!empty($_FILES['other_document']['name']['0'])) {
                $old_path_info = $this->input->post('upload_path');
                if (!empty($old_path_info)) {
                    foreach ($old_path_info as $old_path) {
                        unlink($old_path);
                    }
                }
                $mul_val = $this->basic_model->multi_uploadAllType('other_document');
                $document_data['other_document'] = json_encode($mul_val);
            }

            if (!empty($result) && !empty($mul_val)) {
                $file = array_merge($result, $mul_val);
                $document_data['other_document'] = json_encode($file);
            }

            $document_data['asset_id'] = $profile_info->asset_id;

            $this->basic_model->_table_name = "tbl_employee_document"; // table name
            $this->basic_model->_primary_key = "document_id"; // $id
            $document_id = $this->input->post('document_id', TRUE);
            if (!empty($document_id)) {
                $this->basic_model->save($document_data, $document_id);
            } else {
                $this->basic_model->save($document_data);
            }

            $activities = array(
                'asset' => $this->session->assetdata('asset_id'),
                'module' => 'asset',
                'module_field_id' => $id,
                'activity' => 'activity_documents_update',
                'icon' => 'fa-asset',
                'value1' => $profile_info->fullname
            );
            $this->basic_model->_table_name = 'tbl_activities';
            $this->basic_model->_primary_key = "activities_id";
            $this->basic_model->save($activities);

            $message = lang('emplyee_documents_update');
            $type = 'success';
            set_message($type, $message);
            redirect('admin/asset/asset_details/' . $profile_info->asset_id . '/' . '4'); //redirect page
        } else {
            redirect('admin/asset/asset_list');
        }
    }

    public function new_bank($asset_id, $bank_id = null)
    {
        $data['title'] = lang('new_bank');

        $data['profile_info'] = $this->db->where('asset_id', $asset_id)->get('tbl_account_details')->row();
        if (!empty($bank_id)) {
            $data['bank_info'] = $this->db->where('employee_bank_id', $bank_id)->get('tbl_employee_bank')->row();
        }
        $data['modal_subview'] = $this->load->view('admin/asset/new_bank', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function update_bank_info($asset_id, $bank_id = null)
    {
        $edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $bank_data = $this->basic_model->array_from_post(array('bank_name', 'routing_number', 'account_name', 'account_number','type_of_account'));
            $bank_data['asset_id'] = $asset_id;
            $this->basic_model->_table_name = "tbl_employee_bank"; // table name
            $this->basic_model->_primary_key = "employee_bank_id"; // $id

            if (!empty($bank_id)) {
                $activity = 'activity_update_asset_bank';
                $msg = lang('update_bank_info');
                $this->basic_model->save($bank_data, $bank_id);
            } else {
                $activity = 'activity_new_asset_bank';
                $msg = lang('save_bank_info');
                $bank_id = $this->basic_model->save($bank_data);
            }
            $profile_info = $this->db->where('asset_id', $asset_id)->get('tbl_account_details')->row();
            $activities = array(
                'asset' => $this->session->assetdata('asset_id'),
                'module' => 'asset',
                'module_field_id' => $bank_id,
                'activity' => $activity,
                'icon' => 'fa-asset',
                'value1' => $profile_info->fullname
            );
            $this->basic_model->_table_name = 'tbl_activities';
            $this->basic_model->_primary_key = "activities_id";
            $this->basic_model->save($activities);

            $type = 'success';
            set_message($type, $msg);
            redirect('admin/asset/asset_details/' . $profile_info->asset_id . '/' . '3'); //redirect page
        } else {
            redirect('admin/asset/asset_list');
        }
    }

    public function delete_asset_bank($asset_id, $bank_id)
    {
        $this->basic_model->_table_name = "tbl_employee_bank"; // table name
        $this->basic_model->_primary_key = "employee_bank_id"; // $id
        $this->basic_model->delete($bank_id);

        $profile_info = $this->db->where('asset_id', $asset_id)->get('tbl_account_details')->row();
        $activities = array(
            'asset' => $this->session->assetdata('asset_id'),
            'module' => 'asset',
            'module_field_id' => $bank_id,
            'activity' => 'activity_delete_asset_bank',
            'icon' => 'fa-asset',
            'value1' => $profile_info->fullname
        );
        $this->basic_model->_table_name = 'tbl_activities';
        $this->basic_model->_primary_key = "activities_id";
        $this->basic_model->save($activities);
        $type = 'success';
        $msg = lang('delete_asset_bank');
        set_message($type, $msg);
        redirect('admin/asset/asset_details/' . $profile_info->asset_id);
    }

    /*     * * Save New asset ** */
    public function save_asset()
    {
        $created = can_action('24', 'created');
        $edited = can_action('24', 'edited');
        if (!empty($created) || !empty($edited)) {
            $login_data = $this->basic_model->array_from_post(array('assetname', 'email', 'role_id'));
            $asset_id = $this->input->post('asset_id', true);
            // update root category
            $where = array('assetname' => $login_data['assetname']);
            $email = array('email' => $login_data['email']);
            // duplicate value check in DB
            if (!empty($asset_id)) { // if id exist in db update data
                $check_id = array('asset_id !=' => $asset_id);
            } else { // if id is not exist then set id as null
                $check_id = null;
            }
            // check whether this input data already exist or not
            $check_asset = $this->basic_model->check_update('tbl_assets', $where, $check_id);
            $check_email = $this->basic_model->check_update('tbl_assets', $email, $check_id);
            if (!empty($check_asset) || !empty($check_email)) { // if input data already exist show error alert
                if (!empty($check_asset)) {
                    $error = $login_data['assetname'];
                } else {
                    $error = $login_data['email'];
                }

                // massage for asset
                $type = 'error';
                $message = "<strong style='color:#000'>" . $error . '</strong>  ' . lang('already_exist');

                $password = $this->input->post('password', TRUE);
                $confirm_password = $this->input->post('confirm_password', TRUE);
                if ($password != $confirm_password) {
                    $type = 'error';
                    $message = lang('password_does_not_match');
                }
            } else { // save and update query
                $login_data['last_ip'] = $this->input->ip_address();

                if (empty($asset_id)) {
                    $password = $this->input->post('password', TRUE);
                    $login_data['password'] = $this->hash($password);
                }
                $permission = $this->input->post('permission', true);
                if (!empty($permission)) {
                    if ($permission == 'everyone') {
                        $assigned = 'all';
                    } else {
                        $assigned_to = $this->basic_model->array_from_post(array('assigned_to'));
                        if (!empty($assigned_to['assigned_to'])) {
                            foreach ($assigned_to['assigned_to'] as $assign_asset) {
                                $assigned[$assign_asset] = $this->input->post('action_' . $assign_asset, true);
                            }
                        }
                    }
                    if (!empty($assigned)) {
                        if ($assigned != 'all') {
                            $assigned = json_encode($assigned);
                        }
                    } else {
                        $assigned = 'all';
                    }
                    $login_data['permission'] = $assigned;
                } else {
                    set_message('error', lang('assigned_to') . ' Field is required');
                    redirect($_SERVER['HTTP_REFERER']);
                }
                $this->basic_model->_table_name = 'tbl_assets'; // table name
                $this->basic_model->_primary_key = 'asset_id'; // $id
                if (!empty($asset_id)) {
                    $id = $this->basic_model->save($login_data, $asset_id);
                } else {
                    $login_data['activated'] = '1';
                    $id = $this->basic_model->save($login_data);
                }
                save_custom_field(13, $id);
                // save into tbl_account details
                $profile_data = $this->basic_model->array_from_post(array('fullname', 'employment_id', 'company', 'locale', 'language', 'phone', 'mobile', 'skype', 'designations_id', 'direction'));
                if ($login_data['role_id'] != 2) {
                    $profile_data['company'] = 0;
                }

                $account_details_id = $this->input->post('account_details_id', TRUE);
                if (!empty($_FILES['avatar']['name'])) {
                    $val = $this->basic_model->uploadImage('avatar');
                    $val == TRUE || redirect('admin/asset/asset_list');
                    $profile_data['avatar'] = $val['path'];
                }

                $profile_data['asset_id'] = $id;

                $this->basic_model->_table_name = 'tbl_account_details'; // table name
                $this->basic_model->_primary_key = 'account_details_id'; // $id
                if (!empty($account_details_id)) {
                    $this->basic_model->save($profile_data, $account_details_id);
                } else {
                    $this->basic_model->save($profile_data);
                }
                if (!empty($profile_data['designations_id'])) {
                    $desig = $this->db->where('designations_id', $profile_data['designations_id'])->get('tbl_designations')->row();
                    $department_head_id = $this->input->post('department_head_id', true);
                    if (!empty($department_head_id)) {
                        $head['department_head_id'] = $id;
                    } else {
                        $dep_head = $this->basic_model->check_by(array('departments_id' => $desig->departments_id), 'tbl_departments');

                        if (empty($dep_head->department_head_id)) {
                            $head['department_head_id'] = $id;
                        }
                    }
                    if (!empty($desig->departments_id) && !empty($head)) {
                        $this->basic_model->_table_name = "tbl_departments"; //table name
                        $this->basic_model->_primary_key = "departments_id";
                        $this->basic_model->save($head, $desig->departments_id);
                    }
                }

                $activities = array(
                    'asset' => $this->session->assetdata('asset_id'),
                    'module' => 'asset',
                    'module_field_id' => $id,
                    'activity' => 'activity_added_new_asset',
                    'icon' => 'fa-asset',
                    'value1' => $login_data['assetname']
                );
                $this->basic_model->_table_name = 'tbl_activities';
                $this->basic_model->_primary_key = "activities_id";
                $this->basic_model->save($activities);
                if (!empty($id)) {
                    $this->basic_model->_table_name = 'tbl_client_role'; //table name
                    $this->basic_model->delete_multiple(array('asset_id' => $id));
                    $all_client_menu = $this->db->get('tbl_client_menu')->result();
                    foreach ($all_client_menu as $v_client_menu) {
                        $client_role_data['menu_id'] = $this->input->post($v_client_menu->label, true);
                        if (!empty($client_role_data['menu_id'])) {
                            $client_role_data['asset_id'] = $id;
                            $this->basic_model->_table_name = 'tbl_client_role';
                            $this->basic_model->_primary_key = 'client_role_id';
                            $this->basic_model->save($client_role_data);
                        }
                    }
                }

                if (!empty($asset_id)) {
                    $message = lang('update_asset_info');
                } else {
                    $message = lang('save_asset_info');
                }
                $type = 'success';
            }
            set_message($type, $message);
        }
        redirect('admin/asset/asset_list'); //redirect page
    }

    public function send_welcome_email($id)
    {
        $asset_info = $this->db->where('asset_id', $id)->get('tbl_assets')->row();
        $profile_info = $this->db->where('asset_id', $id)->get('tbl_account_details')->row();
        $email_template = $this->basic_model->check_by(array('email_group' => 'wellcome_email'), 'tbl_email_templates');

        $message = $email_template->template_body;
        $subject = $email_template->subject;
        $NAME = str_replace("{NAME}", $profile_info->fullname, $message);
        $URL = str_replace("{COMPANY_URL}", base_url(), $NAME);
        $message = str_replace("{COMPANY_NAME}", config_item('company_name'), $URL);

        $data['message'] = $message;
        $message = $this->load->view('email_template', $data, TRUE);

        $params['subject'] = $subject;
        $params['message'] = $message;
        $params['resourceed_file'] = '';
        $params['recipient'] = $asset_info->email;

        $this->basic_model->send_email($params);

        $type = 'success';
        $message = lang('welcome_email_success');
        set_message($type, $message);
        redirect('admin/asset/asset_list'); //redirect page
    }

    /*     * * Delete asset ** */

    public function delete_asset($id = null)
    {
        $deleted = can_action('24', 'deleted');
        if (!empty($deleted)) {
            if (!empty($id)) {
                $id = $id;
                $asset_id = $this->session->assetdata('asset_id');

                //checking login asset trying delete his own account
                if ($id == $asset_id) {
                    //same asset can not delete his own account
                    // redirect with error msg
                    $type = 'error';
                    $message = 'Sorry You can not delete your own account!';
                    set_message($type, $message);
                    redirect('admin/asset/asset_list'); //redirect page
                } else {
                    $sbtn = $this->input->post('submit', true);

                    if (!empty($sbtn)) {
                        //delete procedure run
                        // Check asset in db or not
                        $this->basic_model->_table_name = 'tbl_assets'; //table name
                        $this->basic_model->_order_by = 'asset_id';
                        $result = $this->basic_model->get_by(array('asset_id' => $id), true);

                        if (!empty($result)) {
                            //delete asset roll id
                            $this->basic_model->_table_name = 'tbl_account_details';
                            $this->basic_model->delete_multiple(array('asset_id' => $id));//delete asset roll id

                            $cwhere = array('asset_id' => $id);
                            $this->basic_model->_table_name = 'tbl_private_chat';
                            $this->basic_model->delete_multiple($cwhere);

                            $this->basic_model->_table_name = 'tbl_private_chat_assets';
                            $this->basic_model->delete_multiple($cwhere);

                            $this->basic_model->_table_name = 'tbl_private_chat_messages';
                            $this->basic_model->delete_multiple($cwhere);

                            $this->basic_model->_table_name = 'tbl_activities';
                            $this->basic_model->delete_multiple(array('asset' => $id));

                            $this->basic_model->_table_name = 'tbl_payments';
                            $this->basic_model->delete_multiple(array('paid_by' => $id));

                            // delete all tbl_quotations by id
                            $this->basic_model->_table_name = 'tbl_quotations';
                            $this->basic_model->_order_by = 'asset_id';
                            $quotations_info = $this->basic_model->get_by(array('asset_id' => $id), FALSE);

                            if (!empty($quotations_info)) {
                                foreach ($quotations_info as $v_quotations) {
                                    $this->basic_model->_table_name = 'tbl_quotation_details';
                                    $this->basic_model->delete_multiple(array('quotations_id' => $v_quotations->quotations_id));
                                }
                            }

                            $this->basic_model->_table_name = 'tbl_quotations';
                            $this->basic_model->delete_multiple(array('asset_id' => $id));

                            $this->basic_model->_table_name = 'tbl_quotationforms';
                            $this->basic_model->delete_multiple(array('quotations_created_by_id' => $id));

                            $this->basic_model->_table_name = 'tbl_assets';
                            $this->basic_model->delete_multiple(array('asset_id' => $id));

                            $this->basic_model->_table_name = 'tbl_asset_role';
                            $this->basic_model->delete_multiple(array('designations_id' => $id));

                            $this->basic_model->_table_name = 'tbl_inbox';
                            $this->basic_model->delete_multiple(array('asset_id' => $id));

                            $this->basic_model->_table_name = 'tbl_sent';
                            $this->basic_model->delete_multiple(array('asset_id' => $id));

                            $this->basic_model->_table_name = 'tbl_draft';
                            $this->basic_model->delete_multiple(array('asset_id' => $id));

                            $tickets_info = $this->db->get('tbl_tickets')->result();
                            if (!empty($tickets_info)) {
                                foreach ($tickets_info as $v_tickets) {
                                    if (!empty($v_tickets->permission) && $v_tickets->permission != 'all') {
                                        $allowad_asset = json_decode($v_tickets->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $op_asset_id => $v_asset) {
                                                if ($op_asset_id == $id || $v_tickets->reporter == $id) {
                                                    $this->basic_model->_table_name = 'tbl_tickets';
                                                    $this->basic_model->delete_multiple(array('tickets_id' => $v_tickets->tickets_id));
                                                    $this->basic_model->_table_name = 'tbl_tickets_replies';
                                                    $this->basic_model->delete_multiple(array('tickets_id' => $v_tickets->tickets_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // xpm crm
                            // delete all leads by id
                            $leads_info = $this->db->get('tbl_leads')->result();
                            if (!empty($leads_info)) {
                                foreach ($leads_info as $v_leads) {
                                    if (!empty($v_leads->permission) && $v_leads->permission != 'all') {
                                        $allowad_asset = json_decode($v_leads->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $op_asset_id => $v_asset) {
                                                if ($op_asset_id == $id) {
                                                    //delete data into table.
                                                    $this->basic_model->_table_name = "tbl_calls"; // table name
                                                    $this->basic_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    //delete data into table.
                                                    $this->basic_model->_table_name = "tbl_mettings"; // table name
                                                    $this->basic_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    //delete data into table.
                                                    $this->basic_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->basic_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    $this->basic_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->basic_model->_order_by = "leads_id";
                                                    $files_info = $this->basic_model->get_by(array('leads_id' => $v_leads->leads_id), FALSE);

                                                    if (!empty($files_info)) {
                                                        foreach ($files_info as $v_files) {
                                                            //save data into table.
                                                            $this->basic_model->_table_name = "tbl_task_uploaded_files"; // table name
                                                            $this->basic_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                        }
                                                    }
                                                    //save data into table.
                                                    $this->basic_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->basic_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    $this->basic_model->_table_name = "tbl_task"; // table name
                                                    $this->basic_model->delete_multiple(array('leads_id' => $v_leads->leads_id));

                                                    $this->basic_model->_table_name = 'tbl_leads';
                                                    $this->basic_model->_primary_key = 'leads_id';
                                                    $this->basic_model->delete($v_leads->leads_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //save data into table.
                            $this->basic_model->_table_name = "tbl_milestones"; // table name
                            $this->basic_model->delete_multiple(array('asset_id' => $id));
                            // todo
                            $this->basic_model->_table_name = "tbl_todo"; // table name
                            $this->basic_model->delete_multiple(array('asset_id' => $id));

                            // opportunity
                            $oppurtunity_info = $this->db->get('tbl_opportunities')->result();
                            if (!empty($oppurtunity_info)) {
                                foreach ($oppurtunity_info as $v_oppurtunity) {
                                    if (!empty($v_oppurtunity->permission) && $v_oppurtunity->permission != 'all') {
                                        $allowad_asset = json_decode($v_oppurtunity->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $op_asset_id => $v_asset) {
                                                if ($op_asset_id == $id)
                                                    //delete data into table.
                                                    $this->basic_model->_table_name = "tbl_calls"; // table name
                                                $this->basic_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //delete data into table.
                                                $this->basic_model->_table_name = "tbl_mettings"; // table name
                                                $this->basic_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //delete data into table.
                                                $this->basic_model->_table_name = "tbl_task_comment"; // table name
                                                $this->basic_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                $this->basic_model->_table_name = "tbl_task_attachment"; //table name
                                                $this->basic_model->_order_by = "task_id";
                                                $files_info = $this->basic_model->get_by(array('opportunities_id' => $v_oppurtunity->opportunities_id), FALSE);
                                                if (!empty($files_info)) {
                                                    foreach ($files_info as $v_files) {
                                                        //save data into table.
                                                        $this->basic_model->_table_name = "tbl_task_uploaded_files"; // table name
                                                        $this->basic_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                    }
                                                }
                                                //save data into table.
                                                $this->basic_model->_table_name = "tbl_task_attachment"; // table name
                                                $this->basic_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //save data into table.
                                                $this->basic_model->_table_name = "tbl_task"; // table name
                                                $this->basic_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                //save data into table.
                                                $this->basic_model->_table_name = "tbl_bug"; // table name
                                                $this->basic_model->delete_multiple(array('opportunities_id' => $v_oppurtunity->opportunities_id));

                                                $this->basic_model->_table_name = 'tbl_opportunities';
                                                $this->basic_model->_primary_key = 'opportunities_id';
                                                $this->basic_model->delete($v_oppurtunity->opportunities_id);
                                            }
                                        }
                                    }
                                }
                            }
                            // project
                            $project_info = $this->db->get('tbl_project')->result();
                            if (!empty($project_info)) {
                                foreach ($project_info as $v_project) {
                                    if (!empty($v_project->permission) && $v_project->permission != 'all') {
                                        $allowad_asset = json_decode($v_project->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $asset_id => $v_asset) {
                                                if ($asset_id == $id) {
                                                    //delete data into table.
                                                    $this->basic_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->basic_model->delete_multiple(array('project_id' => $v_project->project_id));

                                                    $this->basic_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->basic_model->_order_by = "task_id";
                                                    $files_info = $this->basic_model->get_by(array('project_id' => $v_project->project_id), FALSE);
                                                    if (!empty($files_info)) {
                                                        foreach ($files_info as $v_files) {
                                                            //save data into table.
                                                            $this->basic_model->_table_name = "tbl_task_uploaded_files"; // table name
                                                            $this->basic_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                        }
                                                    }
                                                    //save data into table.
                                                    $this->basic_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->basic_model->delete_multiple(array('project_id' => $v_project->project_id));

                                                    //save data into table.
                                                    $this->basic_model->_table_name = "tbl_milestones"; // table name
                                                    $this->basic_model->delete_multiple(array('project_id' => $v_project->project_id));

                                                    // tasks
                                                    $taskss_info = $this->db->where('project_id', $v_project->project_id)->get('tbl_task')->result();
                                                    if (!empty($taskss_info)) {
                                                        foreach ($taskss_info as $v_taskss) {
                                                            if (!empty($v_taskss->permission) && $v_taskss->permission != 'all') {
                                                                $allowad_asset = json_decode($v_taskss->permission);
                                                                if (!empty($allowad_asset)) {
                                                                    foreach ($allowad_asset as $task_asset_id => $v_asset) {
                                                                        if ($task_asset_id == $id) {

                                                                            $this->basic_model->_table_name = "tbl_task_attachment"; //table name
                                                                            $this->basic_model->_order_by = "task_id";
                                                                            $files_info = $this->basic_model->get_by(array('task_id' => $v_taskss->task_id), FALSE);
                                                                            foreach ($files_info as $v_files) {
                                                                                $this->basic_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                                                $this->basic_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                                            }
                                                                            //delete into table.
                                                                            $this->basic_model->_table_name = "tbl_task_attachment"; // table name
                                                                            $this->basic_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                                            //delete data into table.
                                                                            $this->basic_model->_table_name = "tbl_task_comment"; // table name
                                                                            $this->basic_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                                            $this->basic_model->_table_name = "tbl_task"; // table name
                                                                            $this->basic_model->_primary_key = "task_id"; // $id
                                                                            $this->basic_model->delete($v_taskss->task_id);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    // Bugs
                                                    $bugs_info = $this->db->where('project_id', $v_project->project_id)->get('tbl_bug')->result();
                                                    if (!empty($bugs_info)) {
                                                        foreach ($bugs_info as $v_bugs) {
                                                            if (!empty($v_bugs->permission) && $v_bugs->permission != 'all') {
                                                                $allowad_asset = json_decode($v_bugs->permission);
                                                                if (!empty($allowad_asset)) {
                                                                    foreach ($allowad_asset as $bugs_asset_id => $v_asset) {
                                                                        if ($bugs_asset_id == $id) {

                                                                            $this->basic_model->_table_name = "tbl_task_attachment"; //table name
                                                                            $this->basic_model->_order_by = "bug_id";
                                                                            $files_info = $this->basic_model->get_by(array('bug_id' => $v_bugs->bug_id), FALSE);
                                                                            foreach ($files_info as $v_files) {
                                                                                $this->basic_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                                                $this->basic_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                                            }
                                                                            //delete into table.
                                                                            $this->basic_model->_table_name = "tbl_task_attachment"; // table name
                                                                            $this->basic_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                                            //delete data into table.
                                                                            $this->basic_model->_table_name = "tbl_task_comment"; // table name
                                                                            $this->basic_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                                            //delete data into table.
                                                                            $this->basic_model->_table_name = "tbl_task"; // table name
                                                                            $this->basic_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                                            $this->basic_model->_table_name = "tbl_bug"; // table name
                                                                            $this->basic_model->_primary_key = "bug_id"; // $id
                                                                            $this->basic_model->delete($v_bugs->bug_id);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    $this->basic_model->_table_name = 'tbl_project';
                                                    $this->basic_model->_primary_key = 'project_id';
                                                    $this->basic_model->delete($v_project->project_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // tasks
                            $taskss_info = $this->db->get('tbl_task')->result();
                            if (!empty($taskss_info)) {
                                foreach ($taskss_info as $v_taskss) {
                                    if (!empty($v_taskss->permission) && $v_taskss->permission != 'all') {
                                        $allowad_asset = json_decode($v_taskss->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $task_asset_id => $v_asset) {
                                                if ($task_asset_id == $id) {

                                                    $this->basic_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->basic_model->_order_by = "task_id";
                                                    $files_info = $this->basic_model->get_by(array('task_id' => $v_taskss->task_id), FALSE);
                                                    foreach ($files_info as $v_files) {
                                                        $this->basic_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                        $this->basic_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                    }
                                                    //delete into table.
                                                    $this->basic_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->basic_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                    //delete data into table.
                                                    $this->basic_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->basic_model->delete_multiple(array('task_id' => $v_taskss->task_id));

                                                    $this->basic_model->_table_name = "tbl_task"; // table name
                                                    $this->basic_model->_primary_key = "task_id"; // $id
                                                    $this->basic_model->delete($v_taskss->task_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // Bugs
                            $bugs_info = $this->db->get('tbl_bug')->result();
                            if (!empty($bugs_info)) {
                                foreach ($bugs_info as $v_bugs) {
                                    if (!empty($v_bugs->permission) && $v_bugs->permission != 'all') {
                                        $allowad_asset = json_decode($v_bugs->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $bugs_asset_id => $v_asset) {
                                                if ($bugs_asset_id == $id) {

                                                    $this->basic_model->_table_name = "tbl_task_attachment"; //table name
                                                    $this->basic_model->_order_by = "bug_id";
                                                    $files_info = $this->basic_model->get_by(array('bug_id' => $v_bugs->bug_id), FALSE);
                                                    foreach ($files_info as $v_files) {
                                                        $this->basic_model->_table_name = "tbl_task_uploaded_files"; //table name
                                                        $this->basic_model->delete_multiple(array('task_attachment_id' => $v_files->task_attachment_id));
                                                    }
                                                    //delete into table.
                                                    $this->basic_model->_table_name = "tbl_task_attachment"; // table name
                                                    $this->basic_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                    //delete data into table.
                                                    $this->basic_model->_table_name = "tbl_task_comment"; // table name
                                                    $this->basic_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                    //delete data into table.
                                                    $this->basic_model->_table_name = "tbl_task"; // table name
                                                    $this->basic_model->delete_multiple(array('bug_id' => $v_bugs->bug_id));

                                                    $this->basic_model->_table_name = "tbl_bug"; // table name
                                                    $this->basic_model->_primary_key = "bug_id"; // $id
                                                    $this->basic_model->delete($v_bugs->bug_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // tbl_invoices
                            $invoices_info = $this->db->get('tbl_invoices')->result();
                            if (!empty($invoices_info)) {
                                foreach ($invoices_info as $v_invoices) {
                                    if (!empty($v_invoices->permission) && $v_invoices->permission != 'all') {
                                        $allowad_asset = json_decode($v_invoices->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $invoice_asset_id => $v_asset) {
                                                if ($invoice_asset_id == $id) {
                                                    $this->basic_model->_table_name = "tbl_invoices"; // table name
                                                    $this->basic_model->_primary_key = "invoices_id"; // $id
                                                    $this->basic_model->delete($v_invoices->invoices_id);

                                                    $this->basic_model->_table_name = "tbl_items"; // table name
                                                    $this->basic_model->delete_multiple(array('invoices_id' => $v_invoices->invoices_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // tbl_estimates
                            $estimate_info = $this->db->get('tbl_estimates')->result();
                            if (!empty($estimate_info)) {
                                foreach ($estimate_info as $v_estimate) {
                                    if (!empty($v_estimate->permission) && $v_estimate->permission != 'all') {
                                        $allowad_asset = json_decode($v_estimate->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $estimate_asset_id => $v_asset) {
                                                if ($estimate_asset_id == $id) {
                                                    $this->basic_model->_table_name = "tbl_estimates"; // table name
                                                    $this->basic_model->_primary_key = "estimates_id"; // $id
                                                    $this->basic_model->delete($v_estimate->estimates_id);

                                                    $this->basic_model->_table_name = "tbl_estimate_items"; // table name
                                                    $this->basic_model->delete_multiple(array('estimates_id' => $v_estimate->estimates_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // 	tbl_tax_rates
                            $tax_rate_info = $this->db->get('tbl_tax_rates')->result();
                            if (!empty($tax_rate_info)) {
                                foreach ($tax_rate_info as $v_tax_rat) {
                                    if (!empty($v_tax_rat->permission) && $v_tax_rat->permission != 'all') {
                                        $allowad_asset = json_decode($v_tax_rat->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $tax_rate_asset_id => $v_asset) {
                                                if ($tax_rate_asset_id == $id) {
                                                    $this->basic_model->_table_name = "tbl_tax_rates"; // table name
                                                    $this->basic_model->_primary_key = "tax_rates_id"; // $id
                                                    $this->basic_model->delete($v_tax_rat->tax_rates_id);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $transactions_info = $this->db->get('tbl_transactions')->result();
                            if (!empty($transactions_info)) {
                                foreach ($transactions_info as $v_transactions) {
                                    if (!empty($v_transactions->permission) && $v_transactions->permission != 'all') {
                                        $allowad_asset = json_decode($v_transactions->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $trsn_asset_id => $v_asset) {
                                                if ($trsn_asset_id == $id) {
                                                    $this->basic_model->_table_name = 'tbl_transactions';
                                                    $this->basic_model->delete_multiple(array('transactions_id' => $v_transactions->transactions_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $transfer_info = $this->db->get('tbl_transfer')->result();
                            if (!empty($transfer_info)) {
                                foreach ($transfer_info as $v_transfer) {
                                    if (!empty($v_transfer->permission) && $v_transfer->permission != 'all') {
                                        $allowad_asset = json_decode($v_transfer->permission);
                                        if (!empty($allowad_asset)) {
                                            foreach ($allowad_asset as $trfr_asset_id => $v_asset) {
                                                if ($trfr_asset_id == $id) {
                                                    $this->basic_model->_table_name = 'tbl_transfer';
                                                    $this->basic_model->delete_multiple(array('transfer_id' => $v_transfer->transfer_id));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //redirect successful msg
                            $type = 'success';
                            $message = 'asset Delete Successfully!';
                        } else {
                            //redirect error msg
                            $type = 'error';
                            $message = 'Sorry this asset not find in database!';

                        }
                        set_message($type, $message);
                        redirect('admin/asset/asset_list'); //redirect page
                    } else {
                        $data['title'] = "Delete assets"; //Page title
                        $data['asset_info'] = $this->db->where('asset_id', $id)->get('tbl_account_details')->row();
                        $data['subview'] = $this->load->view('admin/asset/delete_asset', $data, TRUE);
                        $this->load->view('admin/_layout_main', $data); //page load
                    }
                }
            } else {
                redirect('admin/asset/asset_list'); //redirect page
            }
        }
    }

    public function change_status($flag, $id)
    {
        $can_edit = $this->basic_model->can_action('tbl_assets', 'edit', array('asset_id' => $id));
        $edited = can_action('24', 'edited');
        if (!empty($can_edit) && !empty($edited)) {
            $asset_info = $this->db->where('asset_id', $id)->get('tbl_assets')->row();
            // if flag == 1 it is active asset else deactive asset
            if ($flag == 1) {
                $msg = 'Active';
            } else {
                $msg = 'Deactive';
            }
            $where = array('asset_id' => $id);
            $action = array('activated' => $flag);
            $this->basic_model->set_action($where, $action, 'tbl_assets');

            $activities = array(
                'asset' => $this->session->assetdata('asset_id'),
                'module' => 'asset',
                'module_field_id' => $id,
                'activity' => 'activity_change_status',
                'icon' => 'fa-asset',
                'value1' => $asset_info->assetname . ' ' . $msg,
            );
            $this->basic_model->_table_name = 'tbl_activities';
            $this->basic_model->_primary_key = "activities_id";
            $this->basic_model->save($activities);

            $type = "success";
            $message = "asset " . $msg . " Successfully!";
        } else {
            $type = 'error';
            $message = lang('there_in_no_value');
        }
        echo json_encode(array("status" => $type, "message" => $message));
        exit();
    }

    public function set_banned($flag, $id)
    {
        $can_edit = $this->basic_model->can_action('tbl_assets', 'edit', array('asset_id' => $id));
        $edited = can_action('24', 'edited');
        if (!empty($can_edit) && !empty($edited)) {
            if ($flag == 1) {
                $msg = lang('banned');
                $action = array('activated' => 0, 'banned' => $flag, 'ban_reason' => $this->input->post('ban_reason', TRUE));
            } else {
                $msg = lang('unbanned');
                $action = array('activated' => 1, 'banned' => $flag);
            }
            $where = array('asset_id' => $id);

            $this->basic_model->set_action($where, $action, 'tbl_assets');

            $activities = array(
                'asset' => $this->session->assetdata('asset_id'),
                'module' => 'asset',
                'module_field_id' => $id,
                'activity' => 'activity_change_status',
                'icon' => 'fa-asset',
                'value1' => $flag,
            );
            $this->basic_model->_table_name = 'tbl_activities';
            $this->basic_model->_primary_key = "activities_id";
            $this->basic_model->save($activities);

            $type = "success";
            $message = "asset " . $msg . " Successfully!";
        } else {
            $type = 'error';
            $message = lang('there_in_no_value');
        }
        set_message($type, $message);
        redirect('admin/asset/asset_list'); //redirect page
    }

    public function change_banned($id)
    {

        $data['asset_id'] = $id;
        $data['modal_subview'] = $this->load->view('admin/asset/_modal_banned_reson', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);

    }

    public function hash($string)
    {
        return hash('sha512', $string . config_item('encryption_key'));
    }

// crud for sidebar todo list
    function todo($task = '', $todo_id = '', $swap_with = '')
    {
        if ($task == 'add') {
            $this->add_todo();
        }
        if ($task == 'reload_incomplete_todo') {
            $this->get_incomplete_todo();
        }
        if ($task == 'mark_as_done') {
            $this->mark_todo_as_done($todo_id);
        }
        if ($task == 'mark_as_undone') {
            $this->mark_todo_as_undone($todo_id);
        }
        if ($task == 'swap') {

            $this->swap_todo($todo_id, $swap_with);
        }
        if ($task == 'delete') {
            $this->delete_todo($todo_id);
        }
        $todo['opened'] = 1;
        $this->session->set_assetdata($todo);
        redirect('admin/dashboard/');
    }

    function add_todo()
    {
        $data['title'] = $this->input->post('title');
        $data['asset_id'] = $this->session->assetdata('asset_id');

        $this->db->insert('tbl_todo', $data);
        $todo_id = $this->db->insert_id();

        $data['order'] = $todo_id;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function mark_todo_as_done($todo_id = '')
    {
        $data['status'] = 1;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function mark_todo_as_undone($todo_id = '')
    {
        $data['status'] = 0;
        $this->db->where('todo_id', $todo_id);
        $this->db->update('tbl_todo', $data);
    }

    function swap_todo($todo_id = '', $swap_with = '')
    {
        $counter = 0;
        $temp_order = $this->db->get_where('tbl_todo', array('todo_id' => $todo_id))->row()->order;
        $asset = $this->session->assetdata('asset_id');

        // Move current todo up.
        if ($swap_with == 'up') {
            // Fetch all todo lists of current asset in ascending order.
            $this->db->order_by('order', 'ASC');
            $todo_lists = $this->db->get_where('tbl_todo', array('asset_id' => $asset))->result_array();
            $array_length = count($todo_lists);

            // Create separate array for orders and todo_id's from above array.
            foreach ($todo_lists as $todo_list) {
                $id_list[] = $todo_list['todo_id'];
                $order_list[] = $todo_list['order'];
            }
        }

        // Move current todo down.
        if ($swap_with == 'down') {

            // Fetch all todo lists of current asset in descending order.
            $this->db->order_by('order', 'DESC');
            $todo_lists = $this->db->get_where('tbl_todo', array('asset_id' => $asset))->result_array();
            $array_length = count($todo_lists);

            // Create separate array for orders and todo_id's from above array.
            foreach ($todo_lists as $todo_list) {
                $id_list[] = $todo_list['todo_id'];
                $order_list[] = $todo_list['order'];
            }
        }

        // Swap orders between current and next/previous todo.
        for ($i = 0; $i < $array_length; $i++) {
            if ($temp_order == $order_list[$i]) {
                if ($counter > 0) {
                    $swap_order = $order_list[$i - 1];
                    $swap_id = $id_list[$i - 1];

                    // Update order of current todo.
                    $data['order'] = $swap_order;
                    $this->db->where('todo_id', $todo_id);
                    $this->db->update('tbl_todo', $data);

                    // Update order of next/previous todo.
                    $data['order'] = $temp_order;
                    $this->db->where('todo_id', $swap_id);
                    $this->db->update('tbl_todo', $data);
                }
            } else
                $counter++;
        }
    }

    function delete_todo($todo_id = '')
    {
        $this->db->where('todo_id', $todo_id);
        $this->db->delete('tbl_todo');
    }

    function get_incomplete_todo()
    {
        $asset = $this->session->assetdata('asset_id');
        $this->db->where('asset_id', $asset);
        $this->db->where('status', 0);
        $query = $this->db->get('tbl_todo');

        $incomplete_todo_number = $query->num_rows();
        if ($incomplete_todo_number > 0) {
            echo '<span class="badge badge-secondary">';
            echo $incomplete_todo_number;
            echo '</span>';
        }
    }

    public function reset_password($id)
    {
        if ($this->session->assetdata('asset_type') == 1) {
            $new_password = $this->input->post('password', true);
            $old_password = $this->input->post('old_password', true);
            if (!empty($new_password)) {
                $email = $this->session->assetdata('email');
                $asset_info = $this->db->where('asset_id', $id)->get('tbl_assets')->row();
                $old_password = $this->basic_model->hash($old_password);
                if ($asset_info->password == $old_password) {
                    $where = array('asset_id' => $id);
                    $action = array('password' => $this->basic_model->hash($new_password));
                    $this->basic_model->set_action($where, $action, 'tbl_assets');
                    $login_details = $this->db->where('asset_id', $id)->get('tbl_assets')->row();
                    $activities = array(
                        'asset' => $this->session->assetdata('asset_id'),
                        'module' => 'asset',
                        'module_field_id' => $id,
                        'activity' => 'activity_reset_password',
                        'icon' => 'fa-asset',
                        'value1' => $login_details->assetname,
                    );

                    $this->basic_model->_table_name = 'tbl_activities';
                    $this->basic_model->_primary_key = "activities_id";
                    $this->basic_model->save($activities);

                    $this->send_email_reset_password($email, $asset_info, $new_password);

                    $type = "success";
                    $message = lang('message_new_password_sent');
                } else {
                    $type = "error";
                    $message = lang('password_does_not_match');
                }
                set_message($type, $message);
                redirect('admin/asset/asset_details/' . $id); //redirect page

            } else {
                $data['title'] = lang('see_password');
                $data['asset_info'] = $this->db->where('asset_id', $id)->get('tbl_assets')->row();
                $data['subview'] = $this->load->view('admin/settings/reset_password', $data, FALSE);
                $this->load->view('admin/_layout_modal', $data);
            }

        } else {
            $type = 'error';
            $message = lang('there_in_no_value');
            set_message($type, $message);
            redirect('admin/asset/asset_list'); //redirect page
        }


    }

    function send_email_reset_password($email, $asset_info, $password)
    {
        $email_template = $this->basic_model->check_by(array('email_group' => 'reset_password'), 'tbl_email_templates');
        $message = $email_template->template_body;
        $subject = $email_template->subject;

        $assetname = str_replace("{assetNAME}", $asset_info->assetname, $message);
        $asset_email = str_replace("{EMAIL}", $asset_info->email, $assetname);
        $asset_password = str_replace("{NEW_PASSWORD}", $password, $asset_email);
        $message = str_replace("{SITE_NAME}", config_item('company_name'), $asset_password);
        $params['recipient'] = $email;
        $params['subject'] = '[ ' . config_item('company_name') . ' ]' . $subject;
        $params['message'] = $message;
        $params['resourceed_file'] = '';
        $this->basic_model->send_email($params);
    }

}
