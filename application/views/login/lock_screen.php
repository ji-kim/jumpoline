<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <!-- =============== VENDOR STYLES ===============-->
    <!-- FONT AWESOME-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome/css/font-awesome.min.css">
    <!-- SIMPLE LINE ICONS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/simple-line-icons/css/simple-line-icons.css">
    <!-- =============== BOOTSTRAP STYLES ===============-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" id="bscss">
    <!-- =============== APP STYLES ===============-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/app.css" id="maincss">
</head>
<body>
<div class="wrapper " style="margin: 5% auto">
    <?php

    $user_id = $this->session->userdata('user_id');
    $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
    ?>


    <div class="abs-center wd-xl">
        <div class="lockscreen-logo">
            <a href=""><span style="font-size: 20px;">심플 (주)점포라인</span></a>
        </div>
        <?php
        $error = $this->session->flashdata('error');

        if (!empty($error)) {
            ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
        <?php } ?>
        <!-- START panel-->
        <div class="p">
            <img src="<?= base_url() . $profile_info->avatar ?>" alt="Avatar" width="60" height="60"
                 class="img-thumbnail img-circle center-block">
        </div>

        <div class="panel widget b0">
            <div class="panel-body">
                <p class="text-center">화면잠김을 해제 하시려면 로그인해 주세요<?//= lang('login_to_unlock_screen') ?></p>
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>locked/check_login/<?= $this->session->userdata('user_name') ?>"
                      method="post">
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" name="password" required="" placeholder="비밀번호"/>
                        <span class="fa fa-lock form-control-feedback text-muted"></span>
                    </div>
                    <div class="clearfix">
                        <div class="pull-left mt-sm">
                            <a href="<?= base_url() ?>login/logout" class="text-muted">
                                <small>다른 사용자로 로그인</small>
                            </a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-sm btn-primary">화면잠김해제</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END panel-->
        <div class="p-lg text-center">
            <span>&copy;</span>
            <span>(주)점포라인</span>
            <br/>
            <span>2019</span>
            <span></span>
            <span></span>

        </div>
    </div>
</div>
