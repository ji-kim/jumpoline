<p class="text-center pv">비밀번호 재발행</p>
<form data-parsley-validate="" novalidate="" action="<?php echo base_url() ?>login/forgot_password" method="post">
    <p class="text-center">이메일 주소를 입력하시면 등록된 이메일주소로 새로운 비밀번호를 보내드립니다.</p>
    <div class="form-group has-feedback">
        <label for="resetInputEmail1" class="text-muted">이메일</label>
        <input type="text" name="email_or_username" required="true" class="form-control"
               placeholder="이메일"/>
        <span class="fa fa-envelope form-control-feedback text-muted"></span>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <button type="submit" name="flag" value="1"
                    class="btn btn-danger btn-block btn-flat">확인</button>
        </div><!-- /.col -->
        <div class="col-xs-8">
            <label class="btn pull-right"><a href="<?= base_url() ?>login">로그인</a></label>
        </div><!-- /.col -->
    </div>
</form>
<?php if (config_item('allow_client_registration') == 'TRUE') { ?>
    <p class="pt-lg text-center">아이디가 생각나지 않으세요?</p>
    <a href="#" class="btn btn-block btn-default"><i
            class="fa fa-sign-in"></i> 관리팀에 문의 하세요.</a>
<?php } ?>
