<style type="text/css">
    .datepicker {
        z-index: 1151 !important;
    }

    .mt-sm {
        font-size: 14px;
    }

    .close-btn {
        font-weight: 100;
        position: absolute;
        right: 10px;
        top: -10px;
        display: none;
    }

    .close-btn i {
        font-weight: 100;
        color: #89a59e;
    }

    .report:hover .close-btn {
        display: block;
    }

    .mt-lg:hover .close-btn {
        display: block;
    }
</style>
<?php
echo message_box('success');
echo message_box('error');
$curency = $this->admin_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');
if (empty($curency)) {
    $curency = $this->admin_model->check_by(array('code' => 'AUD'), 'tbl_currencies');
}
$all_report = $this->db->where('report', 1)->order_by('order_no', 'ASC')->get('tbl_dashboard')->result();
if ($this->session->userdata('user_type') == 1) {
    $where = array('report' => 0, 'status' => 1);
} else {
    $where = array('report' => 0, 'status' => 1, 'for_staff' => 1);
}
$all_order_data = $this->db->where($where)->order_by('order_no', 'ASC')->get('tbl_dashboard')->result();;
?>
<div class="dashboard">
    <!--        ******** transactions ************** -->
    <?php if ($this->session->userdata('user_type') == 1) { ?>
        <div id="report_menu" class="row">

<?php } ?>
<script type="text/javascript">

    $(document).ready(function () {
        $('.complete-todo input[type="checkbox"]').change(function () {
            var todo_id = $(this).data().id;
            var todo_complete = $(this).is(":checked");

            var formData = {
                'todo_id': todo_id,
                'status': '3'
            };
            $.ajax({
                type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url: '<?= base_url()?>admin/dashboard/completed_todo/' + todo_id, // the url where we want to POST
                data: formData, // our data object
                dataType: 'json', // what type of data do we expect back from the server
                encode: true,
                success: function (res) {
                    console.log(res);
                    if (res) {
                        toastr[res.status](res.message);
                    } else {
                        alert('There was a problem with AJAX');
                    }
                }
            })

        });

    })
    ;
</script>
<script src="<?= base_url() ?>assets/plugins/jquery-ui/jquery-u.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#report_menu").sortable({
            connectWith: ".report_menu",
            placeholder: 'ui-state-highlight',
            forcePlaceholderSize: true,
            stop: function (event, ui) {
                var id = JSON.stringify(
                    $("#report_menu").sortable(
                        'toArray',
                        {
                            attribute: 'id'
                        }
                    )
                );
                var formData = {
                    'report_menu': id
                };
                $.ajax({
                    type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                    url: '<?= base_url()?>admin/settings/save_dashboard/', // the url where we want to POST
                    data: formData, // our data object
                    dataType: 'json', // what type of data do we expect back from the server
                    encode: true,
                    success: function (res) {
                        if (res) {
//                            toastr[res.status](res.message);
                        } else {
                            alert('There was a problem with AJAX');
                        }
                    }
                })

            }
        });
        $(".report_menu").disableSelection();

        $("#menu").sortable({
            connectWith: ".menu",
            placeholder: 'ui-state-highlight',
            forcePlaceholderSize: true,
            stop: function (event, ui) {
                var mid = JSON.stringify(
                    $("#menu").sortable(
                        'toArray',
                        {
                            attribute: 'id'
                        }
                    )
                );
                var formData = {
                    'menu': mid
                };
                $.ajax({
                    type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                    url: '<?= base_url()?>admin/settings/save_dashboard/', // the url where we want to POST
                    data: formData, // our data object
                    dataType: 'json', // what type of data do we expect back from the server
                    encode: true,
                    success: function (res) {
                        if (res) {
//                            toastr[res.status](res.message);
                        } else {
                            alert('There was a problem with AJAX');
                        }
                    }
                })
            }
        });
        $(".menu").disableSelection();
    });
</script>
<?php /*Comment in my JavaScript*/ ?>