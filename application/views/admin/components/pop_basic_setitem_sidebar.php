<aside class="aside">
    <!-- START Sidebar (left)-->
    <div class="aside-inner">
        <nav data-sidebar-anyclick-close="" class="sidebar">


<ul class='nav'>
<li class='sub-menu <?=($type=="sudang")?"active":""?>' >
  <a data-toggle='collapse' href='#basic'> <em class='fa fa-usd'></em><span>수당항목설정</span></a>
	<ul id=basic class='nav sidebar-subnav collapse'><li class="sidebar-subnav-header">수당항목설정</li>
	<li class='<?=($type=="sudang" && $cat=="01")?"active":""?>' >
	  <a  title='각종수당' href='<?php echo base_url() ?>admin/basic/set_item/<?=$ws_co_id?>/sudang/01'>
	 <em class='fa fa-dribbble'></em><span>각종수당</span></a>
	</li> 
	</ul> 
</li> 



<li class='<?=($type=="gongje")?"active":""?>' >
   <a data-toggle='collapse' href='#basic2'> <em class='fa fa-usd'></em><span>공제항목설정</span></a>
	<ul id=basic2 class='nav sidebar-subnav collapse'><li class="sidebar-subnav-header">수당항목설정</li>
	<li class='<?=($type=="gongje" && $cat=="01")?"active":""?>' >
	  <a  title='위.수탁관리비' href='<?php echo base_url() ?>admin/basic/set_item/<?=$ws_co_id?>/gongje/01'>
	 <em class='fa fa-dribbble'></em><span>위.수탁관리비</span></a>
	</li> 
	<li class='<?=($type=="gongje" && $cat=="02")?"active":""?>' >
	  <a  title='일반공제' href='<?php echo base_url() ?>admin/basic/set_item/<?=$ws_co_id?>/gongje/02'>
	 <em class='fa fa-dribbble'></em><span>일반공제</span></a>
	</li> 
	<li class='<?=($type=="gongje" && $cat=="03")?"active":""?>' >
	  <a  title='환급형' href='<?php echo base_url() ?>admin/basic/set_item/<?=$ws_co_id?>/gongje/03'>
	 <em class='fa fa-dribbble'></em><span>환급형공제</span></a>
	</li> 
	</ul> 
</li> 



</ul>

            <!-- END sidebar nav-->
        </nav>
    </div>
    <!-- END Sidebar (left)-->
</aside>
