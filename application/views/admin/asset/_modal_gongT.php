<?php
		if(empty($tr_info->car_1)) $truck_no = ""; else $truck_no = $tr_info->car_1;
?>
<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">공티등록 - <?=$truck_no?> </h4>
    </div>
    <div class="modal-body wrap-modal wrap">

            <div class="form-group" id="border-none">
			<br/>
            </div>

        <form id="form_validation"
              action="<?php echo base_url() ?>admin/cowork/save_misu/" method="post" class="form-horizontal form-groups-bordered">
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">공티등록일 </label>
                <div class="col-sm-9">
														<div class="input-group">
															<input type="text" class="form-control datepicker" name="gongT_date" value="<?php if (!empty($last_info->gongT_date)) echo $last_info->gongT_date;?>">
															<div class="input-group-addon">
																<a href="#"><i class="fa fa-calendar"></i></a>
															</div>
														</div>
                </div>
            </div>

            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">공티재등록마감일 </label>
                <div class="col-sm-9">
														<div class="input-group">
															<input type="text" class="form-control datepicker" name="gongT_cls_date" value="<?php if (!empty($last_info->gongT_cls_date)) echo $last_info->gongT_cls_date;?>">
															<div class="input-group-addon">
																<a href="#"><i class="fa fa-calendar"></i></a>
															</div>
														</div>
                </div>
            </div>

			<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"> 저 장 </button>
            </div>
        </form>
    </div>
</div>
