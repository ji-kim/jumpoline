<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php 
$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');

// 배차지, 운영사, 차량정보, 위수탁사
if(!empty($tr_id)) {
	$tr = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
	$dp = $this->db->where('dp_id', $tr_id)->get('tbl_members')->row();
	if(!empty($dp->code)) $coop = $this->db->where('mb_type', 'customer')->where('code', $dp->code)->get('tbl_members')->row();
	if(!empty($dp->sm_co_id)) $op = $this->db->where('dp_id', $coop->sm_co_id)->get('tbl_members')->row();

	if(!empty($dp->dp_id)) $dp_id = $dp->dp_id;
	if(!empty($dp->code)) $co_code = $dp->code;
	if(!empty($dp->ws_co_id)) $ws_co_id  = $tr->ws_co_id;
} 
?>
<script type="text/javascript">
<!--
$(document).ready(function() {
});
function goView(action,id) {
  $("#ins_id").val(id);
  $("#myform").attr("action", action);
  $("#myform").attr("target", "_self");
  $("#myform").submit();
}
function setPaymentCnt() {
  $("#myform").attr("action", "<?php echo base_url(); ?>admin/asset/pop_carinfo/car_ins/<?php if (!empty($tr_id)) { echo $tr_id; } ?>/edit_ins_info");
  $("#myform").attr("target", "_self");
  $("#myform").submit();
}
</script>


<div class="row">
    <div class="col-sm-12">
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#car_list"
                                                                   data-toggle="tab">자동차보험이력</a></li>



                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_car"
                                                                   data-toggle="tab">자동차보험등록</a></li>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="car_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong>차량목록</strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
       <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table table-striped DataTables">
        <tr align="center" bgcolor="#e0e7ef" style="text-align:center;">
          <td style="color:#ffffff;background-color: #777777;" width="40">No</td>
          <td style="color:#ffffff;background-color: #777777;">차량번호</td>
          <td style="color:#ffffff;background-color: #777777;">차대번호</td>
          <td style="color:#ffffff;background-color: #777777;">보험사</td>
          <td style="color:#ffffff;background-color: #777777;">보험가입일</td>
          <td style="color:#ffffff;background-color: #777777;">보험만기일</td>
          <td style="color:#ffffff;background-color: #777777;">가입보험종류</td>
          <td style="color:#ffffff;background-color: #777777;">총보험료</td>
          <td style="color:#ffffff;background-color: #777777;">납부횟수</td>
          <td style="color:#ffffff;background-color: #777777;"></td>
          <td style="color:#ffffff;background-color: #777777;">수정</td>
		</tr>

<?php
	$i = 0;
	if (!empty($all_ins_group)) {
		foreach ($all_ins_group as $ins_details) {
			$sn_no = ++$i;
			$sn_bg = "#ffffff";
		//$ins = sql_fetch("SELECT * FROM tb_insur_company where deptid = '$row[ins_co_id]'");
?>

        <tr bgcolor="<?=$sn_bg?>">
          <td height="25" align="center"><?=$sn_no?></td>
          <td align="center"><?= $tr->car_1 ?></td>
          <td align="center"><?= $tr->car_7 ?></td>

          <td align="center"><?//$ins->deptname ?></td>
          <td align="center"><?= $ins_details->start_date ?></td>
          <td align="center"><?= $ins_details->end_date ?></td>
          <td align="center">자동차</td>
          <td align="center"><?=number_format($ins_details->person1+$ins_details->person2+$ins_details->object+$ins_details->self_body+$ins_details->self_car+$ins_details->call_svc)?></td>
          <td align="center"><?= $ins_details->pay_cnt ?></td>
          <td align="center"><?= $ins_details->active_yn ?></td>
          <td align="center">
		  
		  <a href="javascript:goView('<?php echo base_url(); ?>admin/asset/pop_carinfo/car_ins/<?= $tr_id ?>/edit_ins_info','<?php if (!empty($ins_details->idx)) { echo $ins_details->idx; } ?>');" class="button red" title="수정"> 수정 </a>
		  <!--a href="javascript:goDelIns('<?=$ins_details->idx?>');" class="button red" title="삭제"> 삭제 </a-->
		  </td>
		</tr>
<?
	}
  } else {
?>
        <tr bgcolor="#ffffff">
          <td colspan="30" align="center">자료가 없습니다.</td>
        </tr>
<?
  }
?>
</table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { 
					
					//	if(empty($spay_cnt) && !empty($ins->spay_cnt)) $spay_cnt = $ins->spay_cnt;
					//	if(empty($ins_co_id) && !empty($ins->ins_co_id)) $ins_co_id = $ins->ins_co_id;
					
					if(!empty($ins->pay_cnt)) $spay_cnt		= $ins->pay_cnt;
					if(!empty($ins->ins_co_id)) $ins_co_id		= $ins->ins_co_id;
					?>
                        <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_car"
                        style="position: relative;">
                        <form role="form" enctype="multipart/form-data" name="myform" id="myform" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/asset/pop_save_carinfo_ins/<?php
                              if (!empty($tr_id)) {
                                  echo $tr_id;
                              }
                              ?>" method="post" class="form-horizontal  ">
							<input type="hidden" name="ins_id" id="ins_id" value="<?php if(!empty($ins_id)) echo $ins_id;?>">
							<input type="hidden" name="md" id="md" value="<?php if(!empty($md)) echo $md?>"/>
							<input type="hidden" name="page" id="page" value="<?php if(!empty($page)) echo $page?>"/>

                            <div class="panel-body">
                                <label class="control-label col-sm-1"></label>
                                <div class="col-sm-12">
                                    <div class="nav-tabs-custom">

<table width="100%" border="0" cellpadding="0">
  <tr>
    <td align="center" valign="top">

      <!-- 타이틀 시작 -->
      <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#e4e4e4" align="center" class="table table-striped DataTables">
        <tr>
          <td align="left" style="font-size:13px;">
		  ◈ 배차지 : <?php if(!empty($coop->co_name)) echo $coop->co_name;?>   >   운영사 : <?php if(!empty($op->co_name)) echo $op->co_name;?>   >   차량번호 : <?php if(!empty($tr->car_1)) echo $tr->car_1;?>   > 위수탁자 : <?php if(!empty($dp->E)) echo $dp->E;?>  
		  </td>
          <td align="left" style="font-size:13px;">
		  <input type="checkbox" name="co_pay" value="Y" <?//= (!empty($ins->co_pay=='Y') && $ins->co_pay=='Y') ? " checked" : "" ?>> 회사부담
		  </td>
        </tr>
      </table>
      <!-- 타이틀 끝 -->
      <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#e4e4e4" align="center" class="table table-striped DataTables">

        <tr>
          <td height="50" align="center" width="12%" style="color:#ffffff;background-color: #777777;">보험사</td>
          <td width="20%" align="left" style="padding-left:5px;">
            <select name="ins_co_id" id="ins_co_id" class="form-control" style="width:98%;">
	<?php 
        $this->asset_model->_table_name = 'tbl_insur_company'; //table name
        $this->asset_model->_order_by = 'deptname';
        $all_insur_cos = $this->asset_model->get();
        foreach ($all_insur_cos as $insur_co) {
	?>
			  <option value="<?=$insur_co->idx?>" <?= (!empty($ins_co_id) && $insur_co->idx==$ins_co_id) ? " selected" : "" ?>><?=$insur_co->deptname?></option>
	<?php } ?>
			</select> 

		  </td>

          <td width="13%" style="color:#ffffff;background-color: #777777;" align="center">분할납부횟수</td>
          <td style="padding-left:5px;" align="center">
            <select name="spay_cnt" id="spay_cnt" class="form-control" style="width:90%;" onChange="setPaymentCnt();">
	<? for($i=1;$i<=10;$i++) {
	?>
			  <option value="<?=$i?>" <?=(!empty($spay_cnt) && $spay_cnt==$i)?" selected" : "" ?>><?=$i?>회</option>
	<? } ?>
			</select> 
		  </td>
          <td width="13%" style="color:#ffffff;background-color: #777777;" align="center">납부일</td>
          <td width="13%" style="color:#ffffff;background-color: #777777;" align="center">공제월선택</td>
          <td width="13%" style="color:#ffffff;background-color: #777777;" align="center">만기일</td>
       </tr>
<?
//$tmp = explode("-",$dp->cls_gj_date);
//$cls_gj_year		= $tmp[0];
//$cls_gj_month		= $tmp[1];
?>
        <tr>
          <td height="50" align="center" colspan="3" style="color:#ffffff;background-color: #555555;">당월마감정보</td>
          <td style="color:#ffffff;background-color: #555555;padding-left:5px;" align="center">
			<input type="text" name="closing" id="closing" value="<?=(!empty($ins->closing))?$ins->closing:""?>" class="form-control" style="width:90%;">
		  </td>
          <td style="color:#ffffff;background-color: #555555;" align="center">
                            <div class="input-group">
                               <input type="text" value="<?php
                                if (!empty($ins->cls_pay_date)) {
                                    echo $ins->cls_pay_date;
                                }
                                ?>" class="form-control datepicker" name="cls_pay_date"
                                       data-date-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>
          <td style="color:#ffffff;background-color: #555555;" align="center">

                            <div class="input-group">
                                <input type="text" value="<?php
                                if (!empty($ins->cls_gj_date)) {
                                    echo $ins->cls_gj_date;
                                }
                                ?>" class="form-control monthyear" name="cls_gj_date"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>


		  </td>
          <td style="color:#ffffff;background-color: #555555;" align="center">
                            <div class="input-group">
                               <input type="text" value="<?php
                                if (!empty($ins->exp_date)) {
                                    echo $ins->exp_date;
                                }
                                ?>" class="form-control datepicker" name="exp_date"
                                       data-date-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>
       </tr>
<?php
//대인배상 Ⅰ
if(!empty($ins->person1)) $person1 = $ins->person1; else $person1 = 0;
?>

        <tr>
          <td height="50" style="color:#ffffff;background-color: #777777;">대인배상 Ⅰ</td>
          <td align="left" style="padding-left:5px;"><input type="text" name="person1" id="person1" value="<?=(!empty($ins->person1))?$ins->person1:"";?>" class="form-control" style="width:90%;"  ></td>
          <td style="color:#000000;background-color: #ffffff;" rowspan="6" colspan="8" valign="top">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table table-striped DataTables">
<?php
	//수정모드
	if(!empty($ins->idx)) { 
		//$all_ins_sub = $this->db->where('pid', $ins->idx)->get('tbl_asset_truck_insur_sub');
			$this->asset_model->_table_name = 'tbl_asset_truck_insur_sub'; //table name
			$this->asset_model->db->where('tbl_asset_truck_insur_sub.pid', $ins->idx);
			$this->asset_model->_order_by = 'gj_month asc';
			$all_ins_sub = $this->asset_model->get();
		$j = 0; 
		foreach ($all_ins_sub as $ins_sub) {
		?>
		<input type=hidden name=chk[] value='<?=$j?>'>
		<input type="hidden" name="pay_cnt[<?=$j?>]" value="<?=($j+1)?>">
       <tr>
          <td width="19%" align="center" style="color:#ffffff;background-color: #777777;"><?=($j+1)?>회분납금
              <a href="javascript:goDelPay('<?=(!empty($ins_sub->pay_date))?$ins_sub->idx:""?>');" class="button red" title="x">x</a>
		  </td>
          <td align="left" width="23%" style="padding-left:5px;"><input type="text" name="pay_amt[<?=$j?>]" id="pay_amt<?=$j?>" value="<?php if (!empty($ins_sub->pay_date)) echo $ins_sub->pay_amt;?>" class='form-control' style="width:90%;"></td>
          <td align="left" width="20%" style="padding-left:5px;color:#00000;">
                            <div class="input-group">
                               <input type="text" value="<?php
                                if (!empty($ins_sub->pay_date)) {
                                    echo $ins_sub->pay_date;
                                }
                                ?>" class="form-control datepicker" name="pay_date[<?=$j?>]"
                                       data-date-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>
          <td align="left" width="19%" style="padding-left:5px;color:#00000;">
                             <div class="input-group">
                                <input type="text" value="<?php
                                if (!empty($ins_sub->gj_month)) {
                                    echo $ins_sub->gj_month;
                                }
                                ?>" class="form-control monthyear" name="gj_month[<?=$j?>]"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>

		  </td>
          <td align="left" width="19%" style="padding-left:5px;color:#00000;">
                            <div class="input-group">
                               <input type="text" value="<?php
                                if (!empty($ins_sub->cls_date)) {
                                    echo $ins_sub->cls_date;
                                }
                                ?>" class="form-control datepicker" name="cls_date[<?=$j?>]"
                                       data-date-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>

       </tr>
		<?
			$j++;
		}
		if($spay_cnt>$j) {
			for($q=$j;$q<$spay_cnt;$q++) {
				?>
				<input type=hidden name=chk[] value='<?=$q?>'>
				<input type="hidden" name="pay_cnt[<?=$q?>]" value="<?=($q)?>">
			   <tr>
				  <td width="19%" align="center" style="color:#ffffff;background-color: #777777;"><?=($q+1)?>회분납금</td>
				  <td align="left" width="23%" style="padding-left:5px;"><input type="text" name="pay_amt[<?=$q?>]" id="pay_amt<?=$q?>" value="" class="form-control" style="width:90%;"></td>
				  <td align="left" width="20%" style="padding-left:5px;color:#00000;">
									<div class="input-group">
									   <input type="text" value="" class="form-control datepicker" name="pay_date[<?=$q?>]"
											   data-date-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

										<div class="input-group-addon">
											<a href="#"><i class="fa fa-calendar"></i></a>
										</div>
									</div>
				  </td>
				  <td align="left" width="19%" style="padding-left:5px;color:#00000;">
									 <div class="input-group">
										<input type="text" value="" class="form-control monthyear" name="gj_month[<?=$q?>]"
											   data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

										<div class="input-group-addon">
											<a href="#"><i class="fa fa-calendar"></i></a>
										</div>
									</div>

				  </td>
				  <td align="left" width="19%" style="padding-left:5px;color:#00000;">
									<div class="input-group">
									   <input type="text" value="" class="form-control datepicker" name="cls_date[<?=$q?>]"
											   data-date-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

										<div class="input-group-addon">
											<a href="#"><i class="fa fa-calendar"></i></a>
										</div>
									</div>
				  </td>
			   </tr>
				<?
			}
		}
		?>
<? } else { ?>
	<? 
	$pay_cnt = 0;
	if(!empty($spay_cnt) && $spay_cnt) $pay_cnt = $spay_cnt;
	for($j=0;$j<$pay_cnt;$j++) { ?>
		<input type=hidden name=chk[] value='<?=$j?>'>
		<input type="hidden" name="pay_cnt[<?=$j?>]" value="<?=($j+1)?>">
       <tr>
          <td width="19%" align="center" style="color:#ffffff;background-color: #777777;"><?=($j+1)?>회분납금</td>
          <td align="left" width="23%" style="padding-left:5px;"><input type="text" name="pay_amt[<?=$j?>]" id="pay_amt<?=$i?>" value="<?//=$row[pay_amt]?>" class="form-control" style="width:90%;"></td>
          <td align="left" width="20%" style="padding-left:5px;color:#00000;">
                            <div class="input-group">
                               <input type="text" value="" class="form-control datepicker" name="pay_date[<?=$j?>]"
                                       data-date-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>
          <td align="left" width="19%" style="padding-left:5px;color:#00000;">
                             <div class="input-group">
                                <input type="text" value="" class="form-control monthyear" name="gj_month[<?=$j?>]"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>

		  </td>
          <td align="left" width="19%" style="padding-left:5px;color:#00000;">
                            <div class="input-group">
                               <input type="text" value="" class="form-control datepicker" name="cls_date[<?=$j?>]"
                                       data-date-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>

       </tr>
	<? } ?>
<? } ?>
      </table>

		  </td>
       </tr>
<?php
//대인배상
if(!empty($ins->person2)) $person2 = $ins->person2; else $person2 = 0;
if(!empty($ins->object)) $object = $ins->object; else $object = 0;
if(!empty($ins->self_body)) $self_body = $ins->self_body; else $self_body = 0;
if(!empty($ins->self_car)) $self_car = $ins->self_car; else $self_car = 0;
if(!empty($ins->call_svc)) $call_svc = $ins->call_svc; else $call_svc = 0;
if(!empty($ins->start_date)) $start_date = $ins->start_date; else $start_date = "";
if(!empty($ins->end_date)) $end_date = $ins->end_date; else $end_date = "";
$ins2_tot = $person1+$person2+$object+$self_body+$self_car+$call_svc;

?>
        <tr align="center">
          <td height="50" style="color:#ffffff;background-color: #777777;">대인배상 Ⅱ</td>
          <td width="14%" align="left" style="padding-left:5px;"><input type="text" name="person2" id="person2" value="<?php echo $person2;?>" class="form-control" style="width:90%;"></td>
       </tr>
        <tr align="center">
          <td height="50" style="color:#ffffff;background-color: #777777;">대물배상</td>
          <td align="left" style="padding-left:5px;"><input type="text" name="object" id="object" value="<?php echo $object;?>" class="form-control" style="width:90%;"  ></td>
       </tr>
        <tr align="center">
          <td height="50" style="color:#ffffff;background-color: #777777;">자기신체사고</td>
          <td align="left" style="padding-left:5px;"><input type="text" name="self_body" id="self_body" value="<?php echo $self_body;?>" class="form-control" style="width:90%;"  ></td>
       </tr>
        <tr align="center">
          <td height="50" style="color:#ffffff;background-color: #777777;">자차배상</td>
          <td align="left" style="padding-left:5px;"><input type="text" name="self_car" id="self_car" value="<?php echo $self_car;?>" class="form-control" style="width:90%;"  ></td>
       </tr>
        <tr align="center">
          <td height="50" style="color:#ffffff;background-color: #777777;">출동서비스</td>
          <td align="left" style="padding-left:5px;"><input type="text" name="call_svc" id="call_svc" value="<?php echo $call_svc;?>" class="form-control" style="width:90%;"  ></td>
       </tr>
       <tr align="center" align="center" bgcolor="#e0e7ef">
          <td height="30" style="color:#ffffff;background-color: #aaaaaa;">보험가입증명서</td>
		  <td align="left" style="padding-rightt:5px;background-color: #ffffff;font-weight:bold;background-color: #efefef;" colspan="3"><input type="file" name="cert_file1" id="cert_file1" value="" class="form-control" style="width:85%;">
<?
if(!empty($ins->cert_file1) && $ins->cert_file1) {
	echo "<a href='".base_url() . $ins->cert_file1."' target='_blank' style='font-weight:bold;color:blue;'>[증명서보기]</a>";
}
?>		  
		  </td>
          <td align="left" style="padding-left:5px;" colspan="4"><input type="text" name="xxx" id="xxx" value="<?// echo $xxx;?>" class="form-control" style="width:90%;"  ></td>
        </tr>
       <tr align="center" align="center" bgcolor="#e0e7ef">
          <td height="30" style="color:#ffffff;background-color: #aaaaaa;">첨부서류 II</td>
		  <td align="left" style="padding-rightt:5px;background-color: #ffffff;font-weight:bold;background-color: #efefef;" colspan="3"><input type="file" name="cert_file2" id="cert_file2" value="" class="form-control" style="width:85%;">
<?
if(!empty($ins->cert_file2) && $ins->cert_file2) {
	echo "<a href='".base_url() . $ins->cert_file2."' target='_blank' style='font-weight:bold;color:blue;'>[첨부서류 II]</a>";
}
?>		  
		  </td>
          <td align="left" style="padding-left:5px;" colspan="4"><input type="text" name="xxx" id="xxx" value="<?// echo $xxx;?>" class="form-control" style="width:90%;"  ></td>
        </tr>
       <tr align="center" align="center" bgcolor="#e0e7ef">
          <td height="30" style="color:#ffffff;background-color: #aaaaaa;">첨부서류 III</td>
		  <td align="left" style="padding-rightt:5px;background-color: #ffffff;font-weight:bold;background-color: #efefef;" colspan="3"><input type="file" name="cert_file3" id="cert_file3" value="" class="form-control" style="width:85%;">
<?
if(!empty($ins->cert_file3) && $ins->cert_file3) {
	echo "<a href='".base_url() . $ins->cert_file3."' target='_blank' style='font-weight:bold;color:blue;'>[첨부서류 III]</a>";
}
?>		  
		  </td>
          <td align="left" style="padding-left:5px;" colspan="4"><input type="text" name="xxx" id="xxx" value="<?// echo $xxx;?>" class="form-control" style="width:90%;"  ></td>
        </tr>
       <tr align="center" align="center" bgcolor="#e0e7ef">
          <td height="30" style="color:#ffffff;background-color: #777777;">계약기간</td>
		  <td align="right" style="padding-rightt:5px;background-color: #ffffff;font-weight:bold;">
			가입일:
		  </td>
		  <td align="left" style="padding-rightt:5px;background-color: #ffffff;font-weight:bold;">
                            <div class="input-group">
                               <input type="text" value="<?php
                                if (!empty($start_date)) {
                                    echo $start_date;
                                }
                                ?>" class="form-control datepicker" name="start_date"
                                       data-date-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>
		  <td align="center" style="padding-rightt:5px;background-color: #ffffff;font-weight:bold;"> ~ </td>
		  <td align="right" style="padding-rightt:5px;background-color: #ffffff;font-weight:bold;">만기일:</td>
		  <td align="left" style="padding-rightt:5px;background-color: #ffffff;font-weight:bold;">
                            <div class="input-group">
                               <input type="text" value="<?php
                                if (!empty($end_date)) {
                                    echo $end_date;
                                }
                                ?>" class="form-control datepicker" name="end_date"
                                       data-date-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>
		  <td align="left" style="padding-rightt:5px;background-color: #ffffff;font-weight:bold;"></td>
        </tr>

	   <tr align="center" align="center" bgcolor="#e0e7ef">
          <td height="30" style="color:#ffffff;background-color: #777777;">합계</td>
		  <td colspan="3" align="right" style="padding-rightt:5px;background-color: #ffffff;font-weight:bold;"><?=number_format($ins2_tot)?></td>



<?
$pay = 0;
/*
$sql5 = " select * from az_scontract_fixinsur_pay where pid='$df[idx]' ";
$result = sql_query($sql5);

for ($i=0; $row=sql_fetch_array($result); $i++) {
		$pay += $row[pay_amt];
}
*/
?>
		  <td align="right" colspan="3" style="padding-rightt:5px;background-color: #ffffff;font-weight:bold;"><?=number_format($pay);?>
		  </td>
        </tr>
		</table>
<?
$mode_txt = "입력";
$mode_param = "";
if(!empty($cid) && $cid) {
	//$cmt = sql_fetch("select * from az_scontract_fixinsur_memo where idxc='$cid'");
	$mode_txt = "수정";
	$mode_param = "'".$cid."'";

}
?>

                                                <div class="form-group">
													<label class="col-lg-12"></label>
													<div class="col-lg-12">
														<button type="submit"
																class="btn btn-sm btn-primary">저장</button>
													</div>
												</div>



      <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#e4e4e4" align="center" class="table table-striped DataTables">
        <tr>
          <td width="12%" height="20" style="color:#ffffff;background-color: #777777;padding-left:5px;">코맨트 <?=$mode_txt?></td>
          <td width="78%" style="padding-left:5px;" align="left" bgcolor="#ffffff">
		  <input type="text" name="memo" id="memo" value="<?//= $cmt[memo] ?>" class="form-control" style="width:85%;">
         </td>
          <td width="10%" style="padding-left:5px;" align="left" bgcolor="#ffffff">
             <a href="javascript:goWriteCmt(<?=$mode_param?>)" class="button red" title="등록"> 등록 </a>
         </td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#e4e4e4" align="center" class="table table-striped DataTables">
        <tr>
          <td width="4%" height="20" style="color:#ffffff;background-color: #777777;text-align:center;">No</td>
          <td width="" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">내 용</td>
          <td width="12%" style="color:#ffffff;background-color: #777777;text-align:center;">등록일</td>
          <td width="15%" style="color:#ffffff;background-color: #777777;text-align:center;">수정</td>
        </tr>
<? if($tr_id) { 
	//$sql = " select * from az_scontract_fixinsur_memo where idxs='$idx' order by reg_datetime desc";
	//$result = sql_query($sql);
	//for ($i=0; $row=sql_fetch_array($result); $i++) {
		$sn_bg = "#ffffff";

?>
        <tr bgcolor="<?=$sn_bg?>">
          <td height="20" style="text-align:center;"><?//=($i+1)?></td>
          <td colspan="2" style="text-align:left;padding-left:5px;"><?//=$row[memo]?></td>
          <td style="text-align:center;"><?//=substr($row[reg_datetime],0,10)?></td>
          <td style="text-align:center;">
             <a href="javascript:goEditCmt('<?//=$row[idxc]?>');" class="button red" title="등록"> 수정 </a>
             <a href="javascript:goDelCmt('<?//=$row[idxc]?>');" class="button red" title="등록"> 삭제 </a>
		  </td>
        </tr>
<?
	 // }
  }
?>

     </table>




      <div style="padding-top:5px;"></div>

      <div style="padding-top:10px;"></div>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#e4e4e4" align="center" class="table table-striped DataTables">
		  <td style="color:#00000;background-color: #ffffff;" width="70">
              <a href="javascript:goSave();" class="button red" title="저장"> 저&nbsp;&nbsp;&nbsp;&nbsp;장 </a>
		  
<!--	<? if($chk[cnt]>0) { ?>
              <a href="javascript:goApply('N');" class="button grey" title="적용완료" style="font-weight:bold;"> 적용완료 </a>
	<? } else { ?>
              <a href="javascript:goApply('Y')" class="button red" title="유통물류적용" style="font-weight:bold;"> 유통물류적용 </a>
	<? } ?>
-->
		  </td>
		</table>


    </td>
  </tr>
</table> 













                                    </div><!-- /.nav-tabs-custom -->
                                </div>
							<input type="hidden" name="tot_amount" id="tot_amount" value="<?php if(!empty($ins2_tot)) echo $ins2_tot?>"/>
                        </form>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?php if (!empty($car_info->idx)) echo  base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>