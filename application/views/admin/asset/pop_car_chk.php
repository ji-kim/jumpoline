<div class="row">
    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#chk_list"
                                                                   data-toggle="tab">정기(정밀)검사목록</a></li>



                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_chk"
                                                                   data-toggle="tab">다음검사일등록 </a></li>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="chk_list" style="position: relative;">
                        <div class="box">
       <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table table-striped DataTables">
        <tr align="center" bgcolor="#e0e7ef" style="text-align:center;">
          <td style="color:#ffffff;background-color: #777777;" width="40" rowspan="2">No</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="2">이전검사정보</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="3">다음검사정보</td>
          <td style="color:#ffffff;background-color: #777777;" rowspan="2">비고</td>
          <td style="color:#ffffff;background-color: #777777;" rowspan="2">작업</td>
		</tr>
        <tr align="center" bgcolor="#e0e7ef" style="text-align:center;">
          <td style="color:#ffffff;background-color: #777777;">지정검사일</td>
          <td style="color:#ffffff;background-color: #777777;">최종검사일</td>

          <td style="color:#ffffff;background-color: #777777;">다음지정검사일</td>
          <td style="color:#ffffff;background-color: #777777;">다음검사개시일</td>
          <td style="color:#ffffff;background-color: #777777;">다음검사만료일</td>
		</tr>

<?php
	$i = 0;
	if (!empty($all_check_group)) {
		foreach ($all_check_group as $check_details) {
		$sn_bg = "#ffffff";
?>
        <tr bgcolor="<?=$sn_bg?>">
          <td height="25" align="center"><?=$i?></td>
          <td align="center"><?= $check_details->last_assigned ?></td>
          <td align="center"><?= $check_details->laste_check ?></td>
          <td align="center"><?= $check_details->next_assigned ?></td>
          <td align="center"><?= $check_details->next_start ?></td>
          <td align="center"><?= $check_details->next_expired ?></td>
          <td align="left"><?= $check_details->remark ?></td>
          <td align="center">
	              <a href="javascript:goEditTchk('<?= $check_details->idx ?>');" class="button gray" title="수정"> 수정 </a>
	              <a href="javascript:goDelTchk('<?= $check_details->idx ?>');" class="button red" title="삭제"> 삭제 </a>
		  </td>
		</tr>
<?
	}
  } else {
?>

        <tr bgcolor="#ffffff">
          <td colspan="30" align="center">자료가 없습니다.</td>
        </tr>
<?
  }
?>
</table>

<script type="text/javascript">
<!--

function goWriteIns(idx) {
	var properties = 'width=600,height=400,top=100,left=100,scrollbars=yes';
	pop = window.open('./car_insure2_write.php?tr_id=<?=$tr_id?>&idx='+idx, 'WinCarEdit2', properties);
	pop.focus();
 }
//-->
</script>
                        </div>
                    </div>
                    <?php if (1) { //!empty($created) || !empty($edited)) { 
					
										//차량정보
						if(!empty($tr_id) ) {
							$last_info = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
						}
					
					?>
                        <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_chk"
                        style="position: relative;">
                        <form role="form" enctype="multipart/form-data" name="myform" id="myform" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/asset/pop_save_carinfo_lins/<?php
                              if (!empty($tr_id)) {
                                  echo $tr_id;
                              }
                              ?>" method="post" class="form-horizontal  ">
							<input type="hidden" name="md" id="md" value="<?php if(!empty($md)) echo $md?>"/>
							<input type="hidden" name="page" id="page" value="<?php if(!empty($page)) echo $page?>"/>

                            <div class="panel-body">
                                <label class="control-label col-sm-1"></label>
                                <div class="col-sm-12">
                                    <div class="nav-tabs-custom">



<table width="100%" border="0" cellpadding="0">
  <tr>
    <td align="center" valign="top">

      <!-- 타이틀 시작 -->
      <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#e4e4e4" align="center" class="table table-striped DataTables">
        <tr>
          <td align="left" style="font-size:13px;">
		  ◈ 배차지 : <?php if(!empty($coop->co_name)) echo $coop->co_name;?>   >   운영사 : <?php if(!empty($op->co_name)) echo $op->co_name;?>   >   차량번호 : <?php if(!empty($tr->car_1)) echo $tr->car_1;?>   > 위수탁자 : <?php if(!empty($dp->E)) echo $dp->E;?>  
		  </td>
          <td align="left" style="font-size:13px;">
		  </td>
        </tr>
      </table>
      <!-- 타이틀 끝 -->
      <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#e4e4e4" align="center" class="table table-striped DataTables">
        <tr>
          <td colspan="2" height="20" style="color:#ffffff;background-color: #777777;text-align:center;">이전검사정보</td>
        </tr>
        <tr>
          <td height="20" width="15%" style="color:#ffffff;background-color: #777777;text-align:center;">지정검사일</td>
          <td style="color:#000000;background-color: ffffff;padding-left:5px;">
                             <div class="input-group">
                                <input type="text" value="<?php if(!empty($info->last_assigned)) echo $info->last_assigned;?>" class="form-control datepicker" name="last_assigned"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>
        </tr>
        <tr>
          <td height="20" style="color:#ffffff;background-color: #777777;text-align:center;">최종검사일</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;">
                             <div class="input-group">
                                <input type="text" value="<?php if(!empty($info->laste_check)) echo $info->laste_check;?>" class="form-control datepicker" name="laste_check"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>
        </tr>

		
        <tr>
          <td colspan="2" height="20" style="color:#ffffff;background-color: #777777;text-align:center;">다음검사정보</td>
        </tr>
        <tr>
          <td height="20" style="color:#ffffff;background-color: #777777;text-align:center;">다음지정검사일</td>
          <td style="color:#000000;background-color: ffffff;padding-left:5px;">
                             <div class="input-group">
                                <input type="text" value="<?php if(!empty($info->next_assigned)) echo $info->next_assigned;?>" class="form-control datepicker" name="next_assigned"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>
        </tr>
        <tr>
          <td height="20" style="color:#ffffff;background-color: #777777;text-align:center;">다음검사개시일</td>
          <td style="color:#000000;background-color: ffffff;padding-left:5px;">
                             <div class="input-group">
                                <input type="text" value="<?php if(!empty($info->next_start)) echo $info->next_start;?>" class="form-control datepicker" name="next_start"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>
        </tr>
        <tr>
          <td height="20" style="color:#ffffff;background-color: #777777;text-align:center;">다음검사만료일</td>
          <td style="color:#000000;background-color: ffffff;padding-left:5px;">
                             <div class="input-group">
                                <input type="text" value="<?php if(!empty($info->next_expired)) echo $info->next_expired;?>" class="form-control datepicker" name="next_expired"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
		  </td>
        </tr>
        <tr>
          <td height="20" style="color:#ffffff;background-color: #777777;text-align:center;">비      고</td>
          <td colspan="2" style="color:#000000;background-color: ffffff;padding-left:5px;">
			<input type="text" name="remark" id="remark" value="<?php if(!empty($info->remark)) echo $info->remark;?>" class="form-control" style="width:96%;">
		  </td>
        </tr>
      </table>




      <div style="padding-top:5px;"></div>

      <div style="padding-top:10px;"></div>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#e4e4e4" align="center" class="table table-striped DataTables">
		  <td style="color:#00000;background-color: #ffffff;" width="70">
														<button type="submit"
																class="btn btn-sm btn-primary">저장</button>
		  
<!--	<? if($chk[cnt]>0) { ?>
              <a href="javascript:goApply('N');" class="button grey" title="적용완료" style="font-weight:bold;"> 적용완료 </a>
	<? } else { ?>
              <a href="javascript:goApply('Y')" class="button red" title="유통물류적용" style="font-weight:bold;"> 유통물류적용 </a>
	<? } ?>
-->
		  </td>
		</table>


    </td>
  </tr>
</table> 













                                    </div><!-- /.nav-tabs-custom -->
                                </div>
							<input type="hidden" name="tot_amount" id="tot_amount" value="<?php if(!empty($ins2_tot)) echo $ins2_tot?>"/>
                        </form>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?php if (!empty($car_info->idx)) echo  base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>