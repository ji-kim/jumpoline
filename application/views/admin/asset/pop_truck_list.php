<?php
// 현재페이지, 총페이지수, 한페이지에 보여줄 행, URL
function get_paging($write_pages, $cur_page, $total_page, $url, $add="")
{
    $str = "";
    if ($cur_page > 1) {
        $str .= "<a href='" . $url . "1{$add}'>처음</a>";
        //$str .= "[<a href='" . $url . ($cur_page-1) . "'>이전</a>]";
    }

    $start_page = ( ( (int)( ($cur_page - 1 ) / $write_pages ) ) * $write_pages ) + 1;
    $end_page = $start_page + $write_pages - 1;

    if ($end_page >= $total_page) $end_page = $total_page;

    if ($start_page > 1) $str .= " &nbsp;<a href='" . $url . ($start_page-1) . "{$add}'>이전</a>";

    if ($total_page > 1) {
        for ($k=$start_page;$k<=$end_page;$k++) {
            if ($cur_page != $k)
                $str .= " &nbsp;<a href='$url$k{$add}'><span>$k</span></a>";
            else
                $str .= " &nbsp;<b>$k</b> ";
        }
    }

    if ($total_page > $end_page) $str .= " &nbsp;<a href='" . $url . ($end_page+1) . "{$add}'>다음</a>";

    if ($cur_page < $total_page) {
        //$str .= "[<a href='$url" . ($cur_page+1) . "'>다음</a>]";
        $str .= " &nbsp;<a href='$url$total_page{$add}'>맨끝</a>";
    }
    $str .= "";

    return $str;
}

?>

      <!-- 검색 시작 -->
      <table border="0" cellspacing="1" cellpadding="0" width="960" align="left" >
        <tr>
          <td align="left" valign="top">
            <table border="0" width="100%" cellpadding="2" cellspacing="5" bgcolor="#ffffff" class="table table-striped DataTables">
              <tr>

                <td width="10%" height="20" align="center" bgcolor="#efefef">선택조회
                </td>
                <td width="20%" height="20" align="center" bgcolor="#FFFFFF">
				<select name="search_field" id="search_field" class="form-control" style="width:100%;">
					<option value="">선택</option>
					<option value="car_1" <?=(!empty($search_field) && $search_field == "car_1") ? "selected" : ""?>>자동차 등록번호</option>
					<option value="car_7" <?=(!empty($search_field) && $search_field == "car_7") ? "selected" : ""?>>차대번호</option>
				</select>
				</td>
                <td width="20%" style="padding-left:3px;" align="left" bgcolor="#ffffff"><input type="text" name="search_keyword" id="search_keyword" maxlength="20" value="<?=(empty($search_keyword))?"":$search_keyword ?>" class="form-control" style="width:100%;">
                </td>
                <td width="30%" style="padding-left:3px;" align="left" bgcolor="#ffffff"> 
					<input type="radio" name="car_status" value="A" <?=($car_status=="A"||!$car_status)?"checked":""?>> 일반차량
					<input type="radio" name="car_status" value="N" <?=($car_status=="N")?"checked":""?>> 할당대기차량
					<input type="radio" name="car_status" value="G" <?=($car_status=="G")?"checked":""?>> 공T/E차량

                </td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff" COLSPAN="9">
            <div align="CENTER">

              <a href="javascript:goSearch()" class="button red" title="검색"> <span class='label label-primary'>검색 </span></a>
            </div>

                </td>
              </tr>
            </table>
		  </td>
        </tr>
      </table>
      <!-- 검색 끝 -->
      <table width="960" border="0" cellpadding="0" cellspacing="0" class="table table-striped DataTables">
		<tr>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">No</td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">자동차 등록번호</td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">차대번호</td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">차 명</td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">형 식</td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">용 도</td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">지입사</td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">선택</td>
		</tr>

<?php
$cnt = 0;
if (!empty($all_truck_group)) {
	foreach ($all_truck_group as $truck_info) { 
		$row_no = $total_count - $from_record - $cnt;
		?>
        <tr bgcolor="#ffffff">
          <td height="25" align="center"><?= $row_no ?></td>
          <td align="center"><?php if(!empty($truck_info->car_1)) echo $truck_info->car_1; ?></td>
          <td align="center"><?php if(!empty($truck_info->car_7)) echo $truck_info->car_7; ?></td>
          <td align="center"><?php if(!empty($truck_info->car_4)) echo $truck_info->car_4; ?></td>
          <td align="center"><?php if(!empty($truck_info->mode)) echo $truck_info->mode; ?></td>
          <td align="center"><?php if(!empty($truck_info->car_3)) echo $truck_info->car_3; ?></td>
          <td align="center"><?php if(!empty($truck_info->ws_co_name)) echo $truck_info->ws_co_name; ?></td>
          <td align="center">
		  <a href="javascript:selectTruck('<?= $truck_info->idx ?>','<?php if(!empty($truck_info->car_1)) echo $truck_info->car_1; ?>','<?php if(!empty($truck_info->car_7)) echo $truck_info->car_7; ?>','<?= $truck_info->car_3 ?>','<?php if(!empty($truck_info->ws_co_id)) echo $truck_info->ws_co_id; ?>','<?php if(!empty($truck_info->ws_co_name)) echo $truck_info->ws_co_name; ?>')" class="button red" title="선택"><span class='label label-primary'> 선택 </span></a>
		  </td>
        </tr>
<?
		$cnt++;
    }
  } else {
?>
        <tr bgcolor="#ffffff">
          <td colspan="8">조회된 결과가 없습니다.</td>
        </tr>
<?
  }
?>

      </table>
      <div style="padding-top:10px;"></div>

      <!-- 페이징 시작 -->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table table-striped DataTables">
      	<tr>
      		<td height="20" align="left">
<?
$pagelist = get_paging(10, $page, $total_page, base_url()."admin/asset/select_truck/".$params."/");
echo $pagelist;
?>
			</td>
      	</tr>
      </table>
      <!-- 페이징 끝 -->
	  
