<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
    ?>
<style>
/* Ensure that the demo table scrolls */
	table > thead > tr > th {
		vertical-align:bottom;
		border-bottom:1px solid #eee;
		border-top:0px solid #eee;
		font-weight : none;
	}
    th, td {
        white-space: nowrap;
        padding-left: 10px !important;
        padding-right: 10px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }

</style>
<script>
	function popAsWindow(md,tr_id) {
	  window.open('<?php echo base_url(); ?>admin/asset/pop_carinfo/'+md+'/'+tr_id, 'winAS', 'left=50, top=50, width=1480, height=700, scrollbars=1');
	}
	function goSearch(val) {
		if(val == 'all' || val == '전체') {
			document.myform.action = "<?php echo base_url() ?>admin/asset/car_list_all";
			document.myform.ws_co_id.value = '';
		} else {
			document.myform.action = "<?php echo base_url() ?>admin/asset/car_list";
		}
		document.myform.submit();
	}
</script>
            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
                
    <!-- 검색 시작 -->
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/asset/car_list"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
	<table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
        <tr>
          <td align="left" valign="top" bgcolor="#ffffff">
            <table border="0" width="100%" cellpadding="12" cellspacing="15" bgcolor="#ffffff">
              <tr>
                <td width="12%" align="center" bgcolor="#efefef">위수탁관리사</td>
                <td width="20%" style="padding-left:5px;" align="left" bgcolor="#ffffff;">
                                                        <div class="input-group">
														<input type="hidden" name="ws_co_id" id="sws_co_id" value="<?php
                                                               if (!empty($ws_co_id)) {
                                                                   echo $ws_co_id;
                                                               }
                                                               ?>">
															<input type="text" name="ws_co_name" id="sws_co_name" value="<?=(empty($ws_co_name))?"":$ws_co_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('sws_co_id||sws_co_name||||||||||','coop');" onChange="goSearch(this.value)">
                                                        </div>


					<!--select name="ws_co_id" id="ws_co_id" style="width:100%;background-color:yellow;" class="form-control input-sm" onChange="goSearch(this.value)">
							<option value="all" <?=($ws_co_id=="all")?"selected":""?>>전체</option>
							<option value="1413" <?=($ws_co_id=="1413")?"selected":""?>>케이티지엘에스(주) </option>
							<option value="1414" <?=($ws_co_id=="1414")?"selected":""?>>(주)아이디일일구닷컴</option>
							<option value="1415" <?=($ws_co_id=="1415")?"selected":""?>>(주)델타온</option>
							<option value="1903" <?=($ws_co_id=="1903")?"selected":""?>>(주)휴먼엘지</option>
							<option value="etc" <?=($ws_co_id=="etc")?"selected":""?>>기타</option>
					</select-->

					<!--a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/select_reqco"><input type="hidden" name="ws_co_id" id="ws_co_id" value="">
					<input type="text" name="ws_co_name" id="ws_co_name" value="" class="form-control" style="width:100%;background-color:yellow;"></a-->

                </td>
                <td width="12%" align="center" bgcolor="#efefef">배차지</td>
                <td width="20%" align="center" >
					<select name="baecha_co_id" id="baecha_co_id" style="width:100%;" class="form-control input-sm" >
				<option value="">전체</option>
<?php
if (!empty($all_baecha_info)) {
	foreach ($all_baecha_info as $baecha_details) {
?>
					<option value="<?=$baecha_details->code?>" <?=($baecha_co_id == $baecha_details->code) ? "selected" : ""?>><?=$baecha_details->co_name?></option>
<?php
	}
}
?>
							</select>
				</td>
                <td>
					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables" href="javascript:goSearch(document.myform.ws_co_name.value);">
					<span><i class="fa fa-search"> </i> 검색</span>
					</a>

                </td>
			</tr>
            </table>
		  </td>
        </tr>
      </table>
		</form>
      <!-- 검색 끝 -->

				

            </div>





<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip"
             data-title="<?php echo lang('filter_by'); ?>">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-filter" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-left"
                style="width:300px;<?php if (!empty($search_by) && $search_by == 'group') {
                    echo 'display:block';
                } ?>">
                <li class="<?php
                if (empty($search_by)) {
                    echo 'active';
                } ?>"><a
                        href="<?= base_url() ?>admin/client/manage_client"><?php echo lang('all'); ?></a>
                </li>
                <li class="divider"></li>
                <?php if (count($all_customer_group) > 0) { ?>
                    <li class="dropdown-submenu pull-left groups <?php if (!empty($id)) {
                        if ($search_by == 'group') {
                            echo 'active';
                        }
                    } ?>">
                        <a href="#" tabindex="-1"><?php echo lang('customer_group'); ?></a>
                        <ul class="dropdown-menu dropdown-menu-left"
                            style="<?php if (!empty($search_by) && $search_by == 'group') {
                                echo 'display:block';
                            } ?>">
                            <?php foreach ($all_customer_group as $group) {
                                ?>
                                <li class="<?php if (!empty($id)) {
                                    if ($search_by == 'group') {
                                        if ($id == $group->customer_group_id) {
                                            echo 'active';
                                        }
                                    }
                                } ?>">
                                    <a href="<?= base_url() ?>admin/client/manage_client/group/<?php echo $group->customer_group_id; ?>"><?php echo $group->customer_group; ?></a>
                                </li>
                            <?php }
                            ?>
                        </ul>
                    </li>
                    <div class="clearfix"></div>
                    <li class="divider"></li>
                <?php } ?>
            </ul>
        </div>
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#car_list"
                                                                   data-toggle="tab">차량목록</a></li>



                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_car"
                                                                   data-toggle="tab">신규등록</a></li>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="car_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong>차량목록</strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
      <table width="1000" border="0" cellpadding="0" cellspacing="0" id="DataTables" class="table table-striped DataTables">
                                <thead>
        <tr align="center" bgcolor="#e0e7ef" style="text-align:center;vertical-align:center;">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>NO</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>반납예정등록일</td>
          <!--td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>반납재등록마감일</td-->
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="6">차량정보</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">자동차보험</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">적재물보험</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">자동차정밀(정기)검사</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="5">최종차주정보</td>
          <td style="color:#ffffff;background-color: #777777;border-left:1px solid #eee;" rowspan="2">수정</td>
		</tr>
		<tr align="center">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차량번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차대번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">년식</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">용도</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차종</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">톤수</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험가입일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험만기일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">분납횟수</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험가입일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험만기일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">분납여부</td>


          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">최종검사일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">다음검사일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">개시일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">만료일</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">배차지</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">위ㆍ수탁관리회사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">이전차주명</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">현차주명</td>
          <td style="color:#ffffff;background-color: #777777;">운전자</td>
		</tr>
                                </thead>

                                <tbody>
                               <?php
                                if (!empty($all_car_info)) {
                                    foreach ($all_car_info as $car_details) {

										// 자동차보험
										$car_ins = $this->db->where('tr_id', $car_details->idx)->where('active_yn', 'Y')->where('type', '자동차')->get('tbl_asset_truck_insur')->row();
										if(!empty($car_ins->ins_co_id)) {
											$car_ins_co = $this->db->where('idx', $car_ins->ins_co_id)->get('tbl_insur_company')->row();
											if(empty($car_ins_co->deptname)) $ins_co_name = ""; else $ins_co_name = $car_ins_co->deptname;
										}
										// 적재물
										$load_ins = $this->db->where('tr_id', $car_details->idx)->where('active_yn', 'Y')->where('type', '적재물')->get('tbl_asset_truck_insur')->row();
										if(!empty($load_ins->ins_co_id)) {
											$load_ins_co = $this->db->where('idx', $load_ins->ins_co_id)->get('tbl_insur_company')->row();
											if(empty($load_ins_co->deptname)) $load_ins_co_name = ""; else $load_ins_co_name = $load_ins_co->deptname;
										}

										//배차지
										$od_co = $this->db->where('code', $car_details->baecha_co)->where('mb_type', 'customer')->get('tbl_members')->row();

										// 차주정보
										$owner = $this->db->where('tr_id', $car_details->idx)->get('tbl_members')->row();
/*
                                        $client_transactions = $this->db->select_sum('amount')->where(array('paid_by' => $client_details->client_id))->get('tbl_transactions')->result();
                                        $customer_group = $this->db->where('customer_group_id', $client_details->customer_group_id)->get('tbl_customer_group')->row();
                                        $client_outstanding = $this->invoice_model->client_outstanding($client_details->client_id);
                                        $client_currency = $this->invoice_model->client_currency_sambol($client_details->client_id);
                                        if (!empty($client_currency)) {
                                            $cur = $client_currency->symbol;
                                        } else {
                                            $currency = $this->db->where(array('code' => config_item('default_currency')))->get('tbl_currencies')->row();
                                            $cur = $currency->symbol;
                                        }
*/
                                        ?>
                                    <tr>
                                        <td>
                                            <?= $car_details->idx ?>
                                        </td>
                                        <td><?php if(!empty($car_details->return_due_date)) echo $car_details->return_due_date; ?>
                                        </td>
                                        <!--td><?php if(!empty($car_details->return_cls_date)) echo $car_details->return_cls_date; ?>
                                        </td-->
                                        <td>
                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="차량정보 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_info','<?= $car_details->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
                                            <span class='label label-primary'><?= $car_details->car_1 ?></span>
	                                        
											<a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/asset/set_gongT/<?= $car_details->idx ?>"
                                               class="text-default ml"><span class='label label-warning'>공T/E</span></a>
											<!--a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/asset/set_rtplate/<?= $car_details->idx ?>"
                                               class="text-default ml"><span class='label label-warning'>반납</span></a-->

                                        </td>
                                        <td>
                                            <a href="<?= base_url() ?>admin/basic/car_details/<?= $car_details->idx ?>"
                                                   class="text-info"><span class='label label-primary'><?= $car_details->car_7 ?></span></a>
										</td>
                                        <td>
                                            <?= $car_details->car_5 ?>
                                        </td>
                                        <td>
                                            <?= $car_details->car_3 ?>
                                        </td>
                                        <td>
                                            <?= $car_details->type ?>
                                        </td>
                                        <td>
                                            <?= $car_details->carinfo_11 ?>
                                        </td>


                                        <td>
                                                <!--span data-placement="top" data-toggle="tooltip"
                                                      title="보험 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_ins','<?= $car_details->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>-->
											<span class='label label-primary'><?php if(!empty($ins_co_name)) echo $ins_co_name; ?></span>
                                        </td>
                                        <td>
											<?php if(!empty($car_ins->start_date)) echo $car_ins->start_date; ?>
                                        </td>
                                        <td>
											<?php if(!empty($car_ins->end_date)) echo $car_ins->end_date; ?>
                                        </td>
                                        <td>
											<?php if(!empty($car_ins->pay_cnt)) echo $car_ins->pay_cnt; ?>
                                        </td>



                                        <td>
                                                <!--span data-placement="top" data-toggle="tooltip"
                                                      title="적재보험 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_lins','<?= $car_details->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span-->
											<span class='label label-primary'><?php if(!empty($load_ins_co_name)) echo $load_ins_co_name; ?></span>
                                        </td>
                                        <td>
                                            <?=(empty($load_ins->start_date))?"":$load_ins->start_date ?>
                                        </td>
                                        <td>
                                            <?=(empty($load_ins->end_date))?"":$load_ins->end_date ?>
                                        </td>
                                        <td>
                                            <?=(empty($load_ins->pay_cnt))?"-":$load_ins->pay_cnt ?>
                                        </td>

                                        <td>
                                                 <!--span data-placement="top" data-toggle="tooltip" title="자동차검사 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_chk','<?= $car_details->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span-->
                                           <span class='label label-primary'><?= $car_details->op_co_id ?></span>
                                        </td>
                                        <td>
                                            <?= $car_details->op_co_id ?>
                                        </td>
                                        <td>
                                            <?= $car_details->op_co_id ?>
                                        </td>
                                        <td>
                                            <?= $car_details->op_co_id ?>
                                        </td>
                                        <td>
                                                <!--span data-placement="top" data-toggle="tooltip"
                                                      title="차주 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_own','<?= $car_details->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span-->
                                            <span class='label label-primary'><?php if(!empty($od_co->co_name)) echo $od_co->co_name; ?></span>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_details->ws_co_name)) echo $car_details->ws_co_name; ?>
                                        </td>
                                        <td>
                                            <?php //if(!empty($car_details->inv_co_name)) echo $car_details->inv_co_name; ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_details->ceo)) echo $car_details->ceo; ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_details->driver)) echo $car_details->driver; ?>
                                        </td>
                                        <td>
                                                <?php if (!empty($edited)) { ?>
                                                    <?php echo btn_edit('admin/asset/car_list/edit_asset/' . $car_details->idx) ?>
                                                <?php }
                                                if (!empty($deleted)) {
                                                    ?>
                                                    <?php echo btn_delete('admin/asset/car_list/delete_asset/' . $car_details->idx) ?>
                                                <?php } ?>
                                                <?php echo btn_view('admin/asset/car_list/edit_asset/' . $car_details->idx) ?>
										</td>
                                    </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php 
								}
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { ?>
                        <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_car"
                        style="position: relative;">
                        <form role="form" enctype="multipart/form-data" id="form" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/asset/save_car/<?php
                              if (!empty($car_info)) {
                                  echo $car_info->idx;
                              }
                              ?>" method="post" class="form-horizontal  ">
						<input type="hidden" name="ws_co_id" value="<?php if(!empty($car_info->ws_co_id)) echo $car_info->ws_co_id; ?>">

                            <div class="panel-body">
                                <label class="control-label col-sm-1"></label>
                                <div class="col-sm-10">
                                    <div class="nav-tabs-custom">
                                        <!-- Tabs within a box -->
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#general_car" data-toggle="tab">기초정보</a></li>
                                            <li><a href="#rinvest_compnay" data-toggle="tab">위수탁파트너</a></li>
                                            <li><a href="#wst_compnay" data-toggle="tab">위수탁관리사</a></li>
                                            <li><a href="#spec_car" data-toggle="tab">제원</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content bg-white">
                                            <!-- ************** 기본정보 *************-->
                                            <div class="chart tab-pane active" id="general_car">

                                                <div class="form-group">
													<label class="col-lg-2 control-label">소속</label>
                                                    <div class="col-lg-4">
														<input type="hidden" name="gr_id" id="gr_id" value="<?php
                                                               if (!empty($car_info->gr_id)) {
                                                                   echo $car_info->gr_id;
																   $gr = $this->db->where('dp_id', $car_info->gr_id)->get('tbl_member')->row();
																   if(!empty($gr->co_name)) $car_info->gr_name = $gr->co_name;
                                                               }
                                                               ?>">
															<input type="text" name="gr_name" id="gr_name" value="<?=(empty($car_info->gr_name))?"":$car_info->gr_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('gr_id||gr_name||||||||||','group');">
													</div>

													<label class="col-lg-2 control-label"></label>
                                                    <div class="col-lg-4">
                                                    </div>
                                                </div>
                                                <div class="form-group">
													<label class="col-lg-2 control-label">자동차 등록번호<span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->car_1)) {
                                                                   echo $car_info->car_1;
                                                               }
                                                               ?>" name="car_1">
                                                    </div>

													<label class="col-lg-2 control-label">차 종<span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($car_info->type)) {
                                                                   echo $car_info->type;
                                                               }
                                                               ?>" name="type">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">용 도
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <div class="input-group">
														<select name="car_3" class="form-control select_box">
															<option value='일반영업용' <?=(!empty($car_info->car_3) && "일반영업용" == $car_info->car_3) ? "selected" : ""?>>일반영업용</option>
															<option value='개별화물' <?=(!empty($car_info->car_3) && "개별화물" == $car_info->car_3) ? "selected" : ""?>>개별화물</option>
															<option value='개별용달' <?=(!empty($car_info->car_3) && "개별용달" == $car_info->car_3) ? "selected" : ""?>>개별용달</option>
															<option value='자가용' <?=(!empty($car_info->car_3) && "자가용" == $car_info->car_3) ? "selected" : ""?>>자가용</option>
														</select>
														</div>
                                                    </div>
                                                    <label class="col-lg-2 control-label">차 명
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($car_info->car_4)) {
                                                                   echo $car_info->car_4;
                                                               }
                                                               ?>" name="car_4">
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">형식
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($car_info->mode)) {
                                                                   echo $car_info->mode;
                                                               }
                                                               ?>" name="mode">
                                                    </div>
                                                    <label class="col-lg-2 control-label">연 식
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($car_info->car_5)) {
                                                                   echo $car_info->car_5;
                                                               }
                                                               ?>" name="car_5">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">차대번호
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($car_info->car_7)) {
                                                                   echo $car_info->car_7;
                                                               }
                                                               ?>" name="car_7">
                                                    </div>
                                                    <label class="col-lg-2 control-label">원동기 형식
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($car_info->motor_mode)) {
                                                                   echo $car_info->motor_mode;
                                                               }
                                                               ?>" name="motor_mode">

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">사용본거지
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->headquarter)) {
                                                                   echo $car_info->headquarter;
                                                               }
                                                               ?>" name="headquarter">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label"></label>
                                                    <div class="col-lg-4">
													</div>
												</div>
                                                <?//= custom_form_Fields(12, $tr_id); ?>
                                            </div><!-- ************** 기본정보 *************-->

                                            <!-- ************** 위수탁파트너 *************-->
                                            <div class="chart tab-pane" id="rinvest_compnay">
<script>
	function selectPartner(params,stype) {
	  if(stype == '') stype = 'partner';
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/'+stype+'/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
</script>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">상 호
                                                        <span class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
														<input type="hidden" name="inv_co_id" id="inv_co_id" value="<?php
                                                               if (!empty($car_info->inv_co_id)) {
                                                                   echo $car_info->inv_co_id;
                                                               }
                                                               ?>">
															<input type="text" name="inv_co_name" id="inv_co_name" value="<?=(empty($car_info->inv_co_name))?"":$car_info->inv_co_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('inv_co_id||inv_co_name||inv_reg_no||inv_location||inv_tel||inv_ceo||inv_fax','customer');">
                                                            <!--select name="xxx"
                                                                    class="form-control select_box"
                                                                    style="width: 100%" onChange="setInv(this.value);">
                                                                <?php
                                                                if (!empty($all_inv_group)) {
                                                                    foreach ($all_inv_group as $inv_group) : ?>
                                                                        <option value="<?=$inv_group->dp_id?>||<?=$inv_group->K?>||<?=$inv_group->co_address?>||<?=$inv_group->ceo_hp?>||<?=$inv_group->ceo?>||<?=$inv_group->fax?>"
																		<?php
                                                                        if (!empty($client_info->inv_co_id) && $car_info->inv_co_id == $inv_group->dp_id) {
                                                                            echo 'selected';
                                                                        } ?>
                                                                        ><?= $inv_group->co_name; ?></option>
                                                                    <?php endforeach;
                                                                }
                                                                $created = can_action('125', 'created');
                                                                ?>
                                                            </select>
                                                            <?php if (!empty($created)) { ?>
                                                                <div class="input-group-addon"
                                                                     title="신규등록"
                                                                     data-toggle="tooltip" data-placement="top">
                                                                    <a data-toggle="modal" data-target="#myModal"
                                                                       href="<?= base_url() ?>admin/basic/customer_group"><i
                                                                            class="fa fa-plus"></i></a>
                                                                </div>
                                                            <?php } ?>-->
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">주민등록번호</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->inv_reg_no)) {
                                                                   echo $car_info->inv_reg_no;
                                                               }
                                                               ?>" name="inv_reg_no" id="inv_reg_no">
													</div>
												</div>
												
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">소재지
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->T)) {
                                                                   echo $car_info->T;
                                                               }
                                                               ?>" name="inv_location" id="inv_location">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">전화번호</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->Y)) {
                                                                   echo $car_info->Y;
                                                               }
                                                               ?>" name="inv_tel" id="inv_tel">
													</div>
												</div>
												
												
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">대표자
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->D)) {
                                                                   echo $car_info->D;
                                                               }
                                                               ?>" name="inv_ceo" id="inv_ceo">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">FAX</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->fax)) {
                                                                   echo $car_info->fax;
                                                               }
                                                               ?>" name="inv_fax" id="inv_fax">
													</div>
												</div>
                                            </div><!-- ************** 현물출자자 *************-->
                                            <!-- ************** 위수탁 *************-->
                                            <div class="chart tab-pane" id="wst_compnay">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">상 호
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                   <div class="col-lg-4">
                                                        <div class="input-group">
														<input type="hidden" name="ws_co_id" id="ws_co_id" value="<?php
                                                               if (!empty($car_info->ws_co_id)) {
                                                                   echo $car_info->ws_co_id;
                                                               }
                                                               ?>">
															<input type="text" name="ws_co_name" id="ws_co_name" value="<?=(empty($car_info->ws_co_name))?"":$car_info->ws_co_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('ws_co_id||ws_co_name||ws_bs_no||ws_location||ws_tel||ws_ceo||ws_fax','coop');">


														<!--input type="hidden" name="wst_co_id" id="wst_co_id" value="<?php
                                                               if (!empty($car_info->wst_co_id)) {
                                                                   echo $car_info->wst_co_id;
                                                               }
                                                               ?>">
                                                            <select name="xxx"
                                                                    class="form-control select_box"
                                                                    style="width: 100%" onChange="setWst(this.value);">
                                                                <?php
                                                                if (!empty($all_inv_group)) {
                                                                    foreach ($all_inv_group as $inv_group) : ?>
                                                                        <option value="<?=$inv_group->dp_id?>||<?=$inv_group->K?>||<?=$inv_group->co_address?>||<?=$inv_group->ceo_hp?>||<?=$inv_group->ceo?>||<?=$inv_group->fax?>"
																		<?php
                                                                        if (!empty($client_info->wst_co_id) && $client_info->wst_co_id == $inv_group->dp_id) {
                                                                            echo 'selected';
                                                                        } ?>
                                                                        ><?= $inv_group->co_name; ?></option>
                                                                    <?php endforeach;
                                                                }
                                                                $created = can_action('125', 'created');
                                                                ?>
                                                            </select>
 															<input type="checkbox" name="ss">  실소유와동일-->
                                                        </div>
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">법인(주민)등록번호</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->Q)) {
                                                                   echo $car_info->Q;
                                                               }
                                                               ?>" name="ws_bs_no" id="ws_bs_no">
													</div>
												</div>
												
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">소재지
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->T)) {
                                                                   echo $car_info->T;
                                                               }
                                                               ?>" name="ws_location" id="ws_location">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">전화번호</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->Y)) {
                                                                   echo $car_info->Y;
                                                               }
                                                               ?>" name="ws_tel" id="ws_tel">
													</div>
												</div>
												
												
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">대표자
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->D)) {
                                                                   echo $car_info->D;
                                                               }
                                                               ?>" name="ws_ceo" id="ws_ceo">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">FAX</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->fax)) {
                                                                   echo $car_info->fax;
                                                               }
                                                               ?>" name="ws_fax" id="ws_fax">
													</div>
												</div>
                                            </div><!-- ************** 위수탁 *************-->
                                            <!-- ************** 제 원  *************-->
                                            <div class="chart tab-pane" id="spec_car">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">길이
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->length)) {
                                                                   echo $car_info->length;
                                                               }
                                                               ?>" name="length">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">너비</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->width)) {
                                                                   echo $car_info->width;
                                                               }
                                                               ?>" name="width">
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">높 이
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->height)) {
                                                                   echo $car_info->height;
                                                               }
                                                               ?>" name="height">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">톤 수</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->carinfo_11)) {
                                                                   echo $car_info->carinfo_11;
                                                               }
                                                               ?>" name="carinfo_11">
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">승차정원
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->max_on)) {
                                                                   echo $car_info->max_on;
                                                               }
                                                               ?>" name="max_on">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">최대적재량</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->max_load)) {
                                                                   echo $car_info->max_load;
                                                               }
                                                               ?>" name="max_load">
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">연료의종류
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <div class="input-group">
														<select name="fuel_type" class="form-control select_box" style="width:100%;">
															<option value="가솔린" <? if(!empty($car_info->fuel_type) && "가솔린"==$car_info->fuel_type) echo " selected"; ?>>가솔린</option>
															<option value="경유" <? if(!empty($car_info->fuel_type) && "경유"==$car_info->fuel_type) echo " selected"; ?>>경유</option>
															<option value="LPG" <? if(!empty($car_info->fuel_type) && "LPG"==$car_info->fuel_type) echo " selected"; ?>>LPG</option>
															<option value="기타" <? if(!empty($car_info->fuel_type) && "기타"==$car_info->fuel_type) echo " selected"; ?>>기타</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">차고지</label>
                                                    <div class="col-lg-4">
                                                        <div class="input-group">
														<select name="carinfo_9" class="form-control select_box" style="width:100%;">
                                                                <?php
                                                                if (!empty($all_garage_group)) {
                                                                    foreach ($all_garage_group as $garage_group) : ?>
                                                                        <option value="<?=$garage_group->idx?>"
																		<?php
                                                                        if (!empty($car_info->carinfo_9) && $car_info->carinfo_9 == $garage_group->idx) {
                                                                            echo 'selected';
                                                                        } ?>
                                                                        >[<?= $garage_group->gr_name; ?>] <?=$garage_group->tel?> <?=$garage_group->ceo?> <?=$garage_group->dp_address?> [총면적: <?=($garage_group->tot_dim)?>]</option>
                                                                    <?php endforeach;
                                                                }
                                                                $created = can_action('125', 'created');
                                                                ?>
                                                            </select>
                                                        </div>
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #1 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach1" id="attach1" value="" class="form-control" style="width:85%;">
<?
if(!empty($car_info->attach1) && $car_info->attach1) {
	echo "<a href='".base_url().'/'.$car_info->attach1."' target='_blank' style='font-weight:bold;color:blue;'>[첨부파일#1]</a>";
}
?>		  
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach1_text" id="attach1_text" value="<?=(!empty($car_info->attach1_text))?$car_info->attach1_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #2 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach2" id="attach2" value="" class="form-control" style="width:85%;">
<?
if(!empty($car_info->attach2) && $car_info->attach2) {
	echo "<a href='".base_url().'/'.$car_info->attach2."' target='_blank' style='font-weight:bold;color:blue;'>[첨부파일#2]</a>";
}
?>		  
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach2_text" id="attach2_text" value="<?=(!empty($car_info->attach2_text))?$car_info->attach2_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #3 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach3" id="attach3" value="" class="form-control" style="width:85%;">
<?
if(!empty($car_info->attach3) && $car_info->attach3) {
	echo "<a href='".base_url().'/'.$car_info->attach3."' target='_blank' style='font-weight:bold;color:blue;'>[첨부파일#3]</a>";
}
?>		  
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach3_text" id="attach3_text" value="<?=(!empty($car_info->attach3_text))?$car_info->attach3_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #4 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach4" id="attach4" value="" class="form-control" style="width:85%;">
<?
if(!empty($car_info->attach4) && $car_info->attach4) {
	echo "<a href='".base_url().'/'.$car_info->attach4."' target='_blank' style='font-weight:bold;color:blue;'>[첨부파일#4]</a>";
}
?>		  
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach4_text" id="attach4_text" value="<?=(!empty($car_info->attach4_text))?$car_info->attach4_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #5 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach5" id="attach5" value="" class="form-control" style="width:85%;">
<?
if(!empty($car_info->attach5) && $car_info->attach5) {
	echo "<a href='".base_url().'/'.$car_info->attach5."' target='_blank' style='font-weight:bold;color:blue;'>[첨부파일#5]</a>";
}
?>		  
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach5_text" id="attach5_text" value="<?=(!empty($car_info->attach5_text))?$car_info->attach5_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
													<label class="col-lg-3"></label>
													<div class="col-lg-1">
														<button type="submit"
																class="btn btn-sm btn-primary">저장</button>
													</div>
													<div class="col-lg-3">
														<!--button type="submit" name="save_and_create_contact" value="1"
																class="btn btn-sm btn-primary"><?= lang('save_and_create_contact') ?></button-->
													</div>
												</div>
                                            </div><!-- ************** 제 원  *************-->
                                        </div>
                                    </div><!-- /.nav-tabs-custom -->
                                    <div class="form-group mt">

                                    </div>
                                </div>
                        </form>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>