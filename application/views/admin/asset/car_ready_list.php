<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
    ?>
<script>
	function popAsWindow(md,tr_id) {
	  window.open('<?php echo base_url(); ?>admin/asset/pop_carinfo/'+md+'/'+tr_id, 'winAS', 'left=50, top=50, width=1480, height=700, scrollbars=1');
	}
	function goSearch(val) {
		if(val == 'all' || val == '전체') {
			document.myform.action = "<?php echo base_url() ?>admin/asset/car_ready_list";
			document.myform.ws_co_id.value = '';
		} else {
			document.myform.action = "<?php echo base_url() ?>admin/asset/car_ready_list";
		}
		document.myform.submit();
	}
</script>
            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
                
    <!-- 검색 시작 -->
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/asset/car_ready_list"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
	<table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
        <tr>
          <td align="left" valign="top" bgcolor="#ffffff">
            <table border="0" width="100%" cellpadding="12" cellspacing="15" bgcolor="#ffffff">
              <tr>
                <td width="12%" align="center" bgcolor="#efefef">위수탁관리사</td>
                <td width="20%" style="padding-left:5px;" align="left" bgcolor="#ffffff;">
                                                        <div class="input-group">
														<input type="hidden" name="ws_co_id" id="sws_co_id" value="<?php
                                                               if (!empty($ws_co_id)) {
                                                                   echo $ws_co_id;
                                                               }
                                                               ?>">
															<input type="text" name="ws_co_name" id="sws_co_name" value="<?=(empty($ws_co_name))?"":$ws_co_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('sws_co_id||sws_co_name||||||||||','coop');" onChange="goSearch(this.value)">
                                                        </div>


					<!--select name="ws_co_id" id="ws_co_id" style="width:100%;background-color:yellow;" class="form-control input-sm" onChange="goSearch(this.value)">
							<option value="all" <?=($ws_co_id=="all")?"selected":""?>>전체</option>
							<option value="1413" <?=($ws_co_id=="1413")?"selected":""?>>케이티지엘에스(주) </option>
							<option value="1414" <?=($ws_co_id=="1414")?"selected":""?>>(주)아이디일일구닷컴</option>
							<option value="1415" <?=($ws_co_id=="1415")?"selected":""?>>(주)델타온</option>
							<option value="1903" <?=($ws_co_id=="1903")?"selected":""?>>(주)휴먼엘지</option>
							<option value="etc" <?=($ws_co_id=="etc")?"selected":""?>>기타</option>
					</select-->

					<!--a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/select_reqco"><input type="hidden" name="ws_co_id" id="ws_co_id" value="">
					<input type="text" name="ws_co_name" id="ws_co_name" value="" class="form-control" style="width:100%;background-color:yellow;"></a-->

                </td>
                <td>
					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables" href="javascript:goSearch(document.myform.ws_co_name.value);">
					<span><i class="fa fa-search"> </i> 검색</span>
					</a>

                </td>
			</tr>
            </table>
		  </td>
        </tr>
      </table>
		</form>
      <!-- 검색 끝 -->

				

            </div>





<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip"
             data-title="<?php echo lang('filter_by'); ?>">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-filter" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-left"
                style="width:300px;<?php if (!empty($search_by) && $search_by == 'group') {
                    echo 'display:block';
                } ?>">
                <li class="<?php
                if (empty($search_by)) {
                    echo 'active';
                } ?>"><a
                        href="<?= base_url() ?>admin/client/manage_client"><?php echo lang('all'); ?></a>
                </li>
                <li class="divider"></li>
                <?php if (count($all_customer_group) > 0) { ?>
                    <li class="dropdown-submenu pull-left groups <?php if (!empty($id)) {
                        if ($search_by == 'group') {
                            echo 'active';
                        }
                    } ?>">
                        <a href="#" tabindex="-1"><?php echo lang('customer_group'); ?></a>
                        <ul class="dropdown-menu dropdown-menu-left"
                            style="<?php if (!empty($search_by) && $search_by == 'group') {
                                echo 'display:block';
                            } ?>">
                            <?php foreach ($all_customer_group as $group) {
                                ?>
                                <li class="<?php if (!empty($id)) {
                                    if ($search_by == 'group') {
                                        if ($id == $group->customer_group_id) {
                                            echo 'active';
                                        }
                                    }
                                } ?>">
                                    <a href="<?= base_url() ?>admin/client/manage_client/group/<?php echo $group->customer_group_id; ?>"><?php echo $group->customer_group; ?></a>
                                </li>
                            <?php }
                            ?>
                        </ul>
                    </li>
                    <div class="clearfix"></div>
                    <li class="divider"></li>
                <?php } ?>
            </ul>
        </div>
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#car_list"
                                                                   data-toggle="tab">차량목록</a></li>


                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_car"
                                                                   data-toggle="tab">공T/E등록</a></li>

            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="car_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong>차량목록</strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">


                            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
								<tr align="center" bgcolor="#e0e7ef" style="text-align:center;vertical-align:center;">
								  <th width="60" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">NO</th>
								  <th style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">위수탁관리사</th>
								  <th style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차량등록번호</th>
								  <th style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">년식</th>
								  <th style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">용도</th>
								  <th style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공T/E등록일</th>
								  <th style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공T/E재등록마감일</th>
								  <th style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">최종차주</th>
								  <th style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">정보등록</th>
								</tr>
                                </thead>

                                <tbody>
                               <?php
							   $cnt = 0;
                                if (!empty($all_car_info)) {
                                    foreach ($all_car_info as $car_details) {
										// 차주정보
										$owner = $this->db->where('tr_id', $car_details->idx)->get('tbl_members')->row();
										$no = $total_cnt - $cnt;
										$cnt++;
                                        ?>
                                    <tr>
                                        <td><?= $no ?></td>
                                        <td><?php if(!empty($car_details->ws_co_name)) echo $car_details->ws_co_name; ?></td>
                                        <td>
											<span class='label label-primary'><?= $car_details->car_1 ?></span>
											<a href="<?php echo base_url() ?>admin/asset/car_ready_list/add/<?=$car_details->idx?>"><span class='label label-success'>신규등록</span></a>
										</td>
                                        <td><?= $car_details->car_5 ?></td>
                                        <td><?= $car_details->car_3 ?></td>
                                        <td><?php if(!empty($car_details->gongT_date)) echo $car_details->gongT_date; ?></td>
                                        <td><?php if(!empty($car_details->return_cls_date)) echo $car_details->return_cls_date; ?></td>
                                        <td><?php if(!empty($car_details->ceo)) echo $car_details->ceo; ?></td>
                                        <td>
                                            <?php echo btn_edit('admin/asset/car_ready_list/edit_asset/' . $car_details->idx) ?>
										</td>
                                    </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php 
								}
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { ?>
                        <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_car"
                        style="position: relative;">
                        <form role="form" enctype="multipart/form-data" id="form" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/asset/save_GTcar/" method="post" class="form-horizontal  ">
						<input type="hidden" name="pidx" value="<?php if(!empty($car_info->idx)) echo $car_info->idx; ?>">

                            <div class="panel-body">
                                <label class="control-label col-sm-1"></label>
                                <div class="col-sm-10">
                                    <div class="nav-tabs-custom">
                                        <!-- Tabs within a box -->
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#general_car" data-toggle="tab">기초정보</a></li>
                                            <li><a href="#wst_compnay" data-toggle="tab">위수탁관리사</a></li>
                                            <li><a href="#spec_car" data-toggle="tab">제원</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content bg-white">
                                            <!-- ************** 기본정보 *************-->
                                            <div class="chart tab-pane active" id="general_car">

                                                <div class="form-group">
													<label class="col-lg-2 control-label">소속</label>
                                                    <div class="col-lg-4">
														<input type="hidden" name="gr_id" id="gr_id" value="<?php
                                                               if (!empty($car_info->gr_id)) {
                                                                   echo $car_info->gr_id;
																   $gr = $this->db->where('dp_id', $car_info->gr_id)->get('tbl_member')->row();
																   if(!empty($gr->co_name)) $car_info->gr_name = $gr->co_name;
                                                               }
                                                               ?>">
															<input type="text" name="gr_name" id="gr_name" value="<?=(empty($car_info->gr_name))?"":$car_info->gr_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('gr_id||gr_name||||||||||','group');">
													</div>

													<label class="col-lg-2 control-label"></label>
                                                    <div class="col-lg-4">
                                                    </div>
                                                </div>
                                                <div class="form-group">
													<label class="col-lg-2 control-label">자동차 등록번호<span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->car_1)) {
                                                                   echo $car_info->car_1;
                                                               }
                                                               ?>" name="car_1">
                                                    </div>

													<label class="col-lg-2 control-label">차 종<span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="" name="type">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">용 도
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <div class="input-group">
														<select name="car_3" class="form-control select_box">
															<option value='일반영업용' >일반영업용</option>
															<option value='냉동영업용' >냉동영업용</option>
															<option value='냉동용달' >냉동용달</option>
															<option value='일반용달' >일반용달</option>
															<option value='살수영업용' >살수영업용</option>
															<option value='개별화물' >개별화물</option>
															<option value='개별용달' >개별용달</option>
															<option value='자가용' >자가용</option>
														</select>
														</div>
                                                    </div>
                                                    <label class="col-lg-2 control-label">차 명
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="" name="car_4">
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">형식
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="" name="mode">
                                                    </div>
                                                    <label class="col-lg-2 control-label">연 식
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="" name="car_5">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">차대번호
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="" name="car_7">
                                                    </div>
                                                    <label class="col-lg-2 control-label">원동기 형식
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="" name="motor_mode">

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">사용본거지
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="" name="headquarter">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label"></label>
                                                    <div class="col-lg-4">
													</div>
												</div>
                                                <?//= custom_form_Fields(12, $tr_id); ?>
                                            </div><!-- ************** 기본정보 *************-->
<script>
	function selectPartner(params,stype) {
	  if(stype == '') stype = 'partner';
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/'+stype+'/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
</script>

                                            <!-- ************** 위수탁 *************-->
                                            <div class="chart tab-pane" id="wst_compnay">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">상 호
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                   <div class="col-lg-4">
                                                        <div class="input-group">
														<input type="hidden" name="ws_co_id" id="ws_co_id" value="">
															<input type="text" name="ws_co_name" id="ws_co_name" value="" class="form-control" style="background-color:yellow;" onClick="selectPartner('ws_co_id||ws_co_name||ws_bs_no||ws_location||ws_tel||ws_ceo||ws_fax','coop');">

                                                        </div>
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">법인(주민)등록번호</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="" name="ws_bs_no" id="ws_bs_no">
													</div>
												</div>
												
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">소재지
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="" name="ws_location" id="ws_location">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">전화번호</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="" name="ws_tel" id="ws_tel">
													</div>
												</div>
												
												
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">대표자
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="" name="ws_ceo" id="ws_ceo">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">FAX</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="" name="ws_fax" id="ws_fax">
													</div>
												</div>
                                            </div><!-- ************** 위수탁 *************-->
                                            <!-- ************** 제 원  *************-->
                                            <div class="chart tab-pane" id="spec_car">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">길이
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="" name="length">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">너비</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="" name="width">
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">높 이
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="" name="height">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">톤 수</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="" name="carinfo_11">
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">승차정원
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="" name="max_on">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">최대적재량</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="" name="max_load">
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">연료의종류
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <div class="input-group">
														<select name="fuel_type" class="form-control select_box" style="width:100%;">
															<option value="가솔린" >가솔린</option>
															<option value="경유" >경유</option>
															<option value="LPG" >LPG</option>
															<option value="기타" >기타</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">차고지</label>
                                                    <div class="col-lg-4">
                                                        <div class="input-group">
														<select name="carinfo_9" class="form-control select_box" style="width:100%;">
                                                                <?php
                                                                if (!empty($all_garage_group)) {
                                                                    foreach ($all_garage_group as $garage_group) : ?>
                                                                        <option value="<?=$garage_group->idx?>">[<?= $garage_group->gr_name; ?>] <?=$garage_group->tel?> <?=$garage_group->ceo?> <?=$garage_group->dp_address?> [총면적: <?=($garage_group->tot_dim)?>]</option>
                                                                    <?php endforeach;
                                                                }
                                                                $created = can_action('125', 'created');
                                                                ?>
                                                            </select>
                                                        </div>
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #1 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach1" id="attach1" value="" class="form-control" style="width:85%;">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach1_text" id="attach1_text" value="<?=(!empty($car_info->attach1_text))?$car_info->attach1_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #2 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach2" id="attach2" value="" class="form-control" style="width:85%;">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach2_text" id="attach2_text" value="<?=(!empty($car_info->attach2_text))?$car_info->attach2_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #3 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach3" id="attach3" value="" class="form-control" style="width:85%;">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach3_text" id="attach3_text" value="<?=(!empty($car_info->attach3_text))?$car_info->attach3_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #4 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach4" id="attach4" value="" class="form-control" style="width:85%;">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach4_text" id="attach4_text" value="<?=(!empty($car_info->attach4_text))?$car_info->attach4_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #5 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach5" id="attach5" value="" class="form-control" style="width:85%;">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach5_text" id="attach5_text" value="<?=(!empty($car_info->attach5_text))?$car_info->attach5_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
													<label class="col-lg-3"></label>
													<div class="col-lg-1">
														<button type="submit"
																class="btn btn-sm btn-primary">저장</button>
													</div>
													<div class="col-lg-3">
														<!--button type="submit" name="save_and_create_contact" value="1"
																class="btn btn-sm btn-primary"><?= lang('save_and_create_contact') ?></button-->
													</div>
												</div>
                                            </div><!-- ************** 제 원  *************-->
                                        </div>
                                    </div><!-- /.nav-tabs-custom -->
                                    <div class="form-group mt">

                                    </div>
                                </div>
                        </form>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>