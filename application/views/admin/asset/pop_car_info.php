<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php 
$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');

?>
<div class="row">
    <div class="col-sm-12">
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#car_list"
                                                                   data-toggle="tab">정보변경이력</a></li>



                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_car"
                                                                   data-toggle="tab">정보변경등록</a></li>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="car_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong>차량목록</strong></div>
                        </header>
                        <?php } ?>
                        <div class="box"> 
       <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table table-striped DataTables">
        <tr align="center" bgcolor="#e0e7ef" style="text-align:center;">
          <td style="color:#ffffff;background-color: #777777;" width="40">No</td>
          <td style="color:#ffffff;background-color: #777777;" width="80">등록일</td>
          <td style="color:#ffffff;background-color: #777777;">차량번호</td>
          <td style="color:#ffffff;background-color: #777777;">차대번호</td>
          <td style="color:#ffffff;background-color: #777777;">배차장소</td>
          <td style="color:#ffffff;background-color: #777777;">위ㆍ수탁관리회사</td>
          <td style="color:#ffffff;background-color: #777777;">차주명</td>

          <td style="color:#ffffff;background-color: #777777;">변경사유</td>
          <td style="color:#ffffff;background-color: #777777;">비고</td>
          <!--td style="color:#ffffff;background-color: #777777;"></td-->
		</tr>
<?php
	$i = 0;
	if (!empty($all_info_group)) {
		foreach ($all_info_group as $info_details) {
		$sn_bg = "#ffffff";
?>
        <tr bgcolor="<?=$sn_bg?>">
          <td height="25" align="center"><?=($i+1)?></td>
          <td align="center" onClick="viewDetailInfo('<?php if (!empty($info_details->idx)) echo  $info_details->idx; ?>');"><span class='label label-default'><?php if(!empty($info_details->reg_datetime)) echo substr($info_details->reg_datetime,0,10); ?></span></td>
          <td align="center" onClick="viewDetailInfo('<?php if (!empty($info_details->idx)) echo  $info_details->idx; ?>');"><span class='label label-info'><?php if(!empty($info_details->car_1)) echo  $info_details->car_1 ?></span></td>
          <td align="center" onClick="viewDetailInfo('<?php if (!empty($info_details->idx)) echo  $info_details->idx; ?>');"><span class='label label-info'><?php if(!empty($info_details->car_7)) echo  $info_details->car_7 ?></span></td>
          <td align="center"><?//= $baechaid ?></td>
          <td align="center"><?php if(!empty($info_details->idx)) echo $info_details->ws_co_name; ?></td>
          <td align="center"><?php if(!empty($info_details->idx)) echo $info_details->inv_co_name; ?></td>
          <td align="center"><span class='label label-warning'><?php if(!empty($info_details->reason)) echo $info_details->reason; ?></span></td>
          <td align="center"><?php if(!empty($info_details->remark)) echo $info_details->remark; ?></td>
          <!--td align="center"> 
		  <a title="" class="btn btn-primary btn-xs" href="http://intra.delta-on.com/admin/basic/manage_customer/5" data-original-title="Edit" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i></a>

			<a title="" class="btn btn-danger btn-xs" onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');" href="http://intra.delta-on.com/admin/basic/delete_customer/5" data-original-title="Delete" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash-o"></i></a>

		  </td-->
		</tr>
<?php
			$i++;
		}
	} else {
?>
		<tr>
			<td colspan="9">
			<?php if (!empty($info_details->idx)) echo  lang('no_data') ?>
			</td>
		</tr>
<?php 
	}
?>
		</table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { 
					
										//차량정보
						if(!empty($tr_id) ) {
							$last_info = $this->db->where('idx', $tr_id)->get('tbl_asset_truck')->row();
						}

						//차주정보
						if(!empty($last_info->idx) ) {
							$owner_info = $this->db->where('tr_id', $last_info->idx)->get('tbl_members')->row();
							if(!empty($owner_info->ceo)) $owner_name = $owner_info->ceo;
							if(!empty($owner_info->driver)) $driver_name = $owner_info->driver;
						}
					
					?>
                        <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_car"
                        style="position: relative;">
                        <form role="form" enctype="multipart/form-data" id="form" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/asset/save_car/<?php
                              if (!empty($last_info->idx)) {
                                  echo $last_info->idx;
                              }
                              ?>" method="post" class="form-horizontal  ">

                            <div class="panel-body">
                                <label class="control-label col-sm-1"></label>
                                <div class="col-sm-10">
                                    <div class="nav-tabs-custom">



	  <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#e4e4e4" align="center" class="table table-striped DataTables">
        <tr>
          <td height="20" colspan="2"  style="color:#ffffff;background-color: #777777;text-align:center;">반납예정등록</td>
          <td colspan="3" style="color:#000000;background-color: #ffffff;padding-left:5px;">
		  
                                <input name="return_ready" value="1" <?php
                                if (!empty($last_info->return_ready) && $last_info->return_ready == "Y") {
                                    echo 'checked';
                                }
                                ?> type="checkbox" onClick="setGongT(this.checked);"> 등록
		  
		  </td>
        </tr>
        <tr>
          <td colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">반납예정등록일</td>
          <td style="color:#000000;padding-left:5px;">
														<div class="input-group">
															<input type="text" class="form-control datepicker" name="return_due_date" value="<?php if (!empty($last_info->return_due_date)) echo $last_info->return_due_date;?>">
															<div class="input-group-addon">
																<a href="#"><i class="fa fa-calendar"></i></a>
															</div>
														</div>
		  </td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">반납재등록마감일</td>
          <td style="color:#000000;padding-left:5px;">
														<div class="input-group">
															<input type="text" class="form-control datepicker" name="return_cls_date" value="<?php if (!empty($last_info->return_cls_date)) echo $last_info->return_cls_date;?>">
															<div class="input-group-addon">
																<a href="#"><i class="fa fa-calendar"></i></a>
															</div>
														</div>
		  </td>
        </tr>
        <tr>
          <td width="25%" height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">구   분</td>
          <td width="33%" style="color:#ffffff;background-color: #777777;text-align:center;">변경 전 정보</td>
          <td width="42%" colspan="2"  style="color:#ffffff;background-color: #777777;text-align:center;">변경 후 정보</td>
        </tr>
        <tr>
          <td height="20" colspan="2"  style="color:#ffffff;background-color: #777777;text-align:center;">차량번호</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->car_1)) echo $last_info->car_1;?></td>
          <td width="12%"  style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="hidden" name="h_car_1" id="h_car_1" value="<?php if (!empty($last_info->car_1)) echo  $last_info->car_1;?>">
			<input type="checkbox" value="1" name="keep_car_1" onClick="setCarInfo('car_1',this.checked);">유지
		  </td>
		  <td width="30%" >
			<input type="text" name="car_1" id="car_1" value="<?php if (!empty($car_info->car_1)) echo $car_info->car_1;?>" class="form-control" style="width:90%;">
		  </td>
        </tr>

        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">차 종</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->type)) echo $last_info->type;?></td>
          <td style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="hidden" name="h_car_type" id="h_car_type" value="<?php if (!empty($last_info->type)) echo  $last_info->type;?>">
			<input type="checkbox" value="1" name="keep_type" onClick="setCarInfo('car_type',this.checked);">유지
		  </td>
		  <td>
			<input type="text" name="car_type" id="car_type" value="<?php if (!empty($car_info->idx)) echo $car_info->type;?>" class="form-control" style="width:90%;">
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">용 도</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->car_3)) echo $last_info->car_3;?></td>
          <td style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="hidden" name="h_car_3" id="h_car_3" value="<?php if (!empty($last_info->car_3)) echo  $last_info->car_3;?>">
			<input type="checkbox" value="1" name="keep_car_3" onClick="setCarInfo('car_3',this.checked);">유지
		  </td>
		  <td>
            <select name="car_3" id="car_3" style="width:90%;" class="form-control">
				<option value=''>선택</option>
				<option value='영업용' <?=(!empty($car_info->car_3) && "영업용" == $car_info->car_3)?"selected":""?>>영업용</option>
				<option value='일반영업용' <?=(!empty($car_info->car_3) && "일반영업용" == $car_info->car_3)?"selected":""?>>일반영업용</option>
				<option value='용달영업용' <?=(!empty($car_info->car_3) && "용달영업용" == $car_info->car_3)?"selected":""?>>용달영업용</option>
				<option value='개별영업용' <?=(!empty($car_info->car_3) && "개별영업용" == $car_info->car_3)?"selected":""?>>개별영업용</option>
				<option value='자가용' <?=(!empty($car_info->car_3) && "자가용" == $car_info->car_3)?"selected":""?>>자가용</option>
			</select>
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">차 명</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->car_4)) echo $last_info->car_4;?></td>
          <td style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="hidden" name="h_car_4" id="h_car_4" value="<?php if (!empty($last_info->car_4)) echo  $last_info->car_4;?>">
			<input type="checkbox" value="1" name="keep_car_4" onClick="setCarInfo('car_4',this.checked);">유지
		  </td>
		  <td>
			<input type="text" name="car_4" id="car_4" value="<?php if (!empty($car_info->car_4)) echo $car_info->car_4;?>" class="form-control" style="width:90%;">
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">형식및연식</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->mode)) echo $last_info->mode;?></td>
          <td style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="hidden" name="h_mode" id="h_car_mode" value="<?php if (!empty($last_info->mode)) echo  $last_info->mode;?>">
			<input type="checkbox" value="1" name="keep_mode" onClick="setCarInfo('car_mode',this.checked);">유지
		  </td>
		  <td>
			<input type="text" name="car_mode" id="car_mode" value="<?php if (!empty($car_info->idx)) echo $car_info->mode;?>" class="form-control" style="width:90%;">
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">연 식</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->car_5)) echo $last_info->car_5;?></td>
          <td style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="hidden" name="h_car_5" id="h_car_5" value="<?php if (!empty($last_info->car_5)) echo  $last_info->car_5;?>">
			<input type="checkbox" value="1" name="keep_car_5" onClick="setCarInfo('car_5',this.checked);">유지
		  </td>
		  <td>
			<input type="text" name="car_5" id="car_5" value="<?php if (!empty($car_info->idx)) echo $car_info->car_5;?>" class="form-control" style="width:90%;">
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">차대번호</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->car_7)) echo $last_info->car_7?></td>
          <td style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="hidden" name="h_car_7" id="h_car_7" value="<?php if (!empty($last_info->car_7)) echo  $last_info->car_7?>">
			<input type="checkbox" value="1" name="keep_car_7" onClick="setCarInfo('car_7',this.checked);">유지
		  </td>
		  <td>
			<input type="text" name="car_7" id="car_7" value="<?php if (!empty($car_info->idx)) echo $car_info->car_7?>" class="form-control" style="width:90%;">
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">원동기형식</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if(!empty($last_info->motor_mode)) echo $last_info->motor_mode;?></td>
          <td style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="hidden" name="h_motor_mode" id="h_motor_mode" value="<?php if (!empty($last_info->motor_mode)) echo  $last_info->motor_mode;?>">
			<input type="checkbox" value="1" name="keep_motor_mode" onClick="setCarInfo('motor_mode',this.checked);">유지
		  </td>
		  <td>
			<input type="text" name="motor_mode" id="motor_mode" value="<?php if (!empty($car_info->idx)) echo $car_info->motor_mode;?>" class="form-control" style="width:90%;">
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">사용본거지</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->headquarter)) echo $last_info->headquarter;?></td>
          <td style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="hidden" name="h_headquarter" id="h_headquarter" value="<?php if (!empty($last_info->headquarter)) echo  $last_info->headquarter;?>">
			<input type="checkbox" value="1" name="keep_headquarter" onClick="setCarInfo('headquarter',this.checked);">유지
		  </td>
		  <td>
			<input type="text" name="headquarter" id="headquarter" value="<?php if (!empty($car_info->idx)) echo $car_info->headquarter;?>" class="form-control" style="width:90%;">
		  </td>
        </tr>



        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">길이 × 너비</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->length)) echo $last_info->length;?> × <?php if (!empty($last_info->width)) echo $last_info->width;?></td>
          <td style="color:#000000;background-color: yellow;padding-left:5px;" rowspan="5">
			<input type="hidden" name="h_length" id="h_length" value="<?php if (!empty($last_info->length)) echo  $last_info->length;?>">
			<input type="hidden" name="h_width" id="h_width" value="<?php if (!empty($last_info->width)) echo  $last_info->width;?>">
			<input type="hidden" name="h_height" id="h_height" value="<?php if (!empty($last_info->height)) echo  $last_info->height;?>">
			<input type="hidden" name="h_carinfo_11" id="h_carinfo_11" value="<?php if (!empty($last_info->carinfo_11)) echo  $last_info->carinfo_11;?>">
			<input type="hidden" name="h_max_load" id="h_max_load" value="<?php if (!empty($last_info->max_load)) echo  $last_info->max_load;?>">
			<input type="hidden" name="h_fuel_type" id="h_fuel_type" value="<?php if (!empty($last_info->fuel_type)) echo  $last_info->fuel_type;?>">
			<input type="checkbox" value="1" name="keep_carinfo" onClick="setCarInfo('carinfo',this.checked);">유지
		  </td>
		  <td>
                                                <div class="form-group">
                                                    <div class="col-lg-4">
			<input type="text" name="length" id="length" value="<?php if (!empty($car_info->idx)) echo $car_info->length;?>" class="form-control" style="width:70px;"> 
                                                    </div>
                                                    <div class="col-lg-1">
			x 
                                                    </div>
                                                    <div class="col-lg-4">
			<input type="text" name="width" id="width" value="<?php if (!empty($car_info->idx)) echo $car_info->width;?>" class="form-control" style="width:70px;">
                                                    </div>
                                                </div>
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">높이</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->height)) echo $last_info->height;?></td>
		  <td>
			<input type="text" name="height" id="height" value="<?php if (!empty($car_info->height)) echo $car_info->height;?>" class="form-control" style="width:90%;">
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">톤수</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->carinfo_11)) echo $last_info->carinfo_11;?></td>
		  <td>
			<input type="text" name="carinfo_11" id="carinfo_11" value="<?php if (!empty($car_info->carinfo_11)) echo $car_info->carinfo_11;?>" class="form-control" style="width:90%;">
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">최대적재량</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->max_load)) echo $last_info->max_load;?></td>
		  <td>
			<input type="text" name="max_load" id="max_load" value="<?php if (!empty($car_info->max_load)) echo $car_info->max_load;?>" class="form-control" style="width:90%;">
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">연료의 종류</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->fuel_type)) echo $last_info->fuel_type;?></td>
		  <td>
			<select name="fuel_type" id="fuel_type" style="width:95%" class="form-control"> 
				<option value="가솔린">가솔린</option>
				<option value="경유">경유</option>
				<option value="LPG">LPG</option>
				<option value="기타">기타</option>
			</select>
		  </td>
        </tr>

        <tr>
          <td width="8%" rowspan="5" style="color:#ffffff;background-color: #777777;text-align:center;">현물<br/>출자자</td>
          <td width="17%" style="color:#ffffff;background-color: #777777;text-align:center;">상호</td>
          <td height="20" style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($owner_info->co_name)) echo $owner_info->co_name;?></td>
          <td rowspan="5" style="color:#000000;background-color: #ddd;padding-left:5px;">
		  </td>
          <td rowspan="5" style="color:#000000;background-color: #ddd;padding-left:5px;">
		  </td>
        </tr>
        <tr>
          <td height="20" style="color:#ffffff;background-color: #777777;text-align:center;">대표</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($owner_info->ceo)) echo $owner_info->ceo;?></td>
        </tr>
        <tr>
          <td height="20" style="color:#ffffff;background-color: #777777;text-align:center;">주민등록번호</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($owner_info->reg_number)) echo $owner_info->reg_number;?></td>
        </tr>
        <tr>
          <td height="20" style="color:#ffffff;background-color: #777777;text-align:center;">사업자번호</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($owner_info->bs_number)) echo $owner_info->bs_number;?></td>
        </tr>
        <tr>
          <td height="20" style="color:#ffffff;background-color: #777777;text-align:center;">전화번호</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($owner_info->co_tel)) echo $owner_info->co_tel;?></td>
        </tr>


        <tr>
          <td rowspan="4" style="color:#ffffff;background-color: #777777;text-align:center;">위수탁<br/>관리사</td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">상호</td>
          <td height="20" style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->ws_co_name)) echo $last_info->ws_co_name;?></td>
          <td rowspan="4" style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="hidden" name="h_ws_co_name" id="h_ws_co_name" value="<?php if (!empty($last_info->ws_co_name)) echo  $last_info->ws_co_name;?>">
			<input type="hidden" name="h_ws_co_id" id="h_ws_co_id" value="<?php if (!empty($last_info->ws_co_id)) echo  $last_info->ws_co_id;?>">
			<input type="hidden" name="ws_location" id="ws_location" value="<?php if (!empty($last_info->ws_location)) echo  $last_info->ws_location;?>">
			<input type="hidden" name="h_ws_ceo" id="h_ws_ceo" value="<?php if (!empty($last_info->ws_ceo)) echo  $last_info->ws_ceo;?>">
			<input type="hidden" name="h_ws_bs_no" id="h_ws_bs_no" value="<?php if (!empty($last_info->ws_bs_no)) echo  $last_info->ws_bs_no;?>">
			<input type="hidden" name="h_ws_tel" id="h_ws_tel" value="<?php if (!empty($last_info->ws_tel)) echo  $last_info->ws_tel;?>">
			<input type="checkbox" value="1" name="keep_ws_co_id" onClick="setCarInfo('ws_co_info',this.checked);">유지
		  </td>
		  <td>
			<input type="hidden" name="ws_co_id" id="ws_co_id" value="<?php if (!empty($last_info->ws_co_id)) echo $last_info->ws_co_id;?>">
			<input type="text" name="ws_co_name" id="ws_co_name" value="<?php if (!empty($car_info->idx)) echo $car_info->ws_co_name;?>" class="form-control" style="width:90%;background-color:yellow;" onClick="selectCompany('group','ws_co_id||ws_co_name||ws_bs_no||__||ws_tel||ws_ceo||__');">
		  </td>
        </tr>
        <tr>
          <td height="20" style="color:#ffffff;background-color: #777777;text-align:center;">법인(주민)번호</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->ws_bs_no)) echo $last_info->ws_bs_no;?></td>
		  <td><input type="text" name="ws_bs_no" id="ws_bs_no" value="<?php if (!empty($car_info->idx)) echo $$car_info->Q?>" class="form-control" style="width:90%;"></td>
        </tr>
        <tr>
          <td height="20" style="color:#ffffff;background-color: #777777;text-align:center;">대표</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->ws_ceo)) echo $last_info->ws_ceo;?></td>
		  <td><input type="text" name="ws_ceo" id="ws_ceo" value="<?php if (!empty($car_info->idx)) echo $$car_info->D?>" class="form-control" style="width:90%;"></td>
        </tr>
        <tr>
          <td height="20" style="color:#ffffff;background-color: #777777;text-align:center;">전화번호</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->idx)) echo $last_info->ws_tel;?></td>
		  <td><input type="text" name="ws_tel" id="ws_tel" value="<?php if (!empty($car_info->idx)) echo $$car_info->Y?>" class="form-control" style="width:90%;"></td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">배차장소</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;"><?php if (!empty($last_info->baecha_co_name)) echo $last_info->baecha_co_name;?></td>
          <td style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="hidden" name="h_baecha_co_id" id="h_baecha_co_id" value="<?php if (!empty($last_info->baecha_co)) echo  $last_info->baecha_co;?>">
			<input type="hidden" name="h_baecha_co_name" id="h_baecha_co_name" value="<?php if (!empty($last_info->baecha_co_name)) echo  $last_info->baecha_co_name;?>">
			<input type="checkbox" value="1" name="keep_baecha_co_id" onClick="setCarInfoBaecha(this.checked);">유지
		  </td>
          <td style="padding-left:5px;" >
			<input type="hidden" name="code" class="form-control" id="code" value="<?php if (!empty($last_info->baecha_co_id)) echo  $last_info->baecha_co_id;?>">
			<select name="baecha_co_id" id="baecha_co_id" class="form-control" style="color:#000000;background-color: #ffffff;width:90%;">
				<option value="">전체</option>
            <?php
			//배차지 정보
			$this->asset_model->_table_name = 'tbl_members'; //table name
			$this->asset_model->db->where('tbl_members.mb_type', 'customer');
			$this->asset_model->_order_by = 'co_name';
			$all_baecha_group = $this->asset_model->get();
		        if (!empty($all_baecha_group)) {
                    foreach ($all_baecha_group as $baecha_info) { ?>
				<option value="<?php if(!empty($baecha_info->code)) echo $baecha_info->code;?>" <? if(!empty($baecha_info->code) && !empty($last_info->baecha_co_id) && $baecha_info->code==$last_info->baecha_co_id) echo " selected"; ?>><?php if(!empty($baecha_info->co_name)) echo $baecha_info->co_name;?></option>
			<?php
					}
				}
			?>
			</select>
        </tr>
        <tr>
          <td colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">차고지</td>
			<?
			//차고지 변경전 = $gara
			$gara = $this->db->where('idx', $last_info->carinfo_9)->get('tbl_garage')->row();
			if(!empty($gara->idx)) $us = $this->db->select('sum(carinfo_8) as sum')->where('carinfo_9', $gara->idx)->get('tbl_asset_truck')->row();
			//if(!empty($us->sum)) $us->sum = 0;
			?>
		  <td height="20" style="color:#000000;background-color: #ffffff;padding-left:5px;">
			<select name="carinfo_9" id="carinfo_9" style="width:90%" class="form-control" onChange="set_garage();"> 
			<?if($last_info->carinfo_9) {
			?>
				<option value="<?php if(!empty($garage_details->idx)) echo $gara->idx;?>||<?php if(!empty($garage_details->tot_dim)) echo $gara->tot_dim;?>||<?php echo $us->sum;?>" <?php if(!empty($last_info->carinfo_9) && $gara->idx==$last_info->carinfo_9) echo " selected"; ?>>[<?php if(!empty($garage_details->gr_name)) echo $gara->gr_name;?>] <?php if(!empty($garage_details->tel)) echo $gara->tel;?> <?php if(!empty($garage_details->ceo)) echo $gara->ceo;?> <?php if(!empty($garage_details->dp_address)) echo $gara->dp_address;?> [총면적: <?php if(!empty($garage_details->tot_dim)) echo $gara->tot_dim;?>]</option>
			<?}else{?>
				<option value="">자료가 없습니다.</option>
			<?}?>
			</select>		  
		  </td>
          <td style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="hidden" name="h_carinfo_9" id="h_carinfo_9" value="<?php if (!empty($last_info->carinfo_9)) echo  $last_info->carinfo_9;?>">
			<input type="checkbox" value="1" name="keep_carinfo_9" onClick="setCarInfo('carinfo_9',this.checked);">유지
		  </td>
		  <td>
			<select name="carinfo_9" id="carinfo_9" style="width:90%" class="form-control" onChange="set_garage();"> 
				<option value="">차고치선택</option>
            <?php


			//차고지 정보-> 향후 정리
			$this->asset_model->_table_name = 'tbl_garage'; //table name
			$this->asset_model->_order_by = 'gr_name';
			$all_garage_group = $this->asset_model->get();
		        if (!empty($all_garage_group)) {
                    foreach ($all_garage_group as $garage_details) { 
						$us = $this->db->select('sum(carinfo_8) as sum')->where('carinfo_9', $garage_details->idx)->get('tbl_asset_truck')->row();
						if(empty($us->sum)) $us->sum = 0;
			?>
				<option value="<?php if(!empty($garage_details->idx)) echo $garage_details->idx;?>||<?php if(!empty($garage_details->tot_dim)) echo $garage_details->tot_dim;?>||<?=$us->sum?>" <? if(!empty($last_info->carinfo_9) && $garage_details->idx==$last_info->carinfo_9) echo " selected"; ?>>[<?php if(!empty($garage_details->gr_name)) echo $garage_details->gr_name;?>] <?php if(!empty($garage_details->tel)) echo $garage_details->tel;?> <?php if(!empty($garage_details->ceo)) echo $garage_details->ceo;?> <?php if(!empty($garage_details->dp_address)) echo $garage_details->dp_address;?> [총면적: <?php if(!empty($garage_details->tot_dim)) echo $garage_details->tot_dim;?>]</option>
			<?php
					}
				}
			?>
			</select>
		  </td>
        </tr>


        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">차량등록증</td>
          <td style="color:#000000;background-color: #ffffff;padding-left:5px;">
			<?
				if(!empty($master->cert_file) && $master->cert_file) {
					echo "<a href='/data/truck/".$master->cert_file."' target='_blank' style='font-weight:bold;color:blue;'>[차량등록증보기]</a>";
				}else{
					echo "파일이없습니다.";
				}
			?>	
		  </td>
		  <td colspan="2" style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="file" name="cert_file" id="cert_file" value="" class="form-control" style="width:250px;">
			<? if(!empty($master->cert_file) && $master->cert_file) { ?><input type="button" value="열람" class="form-control" style="width:80px;" onClick="viewCertFile('<?php if (!empty($master->cert_file)) echo $master->cert_file;?>');"><? } ?>
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">변경사유</td>
          <td colspan="3" style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="text" name="reason" id="reason" value="<?php if (!empty($car_info->idx)) echo $car_info->reason;?>" class="form-control" style="width:96%;">
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">비      고</td>
          <td colspan="3" style="color:#000000;background-color: yellow;padding-left:5px;">
			<input type="text" name="remark" id="remark" value="<?php if (!empty($master->remark)) echo $master->remark;?>" class="form-control" style="width:96%;">
		  </td>
        </tr>
        <tr>
          <td height="20" colspan="2" style="color:#ffffff;background-color: #777777;text-align:center;">실변경일</td>
          <td colspan="3" style="color:#000000;background-color: yellow;padding-left:5px;">
														<div class="input-group">
															<input type="text" class="form-control datepicker" name="change_date" value="">
															<div class="input-group-addon">
																<a href="#"><i class="fa fa-calendar"></i></a>
															</div>
														</div>
		  </td>
        </tr>


	  
	  </table>
<script type="text/javascript">
<!--
function setGongT(flag) {
	//차량번호, 차종, 용도, 연식, 사용본거지, 톤수, 위수탁관리사
	if(flag== true) {
		$("#car_1").val($("#h_car_1").val());
		$("#type").val($("#h_type").val());
		$("#car_3").val($("#h_car_3").val());
		$("#car_5").val($("#h_car_5").val());
		$("#headquarter").val($("#h_headquarter").val());
		$("#carinfo_11").val($("#h_carinfo_11").val());
		$("#ws_co_id").val($("#h_ws_co_id").val());
		$("#ws_co_name").val($("#h_ws_co_name").val());
		$("#ws_bs_no").val($("#h_ws_bs_no").val());
		$("#ws_ceo").val($("#h_ws_ceo").val());
		$("#ws_tel").val($("#h_ws_tel").val());
	} else {
	}
}

function setCarInfo(id,checked) {
	if(id=='carinfo') {
		if(checked == true) {
		  $("#length").val($("#h_length").val());
		  $("#width").val($("#h_width").val());
		  $("#height").val($("#h_height").val());
		  $("#carinfo_11").val($("#h_carinfo_11").val());
		  $("#max_load").val($("#h_max_load").val());
		  $("#fuel_type").val($("#h_fuel_type").val());
		} else {
		  $("#length").val('');
		  $("#width").val('');
		  $("#height").val('');
		  $("#carinfo_11").val('');
		  $("#max_load").val('');
		  $("#fuel_type").val('');
		}
	} else if(id=='owner') {
		  $("#owner_id").val($("#h_owner_id").val());
		  $("#owner_name").val($("#h_owner_name").val());
		  $("#driver_name").val($("#h_driver_name").val());
	} else if(id=='inv_co_info') {
		if(checked == true) {
		  $("#inv_co_id").val($("#h_inv_co_id").val());
		  $("#inv_co_name").val($("#h_inv_co_name").val());
		  $("#inv_ceo").val($("#h_inv_ceo").val());
		  $("#inv_reg_no").val($("#h_inv_reg_no").val());
		  $("#inv_bs_no").val($("#h_inv_bs_no").val());
		  $("#inv_tel").val($("#h_inv_tel").val());
		} else {
		  $("#inv_co_id").val('');
		  $("#inv_co_name").val('');
		  $("#inv_ceo").val('');
		  $("#inv_reg_no").val('');
		  $("#inv_bs_no").val('');
		  $("#inv_tel").val('');
		}
	} else if(id=='ws_co_info') {
		if(checked == true) {
		  $("#ws_co_id").val($("#h_ws_co_id").val());
		  $("#ws_co_name").val($("#h_ws_co_name").val());
		  $("#ws_bs_no").val($("#h_ws_bs_no").val());
		  $("#ws_ceo").val($("#h_ws_ceo").val());
		  $("#ws_tel").val($("#h_ws_tel").val());
		} else {
		  $("#ws_co_id").val('');
		  $("#ws_co_name").val('');
		  $("#ws_bs_no").val('');
		  $("#ws_ceo").val('');
		  $("#ws_tel").val('');
		}
	} else {
//		var a = $("#"+id+" option:selected").val();
//	else
//	$("#"+id +" option:text=" + a +"").prop("selected", "selected");
		var a = $("#h_"+id).val();

		if(checked == true)
		  $("#"+id).val(a);
		else
		  $("#"+id).val('');
	}
}

function setCarInfoBaecha(checked) {
	var a = $("#h_baecha_co_id").val();
	var b = $("#h_baecha_co_name").val();
	if(checked == true) {
	  $("#baecha_co_id").val(a);
	  $("#baecha_co_name").val(b);
	} else {
	  $("#baecha_co_id").val('');
	  $("#baecha_co_name").val('');
	}
}

function setCarNo2(checked) {
	if(checked == true)
	  $("#car_7").val($("#h_car_7").val());
	else
	  $("#car_7").val('');
}

//-->
</script>












                                    </div><!-- /.nav-tabs-custom -->
                                                <div class="form-group">
													<label class="col-lg-3"></label>
													<div class="col-lg-1">
														<button type="submit"
																class="btn btn-sm btn-primary">저장</button>
													</div>
													<div class="col-lg-3">
														<!--button type="submit" name="save_and_create_contact" value="1"
																class="btn btn-sm btn-primary"><?= lang('save_and_create_contact') ?></button-->
													</div>
												</div>
                                </div>
                        </form>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?php if (!empty($car_info->idx)) echo  base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>