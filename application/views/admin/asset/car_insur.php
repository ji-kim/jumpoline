<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
    ?>
<style>
/* Ensure that the demo table scrolls */
	table > thead > tr > th {
		vertical-align:bottom;
		border-bottom:1px solid #eee;
		border-top:0px solid #eee;
		font-weight : none;
	}
    th, td {
        white-space: nowrap;
        padding-left: 10px !important;
        padding-right: 10px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }

</style>
<script>
	function popAsWindow(md,tr_id) {
	  window.open('<?php echo base_url(); ?>admin/asset/pop_carinfo/'+md+'/'+tr_id, 'winAS', 'left=50, top=50, width=1480, height=700, scrollbars=1');
	}
	function goSearch(val) {
		if(val == 'all' || val == '전체') {
			document.myform.action = "<?php echo base_url() ?>admin/asset/car_list_all";
			document.myform.ws_co_id.value = '';
		} else {
			document.myform.action = "<?php echo base_url() ?>admin/asset/car_list";
		}
		document.myform.submit();
	}
</script>
            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
                
    <!-- 검색 시작 -->
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/asset/car_insur"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
	<table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
        <tr>
          <td align="left" valign="top" bgcolor="#ffffff">
            <table border="0" width="100%" cellpadding="12" cellspacing="15" bgcolor="#ffffff">
              <tr>
                <td width="12%" align="center" bgcolor="#efefef">만기년월</td>
                <td width="20%" style="padding-left:5px;" align="left" bgcolor="#ffffff;">
                            <div class="input-group">
                                <input type="text" value="<?php
                                if (!empty($df_month)) {
                                    echo $df_month;
                                }
                                ?>" class="form-control monthyear" name="df_month"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;" onChange="document.myform.submit();">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>

                </td>
                <td width="12%" align="center" bgcolor="#efefef">위수탁관리사</td>
                <td width="20%" style="padding-left:5px;" align="left" bgcolor="#ffffff;">
                                                        <div class="input-group">
														<input type="hidden" name="ws_co_id" id="sws_co_id" value="<?php
                                                               if (!empty($ws_co_id)) {
                                                                   echo $ws_co_id;
                                                               }
                                                               ?>">
															<input type="text" name="ws_co_name" id="sws_co_name" value="<?=(empty($ws_co_name))?"":$ws_co_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('sws_co_id||sws_co_name||||||||||','coop');" onChange="goSearch(this.value)">
                                                        </div>


					<!--select name="ws_co_id" id="ws_co_id" style="width:100%;background-color:yellow;" class="form-control input-sm" onChange="goSearch(this.value)">
							<option value="all" <?=($ws_co_id=="all")?"selected":""?>>전체</option>
							<option value="1413" <?=($ws_co_id=="1413")?"selected":""?>>케이티지엘에스(주) </option>
							<option value="1414" <?=($ws_co_id=="1414")?"selected":""?>>(주)아이디일일구닷컴</option>
							<option value="1415" <?=($ws_co_id=="1415")?"selected":""?>>(주)델타온</option>
							<option value="1903" <?=($ws_co_id=="1903")?"selected":""?>>(주)휴먼엘지</option>
							<option value="etc" <?=($ws_co_id=="etc")?"selected":""?>>기타</option>
					</select-->

					<!--a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/select_reqco"><input type="hidden" name="ws_co_id" id="ws_co_id" value="">
					<input type="text" name="ws_co_name" id="ws_co_name" value="" class="form-control" style="width:100%;background-color:yellow;"></a-->

                </td>
                <td width="12%" align="center" bgcolor="#efefef">배차지</td>
                <td width="20%" align="center" >
					<select name="baecha_co_id" id="baecha_co_id" style="width:100%;" class="form-control input-sm" >
				<option value="">전체</option>
							<option value="wmjd" > 우체국물류지원단 </option>
							<option value="0003" >(주)델타온</option>
							<option value="0001" >(주)아이디일일구닷컴</option>
							<option value="degugyodoso" >KPI대구교도소</option>
							<option value="OUT444" >OUT</option>
							<option value="kn4021" >강남우체국</option>
							<option value="kd2828" >강동우체국</option>
							<option value="ks8900" >강서우체국</option>
							<option value="gong-101" >개별현장</option>
							<option value="kkj5900" >경기광주우체국</option>
							<option value="gyounggyh" >경기혈액원</option>
							<option value="ks3704" >경산우체국</option>
							<option value="kj0114" >경주우체국</option>
							<option value="ky9400" >계양우체국</option>
							<option value="ka0014" >관악우체국</option>
							<option value="km1114" >광명우체국</option>
							<option value="ks2114" >광산우체국</option>
							<option value="kj0280" >광주우체국</option>
							<option value="guro1501" >구로구청</option>
							<option value="km1533" >구미우체국</option>
							<option value="gp8014" >군포우체국</option>
							<option value="icb1512" >근로복지인천병원</option>
							<option value="kj6014" >금정우체국</option>
							<option value="kia5071" >기아자동차협력</option>
							<option value="hangi1501" >기초과학연구원</option>
							<option value="kc0014" >김천우체국</option>
							<option value="kp0014" >김포우체국</option>
							<option value="kh9009" >김해우체국</option>
							<option value="nd0801" >남동우체국</option>
							<option value="nbs9000" >남부산우체국</option>
							<option value="nambuh" >남부혈액검사센터</option>
							<option value="rudrlskadid" >남양우체국</option>
							<option value="nyj0700" >남양주우체국</option>
							<option value="nws2014" >남울산우체국</option>
							<option value="nic0500" >남인천우체국</option>
							<option value="nw3226" >노원우체국</option>
							<option value="dgds4614" >대구달서우체국</option>
							<option value="dg2000" >대구우체국</option>
							<option value="dd2400" >대덕우체국</option>
							<option value="dj7066" >대전우체국</option>
							<option value="redcross-dj" >대전충남혈액원</option>
							<option value="redcross-ws" >대한적십자사울산혈액원</option>
							<option value="dy0014" >덕양우체국</option>
							<option value="db3600" >도봉우체국</option>
							<option value="ddg1900" >동대구우체국</option>
							<option value="dl7014" >동래우체국</option>
							<option value="dsw0532" >동수원우체국</option>
							<option value="dws0100" >동울산우체국</option>
							<option value="dj0385" >동작우체국</option>
							<option value="djj3842" >동전주우체국</option>
							<option value="dca6310" >동천안우체국</option>
							<option value="ds1610" >둔산우체국</option>
							<option value="ms0044" >마산우체국</option>
							<option value="mshp0004" >마산합포우체국</option>
							<option value="mp0014" >마포우체국</option>
							<option value="anstks" >문산우체국</option>
							<option value="seoul" >번호예치</option>
							<option value="inkd01" >본사</option>
							<option value="bs3000" >부산우체국</option>
							<option value="bsj0700" >부산진우체국</option>
							<option value="bc7124" >부천우체국</option>
							<option value="bp4000" >부평우체국</option>
							<option value="bgj9114" >북광주우체국</option>
							<option value="bdg3024" >북대구우체국</option>
							<option value="bbs0864" >북부산우체국</option>
							<option value="bd2900" >분당우체국</option>
							<option value="ss3331" >사상우체국</option>
							<option value="sh7000" >사하우체국</option>
							<option value="ssd" >상수도사업본부</option>
							<option value="skj7190" >서광주우체국</option>
							<option value="sgp3915" >서귀포우체국</option>
							<option value="seodemoon" >서대문구청</option>
							<option value="sdm9114" >서대문우체국</option>
							<option value="sdj3400" >서대전우체국</option>
							<option value="ssw01" >서수원우체국</option>
							<option value="redcross-s" >서울남부혈액원</option>
							<option value="sdhw" >서울동부혈액원</option>
							<option value="seomun1501" >서울문화재단</option>
							<option value="seoult" >서울화물공제조합</option>
							<option value="sic9114" >서인천우체국</option>
							<option value="scj5720" >서청주우체국</option>
							<option value="sn0014" >성남우체국</option>
							<option value="sb0123" >성북우체국</option>
							<option value="center1" >센타프라자</option>
							<option value="sp2700" >송파우체국</option>
							<option value="sw1300" >수원우체국</option>
							<option value="sj0511" >수지우체국</option>
							<option value="sh2700" >시흥우체국</option>
							<option value="cjdt01" >씨제이대한통운(주)</option>
							<option value="as2114" >아산우체국</option>
							<option value="as0014" >안산우체국</option>
							<option value="as7900" >안성우체국</option>
							<option value="ay9788" >안양우체국</option>
							<option value="dkswnddncprnr" >안중우체국</option>
							<option value="yangsan" >양산우체국</option>
							<option value="yc0014" >양천우체국</option>
							<option value="yyd0014" >여의도우체국</option>
							<option value="yj0888" >연제우체국</option>
							<option value="yd5550" >영도우체국</option>
							<option value="os0004" >오산우체국</option>
							<option value="ys0004" >용산우체국</option>
							<option value="yi2849" >용인우체국</option>
							<option value="kypost1" >우정본(경북청)</option>
							<option value="ruddlscjd" >우정본(경인청)</option>
							<option value="pupost1" >우정본(부산청)</option>
							<option value="spost1" >우정본(서울청)</option>
							<option value="jnpost1" >우정본(전남청)</option>
							<option value="jbcpost1" >우정본(전북청)</option>
							<option value="jjpost1" >우정본(제주청)</option>
							<option value="wjs0001" >우정사업본부</option>
							<option value="ws5801" >울산우체국</option>
							<option value="ys8114" >유성우체국</option>
							<option value="ep3514" >은평우체국</option>
							<option value="humanplaza-1" >은평휴먼프라자Ⅰ</option>
							<option value="yjb5556" >의정부우체국</option>
							<option value="ic2820" >이천우체국</option>
							<option value="ic8155" >인천우체국</option>
							<option value="incheonjodal" >인천지방조달청</option>
							<option value="is0205" >일산우체국</option>
							<option value="jj2635" >전주우체국</option>
							<option value="jjwj5200" >제주우편집중국</option>
							<option value="junongangh" >중앙혈액검사센터</option>
							<option value="jh0005" >진해우체국</option>
							<option value="cw1114" >창원우체국</option>
							<option value="ca2660" >천안우체국</option>
							<option value="cj0014" >청주우체국</option>
							<option value="0002" >케이티지엘에스</option>
							<option value="ty2016" >통영우체국</option>
							<option value="pj9004" >파주우체국</option>
							<option value="pt3121" >평택우체국</option>
							<option value="pc1302" >포천우체국</option>
							<option value="ph9937" >포항우체국</option>
							<option value="hn2007" >하남우체국</option>
							<option value="hsk88" >한국승강기안전공단</option>
							<option value="hanjun2" >한국전력(강원)</option>
							<option value="dghj001" >한국전력(대구)</option>
							<option value="hsk20" >한승공강원지사</option>
							<option value="hsk16" >한승공경남동부</option>
							<option value="hsk10" >한승공경북서부</option>
							<option value="hsk15" >한승공대구서부</option>
							<option value="hsk06" >한승공부천지사</option>
							<option value="hskkd" >한승공서울강동</option>
							<option value="hsksb" >한승공서울본부</option>
							<option value="hskss" >한승공서울서부</option>
							<option value="hsksc" >한승공서울서초</option>
							<option value="hsk05" >한승공안산지사</option>
							<option value="hsk13" >한승공영남본부</option>
							<option value="hsk03" >한승공용인지사</option>
							<option value="hskws" >한승공울산지사</option>
							<option value="hskjd" >한승공전남동부</option>
							<option value="hskjs" >한승공전남서부</option>
							<option value="jejuhsk" >한승공제주지사</option>
							<option value="hskca" >한승공천안지사</option>
							<option value="hskcn" >한승공충남지사</option>
							<option value="hsk17" >한승공충북지사</option>
							<option value="hsk07" >한승공파주지사</option>
							<option value="hwd4500" >해운대우체국</option>
							<option value="ws9516" >화성우체국</option>
							<option value="goldb1" >황금빌딩</option>
							</select>
				</td>
                <td>
					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables" href="javascript:goSearch(document.myform.ws_co_name.value);">
					<span><i class="fa fa-search"> </i> 검색</span>
					</a>

                </td>
			</tr>
            </table>
		  </td>
        </tr>
      </table>
		</form>
      <!-- 검색 끝 -->

				

            </div>





<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip"
             data-title="<?php echo lang('filter_by'); ?>">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-filter" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-left"
                style="width:300px;<?php if (!empty($search_by) && $search_by == 'group') {
                    echo 'display:block';
                } ?>">
                <li class="<?php
                if (empty($search_by)) {
                    echo 'active';
                } ?>"><a
                        href="<?= base_url() ?>admin/client/manage_client"><?php echo lang('all'); ?></a>
                </li>
                <li class="divider"></li>
                <?php if (count($all_customer_group) > 0) { ?>
                    <li class="dropdown-submenu pull-left groups <?php if (!empty($id)) {
                        if ($search_by == 'group') {
                            echo 'active';
                        }
                    } ?>">
                        <a href="#" tabindex="-1"><?php echo lang('customer_group'); ?></a>
                        <ul class="dropdown-menu dropdown-menu-left"
                            style="<?php if (!empty($search_by) && $search_by == 'group') {
                                echo 'display:block';
                            } ?>">
                            <?php foreach ($all_customer_group as $group) {
                                ?>
                                <li class="<?php if (!empty($id)) {
                                    if ($search_by == 'group') {
                                        if ($id == $group->customer_group_id) {
                                            echo 'active';
                                        }
                                    }
                                } ?>">
                                    <a href="<?= base_url() ?>admin/client/manage_client/group/<?php echo $group->customer_group_id; ?>"><?php echo $group->customer_group; ?></a>
                                </li>
                            <?php }
                            ?>
                        </ul>
                    </li>
                    <div class="clearfix"></div>
                    <li class="divider"></li>
                <?php } ?>
            </ul>
        </div>
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#car_list"
                                                                   data-toggle="tab">차량목록</a></li>



                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_car"
                                                                   data-toggle="tab">신규등록</a></li>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="car_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong>차량목록</strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
      <table width="1000" border="0" cellpadding="0" cellspacing="0" id="DataTables" class="table table-striped DataTables">
                                <thead>
        <tr align="center" bgcolor="#e0e7ef" style="text-align:center;vertical-align:center;">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">NO</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="6">차량정보</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">자동차보험</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">적재물보험</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">자동차정밀(정기)검사</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="5">최종차주정보</td>
          <td style="color:#ffffff;background-color: #777777;border-left:1px solid #eee;" rowspan="2">수정</td>
		</tr>
		<tr align="center">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차량번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차대번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">년식</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">용도</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차종</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">톤수</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험가입일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험만기일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">분납횟수</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험가입일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보험만기일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">분납여부</td>


          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">최종검사일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">다음검사일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">개시일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">만료일</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">배차지</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">위ㆍ수탁관리회사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">이전차주명</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">현차주명</td>
          <td style="color:#ffffff;background-color: #777777;">운전자</td>
		</tr>
                                </thead>

                                <tbody>
                               <?php
                                if (!empty($all_car_info)) {
                                    foreach ($all_car_info as $car_details) {

										// 자동차보험
										$car_ins = $this->db->where('tr_id', $car_details->idx)->where('active_yn', 'Y')->where('type', '자동차')->get('tbl_asset_truck_insur')->row();
										if(!empty($car_ins->ins_co_id)) {
											$car_ins_co = $this->db->where('idx', $car_ins->ins_co_id)->get('tbl_insur_company')->row();
											if(empty($car_ins_co->deptname)) $ins_co_name = ""; else $ins_co_name = $car_ins_co->deptname;
										}
										// 적재물
										$load_ins = $this->db->where('tr_id', $car_details->idx)->where('active_yn', 'Y')->where('type', '적재물')->get('tbl_asset_truck_insur')->row();
										if(!empty($load_ins->ins_co_id)) {
											$load_ins_co = $this->db->where('idx', $load_ins->ins_co_id)->get('tbl_insur_company')->row();
											if(empty($load_ins_co->deptname)) $load_ins_co_name = ""; else $load_ins_co_name = $load_ins_co->deptname;
										}

										//배차지
										$od_co = $this->db->where('code', $car_details->baecha_co)->where('mb_type', 'customer')->get('tbl_members')->row();

										// 차주정보
										$owner = $this->db->where('tr_id', $car_details->idx)->get('tbl_members')->row();
/*
                                        $client_transactions = $this->db->select_sum('amount')->where(array('paid_by' => $client_details->client_id))->get('tbl_transactions')->result();
                                        $customer_group = $this->db->where('customer_group_id', $client_details->customer_group_id)->get('tbl_customer_group')->row();
                                        $client_outstanding = $this->invoice_model->client_outstanding($client_details->client_id);
                                        $client_currency = $this->invoice_model->client_currency_sambol($client_details->client_id);
                                        if (!empty($client_currency)) {
                                            $cur = $client_currency->symbol;
                                        } else {
                                            $currency = $this->db->where(array('code' => config_item('default_currency')))->get('tbl_currencies')->row();
                                            $cur = $currency->symbol;
                                        }
*/
                                        ?>
                                    <tr>
                                        <td>
                                            <?= $car_details->idx ?>
                                        </td>
                                        <td>
                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="차량정보 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_info','<?= $car_details->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
                                            <span class='label label-primary'><?= $car_details->car_1 ?></span>
                                        </td>
                                        <td>
                                            <a href="<?= base_url() ?>admin/basic/car_details/<?= $car_details->idx ?>"
                                                   class="text-info"><span class='label label-primary'><?= $car_details->car_7 ?></span></a>
										</td>
                                        <td>
                                            <?= $car_details->car_5 ?>
                                        </td>
                                        <td>
                                            <?= $car_details->car_3 ?>
                                        </td>
                                        <td>
                                            <?= $car_details->type ?>
                                        </td>
                                        <td>
                                            <?= $car_details->carinfo_11 ?>
                                        </td>


                                        <td>
                                                <!--span data-placement="top" data-toggle="tooltip"
                                                      title="보험 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_ins','<?= $car_details->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>-->
											<span class='label label-primary'><?php if(!empty($ins_co_name)) echo $ins_co_name; ?></span>
                                        </td>
                                        <td>
											<?php if(!empty($car_ins->start_date)) echo $car_ins->start_date; ?>
                                        </td>
                                        <td>
											<?php if(!empty($car_ins->end_date)) echo $car_ins->end_date; ?>
                                        </td>
                                        <td>
											<?php if(!empty($car_ins->pay_cnt)) echo $car_ins->pay_cnt; ?>
                                        </td>



                                        <td>
                                                <!--span data-placement="top" data-toggle="tooltip"
                                                      title="적재보험 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_lins','<?= $car_details->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span-->
											<span class='label label-primary'><?php if(!empty($load_ins_co_name)) echo $load_ins_co_name; ?></span>
                                        </td>
                                        <td>
                                            <?=(empty($load_ins->start_date))?"":$load_ins->start_date ?>
                                        </td>
                                        <td>
                                            <?=(empty($load_ins->end_date))?"":$load_ins->end_date ?>
                                        </td>
                                        <td>
                                            <?=(empty($load_ins->pay_cnt))?"-":$load_ins->pay_cnt ?>
                                        </td>

                                        <td>
                                                 <!--span data-placement="top" data-toggle="tooltip" title="자동차검사 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_chk','<?= $car_details->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span-->
                                           <span class='label label-primary'><?= $car_details->op_co_id ?></span>
                                        </td>
                                        <td>
                                            <?= $car_details->op_co_id ?>
                                        </td>
                                        <td>
                                            <?= $car_details->op_co_id ?>
                                        </td>
                                        <td>
                                            <?= $car_details->op_co_id ?>
                                        </td>
                                        <td>
                                                <!--span data-placement="top" data-toggle="tooltip"
                                                      title="차주 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_own','<?= $car_details->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span-->
                                            <span class='label label-primary'><?php if(!empty($od_co->co_name)) echo $od_co->co_name; ?></span>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_details->ws_co_name)) echo $car_details->ws_co_name; ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_details->inv_co_name)) echo $car_details->inv_co_name; ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_details->ceo)) echo $car_details->ceo; ?>
                                        </td>
                                        <td>
                                            <?php if(!empty($car_details->driver)) echo $car_details->driver; ?>
                                        </td>
                                        <td>
                                                <?php if (!empty($edited)) { ?>
                                                    <?php echo btn_edit('admin/asset/car_list/edit_asset/' . $car_details->idx) ?>
                                                <?php }
                                                if (!empty($deleted)) {
                                                    ?>
                                                    <?php echo btn_delete('admin/asset/car_list/delete_asset/' . $car_details->idx) ?>
                                                <?php } ?>
                                                <?php echo btn_view('admin/asset/car_list/edit_asset/' . $car_details->idx) ?>
										</td>
                                    </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php 
								}
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { ?>
                        <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_car"
                        style="position: relative;">
                        <form role="form" enctype="multipart/form-data" id="form" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/asset/save_car/<?php
                              if (!empty($car_info)) {
                                  echo $car_info->idx;
                              }
                              ?>" method="post" class="form-horizontal  ">
						<input type="hidden" name="ws_co_id" value="<?php if(!empty($car_info->ws_co_id)) echo $car_info->ws_co_id; ?>">

                            <div class="panel-body">
                                <label class="control-label col-sm-1"></label>
                                <div class="col-sm-10">
                                    <div class="nav-tabs-custom">
                                        <!-- Tabs within a box -->
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#general_car" data-toggle="tab">기초정보</a></li>
                                            <li><a href="#rinvest_compnay" data-toggle="tab">위수탁파트너</a></li>
                                            <li><a href="#wst_compnay" data-toggle="tab">위수탁관리사</a></li>
                                            <li><a href="#spec_car" data-toggle="tab">제원</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content bg-white">
                                            <!-- ************** 기본정보 *************-->
                                            <div class="chart tab-pane active" id="general_car">

                                                <div class="form-group">
													<label class="col-lg-2 control-label">소속</label>
                                                    <div class="col-lg-4">
														<input type="hidden" name="gr_id" id="gr_id" value="<?php
                                                               if (!empty($car_info->gr_id)) {
                                                                   echo $car_info->gr_id;
																   $gr = $this->db->where('dp_id', $car_info->gr_id)->get('tbl_member')->row();
																   if(!empty($gr->co_name)) $car_info->gr_name = $gr->co_name;
                                                               }
                                                               ?>">
															<input type="text" name="gr_name" id="gr_name" value="<?=(empty($car_info->gr_name))?"":$car_info->gr_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('gr_id||gr_name||||||||||','group');">
													</div>

													<label class="col-lg-2 control-label"></label>
                                                    <div class="col-lg-4">
                                                    </div>
                                                </div>
                                                <div class="form-group">
													<label class="col-lg-2 control-label">자동차 등록번호<span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->car_1)) {
                                                                   echo $car_info->car_1;
                                                               }
                                                               ?>" name="car_1">
                                                    </div>

													<label class="col-lg-2 control-label">차 종<span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($car_info->type)) {
                                                                   echo $car_info->type;
                                                               }
                                                               ?>" name="type">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">용 도
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <div class="input-group">
														<select name="car_3" class="form-control select_box">
															<option value='일반영업용' <?=(!empty($car_info->car_3) && "일반영업용" == $car_info->car_3) ? "selected" : ""?>>일반영업용</option>
															<option value='개별화물' <?=(!empty($car_info->car_3) && "개별화물" == $car_info->car_3) ? "selected" : ""?>>개별화물</option>
															<option value='개별용달' <?=(!empty($car_info->car_3) && "개별용달" == $car_info->car_3) ? "selected" : ""?>>개별용달</option>
															<option value='자가용' <?=(!empty($car_info->car_3) && "자가용" == $car_info->car_3) ? "selected" : ""?>>자가용</option>
														</select>
														</div>
                                                    </div>
                                                    <label class="col-lg-2 control-label">차 명
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($car_info->car_4)) {
                                                                   echo $car_info->car_4;
                                                               }
                                                               ?>" name="car_4">
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">형식
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($car_info->mode)) {
                                                                   echo $car_info->mode;
                                                               }
                                                               ?>" name="mode">
                                                    </div>
                                                    <label class="col-lg-2 control-label">연 식
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($car_info->car_5)) {
                                                                   echo $car_info->car_5;
                                                               }
                                                               ?>" name="car_5">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">차대번호
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($car_info->car_7)) {
                                                                   echo $car_info->car_7;
                                                               }
                                                               ?>" name="car_7">
                                                    </div>
                                                    <label class="col-lg-2 control-label">원동기 형식
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($car_info->motor_mode)) {
                                                                   echo $car_info->motor_mode;
                                                               }
                                                               ?>" name="motor_mode">

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">사용본거지
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->headquarter)) {
                                                                   echo $car_info->headquarter;
                                                               }
                                                               ?>" name="headquarter">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label"></label>
                                                    <div class="col-lg-4">
													</div>
												</div>
                                                <?//= custom_form_Fields(12, $tr_id); ?>
                                            </div><!-- ************** 기본정보 *************-->

                                            <!-- ************** 위수탁파트너 *************-->
                                            <div class="chart tab-pane" id="rinvest_compnay">
<script>
	function selectPartner(params,stype) {
	  if(stype == '') stype = 'partner';
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/'+stype+'/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
</script>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">상 호
                                                        <span class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
														<input type="hidden" name="inv_co_id" id="inv_co_id" value="<?php
                                                               if (!empty($car_info->inv_co_id)) {
                                                                   echo $car_info->inv_co_id;
                                                               }
                                                               ?>">
															<input type="text" name="inv_co_name" id="inv_co_name" value="<?=(empty($car_info->inv_co_name))?"":$car_info->inv_co_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('inv_co_id||inv_co_name||inv_reg_no||inv_location||inv_tel||inv_ceo||inv_fax','customer');">
                                                            <!--select name="xxx"
                                                                    class="form-control select_box"
                                                                    style="width: 100%" onChange="setInv(this.value);">
                                                                <?php
                                                                if (!empty($all_inv_group)) {
                                                                    foreach ($all_inv_group as $inv_group) : ?>
                                                                        <option value="<?=$inv_group->dp_id?>||<?=$inv_group->K?>||<?=$inv_group->co_address?>||<?=$inv_group->ceo_hp?>||<?=$inv_group->ceo?>||<?=$inv_group->fax?>"
																		<?php
                                                                        if (!empty($client_info->inv_co_id) && $car_info->inv_co_id == $inv_group->dp_id) {
                                                                            echo 'selected';
                                                                        } ?>
                                                                        ><?= $inv_group->co_name; ?></option>
                                                                    <?php endforeach;
                                                                }
                                                                $created = can_action('125', 'created');
                                                                ?>
                                                            </select>
                                                            <?php if (!empty($created)) { ?>
                                                                <div class="input-group-addon"
                                                                     title="신규등록"
                                                                     data-toggle="tooltip" data-placement="top">
                                                                    <a data-toggle="modal" data-target="#myModal"
                                                                       href="<?= base_url() ?>admin/basic/customer_group"><i
                                                                            class="fa fa-plus"></i></a>
                                                                </div>
                                                            <?php } ?>-->
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">주민등록번호</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->inv_reg_no)) {
                                                                   echo $car_info->inv_reg_no;
                                                               }
                                                               ?>" name="inv_reg_no" id="inv_reg_no">
													</div>
												</div>
												
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">소재지
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->T)) {
                                                                   echo $car_info->T;
                                                               }
                                                               ?>" name="inv_location" id="inv_location">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">전화번호</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->Y)) {
                                                                   echo $car_info->Y;
                                                               }
                                                               ?>" name="inv_tel" id="inv_tel">
													</div>
												</div>
												
												
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">대표자
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->D)) {
                                                                   echo $car_info->D;
                                                               }
                                                               ?>" name="inv_ceo" id="inv_ceo">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">FAX</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->fax)) {
                                                                   echo $car_info->fax;
                                                               }
                                                               ?>" name="inv_fax" id="inv_fax">
													</div>
												</div>
                                            </div><!-- ************** 현물출자자 *************-->
                                            <!-- ************** 위수탁 *************-->
                                            <div class="chart tab-pane" id="wst_compnay">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">상 호
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                   <div class="col-lg-4">
                                                        <div class="input-group">
														<input type="hidden" name="ws_co_id" id="ws_co_id" value="<?php
                                                               if (!empty($car_info->ws_co_id)) {
                                                                   echo $car_info->ws_co_id;
                                                               }
                                                               ?>">
															<input type="text" name="ws_co_name" id="ws_co_name" value="<?=(empty($car_info->ws_co_name))?"":$car_info->ws_co_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('ws_co_id||ws_co_name||ws_bs_no||ws_location||ws_tel||ws_ceo||ws_fax','coop');">


														<!--input type="hidden" name="wst_co_id" id="wst_co_id" value="<?php
                                                               if (!empty($car_info->wst_co_id)) {
                                                                   echo $car_info->wst_co_id;
                                                               }
                                                               ?>">
                                                            <select name="xxx"
                                                                    class="form-control select_box"
                                                                    style="width: 100%" onChange="setWst(this.value);">
                                                                <?php
                                                                if (!empty($all_inv_group)) {
                                                                    foreach ($all_inv_group as $inv_group) : ?>
                                                                        <option value="<?=$inv_group->dp_id?>||<?=$inv_group->K?>||<?=$inv_group->co_address?>||<?=$inv_group->ceo_hp?>||<?=$inv_group->ceo?>||<?=$inv_group->fax?>"
																		<?php
                                                                        if (!empty($client_info->wst_co_id) && $client_info->wst_co_id == $inv_group->dp_id) {
                                                                            echo 'selected';
                                                                        } ?>
                                                                        ><?= $inv_group->co_name; ?></option>
                                                                    <?php endforeach;
                                                                }
                                                                $created = can_action('125', 'created');
                                                                ?>
                                                            </select>
 															<input type="checkbox" name="ss">  실소유와동일-->
                                                        </div>
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">법인(주민)등록번호</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->Q)) {
                                                                   echo $car_info->Q;
                                                               }
                                                               ?>" name="ws_bs_no" id="ws_bs_no">
													</div>
												</div>
												
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">소재지
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->T)) {
                                                                   echo $car_info->T;
                                                               }
                                                               ?>" name="ws_location" id="ws_location">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">전화번호</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->Y)) {
                                                                   echo $car_info->Y;
                                                               }
                                                               ?>" name="ws_tel" id="ws_tel">
													</div>
												</div>
												
												
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">대표자
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->D)) {
                                                                   echo $car_info->D;
                                                               }
                                                               ?>" name="ws_ceo" id="ws_ceo">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">FAX</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->fax)) {
                                                                   echo $car_info->fax;
                                                               }
                                                               ?>" name="ws_fax" id="ws_fax">
													</div>
												</div>
                                            </div><!-- ************** 위수탁 *************-->
                                            <!-- ************** 제 원  *************-->
                                            <div class="chart tab-pane" id="spec_car">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">길이
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->length)) {
                                                                   echo $car_info->length;
                                                               }
                                                               ?>" name="length">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">너비</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->width)) {
                                                                   echo $car_info->width;
                                                               }
                                                               ?>" name="width">
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">높 이
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->height)) {
                                                                   echo $car_info->height;
                                                               }
                                                               ?>" name="height">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">톤 수</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->carinfo_11)) {
                                                                   echo $car_info->carinfo_11;
                                                               }
                                                               ?>" name="carinfo_11">
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">승차정원
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->max_on)) {
                                                                   echo $car_info->max_on;
                                                               }
                                                               ?>" name="max_on">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">최대적재량</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($car_info->max_load)) {
                                                                   echo $car_info->max_load;
                                                               }
                                                               ?>" name="max_load">
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">연료의종류
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <div class="input-group">
														<select name="fuel_type" class="form-control select_box" style="width:100%;">
															<option value="가솔린" <? if(!empty($car_info->fuel_type) && "가솔린"==$car_info->fuel_type) echo " selected"; ?>>가솔린</option>
															<option value="경유" <? if(!empty($car_info->fuel_type) && "경유"==$car_info->fuel_type) echo " selected"; ?>>경유</option>
															<option value="LPG" <? if(!empty($car_info->fuel_type) && "LPG"==$car_info->fuel_type) echo " selected"; ?>>LPG</option>
															<option value="기타" <? if(!empty($car_info->fuel_type) && "기타"==$car_info->fuel_type) echo " selected"; ?>>기타</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">차고지</label>
                                                    <div class="col-lg-4">
                                                        <div class="input-group">
														<select name="carinfo_9" class="form-control select_box" style="width:100%;">
                                                                <?php
                                                                if (!empty($all_garage_group)) {
                                                                    foreach ($all_garage_group as $garage_group) : ?>
                                                                        <option value="<?=$garage_group->idx?>"
																		<?php
                                                                        if (!empty($car_info->carinfo_9) && $car_info->carinfo_9 == $garage_group->idx) {
                                                                            echo 'selected';
                                                                        } ?>
                                                                        >[<?= $garage_group->gr_name; ?>] <?=$garage_group->tel?> <?=$garage_group->ceo?> <?=$garage_group->dp_address?> [총면적: <?=($garage_group->tot_dim)?>]</option>
                                                                    <?php endforeach;
                                                                }
                                                                $created = can_action('125', 'created');
                                                                ?>
                                                            </select>
                                                        </div>
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">차량등록증
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="file" name="cert_file" id="cert_file" value="" class="form-control">
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label"></label>
                                                    <div class="col-lg-4">
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-lg-3"></label>
													<div class="col-lg-1">
														<button type="submit"
																class="btn btn-sm btn-primary">저장</button>
													</div>
													<div class="col-lg-3">
														<!--button type="submit" name="save_and_create_contact" value="1"
																class="btn btn-sm btn-primary"><?= lang('save_and_create_contact') ?></button-->
													</div>
												</div>
                                            </div><!-- ************** 제 원  *************-->
                                        </div>
                                    </div><!-- /.nav-tabs-custom -->
                                    <div class="form-group mt">

                                    </div>
                                </div>
                        </form>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>