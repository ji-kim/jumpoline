                        <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_partner"
                        style="position: relative;">



                        <form role="form" method='POST' enctype="multipart/form-data" id="form" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/basic/save_partner/<?php
                              if (!empty($partner_info)) {
                                  echo $partner_info->dp_id;
                              }
                              ?>" class="form-horizontal  ">
                            <div class="panel-body">
                                <label class="control-label col-sm-1"></label>
                                <div class="col-sm-10">
                                    <div class="nav-tabs-custom">
                                        <!-- Tabs within a box -->
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#general_info" data-toggle="tab">기본정보</a></li>
                                            <li><a href="#compnay_info" data-toggle="tab">사업자정보</a></li>
                                            <li><a href="#add_info" data-toggle="tab">부가정보</a></li>
                                            <li><a href="#attach" data-toggle="tab">첨부파일</a></li>
                                        </ul>
                                        <div class="tab-content bg-white">



                                            <!-- ************** 기본정보 *************-->
                                            <div class="chart tab-pane active" id="general_info">
                                                
												<div class="form-group">
													<label class="col-lg-2 control-label"></label>
														<div class="col-lg-4 fileinput fileinput-new" data-provides="fileinput">
															<div class="fileinput-new thumbnail" style="width: 210px;">
																<?php
																if (!empty($partner_info)) :
																	?>
																	<img src="<?php echo base_url() . $partner_info->mb_pic; ?>">
																<?php else: ?>
																	<img src="http://placehold.it/350x260"
																		 alt="Please Connect Your Internet">
																<?php endif; ?>
															</div>
															<div class="fileinput-preview fileinput-exists thumbnail"
																 style="width: 210px;"></div>
															<div>
															<span class="btn btn-default btn-file">
																<span class="fileinput-new">
																	<input type="file" name="mb_pic" value="upload"
																		   data-buttonText="이미지선택" id="myImg"/>
																	<span class="fileinput-exists">변경</span>    
																</span>
																<a href="#" class="btn btn-default fileinput-exists"
																   data-dismiss="fileinput">삭제</a>

															</div>
															<div id="valid_msg" style="color: #e11221"></div>
														</div>

													<label class="col-lg-2 control-label"></label>
                                                    <div class="col-lg-4">
                                                    </div>
                                                </div>
											   
											   
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">아이디<span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">

															<input type="text" name="userid" id="userid" value="<?=(empty($partner_info->userid ))?"":$partner_info->userid  ?>" class="form-control">
                                                    </div>

													<label class="col-lg-2 control-label">비밀번호<span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($partner_info->userpwd )) {
                                                                   echo $partner_info->userpwd ;
                                                               }
                                                               ?>" name="userpwd">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">실수요처<span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">

<?php
if(!empty($partner_info->code))	$rco_info = $this->db->where('mb_type', 'customer')->where('code', $partner_info->code)->get('tbl_members')->row();
?>														<input type="hidden" name="code" id="rco_code" value="<?php
                                                               if (!empty($partner_info->code)) {
                                                                   echo $partner_info->code;
                                                               }
                                                               ?>">
															<input type="text" name="rco_name" id="rco_name" value="<?=(empty($rco_info->co_name))?"":$rco_info->co_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('__||rco_name||__||__||__||__||__||__||rco_code');">
                                                    </div>

													<label class="col-lg-2 control-label">파트너사명<span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($partner_info->co_name)) {
                                                                   echo $partner_info->co_name;
                                                               }
                                                               ?>" name="co_name">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">대표
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($partner_info->ceo)) {
                                                                   echo $partner_info->ceo;
                                                               }
                                                               ?>" name="ceo">
                                                    </div>
                                                    <label class="col-lg-2 control-label">운전자
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($partner_info->driver)) {
                                                                   echo $partner_info->driver;
                                                               }
                                                               ?>" name="driver">
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">직위
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($partner_info->G)) {
                                                                   echo $partner_info->G;
                                                               }
                                                               ?>" name="G">
                                                    </div>
                                                    <label class="col-lg-2 control-label">주민등록번호
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($partner_info->reg_number)) {
                                                                   echo $partner_info->reg_number;
                                                               }
                                                               ?>" name="reg_number">
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">운전면허번호
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($partner_info->L)) {
                                                                   echo $partner_info->L;
                                                               }
                                                               ?>" name="L">
                                                    </div>
                                                    <label class="col-lg-2 control-label">화물운송종사자번호
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($partner_info->M)) {
                                                                   echo $partner_info->M;
                                                               }
                                                               ?>" name="M">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">계약시작일</label>
                                                    <div class="col-lg-4">
														<div class="input-group">
															<input type="text" class="form-control datepicker" name="N" value="<?php
															if (!empty($partner_info->N)) {
																echo $partner_info->N;
															}
															?>">

															<div class="input-group-addon">
																<a href="#"><i class="fa fa-calendar"></i></a>
															</div>
														</div>
                                                    </div>
                                                    <label class="col-lg-2 control-label">계약종료일</label>
                                                    <div class="col-lg-4">

														<div class="input-group">
															<input type="text" class="form-control datepicker" name="O" value="<?php
															if (!empty($partner_info->O)) {
																echo $partner_info->O;
															}
															?>">

															<div class="input-group-addon">
																<a href="#"><i class="fa fa-calendar"></i></a>
															</div>
														</div>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">과세여부 </label>
                                                    <div class="col-lg-4">
                                                          <select name="tax_yn"
                                                                    class="form-control select_box"
                                                                    style="width: 100%">
														  <option <?=(!empty($partner_info->tax_yn) && $partner_info->tax_yn=="")?"selected":""?> value="">선택</option>
														  <option <?=(!empty($partner_info->tax_yn) && $partner_info->tax_yn=="Y")?"selected":""?> value="Y">예</option>
														  <option <?=(!empty($partner_info->tax_yn) && $partner_info->tax_yn=="N")?"selected":""?> value="N">아니오</option>
														</select>
                                                    </div>
                                                    <label
                                                        class="col-lg-2 control-label">메모</label>
                                                    <div class="col-lg-4">
                                            <textarea class="form-control" name="remark"><?php
                                                if (!empty($partner_info->remark)) {
                                                    echo $partner_info->remark;
                                                }
                                                ?></textarea>

													</div>
												</div>
                                            </div>
											
											<!-- ************** 기본정보 *************-->

                                            <!-- ************** 사업자정보 *************-->
                                            <div class="chart tab-pane" id="compnay_info">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">사업자등록번호
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" 
                                                               value="<?php
                                                               if (!empty($partner_info->bs_number)) {
                                                                   echo $partner_info->bs_number;
                                                               }
                                                               ?>" name="bs_number">
                                                    </div>
                                                    <label class="col-lg-2 control-label">사업장주소 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->T)) {
                                                                   echo $partner_info->T;
                                                               }
                                                               ?>" name="T">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">업태 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->bs_type1)) {
                                                                   echo $partner_info->bs_type1;
                                                               }
                                                               ?>" name="bs_type1">
                                                    </div>
                                                    <label class="col-lg-2 control-label">종목 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->bs_type2)) {
                                                                   echo $partner_info->bs_type2;
                                                               }
                                                               ?>" name="bs_type2">
                                                    </div>
                                                </div>
                                            </div><!-- ************** 사업자정보 *************-->
                                            <!-- ************** Car *************-->
                                            <div class="chart tab-pane" id="add_info">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">차량번호 </label>
                                                    <div class="col-lg-4">
														<input type="hidden" name="tr_id" id="tr_id" value="<?php
                                                               if (!empty($partner_info->tr_id)) {
                                                                   echo $partner_info->tr_id;
                                                               }
                                                               ?>">
                                                        <input type="text" class="form-control" style="background-color:yellow;"
                                                               value="<?php
                                                               if (!empty($partner_info->U)) {
                                                                   echo $partner_info->U;
                                                               }
                                                               ?>" name="U" id="U" onClick="selectTruck('tr_id||U||W||V||ws_co_id||X');">
                                                    </div>
                                                    <label class="col-lg-2 control-label">용도 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->V)) {
                                                                   echo $partner_info->V;
                                                               }
                                                               ?>" name="V" id="V">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">차대번호 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->car_7)) {
                                                                   echo $partner_info->car_7;
                                                               }
                                                               ?>" name="W" id="W">
                                                    </div>
                                                    <label class="col-lg-2 control-label">지입사명 </label>
                                                    <div class="col-lg-4">
													<input type="hidden" name="ws_co_id" id="ws_co_id" value="<?php
                                                               if (!empty($partner_info->ws_co_id)) {
                                                                   echo $partner_info->ws_co_id;
                                                               }
                                                               ?>">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->X)) {
                                                                   echo $partner_info->X;
                                                               }
                                                               ?>" name="X" id="X">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">핸드폰번호 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->co_tel)) {
                                                                   echo $partner_info->co_tel;
                                                               }
                                                               ?>" name="co_tel">
                                                    </div>
                                                    <label class="col-lg-2 control-label">PDA번호 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->Z)) {
                                                                   echo $partner_info->Z;
                                                               }
                                                               ?>" name="Z">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">은행 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->AA)) {
                                                                   echo $partner_info->AA;
                                                               }
                                                               ?>" name="AA">
                                                    </div>
                                                    <label class="col-lg-2 control-label">계좌번호 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->AB)) {
                                                                   echo $partner_info->AB;
                                                               }
                                                               ?>" name="AB">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">예금주 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->AC)) {
                                                                   echo $partner_info->AC;
                                                               }
                                                               ?>" name="AC">
                                                    </div>
                                                    <label class="col-lg-2 control-label">예금주와의관계 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->AD)) {
                                                                   echo $partner_info->AD;
                                                               }
                                                               ?>" name="AD">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">이메일 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->email)) {
                                                                   echo $partner_info->email;
                                                               }
                                                               ?>" name="email">
                                                    </div>
                                                    <label class="col-lg-2 control-label">주소 </label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->co_address)) {
                                                                   echo $partner_info->co_address;
                                                               }
                                                               ?>" name="co_address">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">메모 </label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" value="<?php
                                                               if (!empty($partner_info->remark)) {
                                                                   echo $partner_info->remark;
                                                               }
                                                               ?>" name="remark">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Car *************-->
                                            <!-- ************** attach *************-->
                                            <div class="chart tab-pane" id="attach">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #1 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach1" id="attach1" value="" class="form-control" style="width:85%;">
<?
if(!empty($partner_info->attach1) && $partner_info->attach1) {
	echo "<a href='".base_url().'/'.$partner_info->attach1."' target='_blank' style='font-weight:bold;color:blue;'>[첨부파일#1]</a>";
}
?>		  
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach1_text" id="attach1_text" value="<?=(!empty($partner_info->attach1_text))?$partner_info->attach1_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #2 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach2" id="attach2" value="" class="form-control" style="width:85%;">
<?
if(!empty($partner_info->attach2) && $partner_info->attach2) {
	echo "<a href='".base_url().'/'.$partner_info->attach2."' target='_blank' style='font-weight:bold;color:blue;'>[첨부파일#2]</a>";
}
?>		  
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach2_text" id="attach2_text" value="<?=(!empty($partner_info->attach2_text))?$partner_info->attach2_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #3 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach3" id="attach3" value="" class="form-control" style="width:85%;">
<?
if(!empty($partner_info->attach3) && $partner_info->attach3) {
	echo "<a href='".base_url().'/'.$partner_info->attach3."' target='_blank' style='font-weight:bold;color:blue;'>[첨부파일#3]</a>";
}
?>		  
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach3_text" id="attach3_text" value="<?=(!empty($partner_info->attach3_text))?$partner_info->attach3_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #4 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach4" id="attach4" value="" class="form-control" style="width:85%;">
<?
if(!empty($partner_info->attach4) && $partner_info->attach4) {
	echo "<a href='".base_url().'/'.$partner_info->attach4."' target='_blank' style='font-weight:bold;color:blue;'>[첨부파일#4]</a>";
}
?>		  
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach4_text" id="attach4_text" value="<?=(!empty($partner_info->attach4_text))?$partner_info->attach4_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">첨부파일 #5 </label>
                                                    <div class="col-lg-4">
		  <input type="file" name="attach5" id="attach5" value="" class="form-control" style="width:85%;">
<?
if(!empty($partner_info->attach5) && $partner_info->attach5) {
	echo "<a href='".base_url().'/'.$partner_info->attach5."' target='_blank' style='font-weight:bold;color:blue;'>[첨부파일#5]</a>";
}
?>		  
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" name="attach5_text" id="attach5_text" value="<?=(!empty($partner_info->attach5_text))?$partner_info->attach5_text:""?>" class="form-control" style="width:90%;"  >
                                                    </div>
                                                </div>



                                    <div class="form-group mt">
                                        <label class="col-lg-3"></label>
                                        <div class="col-lg-1">
                                            <button type="submit"
                                                    class="btn btn-sm btn-primary"><?= lang('save') ?></button>
                                        </div>
                                        <div class="col-lg-3">
                                        </div>

                                    </div>
											
											
											</div><!-- ************** attach *************-->
                                        </div>
                                    </div><!-- /.nav-tabs-custom -->
                                </div>
                        </form>
