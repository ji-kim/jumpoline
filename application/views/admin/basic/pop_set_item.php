<?= message_box('success'); ?>
<?php echo message_box('error');
$created = can_action('123', 'created');
$edited = can_action('123', 'edited');
$deleted = can_action('123', 'deleted');
$co_info = $this->db->where('dp_id', $ws_co_id)->get('tbl_members')->row();
?>
<div class="panel panel-custom">
    <header class="panel-heading "><?= $title ?> - <?= $co_info->co_name ?></header>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped ">
                <thead>
                <tr>

                    <th>항목명</th>
                    <th>설명</th>
                    <?php if (!empty($edited) || !empty($deleted)) { ?>
                        <th>작업</th>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($all_item_group)) {
                    foreach ($all_item_group as $item_info) {
                        ?>
                        <tr id="item_info_<?= $item_info->idx?>">
                            <td><?php
                               // $id = $this->uri->segment(5);

                                if (!empty($edit_item_info->idx) && $item_info->idx == $edit_item_info->idx) { ?>
                                <form method="post"
                                      action="<?= base_url() ?>admin/basic/set_item/<?= $ws_co_id?>/<?= $type?>/<?= $cat?>/update/<?php
                                      if (!empty($item_info_info)) {
                                          echo $item_info_info->idx;
                                      }
                                      ?>" class="form-horizontal">
                                    <input type="text" name="item" value="<?php
                                    if (!empty($edit_item_info)) {
                                        echo $edit_item_info->item;
                                    }
                                    ?>" class="form-control" placeholder="항목명" required>
                                <?php } else {
                                    echo $item_info->item;
                                }
                                ?></td>
                            <td><?php
                                if (!empty($edit_item_info->idx) && $item_info->idx == $edit_item_info->idx) { ?>
                                    <textarea name="remark" rows="1" class="form-control"><?php
                                        if (!empty($edit_item_info)) {
                                            echo $edit_item_info->remark;
                                        }
                                        ?></textarea>
                                <?php } else {
                                    echo $item_info->remark;
                                }
                                ?></td>
                            <?php if (!empty($edited) || !empty($deleted)) { ?>
                                <td>
                                    <?php
                                    $id = $this->uri->segment(5);
                                    if (!empty($edit_item_info->idx) && $item_info->idx == $edit_item_info->idx) { ?>
                                        <?= btn_update() ?>
                                        </form>
                                        <?= btn_cancel('admin/basic/set_item/'.$ws_co_id.'/'.$type.'/') ?>
                                    <?php } else {
                                        if (!empty($edited)) { ?>
                                            <?= btn_edit('admin/basic/set_item/'.$ws_co_id.'/'.$type.'/edit_item/' . $item_info->idx) ?>
                                        <?php }
                                        if (!empty($deleted)) { ?>
                                            <?php echo ajax_anchor(base_url("admin/basic/set_item/".$ws_co_id."/".$type."/delete_item/" . $item_info->idx), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#item_info_" . $item_info->idx)); ?>
                                        <?php }
                                    }
                                    ?>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php }
                }
                if (!empty($created) || !empty($edited)) { ?>
                    <form method="post" action="<?= base_url() ?>admin/basic/set_item/<?= $ws_co_id?>/<?= $type?>/<?= $cat?>/update"
                          class="form-horizontal" data-parsley-validate="" novalidate="">
                        <tr>
                            <td><input type="text" name="item" class="form-control"
                                       placeholder="항목명" required></td>
                            <td>
                                <textarea name="remark" rows="1" class="form-control"></textarea>
                            </td>
                            <td><?= btn_add() ?></td>
                        </tr>
                    </form>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>