<?= message_box('success'); ?>
<?= message_box('error');

$eeror_message = $this->session->userdata('error');
if (!empty($eeror_message)):foreach ($eeror_message as $key => $message):
    ?>
    <div class="alert alert-danger">
        <?php echo $message; ?>
    </div>
    <?php
endforeach;
endif;
$this->session->unset_userdata('error');

?>
<?php
$perc_paid = 0;
/*
$all_bug_info = $this->basic_model->get_permission('tbl_bug');
$total_bugs = 0;
if (!empty($all_bug_info)) {
    foreach ($all_bug_info as $v_bugs) {
        if (!empty($v_bugs)) {
            $profile = $this->db->where(array('user_id' => $v_bugs->reporter))->get('tbl_account_details')->row();
            if ($profile->company == $customer_details->dp_id) {
                $total_bugs += count($v_bugs->bug_id);
            }
        }
    }
}
$recently_paid = $this->db
    ->where('paid_by', $customer_details->dp_id)
    ->order_by('created_date', 'desc')
    ->get('tbl_payments')
    ->result();
$all_tickets_info = $this->basic_model->get_permission('tbl_tickets');
$total_tickets = 0;
if (!empty($all_tickets_info)) {
    foreach ($all_tickets_info as $v_tickets_info) {
        if (!empty($v_tickets_info)) {
            $profile_info = $this->db->where(array('user_id' => $v_tickets_info->reporter))->get('tbl_account_details')->row();
            if (!empty($profile_info->company))
                if ($profile_info->company == $customer_details->dp_id) {
                    $total_tickets += count($v_tickets_info->tickets_id);
                }
        }
    }
}
$all_project = $this->db->where('dp_id', $customer_details->dp_id)->get('tbl_project')->result();
$customer_notes = $this->db->where(array('user_id' => $customer_details->dp_id, 'is_customer' => 'Yes'))->get('tbl_notes')->result();

$customer_outstanding = $this->invoice_model->customer_outstanding($customer_details->dp_id);
$customer_payments = $this->invoice_model->get_sum('tbl_payments', 'amount', $array = array('paid_by' => $customer_details->dp_id));
$customer_payable = $customer_payments + $customer_outstanding;
$customer_currency = $this->invoice_model->customer_currency_sambol($customer_details->dp_id);
if (!empty($customer_currency)) {
    $cur = $customer_currency->symbol;
} else {
    $currency = $this->db->where(array('code' => config_item('default_currency')))->get('tbl_currencies')->row();
    $cur = $currency->symbol;
}
if ($customer_payable > 0 AND $customer_payments > 0) {
    $perc_paid = round(($customer_payments / $customer_payable) * 100, 1);
    if ($perc_paid > 100) {
        $perc_paid = '100';
    }
} else {
    $perc_paid = 0;
}
$customer_transactions = $this->db->where('paid_by', $customer_details->dp_id)->get('tbl_transactions')->result();
$all_proposals_info = $this->db->where(array('module' => 'customer', 'module_id' => $customer_details->dp_id))->order_by('proposals_id', 'DESC')->get('tbl_proposals')->result();
$edited = can_action('4', 'edited');
$notified_reminder = count($this->db->where(array('module' => 'customer', 'module_id' => $customer_details->dp_id, 'notified' => 'No'))->get('tbl_reminders')->result());
*/
?>
<div class="row">
    <div class="col-md-3">
        <div class="panel widget mb0 b0">
            <div class="row-table row-flush">
                <div class="col-xs-4 bg-info text-center">
                    <em class="fa fa-money fa-2x"></em>
                </div>
                <div class="col-xs-8">
                    <div class="text-center">
                        <h4 class="mb-sm"><?php
                            if (!empty($customer_payments)) {
                                echo display_money($customer_payments, $cur);
                            } else {
                                echo '0';
                            }
                            ?></h4>
                        <p class="mb0 text-muted">당월 총 관리비</p>
                        <a href="<?= base_url() ?>admin/invoice/all_payments"
                           class="small-box-footer"><?= lang('more_info') ?> <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel widget mb0 b0">
            <div class="row-table row-flush">
                <div class="col-xs-4 bg-danger text-center">
                    <em class="fa fa-usd fa-2x"></em>
                </div>
                <div class="col-xs-8">
                    <div class="text-center">
                        <h4 class="mb-sm"><?php
                           // if ($customer_outstanding > 0) {
                               // echo display_money($customer_outstanding, $cur);
                           // } else {
                                echo '0';
                           // }
                            ?></h4>
                        <p class="mb0 text-muted">당월 총 수당</p>
                        <a href="<?= base_url() ?>admin/invoice/manage_invoice"
                           class="small-box-footer"><?= lang('more_info') ?>
                            <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel widget mb0 b0">
            <div class="row-table row-flush">
                <div class="col-xs-4 bg-inverse text-center">
                    <em class="fa fa-usd fa-2x"></em>
                </div>
                <div class="col-xs-8">
                    <div class="text-center">
                        <h4 class="mb-sm">
                            <?php
                           // if ($customer_payable > 0) {
                               // echo display_money($customer_payable, $cur);
                          //  } else {
                                echo '0';
                          //  }
                            ?></h4>
                        <p class="mb0 text-muted">누적</p>
                        <a href="<?= base_url() ?>admin/invoice/manage_invoice"
                           class="small-box-footer"><?= lang('more_info') ?>
                            <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel widget mb0 b0">
            <div class="row-table row-flush">
                <div class="col-xs-4 bg-purple text-center">
                    <em class="fa fa-usd fa-2x"></em>
                </div>
                <div class="col-xs-8">
                    <div class="text-center">
                        <h4 class="mb-sm">
                            <?//= $perc_paid ?>%</h4>
                        <p class="mb0 text-muted">증감</p>
                        <a href="<?= base_url() ?>admin/invoice/all_payments"
                           class="small-box-footer"><?= lang('more_info') ?>
                            <i
                                class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$url = $this->uri->segment(5);

?>
<div class="row mt-lg">
    <div class="col-sm-3">
        <ul class="nav nav-pills nav-stacked navbar-custom-nav">
            <li class="<?= empty($url) ? 'active' : '' ?>"><a href="#task_details" data-toggle="tab"
                                                              aria-expanded="true">상세정보</a>
            </li>
            <li class="<?= $url == 'add_contacts' ? 'active' : '' ?>"><a href="#contacts"
                                                                         data-toggle="tab"
                                                                         aria-expanded="false">수당항목설정
                    <strong
                        class="pull-right"><?= (!empty($customer_contacts) ? count($customer_contacts) : null) ?></strong></a>
            </li>
            <li class="<?= $url == 'notes' ? 'active' : '' ?>"><a href="#notes" data-toggle="tab"
                                                                  aria-expanded="false">공제항목설정<strong
                        class="pull-right"><?= (!empty($customer_notes) ? count($customer_notes) : null) ?></strong></a>
            </li>
        </ul>
    </div>
    <div class="col-sm-9">
        <div class="tab-content" style="border: 0;padding:0;">
            <!-- Task Details tab Starts -->
            <div class="tab-pane <?= empty($url) ? 'active' : '' ?> " id="task_details"
                 style="position: relative;">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title"><strong><?= $customer_details->co_name ?> - 상세정보 </strong>
                            <div class="pull-right">
                                <?php //}
                                //if (!empty($edited)) {
                                    ?>
                                    <a href="<?php echo base_url() ?>admin/customer/manage_customer/<?= $customer_details->dp_id ?>"
                                       class="btn-xs "><i class="fa fa-edit"></i> 수정</a>
                                <?php //} ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <!-- Details START -->
                        <div class="col-md-6">
                            <div class="group">
                                <h4 class="subdiv text-muted">거래처 상세정보</h4>
                                <div class="row inline-fields">
                                    <div class="col-md-4">거래처명</div>
                                    <div class="col-md-6"><?= $customer_details->co_name ?></div>
                                </div>
                                <div class="row inline-fields">
                                    <div class="col-md-4">대표이사</div>
                                    <div class="col-md-6"><?= $customer_details->ceo ?></div>
                                </div>
                                <div class="row inline-fields">
                                    <div class="col-md-4">사업자등록번호</div>
                                    <div class="col-md-6"><?= $customer_details->bs_number ?></div>
                                </div>
                                 <div class="row inline-fields">
                                    <div class="col-md-4">업태/종목</div>
                                    <div class="col-md-6"><?= $customer_details->bs_type1 ?>/<?= $customer_details->bs_type2 ?></div>
                                </div>
                                <div class="row inline-fields">
                                    <div class="col-md-4">휴대폰</div>
                                    <div class="col-md-6"><?= $customer_details->ceo_hp ?></div>
                                </div>
                                <div class="row inline-fields">
                                    <div class="col-md-4">전화</div>
                                    <div class="col-md-6"><?= $customer_details->co_tel ?></div>
                                </div>
                                <div class="row inline-fields">
                                    <div class="col-md-4">팩스</div>
                                    <div class="col-md-6"><?= $customer_details->fax ?></div>
                                </div>
                                <div class="row inline-fields">
                                    <div class="col-md-4">이메일(세금계산서)</div>
                                    <div class="col-md-6"><?= $customer_details->email ?></div>
                                </div>
                                <div class="row inline-fields">
                                    <div class="col-md-4">은행</div>
                                    <div class="col-md-6"><?= $customer_details->bank ?></div>
                                </div>
                                <div class="row inline-fields">
                                    <div class="col-md-4">계좌</div>
                                    <div class="col-md-6"><?= $customer_details->account_no ?></div>
                                </div>
                                <div class="row inline-fields">
                                    <div class="col-md-4">예금주</div>
                                    <div class="col-md-6"><?= $customer_details->account_name ?></div>
                                </div>
                                <div class="row inline-fields">
                                    <div class="col-md-4">홈페이지</div>
                                    <div class="col-md-6"><?= $customer_details->homepage ?></div>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-6 mb-lg">
                            <div class="group">
                                <div class="row" style="margin-top: 5px">
                                    <div class="rec-pay col-md-12">

                                <h4 class="subdiv text-muted">송금사 정보</h4>
                            <div class="row inline-fields">
                                <div class="col-md-4">송금사</div>
                                <div class="col-md-6"><?= $customer_details->co_name ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4">대표자</div>
                                <div class="col-md-6"><?= $customer_details->co_name ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4">법인(주민)등록번호</div>
                                <div class="col-md-6"><?= $customer_details->co_name ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4">전화번호</div>
                                <div class="col-md-6"><?= $customer_details->co_name ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4">FAX</div>
                                <div class="col-md-6"><?= $customer_details->co_name ?></div>
                            </div>
                            <div class="row inline-fields">
                                <div class="col-md-4">비고</div>
                                <div class="col-md-6"><?= $customer_details->co_name ?></div>
                            </div>




                                        <div class=" mt">
                                            <?php if (!empty($customer_details->hosting_company)) { ?>
                                                <div class="row inline-fields">
                                                    <div class="col-md-4"><?= lang('hosting_company') ?></div>
                                                    <div class="col-md-6"><?= $customer_details->hosting_company ?></div>
                                                </div>
                                            <?php } ?>
                                            <?php if (!empty($customer_details->hostname)) { ?>
                                                <div class="row inline-fields">
                                                    <div class="col-md-4"><?= lang('hostname') ?></div>
                                                    <div class="col-md-6"><?= $customer_details->hostname ?></div>
                                                </div>
                                            <?php } ?>
                                            <?php if (!empty($customer_details->username)) { ?>
                                                <div class="row inline-fields">
                                                    <div class="col-md-4"><?= lang('username') ?></div>
                                                    <div class="col-md-6"><?= $customer_details->username ?></div>
                                                </div>
                                            <?php } ?>
                                            <?php if (!empty($customer_details->password)) {
                                                $hosting_password = strlen(decrypt($customer_details->password));
                                                ?>
                                                <div class="row inline-fields">
                                                    <div class="col-md-4"><?= lang('password') ?></div>
                                                    <div class="col-md-6">
                                                        <span id="show_password">
                                                        <?php
                                                        if (!empty($hosting_password)) {
                                                            for ($p = 1; $p <= $hosting_password; $p++) {
                                                                echo '*';
                                                            }
                                                        } ?>
                                                            </span>
                                                        <a data-toggle="modal" data-target="#myModal"
                                                           href="<?= base_url('admin/customer/see_password/c_' . $customer_details->dp_id) ?>"
                                                           id="see_password"><?= lang('see_password') ?></a>
                                                        <strong id="hosting_password" class="required"></strong>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if (!empty($customer_details->port)) { ?>
                                                <div class="row inline-fields">
                                                    <div class="col-md-4"><?= lang('port') ?></div>
                                                    <div class="col-md-6"><?= $customer_details->port ?></div>
                                                </div>
                                            <?php } ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center block mt">
                            <h4 class="subdiv text-muted"><?= lang('received_amount') ?></h4>
                            <h3 class="amount text-danger cursor-pointer"><strong>
                                    <?php
                        //            $get_curency = $this->basic_model->check_by(array('dp_id' => $customer_details->dp_id), 'tbl_customer');
                        //            $curency = $this->basic_model->check_by(array('code' => $get_curency->currency), 'tbl_currencies');
                                    ?><?//= display_money($this->basic_model->customer_paid($customer_details->dp_id), $curency->symbol); ?>
                                </strong></h3>
                            <div style="display: inline-block">
                                <div id="easypie3" data-percent="<?//= $perc_paid ?>" class="easypie-chart">
                                    <span class="h2"><?= $perc_paid ?>%</span>
                                    <div class="easypie-text"><?= lang('paid') ?></div>
                                </div>
                            </div>
                        </div>

                        <!-- Details END -->
                    </div>
                    <div class="panel-footer">
                        <span><?= lang('invoice_amount') ?>: <strong
                                class="label label-primary">
                                <?//= display_money($customer_payable, $curency->symbol); ?>
                            </strong></span>
                        <span class="text-danger pull-right">
                            <?= lang('outstanding') ?>
                            :<strong
                                class="label label-danger"> <?//= display_money($customer_outstanding, $curency->symbol) ?></strong>
                        </span>
                    </div>
                </div>
            </div>

            <!--            *************** contact tab start ************-->
            <div class="tab-pane <?= $url == 'add_contacts' ? 'active' : '' ?>" id="contacts"
                 style="position: relative;">
                <?php if (0): //!empty($company)): ?>
                    <?php include_once 'assets/admin-ajax.php'; ?>
                    <?php
                    $edited = can_action('4', 'edited');
                    if (!empty($edited)) {
                        ?>
                        <form role="form" data-parsley-validate="" novalidate="" enctype="multipart/form-data" id="form"
                              action="<?php echo base_url(); ?>admin/customer/save_contact/<?php
                              if (!empty($account_details)) {
                                  echo $account_details->user_id;
                              }
                              ?>" method="post" class="form-horizontal  ">

                            <div class="panel panel-custom">
                                <!-- Default panel contents -->
                                <div class="panel-heading">
                                    <div class="panel-title">
                                       111 <?= lang('add_contact') ?>.
                                        <a href="<?= base_url() ?>admin/customer/customer_details/<?= $customer_details->dp_id ?>"
                                           class="btn-sm pull-right">Return to Details</a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-sm-8">
                                        <input type="hidden" name="r_url"
                                               value="<?= base_url() ?>admin/customer/customer_details/<?= $company ?>">
                                        <input type="hidden" name="company" value="<?= $company ?>">
                                        <input type="hidden" name="role_id" value="2">
                                        <input type="hidden" id="user_id" value="<?php
                                        if (!empty($account_details)) {
                                            echo $account_details->user_id;
                                        }
                                        ?>">
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"><?= lang('full_name') ?> <span
                                                    class="text-danger"> *</span></label>
                                            <div class="col-lg-7">
                                                <input type="text" class="form-control" value="<?php
                                                if (!empty($account_details)) {
                                                    echo $account_details->fullname;
                                                }
                                                ?>" placeholder="E.g John Doe" name="fullname" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"><?= lang('email') ?><span
                                                    class="text-danger"> *</span></label>
                                            <div class="col-lg-7">
                                                <input class="form-control" id="check_email_addrees" type="email"
                                                       value="<?php
                                                       if (!empty($user_info)) {
                                                           echo $user_info->email;
                                                       }
                                                       ?>" placeholder="me@domin.com" name="email" required>
                                                <span id="email_addrees_error" class="required"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"><?= lang('phone') ?> </label>
                                            <div class="col-lg-7">
                                                <input type="text" class="form-control" value="<?php
                                                if (!empty($account_details)) {
                                                    echo $account_details->phone;
                                                }
                                                ?>" name="phone" placeholder="+52 782 983 434">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"><?= lang('mobile') ?> <span
                                                    class="text-danger"> *</span></label>
                                            <div class="col-lg-7">
                                                <input type="text" class="form-control" value="<?php
                                                if (!empty($account_details)) {
                                                    echo $account_details->mobile;
                                                }
                                                ?>" name="mobile" placeholder="+8801723611125">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"><?= lang('skype_id') ?> </label>
                                            <div class="col-lg-7">
                                                <input type="text" class="form-control" value="<?php
                                                if (!empty($account_details)) {
                                                    echo $account_details->skype;
                                                }
                                                ?>" name="skype" placeholder="john">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"><?= lang('language') ?></label>
                                            <div class="col-lg-7">
                                                <select name="language" class="form-control">
                                                    <?php foreach ($languages as $lang) : ?>
                                                        <option value="<?= $lang->name ?>"<?php
                                                        if (!empty($account_details->language) && $account_details->language == $lang->name) {
                                                            echo 'selected="selected"';
                                                        } else {
                                                            echo($this->config->item('language') == $lang->name ? ' selected="selected"' : '');
                                                        }
                                                        ?>><?= ucfirst($lang->name) ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-4 control-label"><?= lang('locale') ?></label>
                                            <div class="col-lg-7">
                                                <select class="  form-control" name="locale">
                                                    <?php foreach ($locales as $loc) : ?>
                                                        <option lang="<?= $loc->code ?>"
                                                                value="<?= $loc->locale ?>"<?= ($this->config->item('locale') == $loc->locale ? ' selected="selected"' : '') ?>><?= $loc->name ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php

                                        if (!empty($account_details->direction)) {
                                            $direction = $account_details->direction;
                                        } else {
                                            $RTL = config_item('RTL');
                                            if (!empty($RTL)) {
                                                $direction = 'rtl';
                                            }
                                        }
                                        ?>
                                        <div class="form-group">
                                            <label for="direction"
                                                   class="control-label col-sm-4"><?= lang('direction') ?></label>
                                            <div class="col-sm-7">
                                                <select name="direction" class="selectpicker"
                                                        data-width="100%">
                                                    <option <?php
                                                    if (!empty($direction)) {
                                                        echo $direction == 'ltr' ? 'selected' : '';
                                                    }
                                                    ?> value="ltr"><?= lang('ltr') ?></option>
                                                    <option <?php
                                                    if (!empty($direction)) {
                                                        echo $direction == 'rtl' ? 'selected' : '';
                                                    }
                                                    ?> value="rtl"><?= lang('rtl') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <?php if (empty($account_details)): ?>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label"><?= lang('username') ?> <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-lg-7">
                                                    <input class="form-control" id="check_username" type="text"
                                                           value="<?= set_value('username') ?>" placeholder="johndoe"
                                                           name="username" required>
                                                    <div class="required" id="check_username_error"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label"><?= lang('password') ?> <span
                                                        class="text-danger"> *</span></label>
                                                <div class="col-lg-7">
                                                    <input type="password" class="form-control" id="new_password"
                                                           value="<?= set_value('password') ?>" name="password"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label"><?= lang('confirm_password') ?>
                                                    <span
                                                        class="text-danger"> *</span></label>
                                                <div class="col-lg-7">
                                                    <input type="password" class="form-control"
                                                           data-parsley-equalto="#new_password"
                                                           value="<?= set_value('confirm_password') ?>"
                                                           name="confirm_password" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label
                                                    class="col-lg-4 control-label"><?= lang('send_email') . ' ' . lang('password') ?></label>
                                                <div class="col-lg-6">
                                                    <div class="checkbox c-checkbox">
                                                        <label class="needsclick">
                                                            <input type="checkbox" name="send_email_password">
                                                            <span class="fa fa-check"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="panel ">
                                            <div class="panel-title">
                                                <strong><?= lang('permission') ?></strong>
                                            </div>
                                        </div>
                                        <?php
                                        $all_customer_menu = $this->db->where('parent', 0)->order_by('sort')->get('tbl_customer_menu')->result();
                                        if (!empty($user_info)) {
                                            $user_menu = $this->db->where('user_id', $user_info->user_id)->get('tbl_customer_role')->result();
                                        }
                                        foreach ($all_customer_menu as $key => $v_menu) {
                                            ?>
                                            <div class="form-group">
                                                <label
                                                    class="col-lg-6 control-label"><?= lang($v_menu->label) ?></label>
                                                <div class="col-lg-5 checkbox">
                                                    <input data-id="" data-toggle="toggle"
                                                           name="<?= $v_menu->label ?>"
                                                           value="<?= $v_menu->menu_id ?>" <?php
                                                    if (!empty($user_menu)) {
                                                        foreach ($user_menu as $v_u_menu) {
                                                            if ($v_u_menu->menu_id == $v_menu->menu_id) {
                                                                echo 'checked';
                                                            }
                                                        }
                                                    } ?> data-on="<?= lang('yes') ?>" data-off="<?= lang('no') ?>"
                                                           data-onstyle="success btn-xs"
                                                           data-offstyle="danger btn-xs" type="checkbox">
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"></label>
                                    <div class="col-lg-4">
                                        <button type="submit" id="new_uses_btn"
                                                class="btn btn-primary btn-block"><?= lang('save') . ' ' . lang('customer_contact') ?></button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    <?php } ?>
                <?php else: ?>
                    <section class="panel panel-custom">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong><?= $customer_details->co_name ?> - 수당설정</strong>
                            </div>
                        </div>
                        <div class="panel-body">
                        <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>항목</th>
                                <th class="col-sm-2">금액</th>
                                <th class="col-sm-1">작업 </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($customer_notes)) {
                                foreach ($customer_notes as $v_notes) {
                                    $n_user = $this->db->where('user_id', $v_notes->added_by)->get('tbl_users')->row();
                                    if (empty($n_user)) {
                                        $n_user->fullname = '-';
                                        $n_url = '#';
                                    } else {
                                        $n_url = base_url() . 'admin/user/user_details/' . $n_user->user_id;
                                    }
                                    ?>
                                    <tr>
                                        <td><a class="text-info"
                                               href="<?= base_url() ?>admin/customer/customer_details/<?= $customer_details->dp_id . '/notes/' . $v_notes->notes_id ?>"><?= $v_notes->notes ?></a>
                                        </td>
                                        <td>

                                            <a href="<?= $n_url ?>"> <?= $n_user->username ?></a>
                                        </td>
                                        <td><?= strftime(config_item('date_format'), strtotime($v_notes->added_date)) . ' ' . display_time($v_notes->added_date); ?> </td>
                                        <td>
                                            <?= btn_edit('admin/customer/customer_details/' . $customer_details->dp_id . '/notes/' . $v_notes->notes_id) ?>
                                            <?php echo btn_delete('admin/customer/delete_notes/' . $v_notes->notes_id . '/' . $customer_details->dp_id); ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        </div>
                    </section>
                <?php endif ?>

            </div>
            <div class="tab-pane <?= $url == 'notes' ? 'active' : '' ?>" id="notes" style="position: relative;">
                <section class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>
                                <?= $customer_details->co_name ?> - 공제설정
                            </strong>
                            <button id="new_notes"
                                    class="btn btn-xs pull-right b0"><?= lang('new') . ' ' . lang('notes') ?></button>
                        </div>
                    </div>
                    <?php
                    if ($url == 'notes') {
                        $notes_id = $this->uri->segment(6);
                        $notes_info = $this->db->where('notes_id', $notes_id)->get('tbl_notes')->row();
                    } else {
                        $notes_id = null;
                    }

                    ?>
                    <div class="panel-body">
                        <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>항목</th>
                                <th class="col-sm-2">금액</th>
                                <th class="col-sm-1">작업 </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($customer_notes)) {
                                foreach ($customer_notes as $v_notes) {
                                    $n_user = $this->db->where('user_id', $v_notes->added_by)->get('tbl_users')->row();
                                    if (empty($n_user)) {
                                        $n_user->fullname = '-';
                                        $n_url = '#';
                                    } else {
                                        $n_url = base_url() . 'admin/user/user_details/' . $n_user->user_id;
                                    }
                                    ?>
                                    <tr>
                                        <td><a class="text-info"
                                               href="<?= base_url() ?>admin/customer/customer_details/<?= $customer_details->dp_id . '/notes/' . $v_notes->notes_id ?>"><?= $v_notes->notes ?></a>
                                        </td>
                                        <td>

                                            <a href="<?= $n_url ?>"> <?= $n_user->username ?></a>
                                        </td>
                                        <td><?= strftime(config_item('date_format'), strtotime($v_notes->added_date)) . ' ' . display_time($v_notes->added_date); ?> </td>
                                        <td>
                                            <?= btn_edit('admin/customer/customer_details/' . $customer_details->dp_id . '/notes/' . $v_notes->notes_id) ?>
                                            <?php echo btn_delete('admin/customer/delete_notes/' . $v_notes->notes_id . '/' . $customer_details->dp_id); ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>

        </div>
    </div>
</div>
