<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();
$mdate = date('Y-m-d');
$last_7_days = date('Y-m-d', strtotime('today - 7 days'));
$all_goal_tracking = $this->basic_model->get_permission('tbl_goal_tracking');



$curency = $this->basic_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');

if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
    ?>
	<style>
/* Ensure that the demo table scrolls */
	table > thead > tr > th {
		vertical-align:bottom;
		border-bottom:1px solid #eee;
		border-top:0px solid #eee;
		font-weight : none;
	}
    th, td {
        white-space: nowrap;
        padding-left: 10px !important;
        padding-right: 10px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
	</style>
<script>
	function selectTruck(params) {
	  window.open('<?php echo base_url(); ?>admin/asset/select_truck/'+params, 'winTR', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
	function selectPartner(params) {
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/coop/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
	function goSearch(val,page,list_mode) {
		document.myform.page.value = page;
		document.myform.list_mode.value = list_mode;
		if(val == 'all') 
			document.myform.action = "<?php echo base_url() ?>admin/basic/partner_list_all";
		else
			document.myform.action = "<?php echo base_url() ?>admin/basic/partner_list";
		document.myform.submit();
	}
	function setPartnerStatus(dp_id,status) {
		document.myform.tdp_id.value = dp_id;
		document.myform.action = "<?php echo base_url() ?>admin/basic/partner_set_status/"+dp_id+"/"+status+"/";
		document.myform.submit();
	}
</script>

            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/basic/partner_list"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
					  <input type="hidden" name="list_mode" value="<?= $list_mode ?>">
					  <input type="hidden" name="page" value="<?php if(!empty($page)) echo $page;?>">
    <!-- 검색 시작 -->
	<table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
        <tr>
          <td align="left" valign="top" bgcolor="#ffffff">
            <table border="0" width="100%" bgcolor="#ffffff">
              <tr>
                <td width="12%" height="20" align="center" bgcolor="#efefef">실수요처</td>
                <td width="15%">
					<select name="co_code" id="vco_code" style="width:100%;" class="form-control input-sm" onChange="goSearch(this.value,'<?=$page?>','<?=$list_mode?>');">
                 <option value="all" <?=($co_code=="all")?"selected":""?> value="">전체</option>
					<option  <?=($co_code=="wmjd")?"selected":""?> value="wmjd" > 우체국물류지원단 </option>
					<option  <?=($co_code=="0003")?"selected":""?> value="0003" >(주)델타온</option>
					<option  <?=($co_code=="0001")?"selected":""?> value="0001" >(주)아이디일일구닷컴</option>
					<option  <?=($co_code=="degugyodoso")?"selected":""?> value="degugyodoso" >KPI대구교도소</option>
					<option  <?=($co_code=="OUT444")?"selected":""?> value="OUT444" >OUT</option>
					<option  <?=($co_code=="kn4021")?"selected":""?> value="kn4021">강남우체국</option>
					<option  <?=($co_code=="kd2828")?"selected":""?> value="kd2828" >강동우체국</option>
					<option  <?=($co_code=="ks8900")?"selected":""?> value="ks8900" >강서우체국</option>
					<option  <?=($co_code=="gong-101")?"selected":""?> value="gong-101" >개별현장</option>
					<option  <?=($co_code=="kkj5900")?"selected":""?> value="kkj5900" >경기광주우체국</option>
					<option  <?=($co_code=="gyounggyh")?"selected":""?> value="gyounggyh" >경기혈액원</option>
					<option  <?=($co_code=="ks3704")?"selected":""?> value="ks3704" >경산우체국</option>
					<option  <?=($co_code=="kj0114")?"selected":""?> value="kj0114" >경주우체국</option>
					<option  <?=($co_code=="ky9400")?"selected":""?> value="ky9400" >계양우체국</option>
					<option  <?=($co_code=="ka0014")?"selected":""?> value="ka0014" >관악우체국</option>
					<option  <?=($co_code=="km1114")?"selected":""?> value="km1114" >광명우체국</option>
					<option  <?=($co_code=="ks2114")?"selected":""?> value="ks2114" >광산우체국</option>
					<option  <?=($co_code=="kj0280")?"selected":""?> value="kj0280" >광주우체국</option>
					<option  <?=($co_code=="guro1501")?"selected":""?> value="guro1501" >구로구청</option>
					<option  <?=($co_code=="km1533")?"selected":""?> value="km1533" >구미우체국</option>
					<option  <?=($co_code=="gp8014")?"selected":""?> value="gp8014" >군포우체국</option>
					<option  <?=($co_code=="icb1512")?"selected":""?> value="icb1512" >근로복지인천병원</option>
					<option  <?=($co_code=="kj6014")?"selected":""?> value="kj6014" >금정우체국</option>
					<option  <?=($co_code=="kia5071")?"selected":""?> value="kia5071" >기아자동차협력</option>
					<option  <?=($co_code=="hangi1501")?"selected":""?> value="hangi1501" >기초과학연구원</option>
					<option  <?=($co_code=="kc0014")?"selected":""?> value="kc0014" >김천우체국</option>
					<option  <?=($co_code=="kp0014")?"selected":""?> value="kp0014" >김포우체국</option>
					<option  <?=($co_code=="kh9009")?"selected":""?> value="kh9009" >김해우체국</option>
					<option  <?=($co_code=="nd0801")?"selected":""?> value="nd0801" >남동우체국</option>
					<option  <?=($co_code=="nbs9000")?"selected":""?> value="nbs9000" >남부산우체국</option>
					<option  <?=($co_code=="nambuh")?"selected":""?> value="nambuh" >남부혈액검사센터</option>
					<option  <?=($co_code=="rudrlskadid")?"selected":""?> value="rudrlskadid" >남양우체국</option>
					<option  <?=($co_code=="nyj0700")?"selected":""?> value="nyj0700" >남양주우체국</option>
					<option  <?=($co_code=="nws2014")?"selected":""?> value="nws2014" >남울산우체국</option>
					<option  <?=($co_code=="nic0500")?"selected":""?> value="nic0500" >남인천우체국</option>
					<option  <?=($co_code=="nw3226")?"selected":""?> value="nw3226" >노원우체국</option>
					<option  <?=($co_code=="dgds4614")?"selected":""?> value="dgds4614" >대구달서우체국</option>
					<option  <?=($co_code=="dg2000")?"selected":""?> value="dg2000" >대구우체국</option>
					<option  <?=($co_code=="dd2400")?"selected":""?> value="dd2400" >대덕우체국</option>
					<option  <?=($co_code=="dj7066")?"selected":""?> value="dj7066" >대전우체국</option>
					<option  <?=($co_code=="redcross-dj")?"selected":""?> value="redcross-dj" >대전충남혈액원</option>
					<option  <?=($co_code=="redcross-ws")?"selected":""?> value="redcross-ws" >대한적십자사울산혈액원</option>
					<option  <?=($co_code=="dy0014")?"selected":""?> value="dy0014" >덕양우체국</option>
					<option  <?=($co_code=="db3600")?"selected":""?> value="db3600" >도봉우체국</option>
					<option  <?=($co_code=="ddg1900")?"selected":""?> value="ddg1900" >동대구우체국</option>
					<option  <?=($co_code=="dl7014")?"selected":""?> value="dl7014" >동래우체국</option>
					<option  <?=($co_code=="dsw0532")?"selected":""?> value="dsw0532" >동수원우체국</option>
					<option  <?=($co_code=="dws0100")?"selected":""?> value="dws0100" >동울산우체국</option>
					<option  <?=($co_code=="dj0385")?"selected":""?> value="dj0385" >동작우체국</option>
					<option  <?=($co_code=="djj3842")?"selected":""?> value="djj3842" >동전주우체국</option>
					<option  <?=($co_code=="dca6310")?"selected":""?> value="dca6310" >동천안우체국</option>
					<option  <?=($co_code=="ds1610")?"selected":""?> value="ds1610" >둔산우체국</option>
					<option  <?=($co_code=="ms0044")?"selected":""?> value="ms0044" >마산우체국</option>
					<option  <?=($co_code=="mshp0004")?"selected":""?> value="mshp0004" >마산합포우체국</option>
					<option  <?=($co_code=="mp0014")?"selected":""?> value="mp0014" >마포우체국</option>
					<option  <?=($co_code=="anstks")?"selected":""?> value="anstks" >문산우체국</option>
					<option  <?=($co_code=="seoul")?"selected":""?> value="seoul" >번호예치</option>
					<option  <?=($co_code=="inkd01")?"selected":""?> value="inkd01" >본사</option>
					<option  <?=($co_code=="bs3000")?"selected":""?> value="bs3000" >부산우체국</option>
					<option  <?=($co_code=="bsj0700")?"selected":""?> value="bsj0700" >부산진우체국</option>
					<option  <?=($co_code=="bc7124")?"selected":""?> value="bc7124" >부천우체국</option>
					<option  <?=($co_code=="bp4000")?"selected":""?> value="bp4000" >부평우체국</option>
					<option  <?=($co_code=="bgj9114")?"selected":""?> value="bgj9114" >북광주우체국</option>
					<option  <?=($co_code=="bdg3024")?"selected":""?> value="bdg3024" >북대구우체국</option>
					<option  <?=($co_code=="bbs0864")?"selected":""?> value="bbs0864" >북부산우체국</option>
					<option  <?=($co_code=="bd2900")?"selected":""?> value="bd2900" >분당우체국</option>
					<option  <?=($co_code=="ss3331")?"selected":""?> value="ss3331" >사상우체국</option>
					<option  <?=($co_code=="sh7000")?"selected":""?> value="sh7000" >사하우체국</option>
					<option  <?=($co_code=="ssd")?"selected":""?> value="ssd" >상수도사업본부</option>
					<option  <?=($co_code=="skj7190")?"selected":""?> value="skj7190" >서광주우체국</option>
					<option  <?=($co_code=="sgp3915")?"selected":""?> value="sgp3915" >서귀포우체국</option>
					<option  <?=($co_code=="seodemoon")?"selected":""?> value="seodemoon" >서대문구청</option>
					<option  <?=($co_code=="sdm9114")?"selected":""?> value="sdm9114" >서대문우체국</option>
					<option  <?=($co_code=="sdj3400")?"selected":""?> value="sdj3400" >서대전우체국</option>
					<option  <?=($co_code=="ssw01")?"selected":""?> value="ssw01" >서수원우체국</option>
					<option  <?=($co_code=="redcross-s")?"selected":""?> value="redcross-s" >서울남부혈액원</option>
					<option  <?=($co_code=="sdhw")?"selected":""?> value="sdhw" >서울동부혈액원</option>
					<option  <?=($co_code=="seomun1501")?"selected":""?> value="seomun1501" >서울문화재단</option>
					<option  <?=($co_code=="seoult")?"selected":""?> value="seoult" >서울화물공제조합</option>
					<option  <?=($co_code=="sic9114")?"selected":""?> value="sic9114" >서인천우체국</option>
					<option  <?=($co_code=="scj5720")?"selected":""?> value="scj5720" >서청주우체국</option>
					<option  <?=($co_code=="sn0014")?"selected":""?> value="sn0014" >성남우체국</option>
					<option  <?=($co_code=="sb0123")?"selected":""?> value="sb0123" >성북우체국</option>
					<option  <?=($co_code=="center1")?"selected":""?> value="center1" >센타프라자</option>
					<option  <?=($co_code=="sp2700")?"selected":""?> value="sp2700" >송파우체국</option>
					<option  <?=($co_code=="sw1300")?"selected":""?> value="sw1300" >수원우체국</option>
					<option  <?=($co_code=="sj0511")?"selected":""?> value="sj0511" >수지우체국</option>
					<option  <?=($co_code=="sh2700")?"selected":""?> value="sh2700" >시흥우체국</option>
					<option  <?=($co_code=="cjdt01")?"selected":""?> value="cjdt01" >씨제이대한통운(주)</option>
					<option  <?=($co_code=="as2114")?"selected":""?> value="as2114" >아산우체국</option>
					<option  <?=($co_code=="as0014")?"selected":""?> value="as0014" >안산우체국</option>
					<option  <?=($co_code=="as7900")?"selected":""?> value="as7900" >안성우체국</option>
					<option  <?=($co_code=="ay9788")?"selected":""?> value="ay9788" >안양우체국</option>
					<option  <?=($co_code=="dkswnddncprnr")?"selected":""?> value="dkswnddncprnr" >안중우체국</option>
					<option  <?=($co_code=="yangsan")?"selected":""?> value="yangsan" >양산우체국</option>
					<option  <?=($co_code=="yc0014")?"selected":""?> value="yc0014" >양천우체국</option>
					<option  <?=($co_code=="yyd0014")?"selected":""?> value="yyd0014" >여의도우체국</option>
					<option  <?=($co_code=="yj0888")?"selected":""?> value="yj0888" >연제우체국</option>
					<option  <?=($co_code=="yd5550")?"selected":""?> value="yd5550" >영도우체국</option>
					<option  <?=($co_code=="os0004")?"selected":""?> value="os0004" >오산우체국</option>
					<option  <?=($co_code=="ys0004")?"selected":""?> value="ys0004" >용산우체국</option>
					<option  <?=($co_code=="yi2849")?"selected":""?> value="yi2849" >용인우체국</option>
					<option  <?=($co_code=="kypost1")?"selected":""?> value="kypost1" >우정본(경북청)</option>
					<option  <?=($co_code=="ruddlscjd")?"selected":""?> value="ruddlscjd" >우정본(경인청)</option>
					<option  <?=($co_code=="pupost1")?"selected":""?> value="pupost1" >우정본(부산청)</option>
					<option  <?=($co_code=="spost1")?"selected":""?> value="spost1" >우정본(서울청)</option>
					<option  <?=($co_code=="jnpost1")?"selected":""?> value="jnpost1" >우정본(전남청)</option>
					<option  <?=($co_code=="jbcpost1")?"selected":""?> value="jbcpost1" >우정본(전북청)</option>
					<option  <?=($co_code=="jjpost1")?"selected":""?> value="jjpost1" >우정본(제주청)</option>
					<option  <?=($co_code=="wjs0001")?"selected":""?> value="wjs0001" >우정사업본부</option>
					<option  <?=($co_code=="ws5801")?"selected":""?> value="ws5801" >울산우체국</option>
					<option  <?=($co_code=="ys8114")?"selected":""?> value="ys8114" >유성우체국</option>
					<option  <?=($co_code=="ep3514")?"selected":""?> value="ep3514" >은평우체국</option>
					<option  <?=($co_code=="humanplaza-1")?"selected":""?> value="humanplaza-1" >은평휴먼프라자Ⅰ</option>
					<option  <?=($co_code=="yjb5556")?"selected":""?> value="yjb5556" >의정부우체국</option>
					<option  <?=($co_code=="ic2820")?"selected":""?> value="ic2820" >이천우체국</option>
					<option  <?=($co_code=="ic8155")?"selected":""?> value="ic8155" >인천우체국</option>
					<option  <?=($co_code=="incheonjodal")?"selected":""?> value="incheonjodal" >인천지방조달청</option>
					<option  <?=($co_code=="is0205")?"selected":""?> value="is0205" >일산우체국</option>
					<option  <?=($co_code=="jj2635")?"selected":""?> value="jj2635" >전주우체국</option>
					<option  <?=($co_code=="jjwj5200")?"selected":""?> value="jjwj5200" >제주우편집중국</option>
					<option  <?=($co_code=="junongangh")?"selected":""?> value="junongangh" >중앙혈액검사센터</option>
					<option  <?=($co_code=="jh0005")?"selected":""?> value="jh0005" >진해우체국</option>
					<option  <?=($co_code=="cw1114")?"selected":""?> value="cw1114" >창원우체국</option>
					<option  <?=($co_code=="ca2660")?"selected":""?> value="ca2660" >천안우체국</option>
					<option  <?=($co_code=="cj0014")?"selected":""?> value="cj0014" >청주우체국</option>
					<option  <?=($co_code=="0002")?"selected":""?> value="0002" >케이티지엘에스</option>
					<option  <?=($co_code=="ty2016")?"selected":""?> value="ty2016" >통영우체국</option>
					<option  <?=($co_code=="pj9004")?"selected":""?> value="pj9004" >파주우체국</option>
					<option  <?=($co_code=="pt3121")?"selected":""?> value="pt3121" >평택우체국</option>
					<option  <?=($co_code=="pc1302")?"selected":""?> value="pc1302" >포천우체국</option>
					<option  <?=($co_code=="ph9937")?"selected":""?> value="ph9937" >포항우체국</option>
					<option  <?=($co_code=="hn2007")?"selected":""?> value="hn2007" >하남우체국</option>
					<option  <?=($co_code=="hsk88")?"selected":""?> value="hsk88" >한국승강기안전공단</option>
					<option  <?=($co_code=="hanjun2")?"selected":""?> value="hanjun2" >한국전력(강원)</option>
					<option  <?=($co_code=="dghj001")?"selected":""?> value="dghj001" >한국전력(대구)</option>
					<option  <?=($co_code=="hsk20")?"selected":""?> value="hsk20" >한승공강원지사</option>
					<option  <?=($co_code=="hsk16")?"selected":""?> value="hsk16" >한승공경남동부</option>
					<option  <?=($co_code=="hsk10")?"selected":""?> value="hsk10" >한승공경북서부</option>
					<option  <?=($co_code=="hsk15")?"selected":""?> value="hsk15" >한승공대구서부</option>
					<option  <?=($co_code=="hsk06")?"selected":""?> value="hsk06" >한승공부천지사</option>
					<option  <?=($co_code=="hskkd")?"selected":""?> value="hskkd" >한승공서울강동</option>
					<option  <?=($co_code=="hsksb")?"selected":""?> value="hsksb" >한승공서울본부</option>
					<option  <?=($co_code=="hskss")?"selected":""?> value="hskss" >한승공서울서부</option>
					<option  <?=($co_code=="hsksc")?"selected":""?> value="hsksc" >한승공서울서초</option>
					<option  <?=($co_code=="hsk05")?"selected":""?> value="hsk05" >한승공안산지사</option>
					<option  <?=($co_code=="hsk13")?"selected":""?> value="hsk13" >한승공영남본부</option>
					<option  <?=($co_code=="hsk03")?"selected":""?> value="hsk03" >한승공용인지사</option>
					<option  <?=($co_code=="hskws")?"selected":""?> value="hskws" >한승공울산지사</option>
					<option  <?=($co_code=="hskjd")?"selected":""?> value="hskjd" >한승공전남동부</option>
					<option  <?=($co_code=="hskjs")?"selected":""?> value="hskjs" >한승공전남서부</option>
					<option  <?=($co_code=="jejuhsk")?"selected":""?> value="jejuhsk" >한승공제주지사</option>
					<option  <?=($co_code=="hskca")?"selected":""?> value="hskca" >한승공천안지사</option>
					<option  <?=($co_code=="hskcn")?"selected":""?> value="hskcn" >한승공충남지사</option>
					<option  <?=($co_code=="hsk17")?"selected":""?> value="hsk17" >한승공충북지사</option>
					<option  <?=($co_code=="hsk07")?"selected":""?> value="hsk07" >한승공파주지사</option>
					<option  <?=($co_code=="hwd4500")?"selected":""?> value="hwd4500" >해운대우체국</option>
					<option  <?=($co_code=="ws9516")?"selected":""?> value="ws9516" >화성우체국</option>
					<option  <?=($co_code=="goldb1")?"selected":""?> value="goldb1" >황금빌딩</option>
					</select>				
				</td>
                <td width="12%" height="20" align="center" bgcolor="#efefef">지입사</td>
                <td width="15%">
					<select name="vX" id="vX" style="width:100%;" class="form-control input-sm">
                 <option value="" <?=($co_code=="")?"selected":""?> value="">선택</option>
<?php
if (!empty($all_jiip_info)) {
	foreach ($all_jiip_info as $jiip_details) {
?>
					<option value="<?=$jiip_details->co_name?>" <?=($vX == $jiip_details->co_name) ? "selected" : ""?>><?=$jiip_details->co_name?></option>
<?php
	}
}
?>
					</select>				
				</td>
                <td style="padding-left:5px;padding-top:10px;" align="center" bgcolor="#ffffff">


					<a href="javascript:goSearch('<?php if(!empty($co_code)) echo $co_code;?>','<?=$page?>','<?=$list_mode?>');" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-search"> </i> 검색</span>
					</a>
					<a href="javascript:goPrintAllTax()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-print"> </i> 엑셀 저장</span>
					</a>
					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 엑셀 가져오기</span>
					</a>

				</td>
					
			  </tr>
            </table>
		  </td>
        </tr>
      </table>
	  </form>
      <!-- 검색 끝 -->

	
	
	<div class="col-sm-12 bg-white p0">
        <div class="col-md-7">
            <div class="row row-table pv-lg">
                <div class="col-xs-4">
					<a href="javascript:goSearch('<?php if(!empty($co_code)) echo $co_code;?>','<?=$page?>','ALL');" tabindex="0" class="dt-button buttons-print btn btn-<?=($list_mode=="ALL")?"success":"basic"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 전체파트너현황 </span>
					</a>
                </div>
                <div class="col-xs-4">
					<a href="javascript:goSearch('<?php if(!empty($co_code)) echo $co_code;?>','<?=$page?>','IN');" tabindex="0" class="dt-button buttons-print btn btn-<?=($list_mode=="IN")?"success":"basic"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 계약중파트너현황 </span>
					</a>
                </div>
                <div class="col-xs-4">
					<a href="javascript:goSearch('<?php if(!empty($co_code)) echo $co_code;?>','<?=$page?>','WT');" tabindex="0" class="dt-button buttons-print btn btn-<?=($list_mode=="WT"||$list_mode=="WTU")?"success":"basic"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 계약종료대기현황 </span>
					</a>
                </div>
<?php if($list_mode=="WT" || $list_mode=="WTU") { ?>
                <div class="col-xs-4">
					<a href="javascript:goSearch('<?php if(!empty($co_code)) echo $co_code;?>','<?=$page?>','WTU');" tabindex="0" class="dt-button buttons-print btn btn-warning btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 챠량정보갱신 </span>
					</a>
                </div>
<?php } ?>
                <div class="col-xs-4">
					<a href="javascript:goSearch('<?php if(!empty($co_code)) echo $co_code;?>','<?=$page?>','EP');" tabindex="0" class="dt-button buttons-print btn btn-<?=($list_mode=="EP")?"success":"basic"?> btn-xs mr">
					<span><i class="fa fa-address-book"> </i> 계약종료현황</span>
					</a>
                </div>

            </div>

        </div>
    </div>
<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip"
             data-title="<?php echo lang('filter_by'); ?>">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-filter" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-left"
                style="width:300px;<?php if (!empty($search_by) && $search_by == 'group') {
                    echo 'display:block';
                } ?>">
                <li class="<?php
                if (empty($search_by)) {
                    echo 'active';
                } ?>"><a
                        href="<?= base_url() ?>admin/client/manage_partner"><?php echo lang('all'); ?></a>
                </li>
                <li class="divider"></li>
                <?php if (count($all_customer_group) > 0) { ?>
                    <li class="dropdown-submenu pull-left groups <?php if (!empty($id)) {
                        if ($search_by == 'group') {
                            echo 'active';
                        }
                    } ?>">
                        <a href="#" tabindex="-1"><?php echo lang('customer_group'); ?></a>
                        <ul class="dropdown-menu dropdown-menu-left"
                            style="<?php if (!empty($search_by) && $search_by == 'group') {
                                echo 'display:block';
                            } ?>">
                            <?php foreach ($all_customer_group as $group) {
                                ?>
                                <li class="<?php if (!empty($id)) {
                                    if ($search_by == 'group') {
                                        if ($id == $group->customer_group_id) {
                                            echo 'active';
                                        }
                                    }
                                } ?>">
                                    <a href="<?= base_url() ?>admin/client/manage_partner/group/<?php echo $group->customer_group_id; ?>"><?php echo $group->customer_group; ?></a>
                                </li>
                            <?php }
                            ?>
                        </ul>
                    </li>
                    <div class="clearfix"></div>
                    <li class="divider"></li>
                <?php } ?>
            </ul>
        </div>
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#client_list"
                                                                   data-toggle="tab">파트너관리</a></li>



                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_partner"
                                                                   data-toggle="tab">신규등록</a></li>
                <li><a style="background-color: #1797be;color: #ffffff"
                       href="<?= base_url() ?>admin/client/import">데이터가져오기</a>
                </li>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="client_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="DataTables" class="table table-striped DataTables">
                                <thead>
        <tr align="center" bgcolor="#e0e7ef">
          <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
<?php if($list_mode=="WT" || $list_mode=="WTU") { ?><td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>계약종료</td><?php } ?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>실수요처</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="27">파트너 정보</td>
          <td width="160px" style="color:#ffffff;background-color: #777777;border-left:1px solid #eee;" rowspan="2"></td>
        </tr>
        <tr align="center" bgcolor="#e0e7ef">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">파트너명</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">대표</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">운전자</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">직위</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">주민등록번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">운전면허번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종사자번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약시작일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약종료일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과세여부</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세누적</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자등록번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">업태</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종목</td>
          <td width="300px" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업장주소</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차량번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">용도</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차대번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">지입사명</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">핸드폰번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">PDA번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">은행</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계좌번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">예금주</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관계</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">이메일</td>
          <td width="300px" style="color:#ffffff;background-color: #777777;">주소</td>
        </tr>
                                </thead>

                                <tbody>

                                <?php
                                if (!empty($all_partner_info)) {
                                    foreach ($all_partner_info as $partner_details) {
										//차량정보
										$truck = $this->db->where('idx', $partner_details->tr_id)->get('tbl_asset_truck')->row();
										if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;

 
										// 실수요처
										$rco = $this->db->where('mb_type', 'customer')->where('code', $partner_details->code)->get('tbl_members')->row();


//--------------------------------------------------------------------------------------------
if($list_mode=="WTU") {
					$tr_id = $partner_details->tr_id;
echo $partner_details->ceo;
echo " - trid:".$partner_details->tr_id."<br>";
					//기존 현규씨 데이터 보정(차량정보가 제데로 연결되지 않았음)
					if(!empty($partner_details->U)) {
						$tr = $this->db->where('car_1', TRIM($partner_details->U))->get('tbl_asset_truck')->row();
					echo "U:".$partner_details->U."<br>";
						if(!empty($tr->idx)) {
							$sql = "UPDATE tbl_members SET tr_id='".$tr->idx."' WHERE dp_id = '".$partner_details->dp_id."'";
							$this->db->query($sql);
					echo "sql:".$sql."<br>";
						}
					}
}
//--------------------------------------------------------------------------------------------

                                        ?>
                                        <tr>
				  <td><?= $partner_details->dp_id ?></td>
<?php if($list_mode=="WT" || $list_mode=="WTU") { ?>
				<td>

                    <span data-placement="top" data-toggle="tooltip"  title="계약종료 설정">
                        <a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/basic/set_partner_status/CLS/<?= $partner_details->dp_id ?>/<?= $page ?>/<?= $list_mode ?>/<?= $co_code ?>" class="text-default ml"><i class="fa fa-cog"></i>계약종료</a>
                    </span>
				</td>
<?php } ?>
				  <td><? if(!empty($rco->co_name)) echo $rco->co_name; ?></td>
				  <td><?= $partner_details->co_name ?></td>
				  <td><?= $partner_details->ceo ?></td>
				  <td><?= $partner_details->driver ?></td>
				  <td>
						<select name="G" id="G" class="form-control" style="width:60px;" onChange="goSet('G','<?= $partner_details->dp_id ?>',this.value);">
							<option value="팀원" <?=($partner_details->G == "팀원") ? "selected" : ""?>>팀원</option>
							<option value="조장" <?=($partner_details->G == "조장") ? "selected" : ""?>>조장</option>
							<option value="팀장" <?=($partner_details->G == "팀장") ? "selected" : ""?>>팀장</option>
							<option value="현장대리인" <?=($partner_details->G == "현장대리인") ? "selected" : ""?>>현장대리인</option>
					</select>
								  
				  
				  
				  </td>
				  <!--td><?= $partner_details->H ?></td>
				  <td><?= $partner_details->I ?></td>
				  <td><?= $partner_details->J ?></td-->
				  <td><?= $partner_details->reg_number ?></td>
				  <td><?= $partner_details->L ?></td>
				  <td><?= $partner_details->M ?></td>
				  <td><?= $partner_details->N ?></td>
				  <td><?= $partner_details->O ?></td>
				  <td align="center" style="padding-left:5px;background-color:yellow;">
						<select name="P" id="P" class="form-control" style="width:60px;" onChange="goSet('P','<?= $row[dp_id] ?>',this.value);">
							<option value="" >선택</option>
							<option value="Y" <?=($partner_details->P == "Y") ? "selected" : ""?>>Y</option>
							<option value="N" <?=($partner_details->P == "N") ? "selected" : ""?>>N</option>
							<option value="직원" <?=($partner_details->P == "직원") ? "selected" : ""?>>직원</option>
						</select>
				  </td><!-- 과세여부 -->
				  <td align="center" style="padding-left:5px;background-color:red">
                                <input data-toggle="toggle" name="acc_vat_yn" value="1" <?php
                                if (!empty($partner_details->acc_vat_yn) && $partner_details->acc_vat_yn == "Y") {
                                    echo 'checked';
                                }
                                ?> data-on="Y" data-off="N" data-onstyle="success"
                                       data-offstyle="danger" type="checkbox">

				  </td><!-- 부가세누적여부 -->
						<td><?= $partner_details->bs_number ?></td><!-- 사업자등록번호 -->
						<td><?= $partner_details->bs_type1 ?></td><!-- 업태 -->
						<td><?= $partner_details->bs_type2 ?></td><!-- 종목 -->
						<td align="left" style="padding-left:5px;"><?= $partner_details->T ?></td><!-- 사업장주소 -->

						<td><?= $partner_details->U ?></td><!-- 차량번호 -->
						<td><?= $partner_details->V ?></td><!-- 용도 -->
						<td><?= $partner_details->W ?></td><!-- 차대번호 -->
		  
					  <td><?= $partner_details->X ?></td><!-- 지입사명 -->
					  <td><?= $partner_details->co_tel ?></td>
					  <td><?= $partner_details->Z ?></td>
					  <td><?= $partner_details->AA ?></td>
					  <td><?= $partner_details->AB ?></td>
					  <td><?= $partner_details->AC ?></td>
					  <td><?= $partner_details->AD ?></td>
					  <td><?= $partner_details->email ?></td>
					  <td align="left" style="padding-left:5px;"><?= $partner_details->co_address ?></td>
					  <td align="center">

                                                <?= btn_edit('admin/basic/partner_list/edit_partner/' . $partner_details->dp_id) ?>
                                            <?php if (!empty($can_edit) && !empty($edited)) { ?>

                                            <?php }
                                            if (!empty($can_delete) && !empty($deleted)) { ?>
                                                <?php echo ajax_anchor(base_url("admin/projects/delete_project/" . $partner_details->dp_id), "<i class='btn btn-danger btn-xs fa fa-trash-o'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#table-project-" . $partner_details->dp_id)); ?>
                                            <?php } ?>
					  </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { 
						include "/home/wwwroot/default/application/views/admin/basic/_partner_edit.inc.php";
					?>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>