<?php
// 현재페이지, 총페이지수, 한페이지에 보여줄 행, URL
function get_paging($write_pages, $cur_page, $total_page, $url, $add="")
{
    $str = "";
    if ($cur_page > 1) {
        $str .= "<a href='" . $url . "1{$add}'>처음</a>";
        //$str .= "[<a href='" . $url . ($cur_page-1) . "'>이전</a>]";
    }

    $start_page = ( ( (int)( ($cur_page - 1 ) / $write_pages ) ) * $write_pages ) + 1;
    $end_page = $start_page + $write_pages - 1;

    if ($end_page >= $total_page) $end_page = $total_page;

    if ($start_page > 1) $str .= " &nbsp;<a href='" . $url . ($start_page-1) . "{$add}'>이전</a>";

    if ($total_page > 1) {
        for ($k=$start_page;$k<=$end_page;$k++) {
            if ($cur_page != $k)
                $str .= " &nbsp;<a href='$url$k{$add}'><span>$k</span></a>";
            else
                $str .= " &nbsp;<b>$k</b> ";
        }
    }

    if ($total_page > $end_page) $str .= " &nbsp;<a href='" . $url . ($end_page+1) . "{$add}'>다음</a>";

    if ($cur_page < $total_page) {
        //$str .= "[<a href='$url" . ($cur_page+1) . "'>다음</a>]";
        $str .= " &nbsp;<a href='$url$total_page{$add}'>맨끝</a>";
    }
    $str .= "";

    return $str;
}

?>

      <!-- 검색 시작 -->
      <table border="0" cellspacing="1" cellpadding="0" width="960" align="left" >
        <tr>
          <td align="left" valign="top">
            <table border="0" width="100%" cellpadding="12" cellspacing="15" bgcolor="#ffffff" class="table table-striped DataTables">
              <tr>

                <td width="20%" height="20" align="center" bgcolor="#efefef">선택조회
                </td>
                <td width="20%" height="40" align="center" bgcolor="#FFFFFF" style="">
				<select name="search_field" id="search_field" style="width:120px;"  class="form-control select_box">
                 <option value="">선택</option>
					<option value="co_name" <?=(!empty($search_field) && $search_field == "co_name") ? "selected" : ""?>>회사명</option>
					<option value="ceo" <?=(!empty($search_field) && $search_field == "ceo") ? "selected" : ""?>>대표</option>
					<option value="bs_number" <?=(!empty($search_field) && $search_field == "bs_number") ? "selected" : ""?>>사업자번호</option>
					<option value="K" <?=(!empty($search_field) && $search_field == "K") ? "selected" : ""?>>주민등록번호</option>
					<option value="co_tel" <?=(!empty($search_field) && $search_field == "co_tel") ? "selected" : ""?>>전화번호</option>
			</select>
				</td>
                <td style="padding-left:3px;" align="left" bgcolor="#ffffff"><input type="text" name="search_keyword" id="search_keyword" maxlength="20" value="<?=(empty($search_keyword))?"":$search_keyword ?>" style="width:400px;" class="form-control">
                </td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">

					<button type="button" class="btn btn-sm btn-primary" onClick="goSearch()">검색</button>

                </td>
              </tr>
            </table>
		  </td>
        </tr>
      </table>
      <!-- 검색 끝 -->
      <table width="960" border="0" cellpadding="0" cellspacing="0" class="table table-striped DataTables">
		<tr>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">No</td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">회사명</td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">운전자</td>

          <td style="color:#ffffff;background-color: #777777;text-align:center;">주민등록번호</td>
          <!--td style="color:#ffffff;background-color: #777777;text-align:center;">소재지</td-->
          <td style="color:#ffffff;background-color: #777777;text-align:center;">대표</td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">전화</td>

          <td style="color:#ffffff;background-color: #777777;text-align:center;">팩스</td>
          <td style="color:#ffffff;background-color: #777777;text-align:center;">선택</td>

		</tr>

<?
/*
$sql_common = " from az_members  ";
$sql_search = " where (mb_type='$mode') ";
if ($code) $sql_search .= " and TRIM(code)='".trim($code)."' ";
if ($B) $sql_search .= " and TRIM(B)='".trim($B)."' ";
if ($X) $sql_search .= " and TRIM(X)='".trim($X)."' ";
if ($sG) $sql_search .= " and TRIM(G)='".trim($sG)."' ";
if ($KEYWORD && $FIELD) {
	$sql_search .= " and TRIM(".$FIELD.") like '%".trim($KEYWORD)."%' ";
}



if ($sst)
    $sql_order = " order by $sst $sod ";
else
    $sql_order = " order by E asc ";

//echo $sql;
if($total_count > 0) {
	for ($i=0; $row=sql_fetch_array($result); $i++) {
*/
?>
<?php
$cnt = 0;
if (!empty($all_company_info)) {
	foreach ($all_company_info as $company_info) { 
		$row_no = $total_count - $from_record - $cnt;
		?>
        <tr bgcolor="#ffffff">
          <td height="25" align="center"><?= $row_no ?></td>
          <td align="center"><?php if(!empty($company_info->co_name)) echo $company_info->co_name; ?></td>
          <td align="center"><?php if(!empty($company_info->driver)) echo $company_info->driver; ?></td>
          <td align="center"><?php if(!empty($company_info->reg_number)) echo $company_info->reg_number; ?></td>
          <td align="center"><?php if(!empty($company_info->ceo)) echo $company_info->ceo; ?></td>
          <td align="center"><?php if(!empty($company_info->co_tel)) echo $company_info->co_tel; ?></td>
          <td align="center"><?php if(!empty($company_info->fax)) echo $company_info->fax; ?></td>
          <td align="center">
		  <a href="javascript:selectMember('<?= $company_info->dp_id ?>','<?php if(!empty($company_info->co_name)) echo $company_info->co_name; ?>','<?php if(!empty($company_info->reg_number)) echo $company_info->reg_number; ?>','<?= $company_info->bs_number ?>','<?= $company_info->bs_number ?>','<?php if(!empty($company_info->co_tel)) echo $company_info->co_tel; ?>','<?php if(!empty($company_info->D)) echo $company_info->D; ?>','<?php if(!empty($company_info->fax)) echo $company_info->fax; ?>','<?php if(!empty($company_info->code)) echo $company_info->code; ?>','<?php if(!empty($company_info->driver)) echo $company_info->driver; ?>')" class="button red" title="선택"><span class='label label-primary'> 선택 </span></a>
		  </td>
        </tr>
<?
		$cnt++;
    }
  } else {
?>
        <tr bgcolor="#ffffff">
          <td colspan="8">조회된 결과가 없습니다.</td>
        </tr>
<?
  }
?>

      </table>
      <div style="padding-top:10px;"></div>

      <!-- 페이징 시작 -->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table table-striped DataTables">
      	<tr>
      		<td height="20" align="left">
<?
$pagelist = get_paging(10, $page, $total_page, base_url()."admin/basic/select_company/".$co_type."/".$params."/");
echo $pagelist;
?>
			</td>
      	</tr>
      </table>
      <!-- 페이징 끝 -->
	  
