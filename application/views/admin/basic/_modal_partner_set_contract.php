<?php
		//if(!empty($dp->tr_id)) $tr = $this->db->where('idx', $dp->tr_id)->get('tbl_asset_truck')->row();
?>
<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"> 파트너 계약종료 설정 - <?= $dp->ceo ?> / <?=$dp->driver?> / <?php if(!empty($tr->car_1)) echo $tr->car_1; ?> / </h4>
    </div>
    <div class="modal-body wrap-modal wrap">


        <form id="form_validation"
              action="<?php echo base_url() ?>admin/basic/save_partner_status/<?php echo $dp_id; ?>/N"
              method="post" class="form-horizontal form-groups-bordered">
		<input type="hidden" name="page" value="<?php echo $page; ?>">
		<input type="hidden" name="list_mode" value="<?php echo $list_mode; ?>">
		<input type="hidden" name="co_code" value="<?php echo $co_code; ?>">
		<input type="hidden" name="tr_id" value="<?php echo $co_code; ?>">

                    <div class="form-group" id="generate_type1">
                      <div class="col-sm-12">
							<label for="field-1" class="col-sm-3 control-label">계약만료처리일
								<span
									class="required"> *</span></label>

                          <div class="col-sm-5">
							   <div class="input-group">
									<input type="text" value="<?php
									if (!empty($out_date)) {
										echo $out_date;
									}
									?>" class="form-control datepicker" name="out_date"
										   data-format="yyyy/mm/dd">

									<div class="input-group-addon">
										<a href="#"><i class="fa fa-calendar"></i></a>
									</div>
								</div>
                            </div>
                        </div>


                      <div class="col-sm-12">
							<label for="field-1" class="col-sm-3 control-label">공T/E등록일
								<span
									class="required"> *</span></label>

                    
                          <div class="col-sm-5">
							   <div class="input-group">
									<input type="text" value="<?php
									if (!empty($gongT_date)) {
										echo $gongT_date;
									}
									?>" class="form-control datepicker" name="gongT_date"
										   data-format="yyyy/mm/dd">

									<div class="input-group-addon">
										<a href="#"><i class="fa fa-calendar"></i></a>
									</div>
								</div>
                            </div>
                        </div>

                        <div class="col-sm-12">
						   <label for="field-1" class="col-sm-3 control-label">재등록마감일
								<span
									class="required"> *</span></label>

                    
                          <div class="col-sm-5">
							   <div class="input-group">
									<input type="text" value="<?php
									if (!empty($return_cls_date)) {
										echo $return_cls_date;
									}
									?>" class="form-control datepicker" name="return_cls_date"
										   data-format="yyyy/mm/dd">

									<div class="input-group-addon">
										<a href="#"><i class="fa fa-calendar"></i></a>
									</div>
								</div>
							</div>
                        </div>




						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
							<button type="submit" class="btn btn-primary"> 저 장 </button>
						</div>
					</div>
        </form>
    </div>
</div>
