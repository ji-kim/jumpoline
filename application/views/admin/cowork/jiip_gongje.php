<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
//if (empty($cr_month)) $cr_month = date('Y-m');
//$str_cr_month = substr($cr_month,0,4)."년 ".substr($cr_month,5,2)."월";

//임시 처리
$pdate = $df_month . "-01";
$pm=date("Y-m",strtotime($pdate.'-1month')); 


$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();
$mdate = date('Y-m-d');
$last_7_days = date('Y-m-d', strtotime('today - 7 days'));


//추가항목관련
$add_sudang_cnt = 0;
$add_gongje01_cnt = 0;
$add_gongje02_cnt = 0;
$add_gongje03_cnt = 0;

$add_sudang_cols = "";
$add_gongje01_cols = "";
$add_gongje02_cols = "";
$add_gongje03_cols = "";

if (!empty($all_sudang_group)) {
	foreach ($all_sudang_group as $sudang_info) {
		if($sudang_info->type == 'S') { //일반수당
			$add_sudang_cnt++;
			$add_sudang_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$sudang_info->title."</td>";
		}
	}
}
			
if (!empty($all_gongje_group)) {
	foreach ($all_gongje_group as $gongje_info) {
		if($gongje_info->add_type == 'GW') { //위수탁
			$add_gongje01_cnt++;
			$add_gongje01_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		} else if($gongje_info->add_type == 'GN') { //일반
			$add_gongje02_cnt++;
			$add_gongje02_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		} else if($gongje_info->add_type == 'GR') { //환급
			$add_gongje03_cnt++;
			$add_gongje03_cols .= "          <td style='color:#ffffff;background-color: #777777;border-right:1px solid #eee;'>".$gongje_info->title."</td>";
		}
	}
}

if (1) { //$this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script>
	function check_all(f)
	{
		var chk = document.getElementsByName("sel_df[]");

		for (i=0; i<chk.length; i++)
			chk[i].checked = f.chkall.checked;
	}

	function popAsWindow(md,tr_id) {
	  window.open('<?php echo base_url(); ?>admin/asset/pop_carinfo/'+md+'/'+tr_id, 'winAS', 'left=50, top=50, width=1480, height=700, scrollbars=1');
	}
	function selectPartner(md,params) {
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/'+md+'/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
	function goSearch(val) {
		if(val == 'all' || val == '전체') {
			document.myform.action = "<?php echo base_url() ?>admin/cowork/jiip_gongje_all";
			document.myform.ws_co_id.value = '';
		} else {
			document.myform.action = "<?php echo base_url() ?>admin/cowork/jiip_gongje";
		}
		document.myform.submit();
	}

	//총마감 - 기사 보기 가능
	function setClosing(yn) {
		$("#ws_mode").val('CL');
		$("#close_yn").val(yn);
		document.myform.submit();
	}

	//입력마감
	function setInput(yn) {
		$("#ws_mode").val('IC');
		$("#close_yn").val(yn);
		document.myform.submit();
	}
	function goPrintAll() {
		var df_month		= $("#df_month").val();
		var gongje_req_co	= $("#gongje_req_co").val();

		document.myform.action = '<?php echo base_url() ?>admin/cowork/jiip_gongje/'+df_month+'/'+gongje_req_co+'/print/';
		document.myform.target = '_blank';
		document.myform.submit();
	}
	function goExcelAll() {
		var df_month		= $("#df_month").val();
		var gongje_req_co	= $("#gongje_req_co").val();

		document.myform.action = '<?php echo base_url() ?>admin/cowork/jiip_gongje/'+df_month+'/'+gongje_req_co+'/excel/';
		document.myform.target = '_blank';
		document.myform.submit();
	}

	function goExcelListAll() {
		var df_month		= $("#df_month").val();
		var gongje_req_co	= $("#gongje_req_co").val();

		document.myform.action = '<?php echo base_url() ?>admin/cowork/jiip_gongje/'+df_month+'/'+gongje_req_co+'/excel_list/';
		document.myform.target = '_blank';
		document.myform.submit();
	}

	//임시처리
	function goExcelSingle(df_id) {
		var df_month		= $("#df_month").val();
		var gongje_req_co	= $("#gongje_req_co").val();

		document.myform.action = '<?php echo base_url() ?>admin/cowork/jiip_gongje/'+df_month+'/'+gongje_req_co+'/excel/'+df_id+'/';
		document.myform.target = '_blank';
		document.myform.submit();
	}
</script>


            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
  
    <!-- 검색 시작 -->
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/cowork/jiip_gongje"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
				<input type="hidden" name="ws_mode" id="ws_mode" value="">
				<input type="hidden" name="close_yn" id="close_yn" value="">
	<table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
        <tr>
          <td align="left" valign="top" bgcolor="#ffffff">
            <table border="0" width="100%" cellpadding="12" cellspacing="15" bgcolor="#ffffff">
              <tr>
                <td width="6%" height="20" align="center" bgcolor="#efefef">년월</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff" >
                            <div class="input-group">
                                <input type="text" value="<?php
                                if (!empty($df_month)) {
                                    echo $df_month;
                                }
                                ?>" class="form-control monthyear" name="df_month" id="df_month"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;" onChange="document.myform.submit();">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
                </td>
                <td align="center" bgcolor="#efefef">공제청구사</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff;">
					<!--select name="gongje_req_co" id="gongje_req_co" style="width:100%;background-color:yellow;" class="form-control input-sm" onChange="document.myform.submit();">
							<option value="1413" <?=($gongje_req_co=="1413")?"selected":""?>>케이티지엘에스(주) </option>
							<option value="1414" <?=($gongje_req_co=="1414")?"selected":""?>>(주)아이디일일구닷컴</option>
							<option value="1415" <?=($gongje_req_co=="1415")?"selected":""?>>(주)델타온</option>
							<option value="1903" <?=($gongje_req_co=="1903")?"selected":""?>>(주)휴먼엘지</option>
							<option value="etc" <?=($gongje_req_co=="etc")?"selected":""?>>기타</option>
					</select-->
						<input type="hidden" name="gongje_req_co" id="gongje_req_co" value="<?php
                                                               if (!empty($gongje_req_co)) {
                                                                   echo $gongje_req_co;
                                                               }
                                                               ?>">
						<input type="text" name="gongje_req_co_name" id="gongje_req_co_name" value="<?=(empty($gongje_req_co_name))?"":$gongje_req_co_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('group','gongje_req_co||gongje_req_co_name||__||__||__||__||__||__');" onChange="goSearch(this.value)">
					<!--a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/select_reqco"><input type="hidden" name="ws_co_id" id="ws_co_id" value="">
					<input type="text" name="ws_co_name" id="ws_co_name" value="" class="form-control" style="width:100%;background-color:yellow;"></a-->

                </td>
                <td align="center" bgcolor="#efefef">실수요처</td>
                <td width="10%" align="center" >
					<select name="baecha_co_id" id="baecha_co_id" style="width:100%;" class="form-control input-sm">
				<option value="">전체</option>
							<option value="wmjd" > 우체국물류지원단 </option>
							<option value="0003" >(주)델타온</option>
							<option value="0001" >(주)아이디일일구닷컴</option>
							<option value="degugyodoso" >KPI대구교도소</option>
							<option value="OUT444" >OUT</option>
							<option value="kn4021" >강남우체국</option>
							<option value="kd2828" >강동우체국</option>
							<option value="ks8900" >강서우체국</option>
							<option value="gong-101" >개별현장</option>
							<option value="kkj5900" >경기광주우체국</option>
							<option value="gyounggyh" >경기혈액원</option>
							<option value="ks3704" >경산우체국</option>
							<option value="kj0114" >경주우체국</option>
							<option value="ky9400" >계양우체국</option>
							<option value="ka0014" >관악우체국</option>
							<option value="km1114" >광명우체국</option>
							<option value="ks2114" >광산우체국</option>
							<option value="kj0280" >광주우체국</option>
							<option value="guro1501" >구로구청</option>
							<option value="km1533" >구미우체국</option>
							<option value="gp8014" >군포우체국</option>
							<option value="icb1512" >근로복지인천병원</option>
							<option value="kj6014" >금정우체국</option>
							<option value="kia5071" >기아자동차협력</option>
							<option value="hangi1501" >기초과학연구원</option>
							<option value="kc0014" >김천우체국</option>
							<option value="kp0014" >김포우체국</option>
							<option value="kh9009" >김해우체국</option>
							<option value="nd0801" >남동우체국</option>
							<option value="nbs9000" >남부산우체국</option>
							<option value="nambuh" >남부혈액검사센터</option>
							<option value="rudrlskadid" >남양우체국</option>
							<option value="nyj0700" >남양주우체국</option>
							<option value="nws2014" >남울산우체국</option>
							<option value="nic0500" >남인천우체국</option>
							<option value="nw3226" >노원우체국</option>
							<option value="dgds4614" >대구달서우체국</option>
							<option value="dg2000" >대구우체국</option>
							<option value="dd2400" >대덕우체국</option>
							<option value="dj7066" >대전우체국</option>
							<option value="redcross-dj" >대전충남혈액원</option>
							<option value="redcross-ws" >대한적십자사울산혈액원</option>
							<option value="dy0014" >덕양우체국</option>
							<option value="db3600" >도봉우체국</option>
							<option value="ddg1900" >동대구우체국</option>
							<option value="dl7014" >동래우체국</option>
							<option value="dsw0532" >동수원우체국</option>
							<option value="dws0100" >동울산우체국</option>
							<option value="dj0385" >동작우체국</option>
							<option value="djj3842" >동전주우체국</option>
							<option value="dca6310" >동천안우체국</option>
							<option value="ds1610" >둔산우체국</option>
							<option value="ms0044" >마산우체국</option>
							<option value="mshp0004" >마산합포우체국</option>
							<option value="mp0014" >마포우체국</option>
							<option value="anstks" >문산우체국</option>
							<option value="seoul" >번호예치</option>
							<option value="inkd01" >본사</option>
							<option value="bs3000" >부산우체국</option>
							<option value="bsj0700" >부산진우체국</option>
							<option value="bc7124" >부천우체국</option>
							<option value="bp4000" >부평우체국</option>
							<option value="bgj9114" >북광주우체국</option>
							<option value="bdg3024" >북대구우체국</option>
							<option value="bbs0864" >북부산우체국</option>
							<option value="bd2900" >분당우체국</option>
							<option value="ss3331" >사상우체국</option>
							<option value="sh7000" >사하우체국</option>
							<option value="ssd" >상수도사업본부</option>
							<option value="skj7190" >서광주우체국</option>
							<option value="sgp3915" >서귀포우체국</option>
							<option value="seodemoon" >서대문구청</option>
							<option value="sdm9114" >서대문우체국</option>
							<option value="sdj3400" >서대전우체국</option>
							<option value="ssw01" >서수원우체국</option>
							<option value="redcross-s" >서울남부혈액원</option>
							<option value="sdhw" >서울동부혈액원</option>
							<option value="seomun1501" >서울문화재단</option>
							<option value="seoult" >서울화물공제조합</option>
							<option value="sic9114" >서인천우체국</option>
							<option value="scj5720" >서청주우체국</option>
							<option value="sn0014" >성남우체국</option>
							<option value="sb0123" >성북우체국</option>
							<option value="center1" >센타프라자</option>
							<option value="sp2700" >송파우체국</option>
							<option value="sw1300" >수원우체국</option>
							<option value="sj0511" >수지우체국</option>
							<option value="sh2700" >시흥우체국</option>
							<option value="cjdt01" >씨제이대한통운(주)</option>
							<option value="as2114" >아산우체국</option>
							<option value="as0014" >안산우체국</option>
							<option value="as7900" >안성우체국</option>
							<option value="ay9788" >안양우체국</option>
							<option value="dkswnddncprnr" >안중우체국</option>
							<option value="yangsan" >양산우체국</option>
							<option value="yc0014" >양천우체국</option>
							<option value="yyd0014" >여의도우체국</option>
							<option value="yj0888" >연제우체국</option>
							<option value="yd5550" >영도우체국</option>
							<option value="os0004" >오산우체국</option>
							<option value="ys0004" >용산우체국</option>
							<option value="yi2849" >용인우체국</option>
							<option value="kypost1" >우정본(경북청)</option>
							<option value="ruddlscjd" >우정본(경인청)</option>
							<option value="pupost1" >우정본(부산청)</option>
							<option value="spost1" >우정본(서울청)</option>
							<option value="jnpost1" >우정본(전남청)</option>
							<option value="jbcpost1" >우정본(전북청)</option>
							<option value="jjpost1" >우정본(제주청)</option>
							<option value="wjs0001" >우정사업본부</option>
							<option value="ws5801" >울산우체국</option>
							<option value="ys8114" >유성우체국</option>
							<option value="ep3514" >은평우체국</option>
							<option value="humanplaza-1" >은평휴먼프라자Ⅰ</option>
							<option value="yjb5556" >의정부우체국</option>
							<option value="ic2820" >이천우체국</option>
							<option value="ic8155" >인천우체국</option>
							<option value="incheonjodal" >인천지방조달청</option>
							<option value="is0205" >일산우체국</option>
							<option value="jj2635" >전주우체국</option>
							<option value="jjwj5200" >제주우편집중국</option>
							<option value="junongangh" >중앙혈액검사센터</option>
							<option value="jh0005" >진해우체국</option>
							<option value="cw1114" >창원우체국</option>
							<option value="ca2660" >천안우체국</option>
							<option value="cj0014" >청주우체국</option>
							<option value="0002" >케이티지엘에스</option>
							<option value="ty2016" >통영우체국</option>
							<option value="pj9004" >파주우체국</option>
							<option value="pt3121" >평택우체국</option>
							<option value="pc1302" >포천우체국</option>
							<option value="ph9937" >포항우체국</option>
							<option value="hn2007" >하남우체국</option>
							<option value="hsk88" >한국승강기안전공단</option>
							<option value="hanjun2" >한국전력(강원)</option>
							<option value="dghj001" >한국전력(대구)</option>
							<option value="hsk20" >한승공강원지사</option>
							<option value="hsk16" >한승공경남동부</option>
							<option value="hsk10" >한승공경북서부</option>
							<option value="hsk15" >한승공대구서부</option>
							<option value="hsk06" >한승공부천지사</option>
							<option value="hskkd" >한승공서울강동</option>
							<option value="hsksb" >한승공서울본부</option>
							<option value="hskss" >한승공서울서부</option>
							<option value="hsksc" >한승공서울서초</option>
							<option value="hsk05" >한승공안산지사</option>
							<option value="hsk13" >한승공영남본부</option>
							<option value="hsk03" >한승공용인지사</option>
							<option value="hskws" >한승공울산지사</option>
							<option value="hskjd" >한승공전남동부</option>
							<option value="hskjs" >한승공전남서부</option>
							<option value="jejuhsk" >한승공제주지사</option>
							<option value="hskca" >한승공천안지사</option>
							<option value="hskcn" >한승공충남지사</option>
							<option value="hsk17" >한승공충북지사</option>
							<option value="hsk07" >한승공파주지사</option>
							<option value="hwd4500" >해운대우체국</option>
							<option value="ws9516" >화성우체국</option>
							<option value="goldb1" >황금빌딩</option>
							</select>
				</td>
                <td align="center" bgcolor="#efefef">협력(공제사)</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<input type="hidden" name="ws_co_id" id="ws_co_id" value="">
					<input type="text" name="ws_co_name" id="ws_co_name" value="" class="form-control" style="width:100%;" onClick="selectGroup('ws_co_id||ws_co_name||__||__||__||__||__');">
                </td>
                <td align="center" bgcolor="#efefef">파트너통합정보</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<input type="text" name="search_keyword" id="search_keyword" value="<?=$search_keyword?>" class="form-control" style="width:100%;">
                </td>
			</tr>
			<tr>
                <td style="padding-left:5px;padding-top:10px;" colspan="10" align="center" bgcolor="#ffffff">

					<button name="sbtn" class="dt-button buttons-print btn btn-success mr btn-xs" id="file-save-button" type="button" onClick="goSearch(document.myform.gongje_req_co.value);" value="1"><i class="fa fa-search"> </i>검색</button>
					<a href="/admin/cowork/generate_mfee/<?php echo $df_month;?>" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 1. 청구서생성</span>
					</a>
					
					<a href="javascript:setInput('Y');" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 2. 추가입력데이터 적용</span>
					</a>
					<a href="javascript:setClosing('Y');" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 3. 마감</span>
					</a>



					<a href="javascript:setClosing('N');" tabindex="0" class="dt-button buttons-print btn btn-success btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 마감취소</span>
					</a>

					<a href="javascript:goPrintAll()" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs" aria-controls="DataTables">
					<span><i class="fa fa-print"> </i> 청구서 출력</span>
					</a>

					<input type="checkbox" name="chkall" value="1" id="chkall" onclick="check_all(this.form)">
					<a href="javascript:goExcelAll()" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 청구서 엑셀저장</span>
					</a>
					<a href="javascript:goExcelListAll()" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 목록 엑셀저장</span>
					</a>
					<!--a href="javascript:goPrintAllTax()" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs" aria-controls="DataTables">
					<span><i class="fa fa-pencil"> </i> 유통물류에 적용</span>
					</a-->

				</td>
					
			  </tr>
            </table>
		  </td>
        </tr>
      </table>
		</form>
      <!-- 검색 끝 -->

				

            </div>




<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#client_list" data-toggle="tab"><span style="color:blue;"><?php echo $df_month; ?></span> 위수탁관리비청구현황(총 <?=number_format($total_count)?>건)</a></li>



            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane active" id="client_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
							<table id="FDataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                                <thead>

								
        <tr align="center" bgcolor="#e0e7ef">
          <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">등록<br/>마감</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">마감</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">기사<br/>확인</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>공제청구사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>실수요처</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>운영공제(송금)사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>사업자등록</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>차량번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>계약기간</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="<?=(6+$add_gongje01_cnt)?>">위.수탁관리비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">각종보험</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="<?=(7+$add_gongje02_cnt)?>">일반공제</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="<?=(2+$add_gongje03_cnt)?>">환급형공제</td>
          <!--td style="color:#ffffff;background-color: #777777;" colspan="3">누적금환급</td-->

          <td style="color:#ffffff;background-color: #777777;border-left:1px solid #eee;border-right:1px solid #eee;" rowspan="2"><br/>공제총액</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">환급금지급후<br>공제청구총액</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">상세<br>보기</td>
        </tr>
															

        <tr align="center" bgcolor="#e0e7ef">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관리비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차고지비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">협회비</td>
		  <?=$add_gongje01_cols?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">자동차</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">적재물</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소  계</td>

		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">미수금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과태료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">자동차세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">환경부담금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차고지비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
		  <?=$add_gongje02_cols?>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">해지담보</td>
		  <?=$add_gongje03_cols?>
          <td style="color:#ffffff;background-color: #777777;">소  계</td>
		</tr>
								
								
								</thead>
                                <tbody>



                                <?php
                                if (!empty($all_delivery_fee_info)) {
                                    foreach ($all_delivery_fee_info as $delivery_fee_details) {
										$sn = $total_count--;
										$add_sudang_vals = "";
										$add_gongje01_vals = "";
										$add_gongje02_vals = "";
										$add_gongje03_vals = "";

										//파트너
										$dp = $this->db->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_members')->row();

										//공제청구사
										if(!empty($dp->ws_co_id)) {
											$gj_rq_co = $this->db->where('dp_id', $dp->ws_co_id)->get('tbl_members')->row();
											if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;
										}

										//차량정보
										$truck = $this->db->where('idx', $delivery_fee_details->tr_id)->get('tbl_asset_truck')->row();
										if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;

										//위수탁관리비
//$wsm_info = $this->db->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $df_month)->where('apply_yn', 'Y')->get('tbl_delivery_fee_fixmfee')->row();
										$wsm_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_fixmfee')->row();
										//$wsm_info = $this->db->where('df_id', $delivery_fee_details->df_id)->where('apply_yn', 'Y')->get('tbl_delivery_fee_fixmfee')->row();

										if(empty($wsm_info->wst_mfee)) $wst_mfee = 0; else $wst_mfee = $wsm_info->wst_mfee;
										if(empty($wsm_info->mfee_vat)) $mfee_vat = 0; else $mfee_vat = $wsm_info->mfee_vat;
										if(empty($wsm_info->org_fee)) $org_fee = 0; else $org_fee = $wsm_info->org_fee;
										//if(empty($wsm_info->env_fee)) $env_fee = 0; else $env_fee = $wsm_info->env_fee;
										//if(empty($wsm_info->car_tax)) $car_tax = 0; else $car_tax = $wsm_info->car_tax;
										if(empty($wsm_info->grg_fee)) $mgrg_fee = 0; else $mgrg_fee = $wsm_info->grg_fee;
										if(empty($wsm_info->grg_fee_vat)) $mgrg_fee_vat = 0; else $mgrg_fee_vat = $wsm_info->grg_fee_vat;
										if(empty($wsm_info->etc)) $etc= 0; else $etc = $wsm_info->etc;
										$org_fee_vat = 0;
										$env_fee = 0;
										$env_fee_vat = 0;
										$car_tax = 0;
										$car_tax_vat = 0;
										$etc = 0;
										$wsm_sum = ($wst_mfee + $mfee_vat + $org_fee + $env_fee + $car_tax + $mgrg_fee + $mgrg_fee_vat + $etc);

										//각종보험
										$insur_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_insur')->row();
										if(empty($insur_info->ins_car)) $ins_car = 0; else $ins_car = $insur_info->ins_car;
										if(empty($insur_info->ins_load)) $ins_load = 0; else $ins_load = $insur_info->ins_load;
										if(empty($insur_info->ins_grnt)) $ins_grnt = 0; else $ins_grnt = $insur_info->ins_grnt;
										$insur_sum = ($ins_car + $ins_load + $ins_grnt);

										//일반공제
										$gongje_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_gongje')->row();
										//$gongje_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_gongje')->row();
										if(empty($gongje_info->fine_fee)) $fine_fee = 0; else $fine_fee = $gongje_info->fine_fee;
										//if(empty($gongje_info->acc_app)) $acc_app = 0; else $acc_app = $gongje_info->acc_app;
										if(empty($gongje_info->not_paid)) $not_paid = 0; else $not_paid = $gongje_info->not_paid;
										if(empty($gongje_info->car_tax)) $car_tax = 0; else $car_tax = $gongje_info->car_tax;
										if(empty($gongje_info->env_fee)) $env_fee = 0; else $env_fee = $gongje_info->env_fee;
										//if(empty($gongje_info->mid_pay)) $mid_pay = 0; else $mid_pay = $gongje_info->mid_pay;
										//if(empty($gongje_info->trmt_sec)) $trmt_sec = 0; else $trmt_sec = $gongje_info->trmt_sec;
										//if(empty($gongje_info->etc)) $etc = 0; else $etc = $gongje_info->etc;
										if(empty($gongje_info->grg_fee)) $grg_fee = 0; else $grg_fee = $gongje_info->grg_fee;
										if(empty($gongje_info->grg_fee_vat)) $grg_fee_vat = 0; else $grg_fee_vat = $gongje_info->grg_fee_vat;
										$gongje_sum = ($fine_fee + $not_paid + $car_tax + $env_fee + $grg_fee + $grg_fee_vat);

										//환급형공제
										$rf_gongje_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_refund_gongje')->row();
										if(empty($rf_gongje_info->gj_termination_mortgage)) $gj_termination_mortgage = 0; else $gj_termination_mortgage = $rf_gongje_info->gj_termination_mortgage;
										//if(empty($rf_gongje_info->gj_plate_mortgage)) $gj_plate_mortgage = 0; else $gj_plate_mortgage = $rf_gongje_info->gj_plate_mortgage;
										//if(empty($rf_gongje_info->gj_etc_refund)) $gj_etc_refund = 0; else $etc = $rf_gongje_info->gj_etc_refund;
										$rf_gongje_sum = ($gj_termination_mortgage);

										//---------------------------------------------------------------------------------  추가 항목
										if (!empty($all_sudang_group)) {
											foreach ($all_sudang_group as $sudang_info) {
												if($sudang_info->type == 'S') { //일반수당
												}
											}
										}
													
										$add_gongje01_vals = "";
										$add_gongje02_vals = "";
										$add_gongje03_vals = "";

										if (!empty($all_gongje_group)) {
											foreach ($all_gongje_group as $gongje_info) {
												$add_item = $this->db->where('pid', $gongje_info->idx)->where('df_month', $df_month)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_add')->row();
												if($gongje_info->add_type == 'GW') { //위수탁
													if(!empty($add_item->amount)) {
														$wsm_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje01_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje01_vals .= "<td style='text-align:right;'>*</td>";
												} else if($gongje_info->add_type == 'GN') { //일반
													if(!empty($add_item->amount)) {
														$gongje_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje02_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje02_vals .= "<td style='text-align:right;'>*</td>";

												} else if($gongje_info->add_type == 'GR') { //환급
													if(!empty($add_item->amount)) {
														$rf_gongje_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje03_vals .= "<td style='text-align:right;'>*".$val."</td>";
													} else $add_gongje03_vals .= "<td style='text-align:right;'>*</td>";
												}
											}
										}
										//---------------------------------------------------------------------------------  추가 항목



										$tot_gongje = ($wsm_sum + $insur_sum + $gongje_sum + $rf_gongje_sum);
										$tot_gongje_req = ($tot_gongje - $rf_gongje_sum);

 
										$lv = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_levy')->row();
										$pay_status = "납부전";
										$pay_status_color = "warning";
										if(!empty($lv->pay_status)) {
											if($lv->pay_status == "N") { $pay_status = "미납"; $pay_status_color = "danger"; }
											else if($lv->pay_status == "P") { $pay_status = "부분납"; $pay_status_color = "primary"; } 
											else if($lv->pay_status == "F") { $pay_status = "완납"; $pay_status_color = "success"; } 
										}

										$is_editable = "Y";
										if($close_yn == "Y" || $delivery_fee_details->is_confirm=='Y' || $delivery_fee_details->is_closed=='Y') $is_editable = "N";
										if($ws_mode == "IC") { // 등록마감처리 
											$sql_insure = "";
											$qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
											$qry .= " where a.type = '자동차' and b.gj_month = '".$df_month."' and a.tr_id = '".$delivery_fee_details->tr_id."' and a.active_yn='Y'";
											$ins = $this->cowork_model->db->query($qry)->row();
											if(!empty($ins->pay_amt)) {
												$ins_car = $ins->pay_amt;
											} else {
												$ins_car = 0;
											}
											$sql_insure .= "ins_car ='".$ins_car."'"; 

											$qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
											$qry .= " where a.type = '적재물' and b.gj_month = '".$df_month."' and a.tr_id = '".$delivery_fee_details->tr_id."' and a.active_yn='Y'";
											$lins = $this->cowork_model->db->query($qry)->row();
											if(!empty($lins->pay_amt)) {
												$ins_load = $lins->pay_amt;
											} else {
												$ins_load = 0;
											}
											$sql_insure .= ",ins_load ='".$ins_load."'";

if(trim($delivery_fee_details->tr_id) == "1493") echo $sql_insure;

											//if(empty($insur_info->ins_car)) $ins_car = 0; else $ins_car = $insur_info->ins_car;
											//if(empty($insur_info->ins_load)) $ins_load = 0; else $ins_load = $insur_info->ins_load;

											$sql = "update tbl_delivery_fee set is_confirm = '".$close_yn."' where df_id='".$delivery_fee_details->df_id."'";
											$this->cowork_model->db->query($sql);
											$sql = "update tbl_delivery_fee_insur set $sql_insure where df_id='".$delivery_fee_details->df_id."'";
											$this->cowork_model->db->query($sql);
											$this->cowork_model->db->query($sql);

											$insur_sum = ($ins_car + $ins_load + $ins_grnt);
											$delivery_fee_details->is_confirm = $close_yn;

										}

										if($ws_mode == "CL") { // 마감처리 
											$sql = "update tbl_delivery_fee set is_closed = '".$close_yn."' where df_id='".$delivery_fee_details->df_id."'";
											$this->cowork_model->db->query($sql);
											
											//수납생성
											$chk = $this->cowork_model->db->query("select df_id from tbl_delivery_fee_levy where df_id='".$delivery_fee_details->df_id."'")->row();
											if(empty($chk->df_id)) {
												$sql = "INSERT INTO tbl_delivery_fee_levy SET df_id='".$delivery_fee_details->df_id."', dp_id='".$delivery_fee_details->dp_id."', df_month='$df_month', gongje_req_co = '".$dp->ws_co_id."'";
												$sql .= ", prev_misu = '$not_paid' ,gn_amount = '".$tot_gongje_req."', paid_amount = '0', balance_amount = '".$tot_gongje_req."'";
											} else {
												$sql = "UPDATE tbl_delivery_fee_levy SET prev_misu = '$not_paid' ,gn_amount = '".$tot_gongje_req."', paid_amount = '0', balance_amount = '".$tot_gongje_req."' WHERE df_id='".$delivery_fee_details->df_id."'";
											}
											$this->cowork_model->db->query($sql);
											$delivery_fee_details->is_closed = $close_yn;
										}

//임시 처리 -- 전월회계상 미수금액 당월에 등록 
if(0) { //!empty($plv->pay_status)) {
$plv = $this->db->where('df_month', $pm)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_levy')->row();
if($delivery_fee_details->df_month == "2018-09") { // 9월이면서 전달 완납이 아닌경우
	if(!empty($plv->pay_status) && $plv->pay_status != 'B') { //납부전상태가 아니면
		$not_paid = $plv->balance_amount;

		$sql = "update tbl_delivery_fee_gongje set not_paid = '".$not_paid."' where df_id='".$delivery_fee_details->df_id."'";
		$this->cowork_model->db->query($sql);
	echo $delivery_fee_details->E."-".$sql."<br/>";
	}
}
}

//-> 당월 회계에 등록
if(0) { //!empty($plv->pay_status)) {
	if($delivery_fee_details->df_month == "2018-09") {
// 미납데이터 임시저장
		$plv = $this->db->where('df_month', $pm)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_levy')->row();
		$chk = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_levy')->row();
		if (!empty($chk)) {
			if(empty($plv->balance_amount)) $ba = 0; else $ba = $plv->balance_amount;
			$sql = "update tbl_delivery_fee_levy set gn_amount = '".$tot_gongje_req."',pay_amount = '0',balance_amount = '".$tot_gongje_req."',prev_misu = '".$ba."' where df_id='".$delivery_fee_details->df_id."'";
		} else {
			
			$sql = "insert into tbl_delivery_fee_levy set df_id = '".$delivery_fee_details->df_id."',co_code = '".$delivery_fee_details->co_code."' ,ws_co_id = '".$delivery_fee_details->gongje_req_co."' ,df_month = '".$delivery_fee_details->df_month."',dp_id = '".$delivery_fee_details->dp_id."',gn_amount = '".$tot_gongje_req."',pay_amount = '0',gn_amount = '".$tot_gongje_req."',prev_misu = '".$plv->balance_amount."'";
		}
		//$this->cowork_model->db->query($sql);
	echo $delivery_fee_details->E."-".$sql."<br/>";

	}    
}


//?????
if(0) {//$delivery_fee_details->df_month == "2018-08") {
	$plv = $this->db->where('df_month', $pm)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_levy')->row();
	$pdf = $this->db->where('df_month', $pm)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee')->row();
	$cmisu_amount = 0;
	if(!empty($pdf->is_pay_closed) && $pdf->is_pay_closed == 'Y') {
		$cmisu_amount = $plv->balance_amount;
	}
	//echo $cmisu_amount;
    
	// Update into tbl_delivery_fee_gongje
   // $levy_data = array(
   //     'not_paid' => $cmisu_amount,
    //);
    //$this->cowork_model->_table_name = "tbl_delivery_fee_gongje"; //table name
    //$this->cowork_model->_primary_key = "df_id";
    //$this->cowork_model->save($levy_data, $delivery_fee_details->df_id);
	$chk = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_gongje')->row();
    if (!empty($chk)) {
		$sql = "update tbl_delivery_fee_gongje set not_paid = '".$cmisu_amount."' where df_id='".$delivery_fee_details->df_id."'";
	} else {
		
		$sql = "insert into tbl_delivery_fee_gongje set df_id = '".$delivery_fee_details->df_id."',co_code = '".$delivery_fee_details->co_code."' ,ws_co_id = '".$delivery_fee_details->gongje_req_co."' ,df_month = '".$delivery_fee_details->df_month."',dp_id = '".$delivery_fee_details->dp_id."',not_paid = '".$cmisu_amount."'";
	}
	$this->cowork_model->db->query($sql);
	echo $sql."<br/>";
	//$not_paid = $cmisu_amount;
}


// 보정 --- 회계금액 상이
if(0) {
$lv = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_levy')->row();
if($delivery_fee_details->df_month == "2018-09") { // 7월
	// 부과액 = 협업총액, 잔액 = 협업총액 - 납부액
	$gn_amount = $tot_gongje_req;
	$balance_amount = $tot_gongje_req - $lv->pay_amount;

    $pay_status = "N";
	if($balance_amount == 0) $pay_status = "F";
	else if($balance_amount == $gn_amount) $pay_status = "N";
	else if($gn_amount < $lv->pay_amount) $pay_status = "O";
	else $pay_status = "P";


	$sql = "update tbl_delivery_fee_levy set prev_misu = '".$not_paid."',gn_amount = '".$gn_amount."', balance_amount = '".$balance_amount."', pay_status = '".$pay_status."' where df_id='".$delivery_fee_details->df_id."'";
	$this->cowork_model->db->query($sql);
	echo $delivery_fee_details->E."-[(".$pay_status.")".$lv->gn_amount."||||".$sql."<br/>";//."]-".$sql."<br/>";
}
									}


?>
                                   <tr>
                                        <td><input type="checkbox" name="sel_df[]" value="<?=$delivery_fee_details->df_id?>">
                                            <span class='label label-<?=$pay_status_color?>'><?=$pay_status?></span>
                                            <?= $sn ?>
                                        </td>
                                        <td style="text-align:center;">
                                                <div class="btn-group">
												<?php if($delivery_fee_details->is_confirm != 'Y') { ?>
                                                    <button class="btn btn-xs btn-<?=($delivery_fee_details->is_confirm=='Y')?"success":"danger"?> dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        <?=($delivery_fee_details->is_confirm=='Y')?"마감":"마감전"?>
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu animated zoomIn">
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/cowork/confirm_mfee_id/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$delivery_fee_details->df_id?>/Y">마감하기</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/cowork/confirm_mfee_id/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$delivery_fee_details->df_id?>/N">마감취소</a>
                                                        </li>
                                                    </ul>
												<?php } else { ?>
													<span class='label label-red'>완료</span>
												<?php } ?>
                                                </div>
                                        </td>
                                        <td style="text-align:center;">
                                                <div class="btn-group">
                                                    <button class="btn btn-xs btn-<?=($delivery_fee_details->is_closed=='Y')?"success":"danger"?> dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        <?=($delivery_fee_details->is_closed=='Y')?"마감":"마감전"?>
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu animated zoomIn">
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/cowork/close_mfee_id/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$delivery_fee_details->df_id?>/Y">마감</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= base_url() ?>admin/cowork/close_mfee_id/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$delivery_fee_details->df_id?>/N">취소</a>
                                                        </li>
                                                    </ul>
<!--
                                                            <?php if($delivery_fee_details->is_closed=='N') { ?><a href="<?= base_url() ?>admin/cowork/close_mfee_id/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$delivery_fee_details->df_id?>/Y"><span class='label label-success'>마감</span></a><?php } ?>

                                                            <?php if($delivery_fee_details->is_closed=='Y') { ?><a href="<?= base_url() ?>admin/cowork/close_mfee_id/<?= $df_month ?>/<?= $gongje_req_co ?>/<?=$delivery_fee_details->df_id?>/N"><span class='label label-warning'>취소</span></a><?php } ?>
-->
                                                    </ul>
                                                </div>
                                        </td>
                                        <td style="text-align:center;">
										<?=($delivery_fee_details->read_cnt > 0)?"<span class='label label-success'>".$delivery_fee_details->read_cnt."</span>":"<span class='label label-danger'>0</span>"?>
								
										</td>
                                        <td>
                                            <span class='label label-primary'><?=$gj_rq_co ?></span>
										</td>
                                        <td><?= $delivery_fee_details->D ?></td>
                                        <td><?= $delivery_fee_details->ceo ?>(<?= $delivery_fee_details->driver ?>)</td>
                                        <td><?= $delivery_fee_details->bs_number ?></td>
                                        <td>
										<?php if(!empty($truck->idx)) { ?>
                                              <span data-placement="top" data-toggle="tooltip" title="차량정보 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_info','<?=(!empty($truck->idx))? $truck->idx:"" ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
										
										<span class='label label-primary'><?= $truck_no ?></span>
										<?php } ?>
                                        <td><?php if(!empty($delivery_fee_details->N)) echo $delivery_fee_details->N; ?> ~ <?php if(!empty($delivery_fee_details->O)) echo $delivery_fee_details->O; ?></td>
                                        <td style="text-align:right;">
<?php if($is_editable == 'Y') { ?>
                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="관리비 설정">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/set_mfee/<?= $delivery_fee_details->df_id ?>/<?= $delivery_fee_details->dp_id ?>/<?= $df_month ?>/<?= $gongje_req_co ?>"
                                               class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
<?php } ?>
										<?= number_format($wst_mfee) ?></td>
                                        <td style="text-align:right;"><? if(!empty($mfee_vat)) echo number_format($mfee_vat,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($mgrg_fee)) echo number_format($mgrg_fee,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($mgrg_fee_vat)) echo number_format($mgrg_fee_vat,0); ?></td>
                                        <td style="text-align:right;"><? if(!empty($org_fee)) echo number_format($org_fee,0); ?></td>
									  <?=$add_gongje01_vals?>
										<td style="text-align:right;"><? if(!empty($wsm_sum)) echo number_format($wsm_sum,0); ?></td>

										<td style="text-align:right;">
<?php if(1) { //$is_editable == 'Y') { ?>
                                              <span data-placement="top" data-toggle="tooltip" title="보험 관리">
                                            <a href="javascript:;" onClick="popAsWindow('car_ins','<?= $truck->idx ?>');" class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
<?php } ?>
										<?= number_format($ins_car) ?></td>
										<td style="text-align:right;"><?= number_format($ins_load) ?></td>
										<td style="text-align:right;"><?= number_format($insur_sum) ?></td>

										<td style="text-align:right;">
<?php if($is_editable == 'Y') { ?>
                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="초기미수금액 설정">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/set_misu/<?= $delivery_fee_details->df_id ?>"
                                               class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
<?php } ?>
										<?= number_format($not_paid) ?></td>
										<td style="text-align:right;">
<?php if($is_editable == 'Y') { ?>
                                                <span data-placement="top" data-toggle="tooltip" title="일반공제 업데이트">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/set_ngongje/<?= $delivery_fee_details->df_id ?>/<?= $delivery_fee_details->dp_id ?>/<?= $df_month ?>/<?= $gongje_req_co ?>"
                                               class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
<?php } ?>
										<?= number_format($fine_fee) ?>
										<?php if(!empty($gongje_info->fine_fee_memo)) { ?>
                                                <span data-placement="top" data-toggle="tooltip" title="<?=$gongje_info->fine_fee_memo?>">
                                            [비고]
                                                </span>
										<?php } ?>
										</td>

										<td style="text-align:right;"><?= number_format($car_tax) ?></td><!--//자동차세 -->
										<td style="text-align:right;"><?= number_format($env_fee) ?></td><!--//환경부담금 -->
										<td style="text-align:right;"><?= number_format($grg_fee) ?></td><!--//차고지비 -->
                                        <td style="text-align:right;"><?= number_format($grg_fee_vat) ?></td><!--//차고지비 부가세 -->
									  
									  <?=$add_gongje02_vals?>

										<td style="text-align:right;"><?= number_format($gongje_sum) ?></td>

                                        <td style="text-align:right;">
<?php if($is_editable == 'Y') { ?>
                                                <span data-placement="top" data-toggle="tooltip"
                                                      title="환급형공제 업데이트">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/set_rgongje/<?= $delivery_fee_details->df_id ?>/<?= $delivery_fee_details->dp_id ?>/<?= $df_month ?>/<?= $gongje_req_co ?>"
                                               class="text-default ml"><i class="fa fa-cog"></i></a>
                                                </span>
<?php } ?>
										<?= number_format($gj_termination_mortgage) ?></td>
									  <?=$add_gongje03_vals?>
										<td style="text-align:right;"><?= number_format($rf_gongje_sum) ?></td>

										<td style="text-align:right;"><?= number_format($tot_gongje) ?></td>
										<td style="text-align:right;"><?= number_format($tot_gongje_req) ?></td>
										<td>
										<a href="javascript:goExcelSingle('<?= $delivery_fee_details->df_id ?>')" tabindex="0" class="dt-button buttons-print btn btn-success mr btn-xs" aria-controls="DataTables">
											<span><i class="fa fa-file-excel-o"> </i> </span>
											</a>
										</td>

                                    </tr>
<?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="31">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>






                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
