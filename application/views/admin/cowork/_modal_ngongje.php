<?php

if(empty($ngongje_info->idx )) $ngs_id = ''; else $ngs_id = $ngongje_info->idx ;
if(empty($ngongje_info->not_paid)) $not_paid = 0; else $not_paid = $ngongje_info->not_paid;
if(empty($ngongje_info->car_tax)) $car_tax = 0; else $car_tax = $ngongje_info->car_tax;
if(empty($ngongje_info->env_fee)) $env_fee = 0; else $env_fee = $ngongje_info->env_fee;
if(empty($ngongje_info->fine_fee)) $fine_fee = 0; else $fine_fee = $ngongje_info->fine_fee;
if(empty($ngongje_info->fine_fee_memo)) $fine_fee_memo = ''; else $fine_fee_memo = $ngongje_info->fine_fee_memo;
if(empty($ngongje_info->grg_fee)) $grg_fee = 0; else $grg_fee = $ngongje_info->grg_fee;
if(empty($ngongje_info->grg_fee_vat)) $grg_fee_vat = 0; else $grg_fee_vat = $ngongje_info->grg_fee_vat;

//파트너
$dp = $this->db->where('dp_id', $df_info->dp_id)->get('tbl_members')->row();

//공제청구사
$gj_rq_co = $this->db->where('dp_id', $dp->ws_co_id)->get('tbl_members')->row();
if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;

//차량정보
$truck = $this->db->where('idx', $df_info->tr_id)->get('tbl_asset_truck')->row();
if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;
?>
<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"> 일반공제 설정 - [<?php echo $df_info->df_month ?>]  <?php echo $dp->driver ?> / <?= $truck_no ?> <?=$df_info->dp_id?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">

        <form id="form_validation" enctype="multipart/form-data" 
              action="<?php echo base_url() ?>admin/cowork/update_ngongje"
              method="post" class="form-horizontal form-groups-bordered">
		<input type="hidden" name="ngs_id" value="<?php echo $ngs_id; ?>">
		<input type="hidden" name="df_id" value="<?php echo $df_info->df_id; ?>">
		<input type="hidden" name="dp_id" value="<?php echo $df_info->dp_id; ?>">
		<input type="hidden" name="ct_id" value="<?php echo $df_info->ct_id; ?>">
		<input type="hidden" name="df_month" value="<?php echo $df_info->df_month; ?>">
		<input type="hidden" name="gongje_req_co" value="<?php echo $df_info->gongje_req_co; ?>">

            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;color:#ffffff;background-color: #777777;">항목 <br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #777777;">금액<br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #777777;">부가세<br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #777777;">합계<br/><br/></label>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label">자동차세 </label>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo $car_tax; ?>" class="form-control" name="car_tax" >
                </div>
                <div class="col-sm-3">
                     
                </div>
                <div class="col-sm-3" style="text-align:center;"><?=number_format($car_tax)?></div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label">환경부담금 </label>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo $env_fee; ?>" class="form-control" name="env_fee" >
                </div>
                <div class="col-sm-3">
                     
                </div>
                <div class="col-sm-3" style="text-align:center;"><?=number_format($env_fee)?></div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label">과태료 </label>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo $fine_fee; ?>" class="form-control" name="fine_fee" >
                </div>
                <div class="col-sm-3">
                     
                </div>
                <div class="col-sm-3" style="text-align:center;"><?=number_format($fine_fee)?></div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label">과태료 메모</label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $fine_fee_memo; ?>" class="form-control" name="fine_fee_memo" >
                </div>
            </div>


            <!-- CV Upload -->
            <div class="form-group mb0">
                <label for="field-1" class="col-sm-3 control-label">과태료 첨부파일</label>

                <input type="hidden" name="fine_path" value="<?php
                if (!empty($ngongje_info->fine_path)) {
                    echo $ngongje_info->fine_path;
                }
                ?>">
                <div class="col-sm-9">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <?php if (!empty($ngongje_info->fine)): ?>
                            <span class="btn btn-default btn-file"><span class="fileinput-new"
                                                                         style="display: none">과태로 파일 선택</span>
                                        <span class="fileinput-exists"
                                              style="display: block">변경</span>
                                        <input type="hidden" name="fine" value="<?php echo $ngongje_info->fine ?>">
                                        <input type="file" name="fine">
                                    </span>
                            <span class="fileinput-filename"> <?php echo $ngongje_info->fine_filename ?></span>
                        <?php else: ?>
                            <span class="btn btn-default btn-file"><span
                                    class="fileinput-new">과태로 파일 선택</span>
                                        <span class="fileinput-exists">변경</span>
                                        <input type="file" name="fine">
                                    </span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                               style="float: none;">&times;</a>
                        <?php endif; ?>

                    </div>
                    <div id="msg_pdf" style="color: #e11221"></div>
                </div>
            </div>




            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label">차고지비 </label>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo $grg_fee; ?>" class="form-control" name="grg_fee" >
                </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo $grg_fee_vat; ?>" class="form-control" name="grg_fee_vat" >
                </div>
                <div class="col-sm-3" style="text-align:center;"><?=number_format($grg_fee+$grg_fee_vat)?></div>
            </div>
<?php
if (!empty($all_ngongje_group)) {
?>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;color:#ffffff;background-color: #333333;">추가항목 <br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #333333;">금액<br/><br/></label>
                <label for="field-1" class="col-sm-6 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #333333;">메모<br/><br/></label>
            </div>

<?php
	$i = 0;
	foreach ($all_ngongje_group as $ngongje_info) {
		$add_item = $this->db->where('df_month', $df_month)->where('dp_id', $dp_id)->get('tbl_delivery_fee_add')->row();
?>
			<input type=hidden name=chk[] value='<?=$i?>'>
			<input type="hidden" name="item_pidx[<?=$i?>]" value="<?php echo $ngongje_info->idx; ?>">
			<input type="hidden" name="item_idx[<?=$i?>]" value="<?php if(!empty($add_item->idx)) echo $add_item->idx; ?>">
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label"><?php echo $ngongje_info->title; ?> </label>
                <div class="col-sm-3">
                    <input type="text" value="<?php if(!empty($add_item->amount)) echo $add_item->amount; ?>" class="form-control" name="item_amount[<?=$i?>]" >
                </div>
                <div class="col-sm-6">
                    <input type="text" value="<?php if(!empty($add_item->memo)) echo $add_item->memo; ?>" class="form-control" name="memo[<?=$i?>]" >
                </div>
            </div>

<?php
		$i++;
	}
}
?>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"> 저 장 </button>
            </div>
        </form>
    </div>
</div>
