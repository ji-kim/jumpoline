<?php
//임시 처리
$pdate = $df_month . "-01";
$pm=date("Y-m",strtotime($pdate.'-1month')); 


$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();
$mdate = date('Y-m-d');
$last_7_days = date('Y-m-d', strtotime('today - 7 days'));


//추가항목관련
$add_sudang_cnt = 0;
$add_gongje01_cnt = 0;
$add_gongje02_cnt = 0;
$add_gongje03_cnt = 0;

$add_sudang_cols = "";
$add_gongje01_cols = "";
$add_gongje02_cols = "";
$add_gongje03_cols = "";

if (!empty($all_sudang_group)) {
	foreach ($all_sudang_group as $sudang_info) {
		if($sudang_info->type == 'S') { //일반수당
			$add_sudang_cnt++;
			$add_sudang_cols .= "".$sudang_info->title."";
		}
	}
}
			
if (!empty($all_gongje_group)) {
	foreach ($all_gongje_group as $gongje_info) {
		if($gongje_info->add_type == 'GW') { //위수탁
			$add_gongje01_cnt++;
			$add_gongje01_cols .= "".$gongje_info->title." ";
		} else if($gongje_info->add_type == 'GN') { //일반
			$add_gongje02_cnt++;
			$add_gongje02_cols .= "".$gongje_info->title." ";
		} else if($gongje_info->add_type == 'GR') { //환급
			$add_gongje03_cnt++;
			$add_gongje03_cols .= "".$gongje_info->title." ";
		}
	}
}
?>
<style>
.xl24 { 
mso-style-parent:style0; 
mso-number-format:"\@"; 
border:.5pt solid windowtext; 
} 
</style>
							

                                <?php
                                if (!empty($all_delivery_fee_info)) {
                                    foreach ($all_delivery_fee_info as $delivery_fee_details) {
										//temp
										if(empty($single_df_id) || (!empty($single_df_id) && $single_df_id ==$delivery_fee_details->df_id)) {


										$sn = $total_count--;
										$tot_supp = 0;
										$tot_vat = 0;
										$add_sudang_vals = "";
										$add_gongje01_vals = "";
										$add_gongje02_vals = "";
										$add_gongje03_vals = "";

										//파트너
										$dp = $this->db->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_members')->row();

										//공제청구사
										if(!empty($dp->ws_co_id)) {
											$gj_rq_co = $this->db->where('dp_id', $dp->ws_co_id)->get('tbl_members')->row();
											if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;
										}

										//차량정보
										$truck = $this->db->where('idx', $delivery_fee_details->tr_id)->get('tbl_asset_truck')->row();
										if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;

										//위수탁관리비
//$wsm_info = $this->db->where('dp_id', $delivery_fee_details->dp_id)->where('df_month', $df_month)->where('apply_yn', 'Y')->get('tbl_delivery_fee_fixmfee')->row();
										$wsm_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_fixmfee')->row();
										//$wsm_info = $this->db->where('df_id', $delivery_fee_details->df_id)->where('apply_yn', 'Y')->get('tbl_delivery_fee_fixmfee')->row();

										if(empty($wsm_info->wst_mfee)) $wst_mfee = 0; else $wst_mfee = $wsm_info->wst_mfee;
										if(empty($wsm_info->mfee_vat)) $mfee_vat = 0; else $mfee_vat = $wsm_info->mfee_vat;
										if(empty($wsm_info->org_fee)) $org_fee = 0; else $org_fee = $wsm_info->org_fee;
										//if(empty($wsm_info->env_fee)) $env_fee = 0; else $env_fee = $wsm_info->env_fee;
										//if(empty($wsm_info->car_tax)) $car_tax = 0; else $car_tax = $wsm_info->car_tax;
										if(empty($wsm_info->grg_fee)) $mgrg_fee = 0; else $mgrg_fee = $wsm_info->grg_fee;
										if(empty($wsm_info->grg_fee_vat)) $mgrg_fee_vat = 0; else $mgrg_fee_vat = $wsm_info->grg_fee_vat;
										if(empty($wsm_info->etc)) $etc= 0; else $etc = $wsm_info->etc;
										$org_fee_vat = 0;
										$env_fee = 0;
										$env_fee_vat = 0;
										$car_tax = 0;
										$car_tax_vat = 0;
										$etc = 0;
										$wsm_sum = ($wst_mfee + $mfee_vat + $org_fee + $env_fee + $car_tax + $mgrg_fee + $mgrg_fee_vat + $etc);
										$tot_vat += $mfee_vat + $mgrg_fee_vat;

										//각종보험
										$insur_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_insur')->row();
										if(empty($insur_info->ins_car)) $ins_car = 0; else $ins_car = $insur_info->ins_car;
										if(empty($insur_info->ins_load)) $ins_load = 0; else $ins_load = $insur_info->ins_load;
										if(empty($insur_info->ins_grnt)) $ins_grnt = 0; else $ins_grnt = $insur_info->ins_grnt;
										$insur_sum = ($ins_car + $ins_load + $ins_grnt);

										//일반공제
										$gongje_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_gongje')->row();
										//$gongje_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_gongje')->row();
										if(empty($gongje_info->fine_fee)) $fine_fee = 0; else $fine_fee = $gongje_info->fine_fee;
										//if(empty($gongje_info->acc_app)) $acc_app = 0; else $acc_app = $gongje_info->acc_app;
										if(empty($gongje_info->not_paid)) $not_paid = 0; else $not_paid = $gongje_info->not_paid;
										if(empty($gongje_info->car_tax)) $car_tax = 0; else $car_tax = $gongje_info->car_tax;
										if(empty($gongje_info->env_fee)) $env_fee = 0; else $env_fee = $gongje_info->env_fee;
										//if(empty($gongje_info->mid_pay)) $mid_pay = 0; else $mid_pay = $gongje_info->mid_pay;
										//if(empty($gongje_info->trmt_sec)) $trmt_sec = 0; else $trmt_sec = $gongje_info->trmt_sec;
										//if(empty($gongje_info->etc)) $etc = 0; else $etc = $gongje_info->etc;
										if(empty($gongje_info->grg_fee)) $grg_fee = 0; else $grg_fee = $gongje_info->grg_fee;
										if(empty($gongje_info->grg_fee_vat)) $grg_fee_vat = 0; else $grg_fee_vat = $gongje_info->grg_fee_vat;
										$gongje_sum = ($fine_fee + $not_paid + $car_tax + $env_fee + $grg_fee + $grg_fee_vat);
										$tot_vat += $grg_fee_vat;

										//환급형공제
										$rf_gongje_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_refund_gongje')->row();
										if(empty($rf_gongje_info->gj_termination_mortgage)) $gj_termination_mortgage = 0; else $gj_termination_mortgage = $rf_gongje_info->gj_termination_mortgage;
										//if(empty($rf_gongje_info->gj_plate_mortgage)) $gj_plate_mortgage = 0; else $gj_plate_mortgage = $rf_gongje_info->gj_plate_mortgage;
										//if(empty($rf_gongje_info->gj_etc_refund)) $gj_etc_refund = 0; else $etc = $rf_gongje_info->gj_etc_refund;
										$rf_gongje_sum = ($gj_termination_mortgage);

										//---------------------------------------------------------------------------------  추가 항목
										if (!empty($all_sudang_group)) {
											foreach ($all_sudang_group as $sudang_info) {
												if($sudang_info->type == 'S') { //일반수당
												}
											}
										}
													
										$add_gongje01_vals = "";
										$add_gongje02_vals = "";
										$add_gongje03_vals = "";

										if (!empty($all_gongje_group)) {
											foreach ($all_gongje_group as $gongje_info) {
												$add_item = $this->db->where('pid', $gongje_info->idx)->where('df_month', $df_month)->where('dp_id', $delivery_fee_details->dp_id)->get('tbl_delivery_fee_add')->row();
												if($gongje_info->add_type == 'GW') { //위수탁
													if(!empty($add_item->amount)) {
														$wsm_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje01_vals += $val;
													} 
												} else if($gongje_info->add_type == 'GN') { //일반
													if(!empty($add_item->amount)) {
														$gongje_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje02_vals += $val;
													} 
												} else if($gongje_info->add_type == 'GR') { //환급
													if(!empty($add_item->amount)) {
														$rf_gongje_sum += $add_item->amount;
														$val = number_format($add_item->amount,0);
														$add_gongje03_vals += $val;
													} 
												}
											}
										}
										//---------------------------------------------------------------------------------  추가 항목



										$tot_gongje = ($wsm_sum + $insur_sum + $gongje_sum + $rf_gongje_sum);
										$tot_gongje_req = ($tot_gongje - $rf_gongje_sum);

 
										$lv = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_levy')->row();
										$pay_status = "납부전";
										$pay_status_color = "warning";
										if(!empty($lv->pay_status)) {
											if($lv->pay_status == "N") { $pay_status = "미납"; $pay_status_color = "danger"; }
											else if($lv->pay_status == "P") { $pay_status = "부분납"; $pay_status_color = "primary"; } 
											else if($lv->pay_status == "F") { $pay_status = "완납"; $pay_status_color = "success"; } 
										}


?>
 	  <table width="1000">
		  <tr>
			<td bgcolor="#ffffff">  <!-- 헤더 -->
			
			  <table width="1000">
				  <tr>
					<!--td class="x24" height="50px;" colspan="2" width="15%" style="border: 1px solid #000;text-align:center;"><img src="http://intra.delta-on.com/uploads/deltaon_logo.png" width="20px;"></td-->
					<td class="x24" width="20%" colspan="3" style="border: 1px solid #000;text-align:center;font-weight:bold;font-size:14px;"><?=$gj_rq_co ?></td>
					<td class="x24" width="65%" colspan="6" style="border: 1px solid #000;text-align:center;font-weight:bold;font-size:18px;"><?=substr($df_month,0,4)?>년 <?=substr($df_month,5,2)?>월분 용역수수료 및 위수탁관리비 정산서</td>
				  </tr>
			  </table>
			
			</td>
		  </tr>
		  <tr>
			<td bgcolor="#ffffff">  <!-- 기본정보 -->
			  <table width="1000">
				  <tr>
					<td class="x24" colspan="2" height="25px;" width="15%" style="border: 1px solid #000;text-align:center;font-weight:bold;color:blue;">기사정보 :</td>
					<td class="x24" colspan="8" width="85%" style="border: 1px solid #000;text-align:left;font-weight:bold;color:blue;"><?php if(!empty($dp->co_name)) echo $dp->co_name;?> > <?php if(!empty($truck->car_1)) echo $truck->car_1;?> > <?php if(!empty($truck->carinfo_11)) echo $truck->carinfo_11;?> <?php if(!empty($truck->car_4)) echo $truck->car_4;?></td>
				  </tr>
				  <!--tr>
					<td class="x24" height="25px;" width="15%" style="border: 1px solid #000;text-align:center;font-weight:bold;">용 역 명 :</td>
					<td class="x24" width="85%" style="border: 1px solid #000;text-align:left;font-weight:bold;">xxxxxxxxxxxxxx</td>
				  </tr>
				  <tr>
					<td class="x24" height="25px;" width="15%" style="border: 1px solid #000;text-align:center;font-weight:bold;">수요기관 :</td>
					<td class="x24" width="85%" style="border: 1px solid #000;text-align:left;font-weight:bold;">xxxxxxxxx</td>
				  </tr-->
			  </table>
			</td>
		  </tr>
		  <tr>
			<td bgcolor="#ffffff">  <!-- 지급내역 -->
			  <table width="1000" style="border: 1px solid #000;border: 1px solid #fff;">
				  <tr>
					<td class="x24" rowspan="6" width="3%" style="border: 1px solid #000;text-align:center;">지<br/>급<br/>내<br/>역</td>
					<td class="x24" height="40" colspan="2" rowspan="2" width="15%" style="border: 1px solid #000;text-align:center;">구 분</td>
					<td class="x24" height="20" colspan="3" width="40%" style="border: 1px solid #000;text-align:center;">거래단가</td>
					<td class="x24" colspan="4" width="42%" style="border: 1px solid #000;text-align:center;">거래금액</td>
				  </tr>
				  <tr>
					<td class="x24" height="20" width="13%" style="border: 1px solid #000;text-align:center;">공급단가</td>
					<td class="x24" width="10%" style="border: 1px solid #000;text-align:center;">부가세</td>
					<td class="x24" width="17%" style="border: 1px solid #000;text-align:center;">합계</td>

					<td class="x24" width="8%" style="border: 1px solid #000;text-align:center;">수량</td>
					<td class="x24" width="11%" style="border: 1px solid #000;text-align:center;">공급단가</td>
					<td class="x24" width="10%" style="border: 1px solid #000;text-align:center;">부가세</td>
					<td class="x24" width="13%" style="border: 1px solid #000;text-align:center;">합계</td>
				  </tr>
				  <tr>
					<td class="x24" rowspan="2" width="5%" style="border: 1px solid #000;text-align:center;">거래<br/>내역</td>
					<td class="x24" height="20" width="10%" style="border: 1px solid #000;text-align:center;"></td>

					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>

					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
				  </tr>
				  <tr>
					<td class="x24" colspan="5" style="border: 1px solid #000;text-align:center;">소계</td>

					<td class="x24" height="20" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
				  </tr>
				  <tr>
					<td class="x24" height="20" style="border: 1px solid #000;text-align:center;">기타</td>
					<td class="x24" style="border: 1px solid #000;text-align:center;"></td>

					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>

					<td class="x24" style="border: 1px solid #000;text-align:center;">소계</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
				  </tr>
				  <tr>
					<td class="x24" colspan="6" style="border: 1px solid #000;text-align:center;font-weight:bold;">지급총액</td>

					<td class="x24" height="20" style="border: 1px solid #000;text-align:right;font-weight:bold;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;font-weight:bold;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;font-weight:bold;padding-right:5px;">0</td>
				  </tr>
			  </table>
			</td>
		  </tr>
		  <tr>
			<td style="border: 1px solid #000;padding-top:20px;">    <!-- 공제내역 -->
			  <table width="1000" style="border: 1px solid #000;border: 1px solid #fff;">
				  <tr>
					<td class="x24" rowspan="11" width="3%" style="border: 1px solid #000;text-align:center;">공<br/>제<br/>내<br/>역</td>
					<td class="x24" colspan="3" height="20" width="32%" style="border: 1px solid #000;text-align:center;">구 분</td>
					<td class="x24" width="10%" style="border: 1px solid #000;text-align:center;">공급가</td>
					<td class="x24" width="10%" style="border: 1px solid #000;text-align:center;">부가세</td>

					<td class="x24" width="12%" style="border: 1px solid #000;text-align:center;">합계</td>
					<td class="x24" width="12%" style="border: 1px solid #000;text-align:center;">구분</td>
					<td class="x24" colspan="2" width="21%" style="border: 1px solid #000;text-align:center;">금액</td>
				  </tr>
				  <tr>
					<td class="x24" rowspan="4" width="5%" style="border: 1px solid #000;text-align:center;">위수<br/>탁관<br/>비등</td>
					<td class="x24" colspan="2" height="20" width="27%" style="border: 1px solid #000;text-align:center;">위ㆍ수탁관리비</td>

					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?= number_format($wst_mfee) ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><? if(!empty($mfee_vat)) echo number_format($mfee_vat,0); ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?= number_format($wst_mfee+$mfee_vat) ?></td>

					<td class="x24" height="20" style="border: 1px solid #000;text-align:center;">협회비</td>
					<td class="x24" colspan="2" style="border: 1px solid #000;text-align:right;padding-right:5px;"><? if(!empty($org_fee)) echo number_format($org_fee,0); ?></td>
				  </tr>
				  <tr>
					<td class="x24" colspan="2" height="20" width="27%" style="border: 1px solid #000;text-align:center;">차   고  지   비</td>

					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><? if(!empty($mgrg_fee)) echo number_format($mgrg_fee,0); ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><? if(!empty($mgrg_fee_vat)) echo number_format($mgrg_fee_vat,0); ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?= number_format($mgrg_fee+$mgrg_fee_vat) ?></td>

					<td class="x24" height="20" style="border: 1px solid #000;text-align:center;"> </td>
					<td class="x24" colspan="2" style="border: 1px solid #000;text-align:right;padding-right:5px;"> </td>
				  </tr>
				  <tr>
					<td class="x24" colspan="2" height="20" width="27%" style="border: 1px solid #000;text-align:center;"><?php if(!empty($add_gongje01_cols)) echo $add_gongje01_cols;?></td>

					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?php if(!empty($add_gongje01_vals)) echo number_format($add_gongje01_vals,0); ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?php if(!empty($add_gongje01_vals)) echo  number_format($add_gongje01_vals); ?></td>

					<td class="x24" height="20" style="border: 1px solid #000;text-align:center;"> </td>
					<td class="x24" colspan="2" style="border: 1px solid #000;text-align:right;padding-right:5px;"> </td>
				  </tr>

				  <tr>
					<td class="x24" colspan="2" height="20" width="27%" style="border: 1px solid #000;text-align:center;font-weight:bold;">소 계</td>

					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;font-weight:bold;"><?= number_format($wst_mfee+$mgrg_fee) ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;font-weight:bold;"><?= number_format($mfee_vat+$mgrg_fee_vat) ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;font-weight:bold;"><?= number_format($wst_mfee+$mgrg_fee+$mfee_vat+$mgrg_fee_vat) ?></td>

					<td class="x24" colspan="3" style="border: 1px solid #000;text-align:right;padding-right:5px;"> </td>
				  </tr>

				  <tr>
					<td class="x24" height="20" style="border: 1px solid #000;text-align:center;">보험</td>
					<td class="x24" width="10%" style="border: 1px solid #000;text-align:center;">자동차</td>
					<td class="x24" colspan="2" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?= number_format($ins_car) ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:center;">적재물</td>
					<td class="x24" style="border: 1px solid #000;text-align:center;"><?= number_format($ins_load) ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:center;font-weight:bold;">합계</td>
					<td class="x24" style="border: 1px solid #000;text-align:center;font-weight:bold;" colspan='2'><?= number_format($ins_load+$ins_car) ?></td>
				  </tr>
 
				  <tr>
					<td class="x24" rowspan="6" width="5%" style="border: 1px solid #000;text-align:center;">일반<br/>공제</td>
					<td class="x24" height="20" style="border: 1px solid #000;text-align:center;">구 분</td>
					<td class="x24" style="border: 1px solid #000;text-align:center;">공급가</td>
					<td class="x24" style="border: 1px solid #000;text-align:center;">부가세</td>
					<td class="x24" style="border: 1px solid #000;text-align:center;">합계</td>

					<td class="x24" style="border: 1px solid #000;text-align:center;">구분</td>
					<td class="x24" colspan="2" style="border: 1px solid #000;text-align:center;">금액</td>
					<td class="x24" style="border: 1px solid #000;text-align:center;">합계</td>
				  </tr>

				  <tr>
					<td class="x24" height="20" style="border: 1px solid #000;text-align:center;">차고지비</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?= number_format($grg_fee) ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?= number_format($grg_fee_vat) ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?= number_format($grg_fee+$grg_fee_vat) ?></td>

					<td class="x24" style="border: 1px solid #000;text-align:center;">자동차세</td>
					<td class="x24" style="border: 1px solid #000;text-align:center;"></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?= number_format($car_tax) ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
				  </tr>


				  <tr>
					<td class="x24" height="20" style="border: 1px solid #000;text-align:center;">부가세환수</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>

					<td class="x24" style="border: 1px solid #000;text-align:center;">환경부담</td>
					<td class="x24" style="border: 1px solid #000;text-align:center;"></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?= number_format($env_fee) ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
				  </tr>
				  <tr>
					<td class="x24" height="20" style="border: 1px solid #000;text-align:center;">과 태 료</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?= number_format($fine_fee) ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>

					<td class="x24" rowspan="2" style="border: 1px solid #000;text-align:center;"><?php if(!empty($add_gongje01_cols)) echo $add_gongje01_cols;?></td>
					<td class="x24" rowspan="2" style="border: 1px solid #000;text-align:center;"></td>
					<td class="x24" rowspan="2" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?php if(!empty($add_gongje02_vals)) echo number_format($add_gongje02_vals,0); ?></td>
					<td class="x24" rowspan="2" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?php if(!empty($add_gongje02_vals)) echo number_format($add_gongje02_vals,0); ?></td>
				  </tr>
				  <tr>
					<td class="x24" height="20" style="border: 1px solid #000;text-align:center;">미수금</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?= number_format($not_paid) ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>
				  </tr>



				  <tr>
					<td class="x24" height="20" style="border: 1px solid #000;text-align:center;">환 급 형</td>
					<td class="x24" colspan="2" style="border: 1px solid #000;text-align:right;padding-right:5px;"><?= number_format($gj_termination_mortgage) ?></td>
					<td class="x24" style="border: 1px solid #000;text-align:right;padding-right:5px;">0</td>

					<td class="x24" colspan="5" style="border: 1px solid #000;text-align:center;"></td>
				  </tr>
				  <tr>
					<td class="x24" colspan="5" height="20" style="border: 1px solid #000;text-align:center;font-weight:bold;">공제총액</td>
					<td class="x24" colspan="5" style="border: 1px solid #000;text-align:right;font-weight:bold;padding-right:5px;"><?= number_format($tot_gongje) ?></td>
				  </tr>


				  <tr>
					<td class="x24" rowspan="2" colspan="3" style="border: 1px solid #000;text-align:center;font-weight:bold;font-size:17px;">공제후지급액</td>
					<td class="x24" colspan="2"  height="30" style="border: 1px solid #000;text-align:center;font-weight:bold;font-size:17px;">공급가</td>
					<td class="x24" colspan="2" style="border: 1px solid #000;text-align:center;font-weight:bold;font-size:17px;">부가세</td>
					<td class="x24" colspan="3" style="border: 1px solid #000;text-align:center;font-weight:bold;font-size:17px;">합계</td>
				  </tr>

				  <tr>
					<td class="x24" colspan="2" height="30" style="border: 1px solid #000;text-align:right;font-weight:bold;padding-right:5px;"><?= number_format($tot_gongje_req-$tot_vat) ?></td>
					<td class="x24" colspan="2" style="border: 1px solid #000;text-align:right;font-weight:bold;padding-right:5px;"><?= number_format($tot_vat) ?></td>
					<td class="x24" colspan="3" style="border: 1px solid #000;text-align:right;font-weight:bold;padding-right:5px;font-size:17px;"><?= number_format($tot_gongje_req) ?></td>
				  </tr>




			  </table>
			
			</td>
		  </tr>
	  </table>

							

<?php


									
					} // temp
			}
 }
                                ?>

