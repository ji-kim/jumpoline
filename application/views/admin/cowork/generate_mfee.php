<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

?>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
$lastday=array(0,31,28,31,30,31,30,31,31,30,31,30,31);
if (empty($df_month)) $df_month = date('Y-m');
$dyear = substr($df_month,0,4);
$dmonth = substr($df_month,5,2);
$lday = (int)($lastday[(int)$dmonth]);
$sday = $dyear."-".$dmonth."-01";
$eday = $dyear."-".$dmonth."-".$lday;
/*
그리고 파트너 계약일 기준 위수탁관리비 중: 위수탁관리비, 위수탁관리비부가세 이두가지가 자동 일할계산 되도록
*/

?>
<script>
	function selectCompany(md,params) {
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/'+md+'/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
</script>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-custom" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <strong>관리비 & 수수료 생성</strong>
                </div>
            </div>
            <form id="form" role="form" enctype="multipart/form-data"
                  action="<?php echo base_url() ?>admin/cowork/generate_mfee" method="post"
                  class="form-horizontal form-groups-bordered">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">구분
                            <span
                                class="required"> *</span></label>

                        <div class="col-sm-5">
                            
                                         <div class="checkbox c-checkbox pull-left" data-toggle="tooltip"
                                              data-placement="top"
                                              title="계약에 포함된 관리비 생성">
                                             <label class="needsclick">
                                                 <input class="select_one"
                                                        type="checkbox"  name="calculate_progress" value="through_contracts"
                                                        id="generate_type_select1" <?=((!empty($calculate_progress) && $calculate_progress=="through_contracts")|| (empty($calculate_progress)))?"checked":""?>>
                                                 <span class="fa fa-check"></span>
                                                 <small>계약건 생성</small>
                                             </label>
                                         </div>
										<div class="checkbox c-checkbox pull-left" data-toggle="tooltip"
                                              data-placement="top"
                                              title="비 계약건 관리비 생성">
                                             <label class="needsclick">
                                                 <input class="select_one"
                                                        type="checkbox"  name="calculate_progress" value="through_coops"
                                                        id="generate_type_select2" <?=(!empty($calculate_progress) && $calculate_progress=="through_coops")?"checked":""?>>
                                                 <span class="fa fa-check"></span>
                                                 <small>비계약건(거래처) 생성</small>
                                             </label>
                                         </div>
                        </div>
                    </div>
                    <div class="form-group" id="generate_type1">
                        <label for="field-1" class="col-sm-3 control-label">거래처선택
                            <span
                                class="required"> *</span></label>

                        <div class="col-sm-5">

							<input type="hidden" name="ws_co_id" id="ws_co_id" value="<?php if(!empty($ws_co_id)) echo $ws_co_id; ?>">
							<input type="hidden" name="co_code" id="co_code" value="<?php if(!empty($co_code)) echo $co_code; ?>">
							<input type="text" name="co_name" id="co_name" value="<?php if(!empty($co_name)) echo $co_name; ?>" class="form-control" style="width:90%;background-color:yellow;" onClick="selectCompany('coop','ws_co_id||co_name||__||__||__||__||__||co_code');">
                        </div>
                    </div>
                    <div class="form-group" id="generate_type2">
                        <label for="field-1" class="col-sm-3 control-label">계약건선택
                            <span
                                class="required"> *</span></label>

                        <div class="col-sm-5">
                            <select name="ct_id" class="form-control select_box" style="width:100%;">
                                <option value="">계약건선택</option>
                                <?php if (!empty($all_contract_info)): foreach ($all_contract_info as $v_contract_info) :
                                    if (!empty($v_contract_info->tr_title)) {
                                        $title = $v_contract_info->tr_title;
                                    ?>
                                    <option value="<?php echo $v_contract_info->idx; ?>"
                                        <?php
                                        if (!empty($ct_id)) {
                                            echo $v_contract_info->idx == $ct_id ? 'selected' : '';
                                        }
                                        ?>>[<?php echo $v_contract_info->rr_company ?>] <?php echo $title ?> - <?php echo $v_contract_info->group_name ?></option>
                                <?php 
                                    }
								endforeach; ?>
                                <?php endif; ?>
                           </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">생성 년월 <span
                                class="required"> *</span></label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input type="text" value="<?php
                                if (!empty($df_month)) {
                                    echo $df_month;
                                }
                                ?>" class="form-control monthyear" name="df_month"
                                       data-format="yyyy/mm/dd">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="border-none">
                        <label for="field-1" class="col-sm-3 control-label"></label>
                        <div class="col-sm-5">
                            * 생성시 기존 데이터는 삭제됩니다.
                        </div>
                    </div>
                    <div class="form-group" id="border-none">
                        <label for="field-1" class="col-sm-3 control-label"></label>
                        <div class="col-sm-5">
                            <button id="submit" type="submit" name="flag" value="1"
                                    class="btn btn-primary btn-block">생 성
                            </button>
                        </div>
                    </div>
                    <div class="form-group" id="border-none">
                        <label for="field-1" class="col-sm-3 control-label"></label>
                        <div class="col-sm-5">
                            <button id="submit" type="submit" name="flag" value="2"
                                    class="btn btn-warning btn-block">초기화
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php if (!empty($flag) && $flag == '1'): 
		//추가항목 레코드 생성 -> 위수탁 관리사 반드시 선택되어있어야 함
		// 1. 수당
		$sql = "";
        if (!empty($all_sudang_group)) {
			foreach ($all_sudang_group as $sudang_info) {
				if($sudang_info->type == '01') { //일반수당
					$chk = $this->db->where('ws_co_id', $sudang_info->ws_co_id)->where('df_month', $df_month)->where('add_type', 'S')->get('tbl_delivery_fee_added_items')->row();
					if (!empty($chk)) {
						$sql = "update tbl_delivery_fee_added_items set ws_co_id = '".$sudang_info->ws_co_id."',df_month = '".$df_month."',title = '".$sudang_info->item."',add_type = 'S' where idx='".$sudang_info->idx."'";
					} else {
						$sql = "insert into tbl_delivery_fee_added_items set ws_co_id = '".$sudang_info->ws_co_id."',df_month = '".$df_month."',title = '".$sudang_info->item."',add_type = 'S'";
					}
					$this->cowork_model->db->query($sql);
					echo $sql;
				}
			}
		}
			
		// 2. 공제
        if (!empty($all_gongje_group)) {
			foreach ($all_gongje_group as $gongje_info) {
				if($gongje_info->p_category == '01') { //위수탁
					$chk = $this->db->where('ws_co_id', $gongje_info->ws_co_id)->where('df_month', $df_month)->where('add_type', 'GW')->get('tbl_delivery_fee_added_items')->row();
					if (!empty($chk)) {
						$sql = "update tbl_delivery_fee_added_items set ws_co_id = '".$gongje_info->ws_co_id."',df_month = '".$df_month."',title = '".$gongje_info->item."',add_type = 'GW' where idx='".$gongje_info->idx."'";
					} else {
						$sql = "insert into tbl_delivery_fee_added_items set ws_co_id = '".$gongje_info->ws_co_id."',df_month = '".$df_month."',title = '".$gongje_info->item."',add_type = 'GW'";
					}
				} else if($gongje_info->p_category == '02') { //일반
					$chk = $this->db->where('ws_co_id', $gongje_info->ws_co_id)->where('df_month', $df_month)->where('add_type', 'GN')->get('tbl_delivery_fee_added_items')->row();
					if (!empty($chk)) {
						$sql = "update tbl_delivery_fee_added_items set ws_co_id = '".$gongje_info->ws_co_id."',df_month = '".$df_month."',title = '".$gongje_info->item."',add_type = 'GN' where idx='".$gongje_info->idx."'";
					} else {
						$sql = "insert into tbl_delivery_fee_added_items set ws_co_id = '".$gongje_info->ws_co_id."',df_month = '".$df_month."',title = '".$gongje_info->item."',add_type = 'GN'";
					}
				} else if($gongje_info->p_category == '03') { //환급
					$chk = $this->db->where('ws_co_id', $gongje_info->ws_co_id)->where('df_month', $df_month)->where('add_type', 'GR')->get('tbl_delivery_fee_added_items')->row();
					if (!empty($chk)) {
						$sql = "update tbl_delivery_fee_added_items set ws_co_id = '".$gongje_info->ws_co_id."',df_month = '".$df_month."',title = '".$gongje_info->item."',add_type = 'GR' where idx='".$gongje_info->idx."'";
					} else {
						$sql = "insert into tbl_delivery_fee_added_items set ws_co_id = '".$gongje_info->ws_co_id."',df_month = '".$df_month."',title = '".$gongje_info->item."',add_type = 'GR'";
					}
				}
				$this->cowork_model->db->query($sql);
				//echo $sql;
			}
		}
		

?>
    <div class="row">
        <div class="col-sm-12" data-offset="0">
            <div class="panel panel-custom">
                <div class="panel-heading">
                    <div class="panel-title">
                        <span>
                            <strong>관리비 & 수수료 생성 <?php
                                if (!empty($df_month)) {
                                    echo ' <span class="text-danger">' . date('Y-m', strtotime($df_month)) . '</span>';
                                }
                                ?></strong>
                        </span>
                    </div>
                </div>
                <!-- Table -->

                <table class="table table-striped " id="Transation_DataTables" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="col-sm-1">No</th>
                        <th><strong>공제청구사</strong></th>
                        <th><strong>실수요처</strong></th>
                        <th><strong>운영공제(송금)사</strong></th>
                        <th><strong>사업자등록</strong></th>
                        <th><strong>차량번호</strong></th>
                        <th><strong>상태</strong></th>
                        <th> </th>
                    </tr>
                    </thead>
                    <tbody>
                  <?php
                    if (!empty($all_dp_list)) {
						foreach ($all_dp_list as $dp_details) {
							
							// 계약기간 확인 N, O
							$inm = "";
							$outm = "";
							if(!empty($dp_details->N)) $inm = substr($dp_details->N,0,7);
							if(!empty($dp_details->O)) $outm = substr($dp_details->O,0,7);
							if((!empty($dp_details->N) && !empty($dp_details->O)) && ((int)$df_month >= (int)$inm && (int)$df_month <= (int)$outm)) {

							$chk = $this->db->where('df_month', $df_month)->where('dp_id', $dp_details->dp_id)->get('tbl_delivery_fee')->row();
							if(count($chk)==0) { //---------중복처리 나중에 원인 찾을것


								$sn = $total_count--;
								$tot_gongje_req = $tot_gongje = 0;
								$wsm_sum = $insur_sum = $gongje_sum = $rf_gongje_sum = 0;

								$sql_basic = ""; // 기본정보
								$sql_co_info = ""; // 회사들정보
								$sql_unitprice = ""; // 계약단가정보
								$sql_bank = ""; // 은행정보
								$sql_basic_sudang = ""; // 기본수당
								$sql_basic_gongje = ""; // 기본공제
								$sql_insure = ""; // 보험공제
								$sql_ilhal = "";

								$sql_mf = ""; // 관리비정보

								//일할계산처리
								/* 그리고 1일이 되었는 3일이 되었든 아님 10일지났든 
								계약당월 이런경우 협회비 차고지비 보험료는 보험만료와 계약만료일 과 비교해서 
								생성이 되도록 해야하는것 */
								$is_ilhal = "N";
								if($df_month == $inm || $df_month == $outm) {
									$ilhal_days = 0;
									$s = substr($dp_details->N,8,2);
									$e = substr($dp_details->O,8,2);
									if($df_month == $inm && $df_month == $outm) { // 당월 입.퇴사자
										$ilhal_days = (int)$e - (int)$s + 1;
									} else if($df_month == $inm) { // 당월 입사자
										$ilhal_days = $lday - (int)$s + 1;
									} else if($df_month == $outm) { // 당월 퇴사자
										$ilhal_days = (int)$e;
									}
									$is_ilhal = "Y";
									$sql_ilhal = ", remark ='일할계산(".$ilhal_days."일)'";
								}

								//--------------------------------- 0. 기본정보
								$sql_basic .= "ct_id ='".$ct_id."'";
								$sql_basic .= ",dp_id ='".$dp_details->dp_id."'";
								$sql_basic .= ",userid ='".$dp_details->userid."'";
								$sql_basic .= ",df_month ='".$df_month."'";
								$sql_basic .= ",co_code ='".$dp_details->code."'";
								$sql_basic .= ",co_name ='".$dp_details->co_name."'";
								$sql_basic .= ",C ='".$dp_details->jiip_co."'";
								$sql_basic .= ",D ='".$dp_details->rco_name."'";
								$sql_basic .= ",E ='".$dp_details->driver."'";
								$sql_basic .= ",status ='대기'";
								if(!empty($dp_details->bs_number)) $sql_basic .= ",bs_number ='".$dp_details->bs_number."'"; // 사업자등록  *---------------------*
								else if(!empty($dp_details->reg_number)) $sql_basic .= ",bs_number ='".$dp_details->reg_number."'"; // 사업자등록없는경우 주민번호로 *---------------------*
								$sql_basic .= ",applytax_yn ='".$dp_details->tax_yn."'";

								$vat_acc_yn = 'N'; //부가세 누적관련
								$acc_vat_dmonth_cnt = 0;
								if($dp_details->acc_vat_yn == "Y") {
									$sql_tax2 = ",vat_acc_yn='Y'";
									$acc_vat_dmonth_cnt = $df_month;
								}
								$sql_basic .= ",vat_acc_yn ='".$vat_acc_yn."'";
								//부가세 누적 = acc_vat_paid_yn 부가세지급여부 acc_vat_month=부가세 누적개월
								$vv = $this->db->select('sum(acc_vat_dmonth) as avd, count(acc_vat_dmonth) as cnt')->where('dp_id', $dp_details->ws_co_id)->get('tbl_delivery_fee')->row();
								if(!empty($vv->avd)) $sql_basic .= ",acc_vat_sum='".$vv->avd."'";
								if(!empty($vv->cnt)) $sql_basic .= ",acc_vat_dmonth_cnt ='".$vv->cnt."'";


								//--------------------------------- 1. 공제청구사(위수탁관리사) 등 회사정보 
								$gj_rq_co = $this->db->where('dp_id', $dp_details->ws_co_id)->get('tbl_members')->row();
								$r_co_id = $dp_details->rco_id;//실수요처	
								$r_co_name = $dp_details->rco_name;//실수요처	
								$s_co_name = $dp_details->driver;//운영공제(송금)사	
								
								//과세여부
								$tax_yn = $dp_details->tax_yn;
								// 회사정보쿼리
								if(!empty($dp_details->tr_id)) $sql_co_info .= ",tr_id='".$dp_details->tr_id."'";
								if(!empty($dp_details->ws_co_id)) $sql_co_info .= ", gongje_req_co='".$dp_details->ws_co_id."'";
								//if(!empty($gj_rq_co->sm_co_id)) $sql_co_info .= ", sendmoney_co='".$gj_rq_co->sm_co_id."'"; //-> 송금사 저장문제 체크

								//--------------------------------- 2. 관리비정보 
								$sql_mf .= "ct_id ='".$ct_id."'";
								$sql_mf .= ",dp_id ='".$dp_details->dp_id."'";
								$sql_mf .= ",df_month ='".$df_month."'";
								$sql_mf .= ",co_code ='".$dp_details->code."'";
								if(!empty($dp_details->ws_co_id)) $sql_mf .= ", ws_co_id='".$dp_details->ws_co_id."'";
								//$mf = $this->db->where('dp_id', $dp_details->dp_id)->get('tbl_scontract_mfee')->row(); //기본정보
								//if(!empty($mf->mfee_etc)) $sql_mf .= ",mfee_etc='".$mf->mfee_etc."'";
								$mf = $this->db->where('dp_id', $dp_details->dp_id)->where('master_yn', 'Y')->get('tbl_delivery_fee_fixmfee_set')->row(); //기본정보

								
								
								if(!empty($mf->wst_mfee)) $wst_mfee = $mf->wst_mfee; else $wst_mfee = 0;
								$wsm_sum += $wst_mfee;
								
								if(!empty($mf->mfee_vat)) { $mfee_vat = $mf->mfee_vat; $wsm_sum += $mf->mfee_vat; } else $mfee_vat = 0;
								if(!empty($mf->org_fee)) { $org_fee = $mf->org_fee; $wsm_sum += $mf->org_fee; } else $org_fee = 0;
								if(!empty($mf->grg_fee)) { $grg_fee = $mf->grg_fee; $wsm_sum += $mf->grg_fee; } else $grg_fee = 0;
								if(!empty($mf->grg_fee_vat)) { $grg_fee_vat = $mf->grg_fee_vat; $wsm_sum += $mf->grg_fee_vat; } else $grg_fee_vat = 0;
								//if(!empty($mf->env_fee)) { $sql_mf .= ",env_fee='".$mf->env_fee."'"; $wsm_sum += $mf->env_fee; }
								//if(!empty($mf->env_fee_vat)) { $sql_mf .= ",env_fee_vat='".$mf->env_fee_vat."'"; $wsm_sum += $mf->env_fee_vat; }
								//if(!empty($mf->org_fee_vat)) { $sql_mf .= ",org_fee_vat='".$mf->org_fee_vat."'"; $wsm_sum += $mf->org_fee_vat; }
								//if(!empty($mf->car_tax)) { $sql_mf .= ",car_tax='".$mf->car_tax."'"; $wsm_sum += $mf->car_tax; }
								//if(!empty($mf->car_tax_vat)) { $sql_mf .= ",car_tax_vat='".$mf->car_tax_vat."'"; $wsm_sum += $mf->car_tax_vat; }
								//if(!empty($mf->etc)) { $sql_mf .= ",etc='".$mf->etc."'"; $wsm_sum += $mf->etc; }
								$org_fee_vat = 0;
								$env_fee = 0;
								$env_fee_vat = 0;
								$car_tax = 0;
								$car_tax_vat = 0;
								$etc = 0;
								// 일할계산
								if($is_ilhal == "Y") {
									if($df_month == $inm || $df_month != $outm) {
										if(!empty($wst_mfee) && $wst_mfee > 0) $wst_mfee = (int)(($wst_mfee/$lday) * $ilhal_days);
										if(!empty($mfee_vat) && $mfee_vat > 0) $mfee_vat = (int)(($mfee_vat/$lday) * $ilhal_days);
									} else if($ilhal_days < 15) {
										if(!empty($wst_mfee) && $wst_mfee > 0) $wst_mfee = (int)($wst_mfee/2);
										if(!empty($mfee_vat) && $mfee_vat > 0) $mfee_vat = (int)($mfee_vat/2);
									}

									//if(!empty($org_fee) && $org_fee > 0) $org_fee = (int)(($org_fee/$lday) * $ilhal_days);
									//if(!empty($grg_fee_vat) && $grg_fee_vat > 0) $grg_fee_vat = (int)(($grg_fee_vat/$lday) * $ilhal_days);
								}
								$sql_mf .= ",wst_mfee='".$wst_mfee."'"; 
								$sql_mf .= ",mfee_vat='".$mfee_vat."'";
								$sql_mf .= ",org_fee='".$org_fee."'";
								$sql_mf .= ",grg_fee='".$grg_fee."'"; 
								$sql_mf .= ",grg_fee_vat='".$grg_fee_vat."'"; 

								//--------------------------------- 3. 은행정보 
								if(!empty($dp_details->bank_code)) $sql_bank .= ",bank_code='".$dp_details->bank_code."'";
								if(!empty($dp_details->bank)) $sql_bank .= ",bank='".$dp_details->bank."'";
								if(!empty($dp_details->account_no)) $sql_bank .= ",account_no='".$dp_details->account_no."'";
								if(!empty($dp_details->account_name)) $sql_bank .= ",account_name='".$dp_details->account_name."'";
								if(!empty($dp_details->account_relation)) $sql_bank .= ",account_relation='".$dp_details->account_relation."'";

								//--------------------------------- 4. 계약단가정보 
								if(!empty($ct_id)) {
									$cu = $this->db->where('dp_id', $dp_details->dp_id)->where('ct_id', $ct_id)->get('tbl_scontract_co')->row(); //기본정보
									if(!empty($cu->val1)) $sql_unitprice .= ",unit_price1='".$cu->val1."'";
									if(!empty($cu->val2)) $sql_unitprice .= ",unit_price2='".$cu->val2."'";
									if(!empty($cu->val3)) $sql_unitprice .= ",unit_price3='".$cu->val3."'";
									if(!empty($cu->val4)) $sql_unitprice .= ",unit_price4='".$cu->val4."'";
								}

								//--------------------------------- 5. 수당  
								if(!empty($ct_id)) {
									$sd = $this->db->where('dp_id', $dp_details->dp_id)->where('ct_id', $ct_id)->get('tbl_delivery_fee_bsudang')->row(); //기본정보
									if((!empty($sd->sanje_yn) && !empty($sd->sanje_amount)) && $sd->sanje_yn == "Y") $sanje_amount = $sd->sanje_amount; else $sanje_amount = 0;

									if(!empty($sd->manager_pickup)) $sql_basic_sudang .= ",manager_pickup='".$sd->manager_pickup."'";
									if(!empty($sd->team_leader)) $sql_basic_sudang .= ",team_leader='".$sd->team_leader."'";
									if(!empty($sd->diffcult_area)) $sql_basic_sudang .= ",diffcult_area='".$sd->diffcult_area."'";
									if(!empty($sd->subside)) $sql_basic_sudang .= ",subside='".$sd->subside."'";
									if(!empty($sd->hoisik)) $sql_basic_sudang .= ",hoisik='".$sd->hoisik."'";
									if(!empty($sd->sudang_etc)) $sql_basic_sudang .= ",sudang_etc='".$sd->sudang_etc."'";
									if(!empty($sd->sanje_yn)) $sql_basic_sudang .= ",sanje_yn='".$sd->sanje_yn."'";
									$sql_basic_sudang .= ",sanje_amount='".$sanje_amount."'";
								}

								//--------------------------------- 6. 보험 tbl_asset_truck_insur_sub tr_id & pay_month  pay_amt
								if(!empty($dp_details->tr_id)) {
									$qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
									$qry .= " where a.type = '자동차' and b.gj_month = '".$df_month."' and a.tr_id = '".$dp_details->tr_id."'"; // and a.active_yn='Y'";
									$ins = $this->cowork_model->db->query($qry)->row();
									if(!empty($ins->pay_amt)) 
										$ins_fee = $ins->pay_amt;
									else
										$ins_fee =0;

									$qry = "select sum(b.pay_amt) as pay_amt from tbl_asset_truck_insur a left join tbl_asset_truck_insur_sub b on a.idx = b.pid";
									$qry .= " where a.type = '적재물' and b.gj_month = '".$df_month."' and a.tr_id = '".$dp_details->tr_id."'"; // and a.active_yn='Y'";
									$lins = $this->cowork_model->db->query($qry)->row();
									if(!empty($lins->pay_amt)) 
										$lins_fee = $lins->pay_amt;
									else
										$lins_fee =0;

									// 일할계산
									if($is_ilhal == "Y") {
										//if(!empty($ins_fee) && $ins_fee > 0) $ins_fee = ($ins_fee/$lday) * $ilhal_days;
										//if(!empty($lins_fee) && $lins_fee > 0) $lins_fee = ($lins_fee/$lday) * $ilhal_days;
									}
									$sql_insure .= ",ins_car ='".$ins_fee."'";
									$sql_insure .= ",ins_load ='".$lins_fee."'";

								}

								//DB 삽입
								$sql = "INSERT INTO tbl_delivery_fee SET $sql_basic $sql_co_info $sql_unitprice $sql_bank $sql_basic_sudang $sql_basic_gongje $sql_ilhal";
								$this->cowork_model->db->query($sql);
								$df_id = $this->cowork_model->db->insert_id(); 
								
								//전월 미납건체크
								$pdate = $df_month . "-01";
								$pm=date("Y-m",strtotime($pdate.'-1month')); 
								$not_paid = 0;
								$plv = $this->db->where('df_month', $pm)->where('dp_id', $dp_details->dp_id)->get('tbl_delivery_fee_levy')->row();
								if(!empty($plv->pay_status) && $plv->pay_status != 'B') { //납부전상태가 아니면
									$not_paid = $plv->balance_amount;
									$sql = "insert into tbl_delivery_fee_gongje set not_paid = '".$not_paid."', df_id='".$df_id."'";
									$this->cowork_model->db->query($sql);
								}

								//관리비 tbl_delivery_fee_fixmfee
								$sql = "INSERT INTO tbl_delivery_fee_fixmfee SET $sql_mf , df_id='$df_id'";
								$this->cowork_model->db->query($sql);

								//보험 
								$sql = "INSERT INTO tbl_delivery_fee_insur SET df_id='$df_id', dp_id='".$dp_details->dp_id."', df_month='$df_month', ws_co_id = '".$dp_details->ws_co_id."'".$sql_insure;
								$this->cowork_model->db->query($sql);
	
	
	
								// 일할계산
								if($is_ilhal == "Y") {
									//echo $s_co_name.$sql_mf." <br/>"; //if(!empty($s_co_name)) echo $s_co_name;
								}

	
	/*
								//수납생성 tbl_delivery_fee_levy
								$rf_gongje_sum = 0 ; //($gj_termination_mortgage + $gj_plate_mortgage + $gj_etc_refund);
								$tot_gongje = ($wsm_sum + $insur_sum + $gongje_sum + $rf_gongje_sum);
								$tot_gongje_req = ($tot_gongje - $rf_gongje_sum);

								$sql = "INSERT INTO tbl_delivery_fee_levy SET df_id='$df_id', dp_id='".$dp_details->dp_id."', df_month='$df_month', gongje_req_co = '".$dp_details->ws_co_id."'";
								$sql .= ", prev_misu = '$not_paid' ,gn_amount = '".$tot_gongje_req."', paid_amount = '0', balance_amount = '".$tot_gongje_req."'";
								$this->cowork_model->db->query($sql);
	*/
	/*

								//보험 tbl_delivery_fee_insur
											$insur_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_insur')->row();
											if(empty($insur_info->ins_car)) $ins_car = 0; else $ins_car = $insur_info->ins_car;
											if(empty($insur_info->ins_load)) $ins_load = 0; else $ins_load = $insur_info->ins_load;
											if(empty($insur_info->ins_grnt)) $ins_grnt = 0; else $ins_grnt = $insur_info->ins_grnt;
								//공제 tbl_delivery_fee_gongje
											$gongje_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_gongje')->row();
											//$gongje_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_gongje')->row();
											if(empty($gongje_info->fine_fee)) $fine_fee = 0; else $fine_fee = $gongje_info->fine_fee;
											if(empty($gongje_info->acc_app)) $acc_app = 0; else $acc_app = $gongje_info->acc_app;
											if(empty($gongje_info->not_paid)) $not_paid = 0; else $not_paid = $gongje_info->not_paid;
											if(empty($gongje_info->mid_pay)) $mid_pay = 0; else $mid_pay = $gongje_info->mid_pay;
											if(empty($gongje_info->trmt_sec)) $trmt_sec = 0; else $trmt_sec = $gongje_info->trmt_sec;
											if(empty($gongje_info->etc)) $etc = 0; else $etc = $gongje_info->etc;
								//환급형공제 tbl_delivery_fee_refund_gongje
											$rf_gongje_info = $this->db->where('df_id', $delivery_fee_details->df_id)->get('tbl_delivery_fee_refund_gongje')->row();
											if(empty($rf_gongje_info->gj_termination_mortgage)) $gj_termination_mortgage = 0; else $gj_termination_mortgage = $rf_gongje_info->gj_termination_mortgage;
											if(empty($rf_gongje_info->gj_plate_mortgage)) $gj_plate_mortgage = 0; else $gj_plate_mortgage = $rf_gongje_info->gj_plate_mortgage;
											if(empty($rf_gongje_info->gj_etc_refund)) $gj_etc_refund = 0; else $etc = $rf_gongje_info->gj_etc_refund;
	*/
					   ?>
                    <tr>
                        <td align="right">
                            <?= $sn ?>
                        </td>
                        <td>
                            <?php if(!empty($gj_rq_co->co_name)) echo $gj_rq_co->co_name; ?>
                        </td>
                        <td>
                            <?php if(!empty($r_co_name)) echo $r_co_name; ?>
                        </td>
                        <td>
                            <?php if(!empty($s_co_name)) echo $s_co_name; ?>
                        </td>
                        <td>
                            <?php if(!empty($dp_details->bs_number)) echo $dp_details->bs_number; ?>
                        </td>
                        <td>
                            <?php if(!empty($dp_details->car_1)) echo $dp_details->car_1; ?>
                        </td>
                        <td align="center">
                            생성
                        </td>
                        <td align="right">
                             
                        </td>

                    <tr>
                        <?php 
						} //---------중복처리
						} //---------계약기간
						} 
					} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php endif; ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('#Transation_DataTables').dataTable({
            paging: false,
            "bSort": false
        });
    });

	
	$(document).ready(function () {
 <?php if((!empty($calculate_progress) && $calculate_progress=="through_contracts")|| (empty($calculate_progress))) { ?>
        $('#generate_type1').hide();
        $('#generate_type2').show();
 <?php } else { ?>
         $('#generate_type1').show();
        $('#generate_type2').hide();
<?php } ?>

        $("#generate_type_select1").click(function () {
			$('#generate_type1').hide();
			$('#generate_type2').show();
        });
        $("#generate_type_select2").click(function () {
			$('#generate_type1').show();
			$('#generate_type2').hide();

        });
    });

</script>
<!-- end -->
