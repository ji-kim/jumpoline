<?php
		if(empty($mfee_info->dp_id)) $dp_id = ""; else $dp_id = $mfee_info->dp_id;
		if(empty($mfee_info->remark)) $remark = ""; else $remark = $mfee_info->remark;
		
		//일반공제
		$gongje_info = $this->db->where('df_id', $mfee_info->df_id)->get('tbl_delivery_fee_gongje')->row();
		if(empty($gongje_info->not_paid)) $not_paid = 0; else $not_paid = $gongje_info->not_paid;

		//파트너
		$dp = $this->db->where('dp_id', $dp_id)->get('tbl_members')->row();

		//공제청구사
		$gj_rq_co = $this->db->where('dp_id', $dp->ws_co_id)->get('tbl_members')->row();
		if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;

		//차량정보
		$truck = $this->db->where('idx', $mfee_info->tr_id)->get('tbl_asset_truck')->row();
		if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;
?>
<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"> 초기미수금액 등록 - [<?php echo $mfee_info->df_month ?>]  <?php echo $dp->driver ?> / <?= $truck_no ?> <?=$mfee_info->dp_id?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">

            <div class="form-group" id="border-none">
			* 최초 등록시에만 설정해 주세요.<br/>
            </div>

        <form id="form_validation"
              action="<?php echo base_url() ?>admin/cowork/save_misu/" method="post" class="form-horizontal form-groups-bordered">
		<input type="hidden" name="df_id" value="<?php echo $mfee_info->df_id; ?>">
		<input type="hidden" name="df_month" value="<?php echo $mfee_info->df_month; ?>">
		<input type="hidden" name="gongje_req_co" value="<?php echo $mfee_info->gongje_req_co; ?>">
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">초기미수액 </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $not_paid; ?>" class="form-control" name="not_paid">
                </div>
            </div>

            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #aaaaaa;">비고 </label>
                <div class="col-sm-9">
                    <input type="text" value="<?php echo $remark; ?>" class="form-control" name="remark">
                </div>
            </div>

			<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"> 저 장 </button>
            </div>
        </form>
    </div>
</div>
