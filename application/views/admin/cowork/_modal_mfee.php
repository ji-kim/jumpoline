<?php

if(empty($mfee_info->mfeeset_id)) $mfeeset_id = ''; else $mfeeset_id = $mfee_info->mfeeset_id;
if(empty($mfee_info->wst_mfee)) $wst_mfee = 0; else $wst_mfee = $mfee_info->wst_mfee;
if(empty($mfee_info->mfee_vat)) $mfee_vat = 0; else $mfee_vat = $mfee_info->mfee_vat;
if(empty($mfee_info->org_fee)) $org_fee = 0; else $org_fee = $mfee_info->org_fee;
if(empty($mfee_info->org_fee_vat)) $org_fee_vat = 0; else $org_fee_vat = $mfee_info->org_fee_vat;
if(empty($mfee_info->env_fee)) $env_fee = 0; else $env_fee = $mfee_info->env_fee;
if(empty($mfee_info->env_fee_vat)) $env_fee_vat = 0; else $env_fee_vat = $mfee_info->env_fee_vat;
if(empty($mfee_info->car_tax)) $car_tax = 0; else $car_tax = $mfee_info->car_tax;
if(empty($mfee_info->car_tax_vat)) $car_tax_vat = 0; else $car_tax_vat = $mfee_info->car_tax_vat;
if(empty($mfee_info->grg_fee)) $grg_fee = 0; else $grg_fee = $mfee_info->grg_fee;
if(empty($mfee_info->grg_fee_vat)) $grg_fee_vat = 0; else $grg_fee_vat = $mfee_info->grg_fee_vat;
if(empty($mfee_info->etc)) $etc= 0; else $etc = $mfee_info->etc;

$org_fee_vat = 0;
$env_fee = 0;
$env_fee_vat = 0;
$car_tax = 0;
$car_tax_vat = 0;
$etc = 0;

//파트너
$dp = $this->db->where('dp_id', $df_info->dp_id)->get('tbl_members')->row();

//공제청구사
$gj_rq_co = $this->db->where('dp_id', $dp->ws_co_id)->get('tbl_members')->row();
if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;

//차량정보
$truck = $this->db->where('idx', $df_info->tr_id)->get('tbl_asset_truck')->row();
if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;
?>
<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"> 위.수탁관리비 설정 - <?php echo $df_info->df_id ?>[<?php echo $df_info->df_month ?>]  <?php echo $dp->driver ?> / <?= $truck_no ?> <?=$df_info->dp_id?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">



        <form id="form_validation"
              action="<?php echo base_url() ?>admin/cowork/save_mfee"
              method="post" class="form-horizontal form-groups-bordered">
		<input type="hidden" name="mfeeset_id" value="<?php echo $mfeeset_id; ?>">
		<input type="hidden" name="df_id" value="<?php echo $df_info->df_id; ?>">
		<input type="hidden" name="dp_id" value="<?php echo $df_info->dp_id; ?>">
		<input type="hidden" name="ct_id" value="<?php echo $df_info->ct_id; ?>">
		<input type="hidden" name="df_month" value="<?php echo $df_info->df_month; ?>">
		<input type="hidden" name="gongje_req_co" value="<?php echo $df_info->gongje_req_co; ?>">

            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-2 control-label" style="border-right:1px solid #eee;color:#ffffff;background-color: #777777;">항목 <br/><br/></label>
                <label for="field-1" class="col-sm-2 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #777777;">공급가 <br/><br/></label>
                <label for="field-1" class="col-sm-2 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #777777;">부가세 <br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #777777;">합계 <br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="text-align:center;color:#ffffff;background-color:green;">    </label>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-2 control-label">관리비 </label>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $wst_mfee; ?>" class="form-control" name="wst_mfee" >
                </div>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $mfee_vat; ?>" class="form-control" name="mfee_vat">
                </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo ($mfee_vat+$wst_mfee); ?>" class="form-control" name="mfee_total">
                </div>
                <div class="col-sm-3" style="text-align:center;"> </div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-2 control-label">협회비 </label>
                                <div class="col-sm-2">
                    <input type="text" value="<?php echo $org_fee; ?>" class="form-control" name="org_fee">
                </div>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $org_fee_vat; ?>" class="form-control" name="org_fee_vat" style="display:none;">
                </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo ($org_fee + $org_fee_vat); ?>" class="form-control" name="org_fee_total">
                </div>
                <div class="col-sm-3" style="text-align:center;"> </div>
            </div>
            <div class="form-group" id="border-none" style="display:none;">
                <label for="field-1" class="col-sm-2 control-label" style="font-size:11px;">환경부담금 </label>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $env_fee; ?>" class="form-control" name="env_fee">
                </div>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $env_fee_vat; ?>" class="form-control" name="env_fee_vat">
                </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo ($env_fee+$env_fee_vat); ?>" class="form-control" name="env_fee_total">
                </div>
                <div class="col-sm-3" style="text-align:center;"> </div>
            </div>
            <div class="form-group" id="border-none" style="display:none;">
                <label for="field-1" class="col-sm-2 control-label">자동차세 </label>
                                <div class="col-sm-2">
                    <input type="text" value="<?php echo $car_tax; ?>" class="form-control" name="car_tax">
                </div>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $car_tax_vat; ?>" class="form-control" name="car_tax_vat">
                </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo ($car_tax+$car_tax_vat); ?>" class="form-control" name="car_total">
                </div>
                <div class="col-sm-3" style="text-align:center;"> </div>
            </div>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-2 control-label">차고지비 </label>
                                <div class="col-sm-2">
                    <input type="text" value="<?php echo $grg_fee; ?>" class="form-control" name="grg_fee">
                </div>
                <div class="col-sm-2">
                    <input type="text" value="<?php echo $grg_fee_vat; ?>" class="form-control" name="grg_fee_vat">
                </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo ($grg_fee+$grg_fee_vat); ?>" class="form-control" name="grg_fee_total">
                </div>
                <div class="col-sm-3" style="text-align:center;"> </div>
            </div>
            <div class="form-group" id="border-none" style="display:none;">
                <label for="field-1" class="col-sm-2 control-label">기타 </label>
                                <div class="col-sm-2">
                    <input type="text" value="<?php echo $etc; ?>" class="form-control" name="etc">
                </div>
                <div class="col-sm-2">
                 </div>
                <div class="col-sm-3">
                    <input type="text" value="<?php echo ($etc); ?>" class="form-control" name="etc_total">
                </div>
                <div class="col-sm-3" style="text-align:center;"> </div>
            </div>
<?php
if (!empty($all_wgongje_group)) {
?>
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;color:#ffffff;background-color: #333333;">추가항목 <br/><br/></label>
                <label for="field-1" class="col-sm-3 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #333333;">금액<br/><br/></label>
                <label for="field-1" class="col-sm-6 control-label" style="border-right:1px solid #eee;text-align:center;color:#ffffff;background-color: #333333;">메모<br/><br/></label>
            </div>

<?php
	$i = 0;
	foreach ($all_wgongje_group as $wgongje_info) {
		$add_item = $this->db->where('df_month', $df_month)->where('dp_id', $dp_id)->get('tbl_delivery_fee_add')->row();
?>
			<input type=hidden name=chk[] value='<?=$i?>'>
			<input type="hidden" name="item_pidx[<?=$i?>]" value="<?php echo $wgongje_info->idx; ?>">
			<input type="hidden" name="item_idx[<?=$i?>]" value="<?php if(!empty($add_item->idx)) echo $add_item->idx; ?>">
            <div class="form-group" id="border-none">
                <label for="field-1" class="col-sm-3 control-label"><?php echo $wgongje_info->title; ?> </label>
                <div class="col-sm-3">
                    <input type="text" value="<?php if(!empty($add_item->amount)) echo $add_item->amount; ?>" class="form-control" name="item_amount[<?=$i?>]" >
                </div>
                <div class="col-sm-6">
                    <input type="text" value="<?php if(!empty($add_item->memo)) echo $add_item->memo; ?>" class="form-control" name="memo[<?=$i?>]" >
                </div>
            </div>

<?php
		$i++;
	}
}
?>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"> 저 장 </button>
            </div>
        </form>
    </div>
</div>
