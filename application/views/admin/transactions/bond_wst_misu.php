<?= message_box('success'); ?>
<?= message_box('error'); ?>

<?php
$mdate = date('Y-m-d');
$last_7_days = date('Y-m', strtotime('today - 2 months'));

for ($iDay = 2; $iDay >= 0; $iDay--) {
    $date = date('Y-m', strtotime('today - ' . $iDay . 'months'));
    $this_7_days_deposit[$date] = $this->db->select_sum('amount')->where(array('type' => 'Income', 'date >=' => $date, 'date <=' => $date))->get('tbl_transactions')->result();
}

$this_7_days_all = $this->db->where(array('goal_type_id' => 1, 'start_date >=' => $last_7_days, 'end_date <=' => $mdate))->get('tbl_goal_tracking')->result();

$this_7_days_bank = $this->db->where(array('goal_type_id' => 2, 'start_date >=' => $last_7_days, 'end_date <=' => $mdate))->get('tbl_goal_tracking')->result();

if (!empty($this_7_days_all)) {
    $this_7_days_all = $this_7_days_all;
} else {
    $this_7_days_all = array();
}
if (!empty($this_7_days_bank)) {
    $this_7_days_bank = $this_7_days_bank;
} else {
    $this_7_days_bank = array();
}


$terget_achievement = array_merge($this_7_days_all, $this_7_days_bank);
$tolal_goal = 0;
$complete_achivement = 0;
$pending_goal = 0;
$total_terget = 0;
if (!empty($terget_achievement)) {
    foreach ($terget_achievement as $v_terget) {
        $total_terget += $v_terget->achievement;
    }
}
$curency = $this->transactions_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');

if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
    ?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
    <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
        <div class="col-md-4">
            <div class="row row-table pv-lg">
                <div class="col-xs-6">
                    <p class="m0 lead"><?= display_money($tolal_goal, $curency->symbol) ?></p>
                    <p class="m0">
                        <small>당월 총 미수액</small>
                    </p>
                </div>
                <div class="col-xs-6 ">
                    <p class="m0 lead"><?= display_money($total_terget, $curency->symbol) ?></p>
                    <p class="m0">
                        <small>누적 미수액</small>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/transactions/bond_wst_misu"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
            <div class="row row-table ">
                <div class="col-xs-6">
                    <p class="m0">
                        
                                <input type="text" value="<?php
                                if (!empty($df_month)) {
                                    echo $df_month;
                                } else echo date('Y-m');
                                ?>" class="form-control monthyear" name="df_month"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;" onChange="document.myform.submit();">

                    </p>
                    <p class="m0 lead">
					<select name="gongje_req_co" id="gongje_req_co" style="width:100%;background-color:yellow;" class="form-control input-sm" onChange="document.myform.submit();">
							<option value="" <?=($gongje_req_co=="")?"selected":""?>>전체</option>
							<option value="1413" <?=($gongje_req_co=="1413")?"selected":""?>>케이티지엘에스(주) </option>
							<option value="1414" <?=($gongje_req_co=="1414")?"selected":""?>>(주)아이디일일구닷컴</option>
							<option value="1415" <?=($gongje_req_co=="1415")?"selected":""?>>(주)델타온</option>
							<option value="1903" <?=($gongje_req_co=="1903")?"selected":""?>>(주)휴먼엘지</option>
							<option value="etc" <?=($gongje_req_co=="etc")?"selected":""?>>기타</option>
					</select>
					<input class="client_permission" name="begin_pay" value="1" type="checkbox" onClick="if(this.checked) document.myform.submit();">
					</p>
                </div>
                <div class="col-xs-6 pt">
                    <div data-sparkline="" data-bar-color="#23b7e5" data-height="60" data-bar-width="8"
                         data-bar-spacing="6" data-chart-range-min="0" values="<?php
                    if (!empty($this_7_days_deposit)) {
                        foreach ($this_7_days_deposit as $v_last_deposit) {
                            echo $v_last_deposit[0]->amount . ',';
                        }
                    }
                    ?>">
                    </div>
                    <p class="m0">
                        <small>
                            <?php
                            if (!empty($this_7_days_deposit)) {
                                foreach ($this_7_days_deposit as $date => $v_last_deposit) {
                                    echo date('m', strtotime($date)) . ' ';
                                }
                            }
                            ?>
                        </small>
                    </p>

                </div>
            </div>
				</form>

        </div>
        <div class="col-md-4">
            <div class="row row-table ">
                <div class="col-xs-6">
                    <p class="m0 lead">
                        <?php
                        if ($tolal_goal < $complete_achivement) {
                            $pending_goal = 0;
                        } else {
                            $pending_goal = $tolal_goal - $complete_achivement;
                        } ?>
                        <?= display_money($pending_goal, $curency->symbol) ?>
                    </p>
                    <p class="m0">
                        <small>부과대기 금액</small>
                    </p>
                </div>
                <?php
                if (!empty($tolal_goal)) {
                    if ($tolal_goal <= $complete_achivement) {
                        $total_progress = 100;
                    } else {
                        $progress = ($complete_achivement / $tolal_goal) * 100;
                        $total_progress = round($progress);
                    }
                } else {
                    $total_progress = 0;
                }
                ?>
                <div class="col-xs-6 text-center pt">
                    <div class="inline ">
                        <div class="easypiechart text-success"
                             data-percent="<?= $total_progress ?>"
                             data-line-width="5" data-track-Color="#f0f0f0"
                             data-bar-color="#<?php
                             if ($total_progress == 100) {
                                 echo '8ec165';
                             } elseif ($total_progress >= 40 && $total_progress <= 50) {
                                 echo '5d9cec';
                             } elseif ($total_progress >= 51 && $total_progress <= 99) {
                                 echo '7266ba';
                             } else {
                                 echo 'fb6b5b';
                             }
                             ?>" data-rotate="270" data-scale-Color="false"
                             data-size="50"
                             data-animate="2000">
                                                        <span class="small "><?= $total_progress ?>
                                                            %</span>
                            <span class="easypie-text"><strong>미수율</strong></span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php }
$created = can_action('30', 'created');
$edited = can_action('30', 'edited');
$deleted = can_action('30', 'deleted');
$income_category = $this->db->get('tbl_income_category')->result();
$id = $this->uri->segment(5);
if (!empty($created) || !empty($edited)){
?>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip"
             data-title="<?php echo lang('filter_by'); ?>">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-filter" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-left"
                style="width:300px;<?php if (!empty($search_by) && $search_by == 'category') {
                    echo 'display:block';
                } ?>">
                <li class="<?php
                if (empty($search_by)) {
                    echo 'active';
                } ?>"><a
                        href="<?= base_url() ?>admin/transactions/deposit"><?php echo lang('all'); ?></a>
                </li>
                <li class="divider"></li>
                <?php if (count($income_category) > 0) { ?>
                    <?php foreach ($income_category as $v_category) {
                        ?>
                        <li class="<?php if (!empty($id)) {
                           // if ($search_by == 'category') {
                           //     if ($id == $v_category->income_category_id) {
                           //         echo 'active';
                           //     }
                           // }
                        } ?>">
                            <a href="<?= base_url() ?>admin/transactions/deposit/category/<?php echo $v_category->income_category_id; ?>"><?php echo $v_category->income_category; ?></a>
                        </li>
                    <?php }
                    ?>
                    <div class="clearfix"></div>
                    <li class="divider"></li>
                <?php } ?>
            </ul>
        </div>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : ''; ?>"><a href="#manage"
                                                                    data-toggle="tab">위수탁관리비미수현황 (총 <?=number_format($total_count)?>건)</a>
                </li>
                <li class="<?= $active == 2 ? 'active' : ''; ?>"><a href="#manage"
                                                                    data-toggle="tab">위수탁관리비수납</a>
                </li>
                <!--li><a style="background-color: #1797be;color: #ffffff"
                       href="<?= base_url() ?>admin/transactions/import/Income"><?= lang('import') . ' ' . lang('deposit') ?></a>
                </li-->
            </ul>
            <div class="tab-content no-padding  bg-white">
                <!-- ************** general *************-->
                <div class="tab-pane <?= $active == 1 ? 'active' : ''; ?>" id="manage">
<?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong>위수탁관리비미수현황</strong></div>
                        </header>
                        <?php } ?>
                        <div class="table-responsive">
                            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
									<tr align="center" bgcolor="#e0e7ef">
									  <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>상태</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>년월분</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="2">청구사정보</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">파트너정보</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">위수탁관리비 납부정보</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>비고</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br>수납등록</td>
									</tr>
																						
														

									<tr align="center" bgcolor="#e0e7ef">
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공제청구사</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">실수요처</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">파트너</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차량번호</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자번호</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">누적미납금</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">당월청구금</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">당월총청구금</td>
									</tr>
                                </thead>
                                <tbody>
                                <?php
                                //$curency = $this->transactions_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');
                                $total_amount = 0;
                                $total_credit = 0;
                                $total_balance = 0;
                                if (!empty($all_levy_info)):
                                    foreach ($all_levy_info as $v_levy_info) :
										$sn = $total_count--;
										$df = $this->db->where('df_id', $v_levy_info->df_id)->get('tbl_delivery_fee')->row();
										//파트너
										$dp = $this->db->where('dp_id', $v_levy_info->dp_id)->get('tbl_members')->row();

										//공제청구사
										if(!empty($dp->ws_co_id)) {
											$gj_rq_co = $this->db->where('dp_id', $dp->ws_co_id)->get('tbl_members')->row();
											if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;
										}

										//실수요처
										$r_co = $this->db->where('code', $df->co_code)->get('tbl_members')->row();
										if(empty($r_co->co_name)) $r_co = ""; else $r_co = $r_co->co_name;

										//차량정보
										$truck = $this->db->where('idx', $df->tr_id)->get('tbl_asset_truck')->row();
										if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;

										$pay_status = "납부전";
										$pay_status_color = "warning";
										if(!empty($v_levy_info->pay_status)) {
											if($v_levy_info->pay_status == "N") { $pay_status = "미납"; $pay_status_color = "danger"; }
											else if($v_levy_info->pay_status == "P") { $pay_status = "부분납"; $pay_status_color = "primary"; } 
											else if($v_levy_info->pay_status == "F") { $pay_status = "완납"; $pay_status_color = "success"; } 
											else if($v_levy_info->pay_status == "O") { $pay_status = "과납"; $pay_status_color = "primary"; } 
										}

										//이월 미납액
	
                                        ?>
                                      <tr id="table_deposit_<?= $v_levy_info->df_id ?>">
                                        <td><?=$sn?></td>
                                        <td>
                                            <span class='label label-<?=$pay_status_color?>'><?=$pay_status?></span>
                                        </td>
                                        <td>
                                            <?= $df_month ?>
                                        </td>

                                        <td>
                                            <a href="<?= base_url() ?>admin/basic/v_levy_info/<?= $v_levy_info->df_id ?>"
                                                   class="text-info"><?=$gj_rq_co ?></a>
										</td>
                                        <td><?= $df->D ?></td>

                                        <td> <?= $dp->ceo ?> / <?= $dp->driver ?> </td>
                                        <td><?= $truck_no ?></td>
                                        <td><?= $dp->bs_number ?></td>
										<td style='font-weight:bold;color:red;text-align:right;'><?= number_format($v_levy_info->prev_misu,0) ?></td>
										<td style='text-align:right;'><?= number_format(($v_levy_info->gn_amount-$v_levy_info->prev_misu),0) ?></td>
										<td style='text-align:right;'><?= number_format($v_levy_info->gn_amount,0) ?></td>

										<td> </td>
										<!--td style='text-align:right;'><?= number_format($v_levy_info->paid_amount,0) ?></td>
										<td style='text-align:right;'><?= number_format($v_levy_info->balance_amount,0) ?></td-->
                                                <td>
                                                <div class="btn-group">
<?php if($df->is_pay_closed=='Y') { ?>
													<span class='label label-danger'>수납마감</span>
<?php } else { ?>
                                                    <button class="btn btn-xs btn-success"
                                                            onClick="location.href='/admin/transactions/bond_wst_misu/index/<?=$v_levy_info->df_month?>/<?=$v_levy_info->gongje_req_co?>/<?=$v_levy_info->df_id?>';">
                                                        <i class="fa fa-pencil-square-o"></i>수납처리
                                                    </button>
<?php } ?>
                                                </div>
                                                </td>
                                      </tr>
                                            <?php
                                            //$total_amount += $v_levy_info->amount;
                                            //$total_credit += $v_levy_info->credit;
                                            //$total_balance = $total_credit;
                                            ?>
                                            <?php
                                        //endif;
                                    endforeach;
                                endif;
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if ((!empty($created) || !empty($edited)) && !empty($levy_info)) { 
								$df = $this->db->where('df_id', $levy_info->df_id)->get('tbl_delivery_fee')->row();
								//파트너
								$dp = $this->db->where('dp_id', $levy_info->dp_id)->get('tbl_members')->row();

										//공제청구사
										$gj_rq_co = $this->db->where('dp_id', $dp->ws_co_id)->get('tbl_members')->row();
										if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;
					

										//실수요처
										$r_co = $this->db->where('code', $df->co_code)->get('tbl_members')->row();
										if(empty($r_co->co_name)) $r_co = ""; else $r_co = $r_co->co_name;
					?>
                        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="create">
                            <form role="form" data-parsley-validate="" novalidate="" enctype="multipart/form-data"
                                  id="form"
                                  action="<?php echo base_url(); ?>admin/transactions/save_payment/<?php
                                  if (!empty($levy_info)) {
                                      echo $levy_info->df_id;
                                  }
                                  ?>" method="post" class="form-horizontal ">
								  <input type="hidden" name="df_month" value="<?=$levy_info->df_month?>">
								  <input type="hidden" name="dp_id" value="<?=$levy_info->dp_id?>">
								  <input type="hidden" name="gn_amount" value="<?=$levy_info->gn_amount?>">
								  <input type="hidden" name="balance_amount" value="<?=$levy_info->balance_amount?>">
								  <input type="hidden" name="gongje_req_co" value="<?=$df->gongje_req_co?>">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">부과년월</label>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                              <input type="text" class="form-control " value="<?php
                                            if (!empty($levy_info->df_month)) {
                                                echo $levy_info->df_month;
                                            }
                                            ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">공제청구사</label>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                              <input type="text" class="form-control " value="<?php
                                            if (!empty($gj_rq_co)) {
                                                echo $gj_rq_co;
                                            }
                                            ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">파트너</label>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                              <input type="text" class="form-control " value="<?php
                                            if (!empty($dp->ceo)) {
                                                echo $dp->ceo;
                                            }
                                            ?> (<?=$dp->driver ?>)" readonly>
                                        </div>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-lg-3 control-label">누적미납금</label>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                              <input type="text" class="form-control " style="font-weight:bold;color:red;" value="<?php
                                            if (!empty($levy_info->prev_misu)) {
                                                echo number_format($levy_info->prev_misu);
                                            }
                                            ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">당월청구금</label>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                              <input type="text" class="form-control " style="font-weight:bold;color:blue;" value="<?php
                                            if (!empty($levy_info->prev_misu)) {
                                                echo number_format($levy_info->gn_amount - $levy_info->prev_misu);
                                            }
                                            ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">당월총청구금액</label>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                              <input type="text" class="form-control " style="font-weight:bold;color:blue;" value="<?php
                                            if (!empty($levy_info->gn_amount)) {
                                                echo number_format($levy_info->gn_amount);
                                            }
                                            ?>" readonly>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label">납부일자</label>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                            <input type="text" name="pay_date" class="form-control datepicker" value="<?php
                                            if (!empty($levy_info->pay_date)) {
                                                echo $levy_info->pay_date;
                                            } else {
                                                echo date('Y-m-d');
                                            }
                                            ?>" data-date-format="<?= config_item('date_picker_format'); ?>">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-calendar"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">수납금 <span
                                            class="text-danger">*</span>
                                    </label>
                                    <div class="col-lg-5">
                                        <div class="input-group  ">
                                            <input class="form-control " data-parsley-type="number" style="font-weight:bold;color:blue;font-size:20px;" type="text" value="<?php
                                                   if (!empty($levy_info)) {
                                                      // echo $levy_info->df_id;
                                                   }
                                                   ?>" name="paid_amount" required="" <?php
                                            if (!empty($levy_info)) {
                                               // echo 'disabled';
                                            }
                                            ?>>
                                        </div>
                                    </div>
                                </div>

                                <?php if (!empty($levy_info)) { ?>
                                    <input class="form-control " type="hidden" value="<?php
                                    if (!empty($levy_info)) {
                                        echo $levy_info->df_id;
                                    }
                                    ?>" name="old_account_id">
                                <?php } ?>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">납부방법 </label>
                                        <div class="col-lg-5">
                                            <div class="input-group">
                                                <select class="form-control select_box" style="width: 100%"
                                                        name="payment_methods_id">
                                                    <option value="0">수납방법</option>
                                                    <?php
                                                    $payment_methods = $this->db->order_by('payment_methods_id', 'DESC')->get('tbl_payment_methods')->result();
                                                    if (!empty($payment_methods)) {
                                                        foreach ($payment_methods as $p_method) {
                                                            ?>
                                                            <option value="<?= $p_method->payment_methods_id ?>" <?php
                                                            if (!empty($expense_info)) {
                                                                echo $expense_info->payment_methods_id == $p_method->payment_methods_id ? 'selected' : '';
                                                            }
                                                            ?>><?= $p_method->method_name ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div class="input-group-addon"
                                                     title="<?= lang('new') . ' ' . lang('payment_method') ?>"
                                                     data-toggle="tooltip" data-placement="top">
                                                    <a data-toggle="modal" data-target="#myModal"
                                                       href="<?= base_url() ?>admin/settings/inline_payment_method"><i
                                                            class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">은행 <span
                                            class="text-danger">*</span>
                                    </label>
                                    <div class="col-lg-5">
                                        <div class="input-group  ">
                                            <input class="form-control " style="font-weight:bold;color:blue;font-size:20px;" type="text" value="<?php
                                                   if (!empty($levy_info)) {
                                                      // echo $levy_info->df_id;
                                                   }
                                                   ?>" name="bank_name" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">예금주 <span
                                            class="text-danger">*</span>
                                    </label>
                                    <div class="col-lg-5">
                                        <div class="input-group  ">
                                            <input class="form-control " style="font-weight:bold;color:blue;font-size:20px;" type="text" value="<?php
                                                   if (!empty($levy_info)) {
                                                      // echo $levy_info->df_id;
                                                   }
                                                   ?>" name="bank_account_name" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">계좌번호 <span
                                            class="text-danger">*</span>
                                    </label>
                                    <div class="col-lg-5">
                                        <div class="input-group  ">
                                            <input class="form-control " style="font-weight:bold;color:blue;font-size:20px;" type="text" value="<?php
                                                   if (!empty($levy_info)) {
                                                      // echo $levy_info->df_id;
                                                   }
                                                   ?>" name="bank_account_number" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">의뢰인 <span
                                            class="text-danger">*</span>
                                    </label>
                                    <div class="col-lg-5">
                                        <div class="input-group  ">
                                            <input class="form-control " style="font-weight:bold;color:blue;font-size:20px;" type="text" value="<?php
                                                   if (!empty($levy_info)) {
                                                      // echo $levy_info->df_id;
                                                   }
                                                   ?>" name="bank_transfer_name" required="">
                                        </div>
                                    </div>
                                </div>






                                <div class="form-group terms">
                                    <label class="col-lg-3 control-label">메모</label>
                                    <div class="col-lg-5">
                        <textarea name="remark" class="form-control"><?php
                            if (!empty($levy_info)) {
                                echo $levy_info->remark;
                            }
                            ?></textarea>
                                    </div>
                                </div>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">레퍼런스 </label>
                                        <div class="col-lg-5">

                                            <input class="form-control " type="text" value="<?php
                                            if (!empty($levy_info->reference)) {
                                                echo $levy_info->reference;
                                            }
                                            ?>" name="reference">
                                        </div>
                                    </div>
                                <div class="form-group" style="margin-bottom: 0px">
                                    <label for="field-1"
                                           class="col-sm-3 control-label">첨부파일</label>

                                    <div class="col-sm-5">
                                        <div id="comments_file-dropzone" class="dropzone mb15">

                                        </div>
                                        <div id="comments_file-dropzone-scrollbar">
                                            <div id="comments_file-previews">
                                                <div id="file-upload-row" class="mt pull-left">

                                                    <div class="preview box-content pr-lg" style="width:100px;">
                                                    <span data-dz-remove class="pull-right" style="cursor: pointer">
                                    <i class="fa fa-times"></i>
                                </span>
                                                        <img data-dz-thumbnail class="upload-thumbnail-sm"/>
                                                        <input class="file-count-field" type="hidden" name="files[]"
                                                               value=""/>
                                                        <div
                                                            class="mb progress progress-striped upload-progress-sm active mt-sm"
                                                            role="progressbar" aria-valuemin="0" aria-valuemax="100"
                                                            aria-valuenow="0">
                                                            <div class="progress-bar progress-bar-success"
                                                                 style="width:0%;"
                                                                 data-dz-uploadprogress></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        if (!empty($levy_info->attachement)) {
                                            $uploaded_file = json_decode($levy_info->attachement);
                                        }
                                        if (!empty($uploaded_file)) {
                                            foreach ($uploaded_file as $v_files_image) { ?>
                                                <div class="pull-left mt pr-lg mb" style="width:100px;">
                                                        <span data-dz-remove class="pull-right existing_image"
                                                              style="cursor: pointer"><i
                                                                class="fa fa-times"></i></span>
                                                    <?php if ($v_files_image->is_image == 1) { ?>
                                                        <img data-dz-thumbnail
                                                             src="<?php echo base_url() . $v_files_image->path ?>"
                                                             class="upload-thumbnail-sm"/>
                                                    <?php } else { ?>
                                                        <span data-toggle="tooltip" data-placement="top"
                                                              title="<?= $v_files_image->fileName ?>"
                                                              class="mailbox-attachment-icon"><i
                                                                class="fa fa-file-text-o"></i></span>
                                                    <?php } ?>

                                                    <input type="hidden" name="path[]"
                                                           value="<?php echo $v_files_image->path ?>">
                                                    <input type="hidden" name="fileName[]"
                                                           value="<?php echo $v_files_image->fileName ?>">
                                                    <input type="hidden" name="fullPath[]"
                                                           value="<?php echo $v_files_image->fullPath ?>">
                                                    <input type="hidden" name="size[]"
                                                           value="<?php echo $v_files_image->size ?>">
                                                    <input type="hidden" name="is_image[]"
                                                           value="<?php echo $v_files_image->is_image ?>">
                                                </div>
                                            <?php }; ?>
                                        <?php }; ?>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $(".existing_image").click(function () {
                                                    $(this).parent().remove();
                                                });

                                                fileSerial = 0;
                                                // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
                                                var previewNode = document.querySelector("#file-upload-row");
                                                previewNode.id = "";
                                                var previewTemplate = previewNode.parentNode.innerHTML;
                                                previewNode.parentNode.removeChild(previewNode);
                                                Dropzone.autoDiscover = false;
                                                var projectFilesDropzone = new Dropzone("#comments_file-dropzone", {
                                                    url: "<?= base_url()?>admin/global_controller/upload_file",
                                                    thumbnailWidth: 80,
                                                    thumbnailHeight: 80,
                                                    parallelUploads: 20,
                                                    previewTemplate: previewTemplate,
                                                    dictDefaultMessage: '<?php echo lang("file_upload_instruction"); ?>',
                                                    autoQueue: true,
                                                    previewsContainer: "#comments_file-previews",
                                                    clickable: true,
                                                    accept: function (file, done) {
                                                        if (file.name.length > 200) {
                                                            done("Filename is too long.");
                                                            $(file.previewTemplate).find(".description-field").remove();
                                                        }
                                                        //validate the file
                                                        $.ajax({
                                                            url: "<?= base_url()?>admin/global_controller/validate_project_file",
                                                            data: {file_name: file.name, file_size: file.size},
                                                            cache: false,
                                                            type: 'POST',
                                                            dataType: "json",
                                                            success: function (response) {
                                                                if (response.success) {
                                                                    fileSerial++;
                                                                    $(file.previewTemplate).find(".description-field").attr("name", "comment_" + fileSerial);
                                                                    $(file.previewTemplate).append("<input type='hidden' name='file_name_" + fileSerial + "' value='" + file.name + "' />\n\
                                                                        <input type='hidden' name='file_size_" + fileSerial + "' value='" + file.size + "' />");
                                                                    $(file.previewTemplate).find(".file-count-field").val(fileSerial);
                                                                    done();
                                                                } else {
                                                                    $(file.previewTemplate).find("input").remove();
                                                                    done(response.message);
                                                                }
                                                            }
                                                        });
                                                    },
                                                    processing: function () {
                                                        $("#file-save-button").prop("disabled", true);
                                                    },
                                                    queuecomplete: function () {
                                                        $("#file-save-button").prop("disabled", false);
                                                    },
                                                    fallback: function () {
                                                        //add custom fallback;
                                                        $("body").addClass("dropzone-disabled");
                                                        $('.modal-dialog').find('[type="submit"]').removeAttr('disabled');

                                                        $("#comments_file-dropzone").hide();

                                                        $("#file-modal-footer").prepend("<button id='add-more-file-button' type='button' class='btn  btn-default pull-left'><i class='fa fa-plus-circle'></i> " + "<?php echo lang("add_more"); ?>" + "</button>");

                                                        $("#file-modal-footer").on("click", "#add-more-file-button", function () {
                                                            var newFileRow = "<div class='file-row pb pt10 b-b mb10'>"
                                                                + "<div class='pb clearfix '><button type='button' class='btn btn-xs btn-danger pull-left mr remove-file'><i class='fa fa-times'></i></button> <input class='pull-left' type='file' name='manualFiles[]' /></div>"
                                                                + "<div class='mb5 pb5'><input class='form-control description-field'  name='comment[]'  type='text' style='cursor: auto;' placeholder='<?php echo lang("comment") ?>' /></div>"
                                                                + "</div>";
                                                            $("#comments_file-previews").prepend(newFileRow);
                                                        });
                                                        $("#add-more-file-button").trigger("click");
                                                        $("#comments_file-previews").on("click", ".remove-file", function () {
                                                            $(this).closest(".file-row").remove();
                                                        });
                                                    },
                                                    success: function (file) {
                                                        setTimeout(function () {
                                                            $(file.previewElement).find(".progress-striped").removeClass("progress-striped").addClass("progress-bar-success");
                                                        }, 1000);
                                                    }
                                                });

                                            })
                                        </script>
                                    </div>
                                </div>
                                <?php
                                if (!empty($levy_info)) {
                                    $transactions_id = $levy_info->remark;
                                } else {
                                    $transactions_id = null;
                                }
                                ?>
                                <?= custom_form_Fields(1, $transactions_id); ?>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label"></label>
                                    <div class="col-lg-5">
                                        <button type="submit" id="file-save-button" class="btn btn-sm btn-primary">
                                            <i
                                                class="fa fa-check"></i> 저장</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    <?php }else{ ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var maxAppend = 0;
        $("#add_more").click(function () {
            if (maxAppend >= 4) {
                alert("Maximum 5 File is allowed");
            } else {
                var add_new = $('<div class="form-group" style="margin-bottom: 0px">\n\
                    <label for="field-1" class="col-sm-3 control-label"><?= lang('attachment') ?></label>\n\
        <div class="col-sm-4">\n\
        <div class="fileinput fileinput-new" data-provides="fileinput">\n\
<span class="btn btn-default btn-file"><span class="fileinput-new" >Select file</span><span class="fileinput-exists" >Change</span><input type="file" name="attachement[]" ></span> <span class="fileinput-filename"></span><a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none;">&times;</a></div></div>\n\<div class="col-sm-2">\n\<strong>\n\
<a href="javascript:void(0);" class="remCF"><i class="fa fa-times"></i>&nbsp;Remove</a></strong></div>');
                maxAppend++;
                $("#add_new").append(add_new);
            }
        });

        $("#add_new").on('click', '.remCF', function () {
            $(this).parent().parent().parent().remove();
        });
        $('a.RCF').click(function () {
            $(this).parent().parent().remove();
        });
    });
</script>
