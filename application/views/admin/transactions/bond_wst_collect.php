<?= message_box('success'); ?>
<?= message_box('error'); ?>

<?php
$mdate = date('Y-m-d');
$last_7_days = date('Y-m', strtotime('today - 2 months'));

for ($iDay = 2; $iDay >= 0; $iDay--) {
    $date = date('Y-m', strtotime('today - ' . $iDay . 'months'));
    $this_7_days_deposit[$date] = $this->db->select_sum('amount')->where(array('type' => 'Income', 'date >=' => $date, 'date <=' => $date))->get('tbl_transactions')->result();
}

$this_7_days_all = $this->db->where(array('goal_type_id' => 1, 'start_date >=' => $last_7_days, 'end_date <=' => $mdate))->get('tbl_goal_tracking')->result();

$this_7_days_bank = $this->db->where(array('goal_type_id' => 2, 'start_date >=' => $last_7_days, 'end_date <=' => $mdate))->get('tbl_goal_tracking')->result();

if (!empty($this_7_days_all)) {
    $this_7_days_all = $this_7_days_all;
} else {
    $this_7_days_all = array();
}
if (!empty($this_7_days_bank)) {
    $this_7_days_bank = $this_7_days_bank;
} else {
    $this_7_days_bank = array();
}


$terget_achievement = array_merge($this_7_days_all, $this_7_days_bank);
$tolal_goal = 0;
$complete_achivement = 0;
$pending_goal = 0;
$total_terget = 0;
if (!empty($terget_achievement)) {
    foreach ($terget_achievement as $v_terget) {
        $total_terget += $v_terget->achievement;
    }
}
$curency = $this->transactions_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');

if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
    ?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<script>
	function selectPartner(md,params) {
	  window.open('<?php echo base_url(); ?>admin/basic/select_company/'+md+'/'+params, 'winSC', 'left=50, top=50, width=1200, height=700, scrollbars=1');
	}
	function goSearch() {
		document.myform.submit();
	}
</script>


            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
  
    <!-- 검색 시작 -->
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/transactions/bond_wst_collect"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
	<table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
        <tr>
          <td align="left" valign="top" bgcolor="#ffffff">
            <table border="0" width="100%" cellpadding="12" cellspacing="15" bgcolor="#ffffff">
              <tr>
                <td align="center" bgcolor="#efefef">공제청구사</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff;">
						<input type="hidden" name="gongje_req_co" id="gongje_req_co" value="<?php
                                                               if (!empty($gongje_req_co)) {
                                                                   echo $gongje_req_co;
                                                               }
                                                               ?>">
						<input type="text" name="gongje_req_co_name" id="gongje_req_co_name" value="<?=(empty($gongje_req_co_name))?"":$gongje_req_co_name ?>" class="form-control" style="background-color:yellow;" onClick="selectPartner('group','gongje_req_co||gongje_req_co_name||__||__||__||__||__||__');" onChange="goSearch(this.value)">
					<!--a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/cowork/select_reqco"><input type="hidden" name="ws_co_id" id="ws_co_id" value="">
					<input type="text" name="ws_co_name" id="ws_co_name" value="" class="form-control" style="width:100%;background-color:yellow;"></a-->

                </td>
                <td align="center" bgcolor="#efefef">실수요처</td>
                <td width="10%" align="center" >
					<select name="baecha_co_id" id="baecha_co_id" style="width:100%;" class="form-control input-sm">
				<option value="">전체</option>
							<option value="wmjd" > 우체국물류지원단 </option>
							<option value="0003" >(주)델타온</option>
							<option value="0001" >(주)아이디일일구닷컴</option>
							<option value="degugyodoso" >KPI대구교도소</option>
							<option value="OUT444" >OUT</option>
							<option value="kn4021" >강남우체국</option>
							<option value="kd2828" >강동우체국</option>
							<option value="ks8900" >강서우체국</option>
							<option value="gong-101" >개별현장</option>
							<option value="kkj5900" >경기광주우체국</option>
							<option value="gyounggyh" >경기혈액원</option>
							<option value="ks3704" >경산우체국</option>
							<option value="kj0114" >경주우체국</option>
							<option value="ky9400" >계양우체국</option>
							<option value="ka0014" >관악우체국</option>
							<option value="km1114" >광명우체국</option>
							<option value="ks2114" >광산우체국</option>
							<option value="kj0280" >광주우체국</option>
							<option value="guro1501" >구로구청</option>
							<option value="km1533" >구미우체국</option>
							<option value="gp8014" >군포우체국</option>
							<option value="icb1512" >근로복지인천병원</option>
							<option value="kj6014" >금정우체국</option>
							<option value="kia5071" >기아자동차협력</option>
							<option value="hangi1501" >기초과학연구원</option>
							<option value="kc0014" >김천우체국</option>
							<option value="kp0014" >김포우체국</option>
							<option value="kh9009" >김해우체국</option>
							<option value="nd0801" >남동우체국</option>
							<option value="nbs9000" >남부산우체국</option>
							<option value="nambuh" >남부혈액검사센터</option>
							<option value="rudrlskadid" >남양우체국</option>
							<option value="nyj0700" >남양주우체국</option>
							<option value="nws2014" >남울산우체국</option>
							<option value="nic0500" >남인천우체국</option>
							<option value="nw3226" >노원우체국</option>
							<option value="dgds4614" >대구달서우체국</option>
							<option value="dg2000" >대구우체국</option>
							<option value="dd2400" >대덕우체국</option>
							<option value="dj7066" >대전우체국</option>
							<option value="redcross-dj" >대전충남혈액원</option>
							<option value="redcross-ws" >대한적십자사울산혈액원</option>
							<option value="dy0014" >덕양우체국</option>
							<option value="db3600" >도봉우체국</option>
							<option value="ddg1900" >동대구우체국</option>
							<option value="dl7014" >동래우체국</option>
							<option value="dsw0532" >동수원우체국</option>
							<option value="dws0100" >동울산우체국</option>
							<option value="dj0385" >동작우체국</option>
							<option value="djj3842" >동전주우체국</option>
							<option value="dca6310" >동천안우체국</option>
							<option value="ds1610" >둔산우체국</option>
							<option value="ms0044" >마산우체국</option>
							<option value="mshp0004" >마산합포우체국</option>
							<option value="mp0014" >마포우체국</option>
							<option value="anstks" >문산우체국</option>
							<option value="seoul" >번호예치</option>
							<option value="inkd01" >본사</option>
							<option value="bs3000" >부산우체국</option>
							<option value="bsj0700" >부산진우체국</option>
							<option value="bc7124" >부천우체국</option>
							<option value="bp4000" >부평우체국</option>
							<option value="bgj9114" >북광주우체국</option>
							<option value="bdg3024" >북대구우체국</option>
							<option value="bbs0864" >북부산우체국</option>
							<option value="bd2900" >분당우체국</option>
							<option value="ss3331" >사상우체국</option>
							<option value="sh7000" >사하우체국</option>
							<option value="ssd" >상수도사업본부</option>
							<option value="skj7190" >서광주우체국</option>
							<option value="sgp3915" >서귀포우체국</option>
							<option value="seodemoon" >서대문구청</option>
							<option value="sdm9114" >서대문우체국</option>
							<option value="sdj3400" >서대전우체국</option>
							<option value="ssw01" >서수원우체국</option>
							<option value="redcross-s" >서울남부혈액원</option>
							<option value="sdhw" >서울동부혈액원</option>
							<option value="seomun1501" >서울문화재단</option>
							<option value="seoult" >서울화물공제조합</option>
							<option value="sic9114" >서인천우체국</option>
							<option value="scj5720" >서청주우체국</option>
							<option value="sn0014" >성남우체국</option>
							<option value="sb0123" >성북우체국</option>
							<option value="center1" >센타프라자</option>
							<option value="sp2700" >송파우체국</option>
							<option value="sw1300" >수원우체국</option>
							<option value="sj0511" >수지우체국</option>
							<option value="sh2700" >시흥우체국</option>
							<option value="cjdt01" >씨제이대한통운(주)</option>
							<option value="as2114" >아산우체국</option>
							<option value="as0014" >안산우체국</option>
							<option value="as7900" >안성우체국</option>
							<option value="ay9788" >안양우체국</option>
							<option value="dkswnddncprnr" >안중우체국</option>
							<option value="yangsan" >양산우체국</option>
							<option value="yc0014" >양천우체국</option>
							<option value="yyd0014" >여의도우체국</option>
							<option value="yj0888" >연제우체국</option>
							<option value="yd5550" >영도우체국</option>
							<option value="os0004" >오산우체국</option>
							<option value="ys0004" >용산우체국</option>
							<option value="yi2849" >용인우체국</option>
							<option value="kypost1" >우정본(경북청)</option>
							<option value="ruddlscjd" >우정본(경인청)</option>
							<option value="pupost1" >우정본(부산청)</option>
							<option value="spost1" >우정본(서울청)</option>
							<option value="jnpost1" >우정본(전남청)</option>
							<option value="jbcpost1" >우정본(전북청)</option>
							<option value="jjpost1" >우정본(제주청)</option>
							<option value="wjs0001" >우정사업본부</option>
							<option value="ws5801" >울산우체국</option>
							<option value="ys8114" >유성우체국</option>
							<option value="ep3514" >은평우체국</option>
							<option value="humanplaza-1" >은평휴먼프라자Ⅰ</option>
							<option value="yjb5556" >의정부우체국</option>
							<option value="ic2820" >이천우체국</option>
							<option value="ic8155" >인천우체국</option>
							<option value="incheonjodal" >인천지방조달청</option>
							<option value="is0205" >일산우체국</option>
							<option value="jj2635" >전주우체국</option>
							<option value="jjwj5200" >제주우편집중국</option>
							<option value="junongangh" >중앙혈액검사센터</option>
							<option value="jh0005" >진해우체국</option>
							<option value="cw1114" >창원우체국</option>
							<option value="ca2660" >천안우체국</option>
							<option value="cj0014" >청주우체국</option>
							<option value="0002" >케이티지엘에스</option>
							<option value="ty2016" >통영우체국</option>
							<option value="pj9004" >파주우체국</option>
							<option value="pt3121" >평택우체국</option>
							<option value="pc1302" >포천우체국</option>
							<option value="ph9937" >포항우체국</option>
							<option value="hn2007" >하남우체국</option>
							<option value="hsk88" >한국승강기안전공단</option>
							<option value="hanjun2" >한국전력(강원)</option>
							<option value="dghj001" >한국전력(대구)</option>
							<option value="hsk20" >한승공강원지사</option>
							<option value="hsk16" >한승공경남동부</option>
							<option value="hsk10" >한승공경북서부</option>
							<option value="hsk15" >한승공대구서부</option>
							<option value="hsk06" >한승공부천지사</option>
							<option value="hskkd" >한승공서울강동</option>
							<option value="hsksb" >한승공서울본부</option>
							<option value="hskss" >한승공서울서부</option>
							<option value="hsksc" >한승공서울서초</option>
							<option value="hsk05" >한승공안산지사</option>
							<option value="hsk13" >한승공영남본부</option>
							<option value="hsk03" >한승공용인지사</option>
							<option value="hskws" >한승공울산지사</option>
							<option value="hskjd" >한승공전남동부</option>
							<option value="hskjs" >한승공전남서부</option>
							<option value="jejuhsk" >한승공제주지사</option>
							<option value="hskca" >한승공천안지사</option>
							<option value="hskcn" >한승공충남지사</option>
							<option value="hsk17" >한승공충북지사</option>
							<option value="hsk07" >한승공파주지사</option>
							<option value="hwd4500" >해운대우체국</option>
							<option value="ws9516" >화성우체국</option>
							<option value="goldb1" >황금빌딩</option>
							</select>
				</td>
                <td align="center" bgcolor="#efefef">파트너명(아이디)</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<input type="text" name="partner_info" id="partner_info" value="<?=$partner_info?>" class="form-control" style="width:100%;">
                </td>
			</tr>
			<tr>
                <td style="padding-left:5px;padding-top:10px;" colspan="10" align="center" bgcolor="#ffffff">

					<button name="sbtn" class="dt-button buttons-print btn btn-success mr btn-xs" id="file-save-button" type="button" onClick="goSearch();" value="1"><i class="fa fa-search"> </i>검색</button>

				</td>
					
			  </tr>
            </table>
		  </td>
        </tr>
      </table>
		</form>
      <!-- 검색 끝 -->

				

            </div>





<?php }
$created = can_action('30', 'created');
$edited = can_action('30', 'edited');
$deleted = can_action('30', 'deleted');
$income_category = $this->db->get('tbl_income_category')->result();
$id = $this->uri->segment(5);
if (!empty($created) || !empty($edited)){
?>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right btn-with-tooltip-group _filter_data" data-toggle="tooltip"
             data-title="<?php echo lang('filter_by'); ?>">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-filter" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-left"
                style="width:300px;<?php if (!empty($search_by) && $search_by == 'category') {
                    echo 'display:block';
                } ?>">
                <li class="<?php
                if (empty($search_by)) {
                    echo 'active';
                } ?>"><a
                        href="<?= base_url() ?>admin/transactions/deposit"><?php echo lang('all'); ?></a>
                </li>
                <li class="divider"></li>
                <?php if (count($income_category) > 0) { ?>
                    <?php foreach ($income_category as $v_category) {
                        ?>
                        <li class="<?php if (!empty($id)) {
                           // if ($search_by == 'category') {
                           //     if ($id == $v_category->income_category_id) {
                           //         echo 'active';
                           //     }
                           // }
                        } ?>">
                            <a href="<?= base_url() ?>admin/transactions/deposit/category/<?php echo $v_category->income_category_id; ?>"><?php echo $v_category->income_category; ?></a>
                        </li>
                    <?php }
                    ?>
                    <div class="clearfix"></div>
                    <li class="divider"></li>
                <?php } ?>
            </ul>
        </div>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#manage"
                                                                    data-toggle="tab">위수탁관리비수납현황 (총 <?=number_format($total_count)?>건)</a>
                </li>
                <!--li><a style="background-color: #1797be;color: #ffffff"
                       href="<?= base_url() ?>admin/transactions/import/Income"><?= lang('import') . ' ' . lang('deposit') ?></a>
                </li-->
            </ul>
            <div class="tab-content no-padding  bg-white">
                <!-- ************** general *************-->
                <div class="tab-pane active" id="manage">
<?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong>위수탁관리비미수현황</strong></div>
                        </header>
                        <?php } ?>
                        <div class="table-responsive">
                            <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
                                <thead>
									<tr align="center" bgcolor="#e0e7ef">
									  <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>상태</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>년월분</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="2">청구사정보</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="5">파트너정보</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">청구금정보</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">수납정보</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>비고</td>
									</tr>
																						
														

									<tr align="center" bgcolor="#e0e7ef">
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공제청구사</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">실수요처</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">파트너</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약개시일</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약종료일</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차량번호</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사업자번호</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">당월부과금</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">납부일시</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">수납금</td>

									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">은행</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">예금주</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계좌번호</td>
									  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">의뢰인</td>
									</tr>
                                </thead>
                                <tbody>
                                <?php
                                //$curency = $this->transactions_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');
                                $total_amount = 0;
                                $total_credit = 0;
                                $total_balance = 0;
                                if (!empty($all_levy_info)):
                                    foreach ($all_levy_info as $v_levy_info) :
										$sn = $total_count--;
										$df = $this->db->where('df_id', $v_levy_info->df_id)->get('tbl_delivery_fee')->row();
										//마지막결제
										//$l_levy_info = $this->db->where('pid', $v_levy_info->idx)->get('tbl_delivery_fee_levy_log')->row();

										//공제청구사
										if(!empty($v_levy_info->ws_co_id)) {
											$gj_rq_co = $this->db->where('dp_id', $v_levy_info->ws_co_id)->get('tbl_members')->row();
											if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;
										}

										//실수요처
										$r_co = $this->db->where('code', $df->co_code)->get('tbl_members')->row();
										if(empty($r_co->co_name)) $r_co = ""; else $r_co = $r_co->co_name;

										//차량정보
										$truck = $this->db->where('idx', $df->tr_id)->get('tbl_asset_truck')->row();
										if(empty($truck->car_1)) $truck_no = ""; else $truck_no = $truck->car_1;

										$pay_status = "납부전";
										$pay_status_color = "warning";
										if(!empty($v_levy_info->pay_status)) {
											if($v_levy_info->pay_status == "N") { $pay_status = "미납"; $pay_status_color = "danger"; }
											else if($v_levy_info->pay_status == "P") { $pay_status = "부분납"; $pay_status_color = "primary"; } 
											else if($v_levy_info->pay_status == "F") { $pay_status = "완납"; $pay_status_color = "success"; } 
											else if($v_levy_info->pay_status == "O") { $pay_status = "과납"; $pay_status_color = "primary"; } 
										}

										//이월 미납액
	
                                        ?>
                                      <tr id="table_deposit_<?= $v_levy_info->df_id ?>">
                                        <td>
										<!--<?=number_format($v_levy_info->gn_amount,0)?>원 / <?=number_format($v_levy_info->balance_amount,0)?>원 /
                                            <?= $v_levy_info->pay_amount ?>--><?=$sn?> 
                                        </td>
                                        <td>
                                            <span class='label label-<?=$pay_status_color?>'><?=$pay_status?></span>
                                        </td>
                                        <td>
                                            <?= $v_levy_info->df_month ?>
                                        </td>
                                        <td>
                                            <a href="<?= base_url() ?>admin/basic/v_levy_info/<?= $v_levy_info->df_id ?>"
                                                   class="text-info"><?php if(!empty($gj_rq_co)) echo $gj_rq_co; ?></a>
										</td>
                                        <td><?= $df->D ?></td>

                                        <td><?= $v_levy_info->co_name ?>(<?= $v_levy_info->ceo ?>/<?= $v_levy_info->driver ?>)</td>
                                        <td><?= $v_levy_info->N ?></td>
                                        <td><?= $v_levy_info->O ?></td>
                                        <td><?= $truck_no ?></td>
                                        <td><?= $v_levy_info->bs_number ?></td>

										<td style='text-align:right;'>
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/transactions/update_transactions/<?= $v_levy_info->df_id ?>/<?php if(!empty($v_levy_info->dllidx)) echo $v_levy_info->dllidx; ?>"
                                               class="text-default ml"><i class="fa fa-cog"></i></a>
										<?= number_format($v_levy_info->gn_amount,0) ?></td>
										<td style='text-align:right;'><?php if(!empty($v_levy_info->pay_date)) echo $v_levy_info->pay_date; ?></td>
										<td style='text-align:right;'><?= number_format($v_levy_info->pay_amount,0) ?></td>

										<td style='text-align:right;'><?php if(!empty($v_levy_info->bank_name)) echo $v_levy_info->bank_name; ?></td>
										<td style='text-align:right;'><?php if(!empty($v_levy_info->bank_account_name)) echo $v_levy_info->bank_account_name; ?></td>
										<td style='text-align:right;'><?php if(!empty($v_levy_info->bank_account_number)) echo $v_levy_info->bank_account_number; ?></td>
										<td style='text-align:right;'><?php if(!empty($v_levy_info->bank_transfer_name)) echo $v_levy_info->bank_transfer_name; ?></td>
										<td><?php if(!empty($v_levy_info->remark)) echo $v_levy_info->remark; ?> <?php if(!empty($v_levy_info->reference)) echo $v_levy_info->reference; ?></td>
                                            </tr>
                                            <?php
                                            //$total_amount += $v_levy_info->amount;
                                            //$total_credit += $v_levy_info->credit;
                                            //$total_balance = $total_credit;
                                            ?>
                                            <?php
                                        //endif;
                                    endforeach;
                                endif;
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if ((!empty($created) || !empty($edited)) && !empty($levy_info)) { 
								$df = $this->db->where('df_id', $levy_info->df_id)->get('tbl_delivery_fee')->row();
								//파트너
								$dp = $this->db->where('dp_id', $levy_info->dp_id)->get('tbl_members')->row();

										//공제청구사
										$gj_rq_co = $this->db->where('dp_id', $dp->ws_co_id)->get('tbl_members')->row();
										if(empty($gj_rq_co->co_name)) $gj_rq_co = ""; else $gj_rq_co = $gj_rq_co->co_name;
					

										//실수요처
										$r_co = $this->db->where('code', $df->co_code)->get('tbl_members')->row();
										if(empty($r_co->co_name)) $r_co = ""; else $r_co = $r_co->co_name;
					?>
                        <div class="tab-pane <?= $active == 2 ? 'active' : ''; ?>" id="create">
                            <form role="form" data-parsley-validate="" novalidate="" enctype="multipart/form-data"
                                  id="form"
                                  action="<?php echo base_url(); ?>admin/transactions/save_payment/<?php
                                  if (!empty($levy_info)) {
                                      echo $levy_info->df_id;
                                  }
                                  ?>" method="post" class="form-horizontal ">
								  <input type="hidden" name="df_month" value="<?=$levy_info->df_month?>">
								  <input type="hidden" name="gn_amount" value="<?=$levy_info->gn_amount?>">
								  <input type="hidden" name="balance_amount" value="<?=$levy_info->balance_amount?>">
								  <input type="hidden" name="gongje_req_co" value="<?=$df->gongje_req_co?>">
                                <div class="form-group">
                                    <label
                                        class="col-lg-3 control-label">부과년월</label>
                                    <div class="col-lg-5" style="font-weight:bold;padding-top:8px;">
                                        <?php echo $levy_info->df_month; ?>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="col-lg-3 control-label mt-lg" style="font-weight:bold;padding-top:8px;">공제청구사</label>
                                    <div class="col-lg-5 mt-lg">
                                        <div class="input-group">
                                        <?=$gj_rq_co ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-lg-3 control-label mt-lg">파트너</label>
                                    <div class="col-lg-5 mt-lg">
                                        <div class="input-group" style="font-weight:bold;padding-top:8px;">
                                        <?=$dp->ceo ?> (<?=$dp->driver ?>)
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">납부일자</label>
                                    <div class="col-lg-5">
                                        <div class="input-group">
                                            <input type="text" name="pay_date" class="form-control datepicker" value="<?php
                                            if (!empty($levy_info->pay_date)) {
                                                echo $levy_info->pay_date;
                                            } else {
                                                echo date('Y-m-d');
                                            }
                                            ?>" data-date-format="<?= config_item('date_picker_format'); ?>">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-calendar"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group terms">
                                    <label class="col-lg-3 control-label">메모</label>
                                    <div class="col-lg-5">
                        <textarea name="remark" class="form-control"><?php
                            if (!empty($levy_info)) {
                                echo $levy_info->remark;
                            }
                            ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">부과액 / 납부잔액 / 금회 납부액 <span
                                            class="text-danger">*</span>
                                    </label>
                                    <div class="col-lg-2">
                                        <div class="input-group  ">
											<?=number_format($levy_info->gn_amount,0)?>원 / <?=number_format($levy_info->balance_amount,0)?>원 /
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group  ">
                                            <input class="form-control " data-parsley-type="number" type="text"
                                                   value="<?php
                                                   if (!empty($levy_info)) {
                                                      // echo $levy_info->df_id;
                                                   }
                                                   ?>" name="paid_amount" required="" <?php
                                            if (!empty($levy_info)) {
                                               // echo 'disabled';
                                            }
                                            ?>>
                                        </div>
                                    </div>
                                </div>
                                <?php if (!empty($levy_info)) { ?>
                                    <input class="form-control " type="hidden" value="<?php
                                    if (!empty($levy_info)) {
                                        echo $levy_info->df_id;
                                    }
                                    ?>" name="old_account_id">
                                <?php } ?>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">납부방법 </label>
                                        <div class="col-lg-5">
                                            <div class="input-group">
                                                <select class="form-control select_box" style="width: 100%"
                                                        name="payment_methods_id">
                                                    <option value="0">수납방법</option>
                                                    <?php
                                                    $payment_methods = $this->db->order_by('payment_methods_id', 'DESC')->get('tbl_payment_methods')->result();
                                                    if (!empty($payment_methods)) {
                                                        foreach ($payment_methods as $p_method) {
                                                            ?>
                                                            <option value="<?= $p_method->payment_methods_id ?>" <?php
                                                            if (!empty($expense_info)) {
                                                                echo $expense_info->payment_methods_id == $p_method->payment_methods_id ? 'selected' : '';
                                                            }
                                                            ?>><?= $p_method->method_name ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div class="input-group-addon"
                                                     title="<?= lang('new') . ' ' . lang('payment_method') ?>"
                                                     data-toggle="tooltip" data-placement="top">
                                                    <a data-toggle="modal" data-target="#myModal"
                                                       href="<?= base_url() ?>admin/settings/inline_payment_method"><i
                                                            class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">납부방법 </label>
                                        <div class="col-lg-5">

                                            <input class="form-control " type="text" value="<?php
                                            if (!empty($levy_info->reference)) {
                                                echo $levy_info->reference;
                                            }
                                            ?>" name="reference">


                                            <span class="help-block">전송번호/카드번호 등</span>
                                        </div>
                                    </div>
                                <div class="form-group" style="margin-bottom: 0px">
                                    <label for="field-1"
                                           class="col-sm-3 control-label">첨부파일</label>

                                    <div class="col-sm-5">
                                        <div id="comments_file-dropzone" class="dropzone mb15">

                                        </div>
                                        <div id="comments_file-dropzone-scrollbar">
                                            <div id="comments_file-previews">
                                                <div id="file-upload-row" class="mt pull-left">

                                                    <div class="preview box-content pr-lg" style="width:100px;">
                                                    <span data-dz-remove class="pull-right" style="cursor: pointer">
                                    <i class="fa fa-times"></i>
                                </span>
                                                        <img data-dz-thumbnail class="upload-thumbnail-sm"/>
                                                        <input class="file-count-field" type="hidden" name="files[]"
                                                               value=""/>
                                                        <div
                                                            class="mb progress progress-striped upload-progress-sm active mt-sm"
                                                            role="progressbar" aria-valuemin="0" aria-valuemax="100"
                                                            aria-valuenow="0">
                                                            <div class="progress-bar progress-bar-success"
                                                                 style="width:0%;"
                                                                 data-dz-uploadprogress></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        if (!empty($levy_info->attachement)) {
                                            $uploaded_file = json_decode($levy_info->attachement);
                                        }
                                        if (!empty($uploaded_file)) {
                                            foreach ($uploaded_file as $v_files_image) { ?>
                                                <div class="pull-left mt pr-lg mb" style="width:100px;">
                                                        <span data-dz-remove class="pull-right existing_image"
                                                              style="cursor: pointer"><i
                                                                class="fa fa-times"></i></span>
                                                    <?php if ($v_files_image->is_image == 1) { ?>
                                                        <img data-dz-thumbnail
                                                             src="<?php echo base_url() . $v_files_image->path ?>"
                                                             class="upload-thumbnail-sm"/>
                                                    <?php } else { ?>
                                                        <span data-toggle="tooltip" data-placement="top"
                                                              title="<?= $v_files_image->fileName ?>"
                                                              class="mailbox-attachment-icon"><i
                                                                class="fa fa-file-text-o"></i></span>
                                                    <?php } ?>

                                                    <input type="hidden" name="path[]"
                                                           value="<?php echo $v_files_image->path ?>">
                                                    <input type="hidden" name="fileName[]"
                                                           value="<?php echo $v_files_image->fileName ?>">
                                                    <input type="hidden" name="fullPath[]"
                                                           value="<?php echo $v_files_image->fullPath ?>">
                                                    <input type="hidden" name="size[]"
                                                           value="<?php echo $v_files_image->size ?>">
                                                    <input type="hidden" name="is_image[]"
                                                           value="<?php echo $v_files_image->is_image ?>">
                                                </div>
                                            <?php }; ?>
                                        <?php }; ?>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $(".existing_image").click(function () {
                                                    $(this).parent().remove();
                                                });

                                                fileSerial = 0;
                                                // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
                                                var previewNode = document.querySelector("#file-upload-row");
                                                previewNode.id = "";
                                                var previewTemplate = previewNode.parentNode.innerHTML;
                                                previewNode.parentNode.removeChild(previewNode);
                                                Dropzone.autoDiscover = false;
                                                var projectFilesDropzone = new Dropzone("#comments_file-dropzone", {
                                                    url: "<?= base_url()?>admin/global_controller/upload_file",
                                                    thumbnailWidth: 80,
                                                    thumbnailHeight: 80,
                                                    parallelUploads: 20,
                                                    previewTemplate: previewTemplate,
                                                    dictDefaultMessage: '<?php echo lang("file_upload_instruction"); ?>',
                                                    autoQueue: true,
                                                    previewsContainer: "#comments_file-previews",
                                                    clickable: true,
                                                    accept: function (file, done) {
                                                        if (file.name.length > 200) {
                                                            done("Filename is too long.");
                                                            $(file.previewTemplate).find(".description-field").remove();
                                                        }
                                                        //validate the file
                                                        $.ajax({
                                                            url: "<?= base_url()?>admin/global_controller/validate_project_file",
                                                            data: {file_name: file.name, file_size: file.size},
                                                            cache: false,
                                                            type: 'POST',
                                                            dataType: "json",
                                                            success: function (response) {
                                                                if (response.success) {
                                                                    fileSerial++;
                                                                    $(file.previewTemplate).find(".description-field").attr("name", "comment_" + fileSerial);
                                                                    $(file.previewTemplate).append("<input type='hidden' name='file_name_" + fileSerial + "' value='" + file.name + "' />\n\
                                                                        <input type='hidden' name='file_size_" + fileSerial + "' value='" + file.size + "' />");
                                                                    $(file.previewTemplate).find(".file-count-field").val(fileSerial);
                                                                    done();
                                                                } else {
                                                                    $(file.previewTemplate).find("input").remove();
                                                                    done(response.message);
                                                                }
                                                            }
                                                        });
                                                    },
                                                    processing: function () {
                                                        $("#file-save-button").prop("disabled", true);
                                                    },
                                                    queuecomplete: function () {
                                                        $("#file-save-button").prop("disabled", false);
                                                    },
                                                    fallback: function () {
                                                        //add custom fallback;
                                                        $("body").addClass("dropzone-disabled");
                                                        $('.modal-dialog').find('[type="submit"]').removeAttr('disabled');

                                                        $("#comments_file-dropzone").hide();

                                                        $("#file-modal-footer").prepend("<button id='add-more-file-button' type='button' class='btn  btn-default pull-left'><i class='fa fa-plus-circle'></i> " + "<?php echo lang("add_more"); ?>" + "</button>");

                                                        $("#file-modal-footer").on("click", "#add-more-file-button", function () {
                                                            var newFileRow = "<div class='file-row pb pt10 b-b mb10'>"
                                                                + "<div class='pb clearfix '><button type='button' class='btn btn-xs btn-danger pull-left mr remove-file'><i class='fa fa-times'></i></button> <input class='pull-left' type='file' name='manualFiles[]' /></div>"
                                                                + "<div class='mb5 pb5'><input class='form-control description-field'  name='comment[]'  type='text' style='cursor: auto;' placeholder='<?php echo lang("comment") ?>' /></div>"
                                                                + "</div>";
                                                            $("#comments_file-previews").prepend(newFileRow);
                                                        });
                                                        $("#add-more-file-button").trigger("click");
                                                        $("#comments_file-previews").on("click", ".remove-file", function () {
                                                            $(this).closest(".file-row").remove();
                                                        });
                                                    },
                                                    success: function (file) {
                                                        setTimeout(function () {
                                                            $(file.previewElement).find(".progress-striped").removeClass("progress-striped").addClass("progress-bar-success");
                                                        }, 1000);
                                                    }
                                                });

                                            })
                                        </script>
                                    </div>
                                </div>
                                <?php
                                if (!empty($levy_info)) {
                                    $transactions_id = $levy_info->remark;
                                } else {
                                    $transactions_id = null;
                                }
                                ?>
                                <?= custom_form_Fields(1, $transactions_id); ?>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label"></label>
                                    <div class="col-lg-5">
                                        <button type="submit" id="file-save-button" class="btn btn-sm btn-primary">
                                            <i
                                                class="fa fa-check"></i> 저장</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    <?php }else{ ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var maxAppend = 0;
        $("#add_more").click(function () {
            if (maxAppend >= 4) {
                alert("Maximum 5 File is allowed");
            } else {
                var add_new = $('<div class="form-group" style="margin-bottom: 0px">\n\
                    <label for="field-1" class="col-sm-3 control-label"><?= lang('attachment') ?></label>\n\
        <div class="col-sm-4">\n\
        <div class="fileinput fileinput-new" data-provides="fileinput">\n\
<span class="btn btn-default btn-file"><span class="fileinput-new" >Select file</span><span class="fileinput-exists" >Change</span><input type="file" name="attachement[]" ></span> <span class="fileinput-filename"></span><a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none;">&times;</a></div></div>\n\<div class="col-sm-2">\n\<strong>\n\
<a href="javascript:void(0);" class="remCF"><i class="fa fa-times"></i>&nbsp;Remove</a></strong></div>');
                maxAppend++;
                $("#add_new").append(add_new);
            }
        });

        $("#add_new").on('click', '.remCF', function () {
            $(this).parent().parent().parent().remove();
        });
        $('a.RCF').click(function () {
            $(this).parent().parent().remove();
        });
    });
</script>
