<?php
$opened = $this->session->userdata('opened');
$this->session->unset_userdata('opened');
$time = date('h:i:s');
$r = display_time($time);
$time1 = explode(' ', $r);
?>
<html>
<head>
<title>위수탁청구서출력</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
body,table,tr,td {font-family: "굴림", "Seoul", "arial", "Verdana", "helvetica"; font-size: 9pt; line-height: 13px; color:333333;
	scrollbar-3dlight-color:595959;
	scrollbar-arrow-color:7F7F7F;
	scrollbar-face-color:DFDFDF;
	scrollbar-highlight-color:FFFFF;
	scrollbar-shadow-color:595959
	scrollbar-base-color:CFCFCF;
	scrollbar-darkshadow-color:FFFFFF;
	border: 1px solid #fff;
	padding-right:5px;
}
.m_a {font-size:8pt;font-family: "돋움";letter-spacing:-1;}
.01 {font-size:9pt; color:#<?=$lColor?>; padding-top:2pt;}
.02 {font-size:9pt; color:#333333; padding-top:2pt;}
.03 {font-size:9pt; color:#FF4633; padding-top:2pt;}
.xx {font-size:9pt; border:solid 1 #A2A2A2; background-color:f0f0f0;}
.yy {font-size:9pt; border:solid 1 #A2A2A2; background-color:;ffffff}

a:link { text-decoration: none; color:585858; }
a:visited { text-decoration: none; color:585858; }
a:active { text-decoration: none; color:585858; }
a:hover { text-decoration:none; color:585858; }
#floater {position:absolute; visibility:visible}
</style>
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="http://svc.deltaq.co.kr/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="http://svc.deltaq.co.kr/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="http://svc.deltaq.co.kr/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="http://svc.deltaq.co.kr/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="http://svc.deltaq.co.kr/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" /><link href="http://svc.deltaq.co.kr/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" /><link href="http://svc.deltaq.co.kr/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" /><link href="http://svc.deltaq.co.kr/assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" /> <link href="http://svc.deltaq.co.kr/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />		<!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="http://svc.deltaq.co.kr/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <!--link href="/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" /-->
    <link href="http://svc.deltaq.co.kr/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="http://svc.deltaq.co.kr/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="http://svc.deltaq.co.kr/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="http://svc.deltaq.co.kr/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="http://svc.deltaq.co.kr/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="http://svc.deltaq.co.kr/assets/global/plugins/monthpicker/MonthPicker.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->

		<!-- BEGIN PAGE LEVEL STYLES -->
<link href="http://svc.deltaq.co.kr/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" /><link href="http://svc.deltaq.co.kr/assets/pages/css/mfee_invoice.css" rel="stylesheet" type="text/css" />        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="http://svc.deltaq.co.kr/assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="http://svc.deltaq.co.kr/assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="http://svc.deltaq.co.kr/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->

	<SCRIPT>
		function Installed()
		{
			try
			{
				return (new ActiveXObject('IEPageSetupX.IEPageSetup'));
			}
			catch (e)
			{
				return false;
			}
		}

		function printMe()
		{
			if (Installed())
			{
				IEPageSetupX.header = "";
				IEPageSetupX.footer = "";
				IEPageSetupX.PrintBackground = true;
				IEPageSetupX.leftMargin   = "10";
				IEPageSetupX.rightMargin   = "10";
				IEPageSetupX.topMargin   = "10";
				IEPageSetupX.bottomMargin   = "10";
				IEPageSetupX.ShrinkToFit = true;
				IEPageSetupX.PaperSize  = "A4";
				IEPageSetupX.Preview();
			}
			else
				alert("컨트롤을 설치하지 않았네요.. 정상적으로 인쇄되지 않을 수 있습니다.");
		}

	</SCRIPT>

	<SCRIPT language="JavaScript" for="IEPageSetupX" event="OnError(ErrCode, ErrMsg)">
		alert('에러 코드: ' + ErrCode + "\n에러 메시지: " + ErrMsg);
	</SCRIPT>
</head>

<body topmargin="0" OnLoad="printMe();" OnUnload="if (Installed()) IEPageSetupX.RollBack();">
<center>

                    <?php echo $subview ?>

</center>

</body>
</html>
