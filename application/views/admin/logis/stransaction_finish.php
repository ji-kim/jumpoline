<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();
$mdate = date('Y-m-d');
$last_7_days = date('Y-m-d', strtotime('today - 7 days'));


if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>

<script>
	function goSearch(val) {
		if(val == 'all' || val == '전체') {
			document.myform.action = "<?php echo base_url() ?>admin/logis/stransaction_finish"; //_all";
			document.myform.ws_co_id.value = '';
		} else {
			document.myform.action = "<?php echo base_url() ?>admin/logis/stransaction_finish";
		}
		document.myform.submit();
	}
</script>

            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
                <form data-parsley-validate="" novalidate=""
                      action="<?php echo base_url() ?>admin/logis/stransaction_finish"
                      method="post" enctype="multipart/form-data" class="form-horizontal" name="myform">
                
      <!-- 검색 시작 -->
<table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
        <tr>
          <td align="left" valign="top" bgcolor="#ffffff">
            <table border="0" width="100%" cellpadding="12" cellspacing="15" bgcolor="#ffffff">
              <tr>
                <td width="10%" height="20" align="center" bgcolor="#efefef">년월</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff" >

                            <div class="input-group">
                                <input type="text" value="<?php
                                if (!empty($df_month)) {
                                    echo $df_month;
                                }
                                ?>" class="form-control monthyear" name="df_month" id="df_month"
                                       data-format="yyyy/mm/dd" style="width:100%;background-color:yellow;" onChange="document.myform.submit();">

                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>

                </td>
                <td width="10%" height="20" align="center" >
				<select name="FIELD" id="FIELD" style="width:100%;" class="form-control input-sm">
                 <option value="">선택</option>
					<option value="E" >운전자명</option>
					<option value="D" >대표</option>
					<option value="K" >주민등록번호</option>
					<option value="Y" >전화번호</option>
					<option value="U" >차량번호</option>
					<option value="W" >차대번호</option>

					<!--option value="W" >거래처</option>
					<option value="C" >지입회사</option>
					<option value="BS" >사업자만</option>
					<option value="NB" >비사업자만</option-->
				</select>
				
				</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
				<input type="text" name="KEYWORD" id="KEYWORD" maxlength="20" value="" class="form-control" style="width:100%;">
				</td>
                <td height="20" align="center" bgcolor="#efefef">그룹사</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<input type="hidden" name="ws_co_id" id="ws_co_id" value="">
					<input type="text" name="ws_co_name" id="ws_co_name" value="" class="form-control" style="width:100%;background-color:yellow;" onClick="selectGroup('ws_co_id||ws_co_name||__||__||__||__||__');">
                </td>
			</tr>
			<tr>
                <td height="20" align="center" bgcolor="#efefef">계산서 발행일</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<input type="text" name="tax_date" id="tax_date" class="form-control datepicker" value="2018-07-31" data-date-format="yyyy-mm-dd" style="color:blue;width:100%;text-align:left;border: 1px solid #777777;">
                </td>
                <td height="20" align="center" bgcolor="#efefef">공급받는자</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<select name="p_co_id" id="p_co_id" style="width:100%;" class="form-control input-sm">
										</select>  
                </td>
                <td height="20" align="center" bgcolor="#efefef">출력</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
					<select name="pmode" id="pmode" class="form-control input-sm" style="width:100%;">
						  <option value="A" >양면</option>
						  <option value="S" >공급자</option>
						  <option value="C" >공급받는자</option>
					</select>
                </td>
			</tr>
			<tr>
                <td style="padding-left:5px;padding-top:10px;" colspan="6" align="center" bgcolor="#ffffff">

            <!--a href="javascript:goPrintAllTax()" class="button red" title="세금계산서"> 세금계산서전체출력 </a>

					<a href="javascript:goSearch()" class="button red" title="검색"> 검색 </a>
					<a href="javascript:goExcel()" class="button red" title="엑셀저장"> 엑셀저장 </a>
					<a href="javascript:goExcelBank()" class="button red" title="엑셀저장"> 엑셀저장(은행용) </a>
		 			<a href="javascript:goPrintAll();" class="button red" title="일괄프린트"> 일괄프린트 </a-->

					<button name="sbtn" class="dt-button buttons-print btn btn-success mr btn-xs" id="file-save-button" type="button" onClick="goSearch(document.myform.gongje_req_co.value);" value="1"><i class="fa fa-search"> </i>검색</button>

					<a href="javascript:goPrintAllTax()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-print"> </i> 세금계산서전체출력</span>
					</a>
					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 엑셀저장</span>
					</a>
					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 엑셀저장(은행용)</span>
					</a>
					<a tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-print"> </i> 일괄프린트</span>
					</a>

				</td>
					
			  </tr>
            </table>
		  </td>
        </tr>
      </table>
      <!-- 검색 끝 -->

</form>			

            </div>




<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#client_list"
                                                                   data-toggle="tab">거래등록대기</a></li>



                <!--li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_client"
                                                                   data-toggle="tab">신규등록</a></li>
                <li><a style="background-color: #1797be;color: #ffffff"
                       href="<?= base_url() ?>admin/client/import">데이터가져오기</a-->
                </li>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="client_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
							<table id="DataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                                <thead>
        <tr align="center" bgcolor="#e0e7ef">
          <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>년월</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"> <br/>수당 </td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"> <br/>공제 </td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>우체국</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>성 명</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>사업자번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>차량번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>주민등록번호</td>
  									  				

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="5">거래(운송,배달)정보</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="4">하도급거래계약단가</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="5">위탁배달수수료</td>
											

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="7">각종수당</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="3">수수료지급합계</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="2">부과기준</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="5">누적금환급</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>총지급액</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2">지입회사</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="9">위ㆍ수탁관리비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="6">복리후생비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="5">각종보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="9">일반공제</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="3">누적부가세</td>

          <td style="color:#ffffff;background-color: #777777;border-left:1px solid #eee;border-right:1px solid #eee;" rowspan="2"><br/>공제합계</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>공제후지급액</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="5">수납인정보</td>
        </tr>
        <tr align="center" bgcolor="#e0e7ef">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">10Kg이하</td>
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">10Kg초과 ~ 20Kg이하</td>
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">20Kg초과</td>
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">집하(픽업)</td>          
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">총계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">10Kg이하</td>
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">10Kg초과 ~ 20Kg이하</td>
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">20Kg초과</td>
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">집하(픽업)</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">10Kg이하</td>
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">10Kg초과 ~ 20Kg이하</td>
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">20Kg초과</td>
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">집하(픽업)</td>          
		  <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관리자<br>급여</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">팀(조)장<br>수당</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">난지역<br>수당</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보조금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">회식비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">기타</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">공급가</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">합계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세<br>누적</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과세<br>여부</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">원천징수</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세<br>환급월분</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">상조회비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관리비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">협회비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">환경부담금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">자동차세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">차고지비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">미수금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">기타</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">국민연금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">건강보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">고용보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">퇴직급여<br>충당금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">상조회비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">자량보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">적재물보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">보증보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">산재보험료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과오납환수</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">PDA</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">민원처리</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">과태료</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">사고접부비</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">비사업자<br>원천징수</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">비사업자<br>부가세환수</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">기타</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">소계</td>
		
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">당월누적<br>부가세</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">총부가세<br>누적금</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">부가세<br>누적월</td>

          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">은행</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계좌번호</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">예금주</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">관계</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">명세서보기</td>
		</tr>
                                </thead>
                                <tbody>




















                                <?php
                                if (!empty($all_stransaction_info)) {
                                    foreach ($all_stransaction_info as $stransaction_details) {
                                        ?>
                                        <tr>
                                            <td>
                                                <a href="<?= base_url() ?>admin/client/client_details/<?= $client_details->client_id ?>"
                                                   class="text-info">
                                                    <?= $client_details->name ?></a></td>
                                            <td><span class="label label-success" data-toggle="tooltip"
                                                      data-palcement="top"
                                                      title="<?= lang('contacts') ?>"><?//= $this->client_model->count_rows('tbl_account_details', array('company' => $client_details->client_id)) ?></span>
                                            </td>
                                            <td class="hidden-sm"><?php
                                                if ($client_details->primary_contact != 0) {
                                                    $contacts = $client_details->primary_contact;
                                                } else {
                                                    $contacts = NULL;
                                                }
                                                //$primary_contact = $this->client_model->check_by(array('account_details_id' => $contacts), 'tbl_account_details');
                                               // if ($primary_contact) {
                                                //    echo $primary_contact->fullname;
                                               // }
                                                ?></td>
                                            <td><?//= count($this->db->where('client_id', $client_details->client_id)->get('tbl_project')->result()) ?></td>
                                            <td><?php
                                                if ($client_outstanding > 0) {
                                                    echo display_money($client_outstanding, $cur);
                                                } else {
                                                    echo '0.00';
                                                }
                                                ?></td>
                                            <td><?//= display_money($this->client_model->client_paid($client_details->client_id), $cur); ?></td>
                                            <td><?php
                                           //     if ($client_transactions[0]->amount > 0) {
                                           //         echo display_money($client_transactions[0]->amount, $cur);
                                            //    } else {
                                            //        echo '0.00';
                                            //    }
                                                ?></td>

                                            <td><span class="label label-default"><?php
                                                    if (!empty($customer_group->customer_group)) {
                                                        echo $customer_group->customer_group;
                                                    }
                                                    ?></span></td>
                                            <?php $show_custom_fields = custom_form_table(12, $client_details->client_id);
                                            if (!empty($show_custom_fields)) {
                                                foreach ($show_custom_fields as $c_label => $v_fields) {
                                                    if (!empty($c_label)) {
                                                        ?>
                                                        <td><?= $v_fields ?> </td>
                                                    <?php }
                                                }
                                            }
                                            ?>
                                            <td>
                                                <?php if (!empty($edited)) { ?>
                                                    <?php echo btn_edit('admin/client/manage_client/' . $client_details->client_id) ?>
                                                <?php }
                                                if (!empty($deleted)) {
                                                    ?>
                                                    <?php echo btn_delete('admin/client/delete_client/' . $client_details->client_id) ?>
                                                <?php } ?>
                                                <?php echo btn_view('admin/client/client_details/' . $client_details->client_id) ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { ?>
                        <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_client"
                        style="position: relative;">
                        <form role="form" enctype="multipart/form-data" id="form" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/client/save_client/<?php
                              if (!empty($client_info)) {
                                  echo $client_info->client_id;
                              }
                              ?>" method="post" class="form-horizontal  ">
                            <div class="panel-body">
                                <label class="control-label col-sm-3"></label
                                <div class="col-sm-6">
                                    <div class="nav-tabs-custom">
                                        <!-- Tabs within a box -->
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#general_compnay"
                                                                  data-toggle="tab"><?= lang('general') ?></a>
                                            </li>
                                            <li><a href="#contact_compnay"
                                                   data-toggle="tab"><?= lang('client_contact') . ' ' . lang('details') ?></a>
                                            </li>
                                            <li><a href="#web_compnay" data-toggle="tab"><?= lang('web') ?></a></li>
                                            <li><a href="#hosting_compnay" data-toggle="tab"><?= lang('hosting') ?></a>
                                            </li>
                                        </ul>
                                        <div class="tab-content bg-white">
                                            <!-- ************** general *************-->
                                            <div class="chart tab-pane active" id="general_compnay">
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('company_name') ?>
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($client_info->name)) {
                                                                   echo $client_info->name;
                                                               }
                                                               ?>" name="name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('company_email') ?>
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                        <input type="email" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($client_info->email)) {
                                                                   echo $client_info->email;
                                                               }
                                                               ?>" name="email">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_vat') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->vat)) {
                                                            echo $client_info->vat;
                                                        }
                                                        ?>" name="vat">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-sm-3 control-label"><?= lang('customer_group') ?></label>
                                                    <div class="col-sm-5">
                                                        <div class="input-group">
                                                            <select name="customer_group_id"
                                                                    class="form-control select_box"
                                                                    style="width: 100%">
                                                                <?php
                                                                if (!empty($all_customer_group)) {
                                                                    foreach ($all_customer_group as $customer_group) : ?>
                                                                        <option
                                                                            value="<?= $customer_group->customer_group_id ?>"<?php
                                                                        if (!empty($client_info->customer_group_id) && $client_info->customer_group_id == $customer_group->customer_group_id) {
                                                                            echo 'selected';
                                                                        } ?>
                                                                        ><?= $customer_group->customer_group; ?></option>
                                                                    <?php endforeach;
                                                                }
                                                                $created = can_action('125', 'created');
                                                                ?>
                                                            </select>
                                                            <?php if (!empty($created)) { ?>
                                                                <div class="input-group-addon"
                                                                     title="<?= lang('new') . ' ' . lang('customer_group') ?>"
                                                                     data-toggle="tooltip" data-placement="top">
                                                                    <a data-toggle="modal" data-target="#myModal"
                                                                       href="<?= base_url() ?>admin/client/customer_group"><i
                                                                            class="fa fa-plus"></i></a>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-sm-3 control-label"><?= lang('language') ?></label>
                                                    <div class="col-sm-5">
                                                        <select name="language" class="form-control select_box"
                                                                style="width: 100%">
                                                            <?php

                                                            foreach ($languages as $lang) : ?>
                                                                <option
                                                                    value="<?= $lang->name ?>"<?php
                                                                if (!empty($client_info->language) && $client_info->language == $lang->name) {
                                                                    echo 'selected';
                                                                } elseif (empty($client_info->language) && $this->config->item('language') == $lang->name) {
                                                                    echo 'selected';
                                                                } ?>
                                                                ><?= ucfirst($lang->name) ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('currency') ?></label>
                                                    <div class="col-lg-5">
                                                        <select name="currency" class="form-control select_box"
                                                                style="width: 100%">

                                                            <?php if (!empty($currencies)): foreach ($currencies as $currency): ?>
                                                                <option
                                                                    value="<?= $currency->code ?>"
                                                                    <?php
                                                                    if (!empty($client_info->currency) && $client_info->currency == $currency->code) {
                                                                        echo 'selected';
                                                                    } elseif (empty($client_info->currency) && $this->config->item('default_currency') == $currency->code) {
                                                                        echo 'selected';
                                                                    } ?>
                                                                ><?= $currency->name ?></option>
                                                                <?php
                                                            endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('short_note') ?></label>
                                                    <div class="col-lg-5">
                                            <textarea class="form-control" name="short_note"><?php
                                                if (!empty($client_info->short_note)) {
                                                    echo $client_info->short_note;
                                                }
                                                ?></textarea>
                                                    </div>
                                                </div>
                                                <?php
                                                if (!empty($client_info)) {
                                                    $client_id = $client_info->client_id;
                                                } else {
                                                    $client_id = null;
                                                }
                                                ?>
                                                <?= custom_form_Fields(12, $client_id); ?>
                                            </div><!-- ************** general *************-->

                                            <!-- ************** Contact *************-->
                                            <div class="chart tab-pane" id="contact_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_phone') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->phone)) {
                                                            echo $client_info->phone;
                                                        }
                                                        ?>" name="phone">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_mobile') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control"
                                                               value="<?php
                                                               if (!empty($client_info->mobile)) {
                                                                   echo $client_info->mobile;
                                                               }
                                                               ?>" name="mobile">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('zipcode') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->zipcode)) {
                                                            echo $client_info->zipcode;
                                                        }
                                                        ?>" name="zipcode">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_city') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->city)) {
                                                            echo $client_info->city;
                                                        }
                                                        ?>" name="city">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_country') ?></label>
                                                    <div class="col-lg-5">
                                                        <select name="country" class="form-control select_box"
                                                                style="width: 100%">
                                                            <optgroup label="Default Country">
                                                                <option
                                                                    value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
                                                            </optgroup>
                                                            <optgroup label="<?= lang('other_countries') ?>">
                                                                <?php if (!empty($countries)): foreach ($countries as $country): ?>
                                                                    <option
                                                                        value="<?= $country->value ?>" <?= (!empty($client_info->country) && $client_info->country == $country->value ? 'selected' : NULL) ?>><?= $country->value ?>
                                                                    </option>
                                                                    <?php
                                                                endforeach;
                                                                endif;
                                                                ?>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_fax') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->fax)) {
                                                            echo $client_info->fax;
                                                        }
                                                        ?>" name="fax">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_address') ?></label>
                                                    <div class="col-lg-5">
                                            <textarea class="form-control" name="address"><?php
                                                if (!empty($client_info->address)) {
                                                    echo $client_info->address;
                                                }
                                                ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label">
                                                        <a href="#"
                                                           onclick="fetch_lat_long_from_google_cprofile(); return false;"
                                                           data-toggle="tooltip"
                                                           data-title="<?php echo lang('fetch_from_google') . ' - ' . lang('customer_fetch_lat_lng_usage'); ?>"><i
                                                                id="gmaps-search-icon" class="fa fa-google"
                                                                aria-hidden="true"></i></a>
                                                        <?= lang('latitude') . '( ' . lang('google_map') . ' )' ?>
                                                    </label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->latitude)) {
                                                            echo $client_info->latitude;
                                                        }
                                                        ?>" name="latitude">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('longitude') . '( ' . lang('google_map') . ' )' ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control"
                                                               value="<?php
                                                               if (!empty($client_info->longitude)) {
                                                                   echo $client_info->longitude;
                                                               }
                                                               ?>" name="longitude">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Contact *************-->
                                            <!-- ************** Web *************-->
                                            <div class="chart tab-pane" id="web_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_domain') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->website)) {
                                                            echo $client_info->website;
                                                        }
                                                        ?>" name="website">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('skype_id') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->skype_id)) {
                                                            echo $client_info->skype_id;
                                                        }
                                                        ?>" name="skype_id">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('facebook_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->facebook)) {
                                                            echo $client_info->facebook;
                                                        }
                                                        ?>" name="facebook">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('twitter_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->twitter)) {
                                                            echo $client_info->twitter;
                                                        }
                                                        ?>" name="twitter">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('linkedin_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->linkedin)) {
                                                            echo $client_info->linkedin;
                                                        }
                                                        ?>" name="linkedin">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Web *************-->
                                            <!-- ************** Hosting *************-->
                                            <div class="chart tab-pane" id="hosting_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('hosting_company') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->hosting_company)) {
                                                            echo $client_info->hosting_company;
                                                        }
                                                        ?>" name="hosting_company">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('hostname') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->hostname)) {
                                                            echo $client_info->hostname;
                                                        }
                                                        ?>" name="hostname">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('username') ?> </label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->username)) {
                                                            echo $client_info->username;
                                                        }
                                                        ?>" name="username">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('password') ?></label>
                                                    <div class="col-lg-5">
                                                        <?php
                                                        if (!empty($client_info->password)) {
                                                            $password = strlen(decrypt($client_info->password));
                                                        }
                                                        ?>
                                                        <input type="password" name="password" value=""
                                                               placeholder="<?php
                                                               if (!empty($password)) {
                                                                   for ($p = 1; $p <= $password; $p++) {
                                                                       echo '*';
                                                                   }
                                                               }
                                                               ?>" class="form-control">
                                                        <strong id="show_password" class="required"></strong>
                                                    </div>
                                                    <?php if (!empty($client_info->password)) { ?>
                                                        <div class="col-lg-3">
                                                            <a data-toggle="modal" data-target="#myModal"
                                                               href="<?= base_url('admin/client/see_password/c_' . $client_info->client_id) ?>"
                                                               id="see_password"><?= lang('see_password') ?></a>
                                                            <strong id="hosting_password" class="required"></strong>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('port') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->port)) {
                                                            echo $client_info->port;
                                                        }
                                                        ?>" name="port">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Hosting *************-->
                                        </div>
                                    </div><!-- /.nav-tabs-custom -->
                                    <div class="form-group mt">
                                        <label class="col-lg-3"></label>
                                        <div class="col-lg-1">
                                            <button type="submit"
                                                    class="btn btn-sm btn-primary"><?= lang('save') ?></button>
                                        </div>
                                        <div class="col-lg-3">
                                            <button type="submit" name="save_and_create_contact" value="1"
                                                    class="btn btn-sm btn-primary"><?= lang('save_and_create_contact') ?></button>
                                        </div>

                                    </div>
                                </div>
                        </form>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>