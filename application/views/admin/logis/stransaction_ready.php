<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php
$all_customer_group = $this->db->where('type', 'client')->order_by('customer_group_id', 'DESC')->get('tbl_customer_group')->result();
$mdate = date('Y-m-d');
$last_7_days = date('Y-m-d', strtotime('today - 7 days'));


if ($this->session->userdata('user_type') == 1) {
    $margin = 'margin-bottom:30px';
?>
<style>
/* Ensure that the demo table scrolls */
    th, td {
        white-space: nowrap;
        padding-left: 2px !important;
        padding-right: 2px !important;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>


            <div class="col-sm-12 bg-white p0" style="<?= $margin ?>">
                
      <!-- 검색 시작 -->
	<table border="0" cellspacing="1" cellpadding="5" width="96%" align="center" style="margin-top:20px;margin-bottom:20px;margin-left:20px;margin-right:20px;">
        <tr>
          <td align="left" valign="top">
            <table border="0" width="100%" cellpadding="2" cellspacing="5" bgcolor="#ffffff" class="search_table" style="padding-left:5px;font:12px 'Tahoma', sans-serif">
              <tr>
                <td style="padding-left:3px;" align="left" bgcolor="#ffffff"><input type="text" name="KEYWORD" id="KEYWORD" maxlength="20" value="" class="form-control" style="width:100%;">
                </td>
                <td width="10%" height="20" align="center" bgcolor="#efefef">공급사명</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
				<select name="s_co" id="s_co" class="form-control input-sm" style="width:100%;">
                 <option value="">전체</option>
					<option value="" selected></option>
					<option value="(주)델타온" >(주)델타온</option>
					<option value="(주)씨엔케이물류" >(주)씨엔케이물류</option>
					<option value="(주)아이디일일구닷컴" >(주)아이디일일구닷컴</option>
					<option value="(주)케이티지엘에스" >(주)케이티지엘에스</option>
					<option value="케이티지엘에스(주)" >케이티지엘에스(주)</option>
			</select>

                </td>
                <td width="10%" height="20" align="center" bgcolor="#efefef">지역</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
				<select name="B" id="B" class="form-control input-sm" style="width:100%;">
                 <option value="">전체</option>
					<option value="" selected></option>
					<option value="" selected></option>
					<option value="강서" >강서</option>
					<option value="경기광주" >경기광주</option>
					<option value="경산" >경산</option>
					<option value="계양" >계양</option>
					<option value="구미" >구미</option>
					<option value="김포" >김포</option>
					<option value="남부산" >남부산</option>
					<option value="남인천" >남인천</option>
					<option value="대덕" >대덕</option>
					<option value="대전" >대전</option>
					<option value="동대구" >동대구</option>
					<option value="동작" >동작</option>
					<option value="둔산" >둔산</option>
					<option value="마포" >마포</option>
					<option value="부평" >부평</option>
					<option value="서대전" >서대전</option>
					<option value="서인천" >서인천</option>
					<option value="성북" >성북</option>
					<option value="안산" >안산</option>
					<option value="안양" >안양</option>
					<option value="용산" >용산</option>
					<option value="용인" >용인</option>
					<option value="유성" >유성</option>
					<option value="인천" >인천</option>
					<option value="전주" >전주</option>
					<option value="진주" >진주</option>
					<option value="창원" >창원</option>
					<option value="청주" >청주</option>
					<option value="하남" >하남</option>
					<option value="해운대" >해운대</option>
			</select>

                </td>
                <td width="10%" height="20" align="center" bgcolor="#efefef">실수요처</td>
                <td style="padding-left:5px;" align="left" bgcolor="#ffffff">
				<select name="r_co" id="r_co" class="form-control input-sm" style="width:100%;">
                 <option value="">전체</option>
						<option value="한국승강기안전공단" >한국승강기안전공단</option>
							<option value="한승공제주지사" >한승공제주지사</option>
				</select>
                </td>
                <td width="10%" height="20" align="center" bgcolor="#efefef">선택조회
                </td>
                <td width="10%" height="20" align="center" bgcolor="#FFFFFF">
				<select name="FIELD" id="FIELD" class="form-control input-sm" style="width:100%;">
                 <option value="">선택</option>
					<option value="E" >운전자명</option>
					<option value="D" >대표</option>
					<option value="Q" >사업자번호</option>
					<option value="K" >주민등록번호</option>
					<option value="Y" >전화번호</option>
					<option value="U" >차량번호</option>
					<option value="W" >차대번호</option>
			</select>
				</td>
                <td colspan="3" style="padding-left:3px;" align="left" bgcolor="#ffffff" colspan="3"><input type="text" name="KEYWORD" id="KEYWORD" maxlength="20" value="" class="form-control" style="width:100%;">
                </td>
              </tr>
              <tr>
				<td>
				</td>
                <td style="padding-left:5px;padding-top:10px;" align="left" bgcolor="#ffffff" COLSPAN="7">
            <div align="CENTER">

					<a href="javascript:goInit()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-search"> </i> RESET</span>
					</a>
					<a href="javascript:goSearch()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-print"> </i> 검색</span>
					</a>
					<a href="javascript:goSearch()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 엑셀저장</span>
					</a>
					<a href="javascript:goSearch()" tabindex="0" class="dt-button buttons-print btn btn-danger btn-xs mr" aria-controls="DataTables">
					<span><i class="fa fa-file-excel-o"> </i> 엑셀가져오기</span>
					</a>
            </div>

                </td>
              </tr>
            </table>
		  </td>
        </tr>
      </table>
      <!-- 검색 끝 -->

				

            </div>




<?php }

$id = $this->uri->segment(5);
$search_by = $this->uri->segment(4);
$created = can_action('4', 'created');
$edited = can_action('4', 'edited');
$deleted = can_action('4', 'deleted');
?>
<div class="row">
    <div class="col-sm-12">
        
        <?php if (!empty($created) || !empty($edited)){ ?>
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#client_list"
                                                                   data-toggle="tab">거래등록대기</a></li>



                <!--li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#new_client"
                                                                   data-toggle="tab">신규등록</a></li>
                <li><a style="background-color: #1797be;color: #ffffff"
                       href="<?= base_url() ?>admin/client/import">데이터가져오기</a-->
                </li>
            </ul>
            <div class="tab-content bg-white">
                <!-- Stock Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="client_list" style="position: relative;">
                    <?php } else { ?>
                    <div class="panel panel-custom">
                        <header class="panel-heading ">
                            <div class="panel-title"><strong><?= lang('client_list') ?></strong></div>
                        </header>
                        <?php } ?>
                        <div class="box">
                            <!--table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%"-->
							<table id="DataTables" class="table table-striped DataTables" cellspacing="0" style="width:100%;border:1px solid #fff;">
                                <thead>
        <tr align="center" bgcolor="#e0e7ef">
          <td height="50" style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>No</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>상태</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>계약(공급)사</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" rowspan="2"><br/>실수요처</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;" colspan="5">계약정보</td>
          <td style="color:#ffffff;background-color: #777777;" colspan="3">거래기간정보</td>
          <td style="color:#ffffff;background-color: #777777;border-left:1px solid #eee;" rowspan="2"></td>
        </tr>
        <tr align="center" bgcolor="#e0e7ef">
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약(용역)명</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약시작일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약종료일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">계약형태</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">하도급자등록수</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">거래년월분</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">시작일</td>
          <td style="color:#ffffff;background-color: #777777;border-right:1px solid #eee;">종료일</td>
        </tr>
                                </thead>
                                <tbody>

                                <?php
                                if (!empty($all_scontract_info)) {
                                    foreach ($all_scontract_info as $scontract_details) {
                                        ?>
                                        <tr>
          <td height="25" align="center"><?=$scontract_details->idx?></td>
          <td align="center" style="padding-left:5px;">
		  
<?
//echo "1.".$scontract_details->sct_status]."-";
//echo "2.".$scontract_details->tr_status];
if((($scontract_details->sct_status =="완료" || $scontract_details->sct_status =="대기") && $scontract_details->tr_status =="대기") || $scontract_details->sct_status =="만료대기" || $scontract_details->ct_status =="계약만료대기") { ?>

<input type="button" style="WIDTH: 70pt; HEIGHT: 18pt;background-color:red;color:#ffffff;" value="<?//=$scontract_details->idx ?> 거래등록 " onClick="location.href='<?php echo base_url(); ?>admin/logis/stransaction_write/<?=$scontract_details->idx ?>/<?= $scontract_details->s_co_id  ?>';">		 
<?
} else if($scontract_details->sct_status =="완료" && $scontract_details->tr_status  =="등록") { ?>

<input type="button" style="WIDTH: 50pt; HEIGHT: 18pt;background-color:blue;color:#ffffff;" value=" 등록 " onClick="location.href='<?php echo base_url(); ?>admin/logis/stransaction_write/<?=$scontract_details->idx ?>';">		 
<?
} else if($scontract_details->sct_status =="완료") {
?>		 
<input type="button" style="WIDTH: 50pt; HEIGHT: 18pt;background-color:#ffbbff;" value=" 대기 " onClick="location.href='<?php echo base_url(); ?>admin/logis/stransaction_write/<?=$scontract_details->idx ?>';">		 
<? } else echo "-"; ?>
		  
<?
//if(!$scontract_details->r_co ) {
	$qry = " select * from tbl_contract_rco where ct_id='$scontract_details->idx ' order by is_main desc limit 0,1 ";
	$rc = $this->logis_model->db->query($qry)->row();
	if(!empty($rc->r_co)) $scontract_details->r_co  = $rc->r_co ;
//}

if(!$scontract_details->tr_title ) {
	//$ct = sql_fetch(" select * from az_write_announcement where wr_id='$scontract_details->wr_id ' ");
	//$scontract_details->tr_title  = $ct[wr_subject ;
}
//$r_co = $scontract_details->r_co 
?>		  
		  <?//= $scontract_details->tr_status  ?></td>
          <td align="center" style="padding-left:5px;"><?= $scontract_details->s_co  ?><?//= $scontract_details->s_co_id  ?></td>
          <td align="center" style="padding-left:5px;"><?= $scontract_details->r_co  ?></td>
          <td align="left" style="padding-left:5px;"><?= $scontract_details->tr_title  ?></td>
          <td align="center" style="padding-left:5px;"><?= $scontract_details->ct_sdate  ?></td>
          <td align="center" style="padding-left:5px;"><?= $scontract_details->ct_edate  ?></td>
          <td align="center" style="padding-left:5px;"><?= $scontract_details->ct_type  ?></td>
          <td align="center" style="padding-left:5px;">0</td>
          <td align="center" style="padding-left:5px;">2014-08</td>
          <td align="center" style="padding-left:5px;">2014-08-01</td>
          <td align="center" style="padding-left:5px;">2014-08-31</td>
                                            <td>
                                                <?php if (!empty($edited)) { ?>
                                                    <?php echo btn_edit('admin/client/manage_client/' . $scontract_details->idx) ?>
                                                <?php }
                                                if (!empty($deleted)) {
                                                    ?>
                                                    <?php echo btn_delete('admin/client/delete_client/' . $scontract_details->idx) ?>
                                                <?php } ?>
                                                <?php echo btn_view('admin/client/client_details/' . $scontract_details->idx) ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">
                                            <?= lang('no_data') ?>
                                        </td>
                                    </tr>
                                <?php }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (!empty($created) || !empty($edited)) { ?>
                        <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="new_client"
                        style="position: relative;">
                        <form role="form" enctype="multipart/form-data" id="form" data-parsley-validate="" novalidate=""
                              action="<?php echo base_url(); ?>admin/client/save_client/<?php
                              if (!empty($client_info)) {
                                  echo $client_info->client_id;
                              }
                              ?>" method="post" class="form-horizontal  ">
                            <div class="panel-body">
                                <label class="control-label col-sm-3"></label
                                <div class="col-sm-6">
                                    <div class="nav-tabs-custom">
                                        <!-- Tabs within a box -->
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#general_compnay"
                                                                  data-toggle="tab"><?= lang('general') ?></a>
                                            </li>
                                            <li><a href="#contact_compnay"
                                                   data-toggle="tab"><?= lang('client_contact') . ' ' . lang('details') ?></a>
                                            </li>
                                            <li><a href="#web_compnay" data-toggle="tab"><?= lang('web') ?></a></li>
                                            <li><a href="#hosting_compnay" data-toggle="tab"><?= lang('hosting') ?></a>
                                            </li>
                                        </ul>
                                        <div class="tab-content bg-white">
                                            <!-- ************** general *************-->
                                            <div class="chart tab-pane active" id="general_compnay">
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('company_name') ?>
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($client_info->name)) {
                                                                   echo $client_info->name;
                                                               }
                                                               ?>" name="name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('company_email') ?>
                                                        <span
                                                            class="text-danger"> *</span></label>
                                                    <div class="col-lg-5">
                                                        <input type="email" class="form-control" required=""
                                                               value="<?php
                                                               if (!empty($client_info->email)) {
                                                                   echo $client_info->email;
                                                               }
                                                               ?>" name="email">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_vat') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->vat)) {
                                                            echo $client_info->vat;
                                                        }
                                                        ?>" name="vat">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-sm-3 control-label"><?= lang('customer_group') ?></label>
                                                    <div class="col-sm-5">
                                                        <div class="input-group">
                                                            <select name="customer_group_id"
                                                                    class="form-control select_box"
                                                                    style="width: 100%">
                                                                <?php
                                                                if (!empty($all_customer_group)) {
                                                                    foreach ($all_customer_group as $customer_group) : ?>
                                                                        <option
                                                                            value="<?= $customer_group->customer_group_id ?>"<?php
                                                                        if (!empty($client_info->customer_group_id) && $client_info->customer_group_id == $customer_group->customer_group_id) {
                                                                            echo 'selected';
                                                                        } ?>
                                                                        ><?= $customer_group->customer_group; ?></option>
                                                                    <?php endforeach;
                                                                }
                                                                $created = can_action('125', 'created');
                                                                ?>
                                                            </select>
                                                            <?php if (!empty($created)) { ?>
                                                                <div class="input-group-addon"
                                                                     title="<?= lang('new') . ' ' . lang('customer_group') ?>"
                                                                     data-toggle="tooltip" data-placement="top">
                                                                    <a data-toggle="modal" data-target="#myModal"
                                                                       href="<?= base_url() ?>admin/client/customer_group"><i
                                                                            class="fa fa-plus"></i></a>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-sm-3 control-label"><?= lang('language') ?></label>
                                                    <div class="col-sm-5">
                                                        <select name="language" class="form-control select_box"
                                                                style="width: 100%">
                                                            <?php

                                                            foreach ($languages as $lang) : ?>
                                                                <option
                                                                    value="<?= $lang->name ?>"<?php
                                                                if (!empty($client_info->language) && $client_info->language == $lang->name) {
                                                                    echo 'selected';
                                                                } elseif (empty($client_info->language) && $this->config->item('language') == $lang->name) {
                                                                    echo 'selected';
                                                                } ?>
                                                                ><?= ucfirst($lang->name) ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('currency') ?></label>
                                                    <div class="col-lg-5">
                                                        <select name="currency" class="form-control select_box"
                                                                style="width: 100%">

                                                            <?php if (!empty($currencies)): foreach ($currencies as $currency): ?>
                                                                <option
                                                                    value="<?= $currency->code ?>"
                                                                    <?php
                                                                    if (!empty($client_info->currency) && $client_info->currency == $currency->code) {
                                                                        echo 'selected';
                                                                    } elseif (empty($client_info->currency) && $this->config->item('default_currency') == $currency->code) {
                                                                        echo 'selected';
                                                                    } ?>
                                                                ><?= $currency->name ?></option>
                                                                <?php
                                                            endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('short_note') ?></label>
                                                    <div class="col-lg-5">
                                            <textarea class="form-control" name="short_note"><?php
                                                if (!empty($client_info->short_note)) {
                                                    echo $client_info->short_note;
                                                }
                                                ?></textarea>
                                                    </div>
                                                </div>
                                                <?php
                                                if (!empty($client_info)) {
                                                    $client_id = $client_info->client_id;
                                                } else {
                                                    $client_id = null;
                                                }
                                                ?>
                                                <?= custom_form_Fields(12, $client_id); ?>
                                            </div><!-- ************** general *************-->

                                            <!-- ************** Contact *************-->
                                            <div class="chart tab-pane" id="contact_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_phone') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->phone)) {
                                                            echo $client_info->phone;
                                                        }
                                                        ?>" name="phone">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_mobile') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control"
                                                               value="<?php
                                                               if (!empty($client_info->mobile)) {
                                                                   echo $client_info->mobile;
                                                               }
                                                               ?>" name="mobile">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('zipcode') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->zipcode)) {
                                                            echo $client_info->zipcode;
                                                        }
                                                        ?>" name="zipcode">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_city') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->city)) {
                                                            echo $client_info->city;
                                                        }
                                                        ?>" name="city">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_country') ?></label>
                                                    <div class="col-lg-5">
                                                        <select name="country" class="form-control select_box"
                                                                style="width: 100%">
                                                            <optgroup label="Default Country">
                                                                <option
                                                                    value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
                                                            </optgroup>
                                                            <optgroup label="<?= lang('other_countries') ?>">
                                                                <?php if (!empty($countries)): foreach ($countries as $country): ?>
                                                                    <option
                                                                        value="<?= $country->value ?>" <?= (!empty($client_info->country) && $client_info->country == $country->value ? 'selected' : NULL) ?>><?= $country->value ?>
                                                                    </option>
                                                                    <?php
                                                                endforeach;
                                                                endif;
                                                                ?>
                                                            </optgroup>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_fax') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->fax)) {
                                                            echo $client_info->fax;
                                                        }
                                                        ?>" name="fax">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_address') ?></label>
                                                    <div class="col-lg-5">
                                            <textarea class="form-control" name="address"><?php
                                                if (!empty($client_info->address)) {
                                                    echo $client_info->address;
                                                }
                                                ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label">
                                                        <a href="#"
                                                           onclick="fetch_lat_long_from_google_cprofile(); return false;"
                                                           data-toggle="tooltip"
                                                           data-title="<?php echo lang('fetch_from_google') . ' - ' . lang('customer_fetch_lat_lng_usage'); ?>"><i
                                                                id="gmaps-search-icon" class="fa fa-google"
                                                                aria-hidden="true"></i></a>
                                                        <?= lang('latitude') . '( ' . lang('google_map') . ' )' ?>
                                                    </label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->latitude)) {
                                                            echo $client_info->latitude;
                                                        }
                                                        ?>" name="latitude">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('longitude') . '( ' . lang('google_map') . ' )' ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control"
                                                               value="<?php
                                                               if (!empty($client_info->longitude)) {
                                                                   echo $client_info->longitude;
                                                               }
                                                               ?>" name="longitude">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Contact *************-->
                                            <!-- ************** Web *************-->
                                            <div class="chart tab-pane" id="web_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('company_domain') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->website)) {
                                                            echo $client_info->website;
                                                        }
                                                        ?>" name="website">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('skype_id') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->skype_id)) {
                                                            echo $client_info->skype_id;
                                                        }
                                                        ?>" name="skype_id">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('facebook_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->facebook)) {
                                                            echo $client_info->facebook;
                                                        }
                                                        ?>" name="facebook">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('twitter_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->twitter)) {
                                                            echo $client_info->twitter;
                                                        }
                                                        ?>" name="twitter">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('linkedin_profile_link') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->linkedin)) {
                                                            echo $client_info->linkedin;
                                                        }
                                                        ?>" name="linkedin">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Web *************-->
                                            <!-- ************** Hosting *************-->
                                            <div class="chart tab-pane" id="hosting_compnay">
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('hosting_company') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->hosting_company)) {
                                                            echo $client_info->hosting_company;
                                                        }
                                                        ?>" name="hosting_company">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('hostname') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->hostname)) {
                                                            echo $client_info->hostname;
                                                        }
                                                        ?>" name="hostname">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('username') ?> </label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->username)) {
                                                            echo $client_info->username;
                                                        }
                                                        ?>" name="username">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label
                                                        class="col-lg-3 control-label"><?= lang('password') ?></label>
                                                    <div class="col-lg-5">
                                                        <?php
                                                        if (!empty($client_info->password)) {
                                                            $password = strlen(decrypt($client_info->password));
                                                        }
                                                        ?>
                                                        <input type="password" name="password" value=""
                                                               placeholder="<?php
                                                               if (!empty($password)) {
                                                                   for ($p = 1; $p <= $password; $p++) {
                                                                       echo '*';
                                                                   }
                                                               }
                                                               ?>" class="form-control">
                                                        <strong id="show_password" class="required"></strong>
                                                    </div>
                                                    <?php if (!empty($client_info->password)) { ?>
                                                        <div class="col-lg-3">
                                                            <a data-toggle="modal" data-target="#myModal"
                                                               href="<?= base_url('admin/client/see_password/c_' . $client_info->client_id) ?>"
                                                               id="see_password"><?= lang('see_password') ?></a>
                                                            <strong id="hosting_password" class="required"></strong>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label"><?= lang('port') ?></label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" value="<?php
                                                        if (!empty($client_info->port)) {
                                                            echo $client_info->port;
                                                        }
                                                        ?>" name="port">
                                                    </div>
                                                </div>
                                            </div><!-- ************** Hosting *************-->
                                        </div>
                                    </div><!-- /.nav-tabs-custom -->
                                    <div class="form-group mt">
                                        <label class="col-lg-3"></label>
                                        <div class="col-lg-1">
                                            <button type="submit"
                                                    class="btn btn-sm btn-primary"><?= lang('save') ?></button>
                                        </div>
                                        <div class="col-lg-3">
                                            <button type="submit" name="save_and_create_contact" value="1"
                                                    class="btn btn-sm btn-primary"><?= lang('save_and_create_contact') ?></button>
                                        </div>

                                    </div>
                                </div>
                        </form>
                    <?php } else { ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fetch_lat_long_from_google_cprofile() {
        var data = {};
        data.address = $('textarea[name="address"]').val();
        data.city = $('input[name="city"]').val();
        data.country = $('select[name="country"] option:selected').text();
        console.log(data);
        $('#gmaps-search-icon').removeClass('fa-google').addClass('fa-spinner fa-spin');
        $.post('<?= base_url()?>admin/global_controller/fetch_address_info_gmaps', data).done(function (data) {
            data = JSON.parse(data);
            $('#gmaps-search-icon').removeClass('fa-spinner fa-spin').addClass('fa-google');
            if (data.response.status == 'OK') {
                $('input[name="latitude"]').val(data.lat);
                $('input[name="longitude"]').val(data.lng);
            } else {
                if (data.response.status == 'ZERO_RESULTS') {
                    toastr.warning("<?php echo lang('g_search_address_not_found'); ?>");
                } else {
                    toastr.warning(data.response.status);
                }
            }
        });
    }
</script>