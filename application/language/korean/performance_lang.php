<?php
$lang['performance'] = '성과';
$lang['performance_indicator'] = '지표';
$lang['give_appraisal'] = '평가하기';
$lang['performance_report'] = '성과보고서';
$lang['indicator_list'] = '지표목록';
$lang['set_indicator'] = '지표설정';
$lang['technical_competency'] = '기술능력';
$lang['customer_experience_management'] = 'Customer Experience Management';
$lang['beginner'] = '초급';
$lang['intermediate'] = '중급';
$lang['advanced'] = '고급';
$lang['expert_leader'] = '전문가 / 초고급';
$lang['marketing'] = '마케팅';
$lang['management'] = '관리';
$lang['administration'] = '관리자';
$lang['presentation_skill'] = '프레젠테이션능력';
$lang['quality_of_work'] = '직무퀄리티';
$lang['efficiency'] = '효율';
$lang['behavioural_competency'] = 'Behavioural / Organizational Competencies';
$lang['integrity'] = '진실성';
$lang['professionalism'] = '프로기질';
$lang['team_work'] = '팀워크';
$lang['critical_thinking'] = '비판적인생각';
$lang['conflict_management'] = '갈등관리';
$lang['attendance'] = '근태';
$lang['ability_to_meet_deadline'] = 'Ability To Meet Deadline';
$lang['activity_performance_indicator_saved'] = 'Performance Indicator Saved';
$lang['activity_performance_indicator_updated'] = 'Performance Indicator Updated';
$lang['indicator_update'] = 'Indicator Information Successfully Updated';
$lang['indicator_saved'] = 'Indicator Information Successfully Saved';
$lang['performance_indicator_details'] = 'Performance Indicator Details';
$lang['indicator_value_not_set'] = 'Indicator Values Not Set Yet !!';
$lang['give_performance_appraisal'] = 'Give Performance Appraisal';
$lang['remarks'] = '비고';
$lang['expected_value'] = 'Expected Value';
$lang['set_value'] = 'Set Value';
$lang['not_set'] = 'Not Set';
$lang['appraisal_already_provided'] = 'Appraisal Information Already provided to this user once for: ';
$lang['activity_appraisal_update'] = 'Performance Appraisal Updated';
$lang['activity_appraisal_saved'] = 'Performance Appraisal Saved';
$lang['appraisal_update'] = 'Appraisal Information Successfully updated';
$lang['appraisal_saved'] = 'Appraisal Information Successfully Saved';
$lang['emp_id'] = '사번';
$lang['appraisal_month'] = 'Appraisal Month';
$lang['performance_appraisal_details'] = 'Performance Appraisal Details';
$lang['performance_appraisal_of'] = 'Performance Appraisal Of';
$lang['assigned'] = 'Assigned';
$lang['expected'] = 'Expected';
$lang['atleast_one_appraisal'] = 'Please set Expected value at least one';


/* End of file performance_lang.php */
/* Location: ./application/language/korean/performance_lang.php */
