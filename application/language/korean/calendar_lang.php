<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['cal_su'] = 'Su';
$lang['cal_mo'] = 'Mo';
$lang['cal_tu'] = 'Tu';
$lang['cal_we'] = 'We';
$lang['cal_th'] = 'Th';
$lang['cal_fr'] = 'Fr';
$lang['cal_sa'] = 'Sa';
$lang['cal_sun'] = 'Sun';
$lang['cal_mon'] = 'Mon';
$lang['cal_tue'] = 'Tue';
$lang['cal_wed'] = 'Wed';
$lang['cal_thu'] = 'Thu';
$lang['cal_fri'] = 'Fri';
$lang['cal_sat'] = 'Sat';
$lang['cal_sunday'] = 'Sunday';
$lang['cal_monday'] = 'Monday';
$lang['cal_tuesday'] = 'Tuesday';
$lang['cal_wednesday'] = 'Wednesday';
$lang['cal_thursday'] = 'Thursday';
$lang['cal_friday'] = 'Friday';
$lang['cal_saturday'] = 'Saturday';
$lang['cal_jan'] = '1월';
$lang['cal_feb'] = '2월';
$lang['cal_mar'] = '3월';
$lang['cal_apr'] = '4월';
$lang['cal_may'] = '5월';
$lang['cal_jun'] = '6월';
$lang['cal_jul'] = '7월';
$lang['cal_aug'] = '8월';
$lang['cal_sep'] = '9월';
$lang['cal_oct'] = '10월';
$lang['cal_nov'] = '11월';
$lang['cal_dec'] = '12월';
$lang['cal_january'] = '1월';
$lang['cal_february'] = '2월';
$lang['cal_march'] = '3월';
$lang['cal_april'] = '4월';
$lang['cal_mayl'] = '5월';
$lang['cal_june'] = '6월';
$lang['cal_july'] = '7월';
$lang['cal_august'] = '8월';
$lang['cal_september'] = '9월';
$lang['cal_october'] = '10월';
$lang['cal_november'] = '11월';
$lang['cal_december'] = '12월';
